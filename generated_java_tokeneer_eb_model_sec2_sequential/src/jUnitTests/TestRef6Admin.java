package jUnitTests;

import eventb_prelude.*;

import org.junit.*;

import tokeneer_eb_model_sec2_sequential.Test_ref6_admin;
import tokeneer_eb_model_sec2_sequential.ref6_admin;

public class TestRef6Admin {

	private ref6_admin machine;
	
	@BeforeClass
	public static void init() {
		Test_ref6_admin.initialize();
	}
	
	@Before
	public void setUp() {
		machine = new ref6_admin();

	}

	@SuppressWarnings({ "static-access", "unchecked" })
	@Test
	public void test_Enrolment1(){
		//certificates
		Integer cert1 = 1;
		machine.set_certificates(new BSet<Integer>(cert1));

		//users
		Integer user1 = 1;
		machine.set_user(new BSet<Integer>(user1));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1))
				));*/

		//serials
		Integer serial1 = 1;
		//machine.set_serial(new BSet<Integer>(serial1));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '59', 'Month': '12', 'Day': '31', 'Hour': '23', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}



		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>());

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>());

		//privCert
		machine.set_privCert(new BSet<Integer>());

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>());

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		machine.set_fingerprint(new BSet<Integer>());

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>());

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>());

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>());

		//tokenID
		machine.set_tokenID(new BSet<Integer>());

		//tokenIDCert
		//machine.set_tokenIDCert(new BRelation<Integer,Integer>());

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>());

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>());

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>());

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>());

		machine.set_goodFP(new BRelation<Integer,Integer>());

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						1)
				));*/

		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.guard, s)));

		machine.set_validEnrol(new BSet<Integer>());
		machine.set_enrolmentFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1,1)
				)); 


		Integer floppy_to_be_read = 1;
		machine.set_floppyPresence(machine.absent);
		machine.set_enclaveStatus1(machine.notEnrolled);

		//FD.Enclave.RequestEnrolment
		Assert.assertTrue("Guard evt_RequestEnrolment not satisfied.", machine.evt_RequestEnrolment.guard_RequestEnrolment());
		machine.evt_RequestEnrolment.run_RequestEnrolment();
		Assert.assertEquals(machine.get_screenMsg1(), ref6_admin.insertEnrolmentData);
		Assert.assertEquals(machine.get_displayMessage2(), ref6_admin.blank);

		machine.set_floppyPresence(ref6_admin.present); 
		//FD.Enclave.ReadEnrolmentFloppy
		Assert.assertTrue("Guard evt_ReadEnrolmentFloppy not satisfied.", machine.evt_ReadEnrolmentFloppy.guard_ReadEnrolmentFloppy());
		machine.evt_ReadEnrolmentFloppy.run_ReadEnrolmentFloppy();
		Assert.assertEquals(machine.get_screenMsg1(), ref6_admin.validatingEnrolmentData);
		Assert.assertEquals(machine.get_displayMessage2(), ref6_admin.blank);

		//FD.Enclave.ValidateEnrolmentDataFail
		Assert.assertTrue("Guard evt_ValidateEnrolmentDataFail not satisfied.", machine.evt_ValidateEnrolmentDataFail.guard_ValidateEnrolmentDataFail(floppy_to_be_read));
		machine.evt_ValidateEnrolmentDataFail.run_ValidateEnrolmentDataFail(floppy_to_be_read);
		Assert.assertEquals(machine.get_screenMsg1(), ref6_admin.enrolmentFailed);
		Assert.assertEquals(machine.get_displayMessage2(), ref6_admin.blank);

		//FD.Enclave.WaitingFloppyRemoval
		machine.set_floppyPresence(ref6_admin.absent);

		//FD.Enclave.FailedEnrolFloppyRemoved
		Assert.assertTrue("Guard evt_FailedEnrolFloppyRemoved not satisfied.", machine.evt_FailedEnrolFloppyRemoved.guard_FailedEnrolFloppyRemoved());
		machine.evt_FailedEnrolFloppyRemoved.run_FailedEnrolFloppyRemoved();
		Assert.assertEquals(machine.get_screenMsg1(), ref6_admin.insertEnrolmentData);
		Assert.assertEquals(machine.get_displayMessage2(), ref6_admin.blank);

		//FD.AuditLog.LogChange
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_Enrolment2(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1;
		Integer serial2 = 2;
		Integer serial3 = 3;
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '59', 'Month': '12', 'Day': '31', 'Hour': '23', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}



		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1,cert2,cert3));

		//attCert
		machine.set_attCert(new BSet<Integer>());

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1),
				new Pair<Integer,Integer>(cert2,user1),
				new Pair<Integer,Integer>(cert3,user2)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>());

		//privCert
		machine.set_privCert(new BSet<Integer>());

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>());

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				ref6_admin.userOnly, ref6_admin.guard, ref6_admin.securityOfficer, ref6_admin.auditManager, ref6_admin.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				ref6_admin.unmarked, ref6_admin.unclassified, ref6_admin.restricted, ref6_admin.confidential, ref6_admin.secret, ref6_admin.topsecret
				));

		//fingerprint
		machine.set_fingerprint(new BSet<Integer>());

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>());

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>());

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>());

		//tokenID
		machine.set_tokenID(new BSet<Integer>());

		//tokenIDCert
		//machine.set_tokenIDCert(new BRelation<Integer,Integer>());

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>());

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>());

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>());

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>());

		machine.set_goodFP(new BRelation<Integer,Integer>());

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(ref6_admin.userOnly,1),
				new Pair<Integer,Integer>(ref6_admin.guard,1),
				new Pair<Integer,Integer>(ref6_admin.securityOfficer,1),
				new Pair<Integer,Integer>(ref6_admin.auditManager,1),
				new Pair<Integer,Integer>(ref6_admin.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(ref6_admin.unmarked,ref6_admin.secret), 
						1)
				));*/

		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.guard, s)));

		machine.set_validEnrol(new BSet<Integer>(3,2));
		machine.set_enrolmentFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1,1)
				)); 

		//FD.TIS.TISStartup is defined as StartEnrolledStation
		//StartEnrolledStation -> not modeled in EB
		machine.set_screenMsg1(ref6_admin.welcomeAdmin);
		machine.set_screenMsg2(ref6_admin.welcomeAdmin);
		machine.set_displayMessage1(ref6_admin.welcome);
		machine.set_displayMessage2(ref6_admin.welcome);
		machine.set_displayMessage3(ref6_admin.welcome);
		machine.set_enclaveStatus1(ref6_admin.enclaveQuiescent);
		machine.set_enclaveStatus2(ref6_admin.enclaveQuiescent);
		machine.set_entry_status1(ref6_admin.quiescent);
		machine.set_entry_status2(ref6_admin.quiescent);

		Integer currentFloppy = 1;

		machine.set_enclaveStatus1(ref6_admin.notEnrolled); 
		machine.set_floppyPresence(ref6_admin.present);

		//FD.Enclave.TISEnrolOp => ReadEnrolmentData => ReadEnrolmentFloppy 
		//FD.Enclave.ReadEnrolmentFloppy 
		Assert.assertTrue("Guard evt_ReadEnrolmentFloppy not satisfied.", machine.evt_ReadEnrolmentFloppy.guard_ReadEnrolmentFloppy());
		machine.evt_ReadEnrolmentFloppy.run_ReadEnrolmentFloppy();
		Assert.assertEquals(machine.get_screenMsg1(), ref6_admin.validatingEnrolmentData);
		Assert.assertEquals(machine.get_displayMessage2(), ref6_admin.blank);

		//FD.Enclave.ValidateEnrolmentDataFail
		Assert.assertTrue("Guard evt_ValidateEnrolmentDataFail not satisfied.", machine.evt_ValidateEnrolmentDataFail.guard_ValidateEnrolmentDataFail(currentFloppy));
		machine.evt_ValidateEnrolmentDataFail.run_ValidateEnrolmentDataFail(currentFloppy);
		Assert.assertEquals(machine.get_screenMsg1(), ref6_admin.enrolmentFailed);
		Assert.assertEquals(machine.get_displayMessage2(), ref6_admin.blank);

		//FD.Enclave.WaitingFloppyRemoval 
		machine.set_floppyPresence(ref6_admin.absent);
		//FD.Enclave.FailedEnrolFloppyRemoved
		Assert.assertTrue("Guard evt_FailedEnrolFloppyRemoved not satisfied.", machine.evt_FailedEnrolFloppyRemoved.guard_FailedEnrolFloppyRemoved());
		machine.evt_FailedEnrolFloppyRemoved.run_FailedEnrolFloppyRemoved();
		Assert.assertEquals(machine.get_screenMsg1(), ref6_admin.insertEnrolmentData);
		Assert.assertEquals(machine.get_displayMessage2(), ref6_admin.blank);

		//FD.Enclave.RequestEnrolment
		Assert.assertTrue("Guard evt_RequestEnrolment not satisfied.", machine.evt_RequestEnrolment.guard_RequestEnrolment());
		machine.evt_RequestEnrolment.run_RequestEnrolment();
		Assert.assertEquals(machine.get_screenMsg1(), ref6_admin.insertEnrolmentData);
		Assert.assertEquals(machine.get_displayMessage2(), ref6_admin.blank);

		//FD.AuditLog.LogChange

		//In the test.dat (Enrol2/Temp/) -> I do not know what door.breakOpen() is.
	}
	
	@SuppressWarnings({ "static-access", "unchecked" })
	@Test
	public void test_Enrolment3(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		Integer user3 = 3;
		machine.set_user(new BSet<Integer>(user1,user2,user3));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2),
				new BSet<Integer>(user3)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user3))
				));*/

		//serials
		Integer serial1 = 1;
		Integer serial2 = 2;
		Integer serial3 = 3;
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '59', 'Month': '12', 'Day': '31', 'Hour': '23', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}



		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1,cert2,cert3));

		//attCert
		machine.set_attCert(new BSet<Integer>());

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1),
				new Pair<Integer,Integer>(cert2,user2),
				new Pair<Integer,Integer>(cert3,user3)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>());

		//privCert
		machine.set_privCert(new BSet<Integer>());

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>());

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		machine.set_fingerprint(new BSet<Integer>());

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>());

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>());

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>());

		//tokenID
		machine.set_tokenID(new BSet<Integer>());

		//tokenIDCert
		//machine.(new BRelation<Integer,Integer>());

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>());

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>());

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>());

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>());

		machine.set_goodFP(new BRelation<Integer,Integer>());

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						1)
				));*/

		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.guard, s)));

		machine.set_validEnrol(new BSet<Integer>(1,2,3));
		machine.set_enrolmentFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1,1),
				new Pair<Integer,Integer>(1,2),
				new Pair<Integer,Integer>(1,3)
				)); 

		//FD.TIS.TISStartup is defined as StartEnrolledStation
		//StartEnrolledStation -> not modeled in EB
		machine.set_screenMsg1(machine.welcomeAdmin);
		machine.set_screenMsg2(machine.welcomeAdmin);
		machine.set_displayMessage1(machine.welcome);
		machine.set_displayMessage2(machine.welcome);
		machine.set_displayMessage3(machine.welcome);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);
		machine.set_entry_status1(machine.quiescent);
		machine.set_entry_status2(machine.quiescent);

		Integer currentFloppy = 1;

		machine.set_enclaveStatus1(machine.notEnrolled); 
		machine.set_floppyPresence(machine.present);

		//FD.Enclave.TISEnrolOp => ReadEnrolmentData => ReadEnrolmentFloppy 
		//FD.Enclave.ReadEnrolmentFloppy 
		Assert.assertTrue("Guard evt_ReadEnrolmentFloppy not satisfied.", machine.evt_ReadEnrolmentFloppy.guard_ReadEnrolmentFloppy());
		machine.evt_ReadEnrolmentFloppy.run_ReadEnrolmentFloppy();
		Assert.assertEquals(machine.get_screenMsg1(), ref6_admin.validatingEnrolmentData);
		Assert.assertEquals(machine.get_displayMessage2(), ref6_admin.blank);

		//FD.Enclave.ValidateEnrolmentDataFail
		Assert.assertTrue("Guard evt_ValidateEnrolmentDataOK not satisfied.", machine.evt_ValidateEnrolmentDataOK.guard_ValidateEnrolmentDataOK(currentFloppy));
		machine.evt_ValidateEnrolmentDataOK.run_ValidateEnrolmentDataOK(currentFloppy);
		Assert.assertEquals(machine.get_screenMsg1(), ref6_admin.welcomeAdmin);
		Assert.assertEquals(machine.get_displayMessage2(), ref6_admin.welcome);
		
		//FD.AuditLog.LogChange

	}
	
	@SuppressWarnings({ "static-access", "unchecked" })
	@Test
	public void test_UserEntry1(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3));

		//users
		Integer user1 = 1; //p01.dat: 4294967295
		Integer user2 = 2; //p01.dat: 0000032767
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1; //p01.dat: 100000001
		Integer serial2 = 2; //p01.dat: 1029384756
		Integer serial3 = 3; //p01.dat: 987654321
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '59', 'Month': '12', 'Day': '31', 'Hour': '23', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}



		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1) //p01.dat: it is itself ('Text': 'User01', 'ID': '100000001')
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User01fp = 1; //p01.dat: User01fp
		machine.set_fingerprint(new BSet<Integer>(User01fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.securityOfficer)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User01fp)
				));

		//tokenID
		Integer tok = 1;
		machine.set_tokenID(new BSet<Integer>(tok));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User01fp,machine.goodF)
				));
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						1)
				));*/

		//entryPeriod
		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(31)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.securityOfficer, s)));


		machine.set_tokenRemovalTimeout(0);
		machine.set_tokenRemovalDuration(0);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);
		machine.set_latchUnlockDuration(31);
		machine.set_alarmSilentDuration(10);

		machine.set_currentToken(-1);

		//Init parameters
		Integer token_user_to_read = 1;
		machine.set_entry_status1(machine.quiescent);
		machine.set_userTokenPresence(machine.present);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);
		Integer currentTime = 31;

		//FD.UserEntry.TISReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.BioCheckRequired 
		Assert.assertTrue("Guard evt_BioCheckRequired not satisfied.", machine.evt_BioCheckRequired.guard_BioCheckRequired(currentTime));
		machine.evt_BioCheckRequired.run_BioCheckRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.waitingFinger);
	
		machine.set_FingerPresence(machine.present);
		//FD.UserEntry.ReadFingerOK
		Assert.assertTrue("Guard evt_ReadFingerOK not satisfied.", machine.evt_ReadFingerOK.guard_ReadFingerOK());
		machine.evt_ReadFingerOK.run_ReadFingerOK();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		Integer fingerPrint = User01fp;
		//FD.UserEntry.ValidateFingerOK
		Assert.assertTrue("Guard evt_ValidateFingerOK not satisfied.", machine.evt_ValidateFingerOK.guard_ValidateFingerOK(fingerPrint));
		machine.evt_ValidateFingerOK.run_ValidateFingerOK(fingerPrint);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.ConstructAuthCert -> built-in writeUserTokenOK

		//FD.UserEntry.WriteUserTokenOK
		Assert.assertTrue("Guard evt_WriteUserTokenOK not satisfied.", machine.evt_WriteUserTokenOK.guard_WriteUserTokenOK());
		machine.evt_WriteUserTokenOK.run_WriteUserTokenOK();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.EntryOK
		Assert.assertTrue("Guard evt_EntryOK not satisfied.", machine.evt_EntryOK_1.guard_EntryOK_1(currentTime)
															|| machine.evt_EntryOK_2.guard_EntryOK_2(currentTime));
		machine.evt_EntryOK_1.run_EntryOK_1(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.openDoor);


		machine.set_userTokenPresence(machine.absent);
		//FD.UserEntry.UnlockDoorOK
		Assert.assertTrue("Guard evt_UnlockDoorOK not satisfied.", 
				machine.evt_unlockDoorOK.guard_unlockDoorOK(currentTime));
		machine.evt_unlockDoorOK.run_unlockDoorOK(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.doorUnlocked);
		//FD.AuditLog.LogChange
	}
	
	@SuppressWarnings({ "static-access", "unchecked" })
	@Test
	public void test_UpdateConfig1(){
		test_UserEntry1(); //the test starts after the test_UserEntry1
		Integer admin1 = 1;
		Integer tokAdmin = 1;
		Integer currentTime = 1;

		//admin
		machine.set_admin(new BSet<Integer>(admin1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(admin1,tokAdmin)
				));
		
		
		machine.set_tokenAuthCert(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,1)
						)
				);
		
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1, machine.securityOfficer)
				));
		
		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.NoRole)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.updateConfigData)
						));
		
		machine.set_availableOps(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1, machine.updateConfigData)
						)
				);
		
		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.updateConfigData,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));


		machine.set_enclaveStatus2(machine.enclaveQuiescent);
		machine.set_entry_status2(machine.quiescent);
		//Init parameters
		Integer token_admin_to_read = 1;
		machine.set_adminTokenPresence(machine.present);
		Integer currentKeyedData = 1;
		Integer floppy_to_be_read = 1;

		//FD.Enclave.TISAdminLogin
		//FD.Enclave.GetPresentAdminToken (ReadAdminToken) 
		Assert.assertTrue("Guard evt_readAdminToken not satisfied.", 
				machine.evt_readAdminToken.guard_readAdminToken(token_admin_to_read));
		machine.evt_readAdminToken.run_readAdminToken(token_admin_to_read);
		Assert.assertEquals(machine.get_currentAdminToken(), token_admin_to_read);
		//FD.Enclave.ValidateAdminTokenOK
		Assert.assertTrue("Guard evt_ValidateAdminTokenOK not satisfied.", machine.evt_ValidateAdminTokenOK.guard_ValidateAdminTokenOK(currentTime));
		
		machine.evt_ValidateAdminTokenOK.run_ValidateAdminTokenOK(currentTime);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);
		//FD.Enclave.TISStartAdminOp
		machine.set_keyedDataPresence(machine.present);
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.securityOfficer)
						));
		//FD.Enclave.ValidateOpRequestOK 
		Assert.assertTrue("Guard evt_ValidateOpRequestOK not satisfied.", 
				machine.evt_ValidateOpRequestOK.guard_ValidateOpRequestOK(currentKeyedData));
		
		machine.evt_ValidateOpRequestOK.run_ValidateOpRequestOK(currentKeyedData);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.waitingStartAdminOp);

		//FD.Enclave.TISUpdateConfigDataOp 
		machine.set_floppyPresence(machine.present);
		//FD.Enclave.StartUpdateConfigDataOK 
		Assert.assertTrue("Guard evt_StartUpdateConfigOK not satisfied.", machine.evt_StartUpdateConfigOK.guard_StartUpdateConfigOK());
		machine.evt_StartUpdateConfigOK.run_StartUpdateConfigOK();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		//FD.Enclave.FinishUpdateConfigDataOK
		Assert.assertTrue("Guard evt_FinishUpdateConfigDataOK not satisfied.", machine.evt_FinishUpdateConfigDataOK.guard_FinishUpdateConfigDataOK(floppy_to_be_read));
		machine.evt_FinishUpdateConfigDataOK.run_FinishUpdateConfigDataOK(floppy_to_be_read);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);

		//FD.Enclave.TISAdminLogout 
		//FD.Enclave.AdminLogout
		machine.set_adminTokenPresence(machine.absent);
		//-> TokenRemovedAdminLogout
		Assert.assertTrue("Guard evt_TokenRemovedAdminLogout not satisfied.", machine.evt_TokenRemovedAdminLogout.guard_TokenRemovedAdminLogout());
		machine.evt_TokenRemovedAdminLogout.run_TokenRemovedAdminLogout();
		Assert.assertEquals(machine.get_enclaveStatus1(), machine.enclaveQuiescent);

		//FD.AuditLog.LogChange

	}

	@SuppressWarnings({ "static-access", "unchecked" })
	@Test
	public void test_UserEntry2(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3));

		//users
		Integer user1 = 1; //p01.dat: 4294967295
		Integer user2 = 2; //p01.dat: 0000032767
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1; //p01.dat: 100000002
		Integer serial2 = 2; //p01.dat: 1029384756
		Integer serial3 = 3; //p01.dat: 987654321
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '59', 'Month': '12', 'Day': '31', 'Hour': '23', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}



		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1) //p01.dat: it is itself ('Text': 'User02', 'ID': '100000002')
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>());


		
		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User02fp = 1; //p01.dat: User02fp
		machine.set_fingerprint(new BSet<Integer>(User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.guard) //p02.dat: 'Role': '1'
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret) //p02.dat 'Class': '4'
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User02fp)
				));

		//tokenID
		Integer tok = 1;
		machine.set_tokenID(new BSet<Integer>(tok));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						1)
				));*/

		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(31)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.guard, s)));

		machine.set_tokenRemovalTimeout(0);
		machine.set_tokenRemovalDuration(0);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);
		machine.set_latchUnlockDuration(31);
		machine.set_alarmSilentDuration(10);

		Integer currentTime = 31;
		Integer fingerPrint = User02fp;

		machine.set_currentToken(-1);

		//Init parameters
		Integer token_user_to_read = 1;
		machine.set_entry_status1(machine.quiescent);
		machine.set_userTokenPresence(machine.present);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);

		//FD.UserEntry.TISReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", 
				machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), machine.wait);
		
		//FD.UserEntry.BioCheckRequired 
		Assert.assertTrue("Guard evt_BioCheckRequired not satisfied.", machine.evt_BioCheckRequired.guard_BioCheckRequired(currentTime));
		machine.evt_BioCheckRequired.run_BioCheckRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), machine.waitingFinger);

		machine.set_FingerPresence(machine.present);
		//FD.UserEntry.ReadFingerOK
		Assert.assertTrue("Guard evt_ReadFingerOK not satisfied.", machine.evt_ReadFingerOK.guard_ReadFingerOK());
		machine.evt_ReadFingerOK.run_ReadFingerOK();
		Assert.assertEquals(machine.get_displayMessage1(), machine.wait);

		//FD.UserEntry.ValidateFingerOK
		Assert.assertTrue("Guard evt_ValidateFingerOK not satisfied.", machine.evt_ValidateFingerOK.guard_ValidateFingerOK(fingerPrint));
		machine.evt_ValidateFingerOK.run_ValidateFingerOK(fingerPrint);
		Assert.assertEquals(machine.get_displayMessage1(), machine.wait);
	
		//FD.UserEntry.ConstructAuthCert -> built-in writeUserTokenOK

		//FD.UserEntry.WriteUserTokenOK
		Assert.assertTrue("Guard evt_WriteUserTokenOK not satisfied.", machine.evt_WriteUserTokenOK.guard_WriteUserTokenOK());
		machine.evt_WriteUserTokenOK.run_WriteUserTokenOK();
		Assert.assertEquals(machine.get_displayMessage1(), machine.wait);

		//FD.UserEntry.EntryOK
		Assert.assertTrue("Guard evt_EntryOK not satisfied.", machine.evt_EntryOK_1.guard_EntryOK_1(currentTime)
				|| machine.evt_EntryOK_2.guard_EntryOK_2(currentTime));
		machine.evt_EntryOK_1.run_EntryOK_1(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), machine.openDoor);

		machine.set_userTokenPresence(machine.absent);
		//FD.UserEntry.UnlockDoorOK
		Assert.assertTrue("Guard evt_UnlockDoorOK not satisfied.", machine.evt_unlockDoorOK.guard_unlockDoorOK(currentTime));
		machine.evt_unlockDoorOK.run_unlockDoorOK(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), machine.doorUnlocked);
		//FD.AuditLog.LogChange
	}
	@SuppressWarnings({ "static-access", "unchecked" })
	@Test
	public void test_UserEntry3(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		Integer cert4 = 4;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,cert4));

		//users
		Integer user1 = 1; 
		Integer user2 = 2; 
		Integer user3 = 3; 
		machine.set_user(new BSet<Integer>(user1,user2,user3));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2),
				new BSet<Integer>(user3)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert4,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1;
		Integer serial2 = 2;
		Integer serial3 = 3;
		Integer serial4 = 4;
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,serial4));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert4,serial4)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,100),
				new Pair<Integer,Integer>(cert2,100),
				new Pair<Integer,Integer>(cert3,100),
				new Pair<Integer,Integer>(cert4,100)
				));

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert4,pubKey4)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1) 
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert4,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert4));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User02fp = 1; //p01.dat: User02fp
		machine.set_fingerprint(new BSet<Integer>(User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.guard) 
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.guard) 
				));

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User02fp)
				));

		//tokenID
		Integer tok = 1;
		machine.set_tokenID(new BSet<Integer>(tok));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert4)
				));

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok),
				new Pair<Integer,Integer>(cert4,tok)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						1)
				));*/

		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(100)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.guard, s)));


		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalDuration(15);

		machine.set_tokenRemovalTimeout(0);
		machine.set_latchTimeout1(40);
		machine.set_latchTimeout2(40);



		machine.set_currentToken(-1);

		//Init parameters
		Integer token_user_to_read = 1;
		Integer currentTime = 100;
		machine.set_entry_status1(machine.quiescent);
		machine.set_userTokenPresence(machine.present);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);

		//FD.UserEntry.TISReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);
		
		//FD.UserEntry.BioCheckNotRequired 
		Assert.assertTrue("Guard evt_BioCheckNotRequired not satisfied.", 
				machine.evt_BioCheckNotRequired.guard_BioCheckNotRequired(currentTime));
		machine.evt_BioCheckNotRequired.run_BioCheckNotRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.EntryOK
		Assert.assertTrue("Guard evt_EntryOK not satisfied.", machine.evt_EntryOK_1.guard_EntryOK_1(currentTime)
				|| machine.evt_EntryOK_2.guard_EntryOK_2(currentTime));
		machine.evt_EntryOK_1.run_EntryOK_1(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.openDoor);

		machine.set_userTokenPresence(machine.absent);

		//FD.UserEntry.UnlockDoorOK
		Assert.assertTrue("Guard evt_UnlockDoorOK not satisfied.", 
				machine.evt_unlockDoorOK.guard_unlockDoorOK(currentTime));
		machine.evt_unlockDoorOK.run_unlockDoorOK(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.doorUnlocked);
		//FD.AuditLog.LogChange
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_Override1(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		Integer cert4 = 4;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,cert4));

		//users
		Integer user1 = 1; 
		Integer user2 = 2; 
		Integer user3 = 3; 
		machine.set_user(new BSet<Integer>(user1,user2,user3));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2),
				new BSet<Integer>(user3)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert4,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1;
		Integer serial2 = 2;
		Integer serial3 = 3;
		Integer serial4 = 4;
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,serial4));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert4,serial4)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1),
				new Pair<Integer,Integer>(cert4,1)
				));

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert4,pubKey4)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1) 
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert4,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert4));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User02fp = 1; //p01.dat: User02fp
		machine.set_fingerprint(new BSet<Integer>(User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.guard) 
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.guard) 
				));

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.secret)
				));

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User02fp)
				));

		//tokenID
		Integer tok = 1;
		machine.set_tokenID(new BSet<Integer>(tok));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert4)
				));

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok),
				new Pair<Integer,Integer>(cert4,tok)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						1)
				));*/

		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.guard, s)));


		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalDuration(15);

		machine.set_tokenRemovalTimeout(0);
		machine.set_latchTimeout1(40);
		machine.set_latchTimeout2(40);



		machine.set_currentToken(-1);
		Integer admin1 = 1;
		Integer tokAdmin = 1;

		//admin
		machine.set_admin(new BSet<Integer>(admin1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(admin1,tokAdmin)
				));

		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.NoRole)
						));

		//currentAdminOp
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.archiveLog)
						));
		
		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.overrideLock,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));

		//Init Parameters
		Integer token_admin_to_read = 1;
		Integer currentTime = 1;
		
		machine.set_entry_status2(machine.quiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);
		machine.set_adminTokenPresence(machine.present);

		//FD.Enclave.TISAdminLogin 
		//FD.Enclave.GetPresentAdminToken
		//It is equivalent to: ReadAdminToken
		Assert.assertTrue("Guard evt_readAdminToken not satisfied.", machine.evt_readAdminToken.guard_readAdminToken(token_admin_to_read));
		machine.evt_readAdminToken.run_readAdminToken(token_admin_to_read);
		Assert.assertEquals(machine.get_enclaveStatus2(), ref6_admin.gotAdminToken);
		
		//FD.Enclave.ValidateAdminTokenOK
		Assert.assertTrue("Guard evt_ValidateAdminTokenOK not satisfied.", machine.evt_ValidateAdminTokenOK.guard_ValidateAdminTokenOK(currentTime));
		machine.evt_ValidateAdminTokenOK.run_ValidateAdminTokenOK(currentTime);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);

		Integer currentKeyedData1 = 2;
		Integer currentKeyedData2 = 1;
		//FD.Enclave.TISStartAdminOp 
		machine.set_keyedDataPresence(machine.present);
		//FD.Enclave.ValidateOpRequestFail
		//Administrator with role Guard performing a garbage operation
		//rolePresent 
				machine.set_rolePresent(
						new BRelation<Integer,Integer>(
								new Pair<Integer,Integer>(admin1,machine.guard)
								));
		Assert.assertTrue("Guard evt_ValidateOpRequestFail not satisfied.", 
				machine.evt_ValidateOpRequestFail.guard_ValidateOpRequestFail(currentKeyedData1));
		machine.evt_ValidateOpRequestFail.run_ValidateOpRequestFail(currentKeyedData1);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.invalidRequest);

		//FD.Enclave.ValidateOpRequestOK
		//Administrator with role Guard performing an overrideLock operation
		Assert.assertTrue("Guard evt_ValidateOpRequestOK not satisfied.", 
				machine.evt_ValidateOpRequestOK.guard_ValidateOpRequestOK(currentKeyedData2));
		machine.evt_ValidateOpRequestOK.run_ValidateOpRequestOK(currentKeyedData2);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		//FD.Enclave.TISUnlockDoorOp 
		//FD.Enclave.OverrideDoorLockOK
		Assert.assertTrue("Guard evt_OverrideDoorLockOK not satisfied.", machine.evt_OverrideDoorLockOK.guard_OverrideDoorLockOK(currentTime));
		machine.evt_OverrideDoorLockOK.run_OverrideDoorLockOK(currentTime);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);
		Assert.assertEquals(machine.get_displayMessage3(), ref6_admin.doorUnlocked);

		//FD.AuditLog.LogChange

	}
	
	@SuppressWarnings({ "static-access", "unchecked" })
	@Test
	public void test_UserEntry4(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3));

		//users
		Integer user1 = 1; //p01.dat: 4294967295
		Integer user2 = 2; //p01.dat: 0000032767
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1; //p01.dat: 100000003
		Integer serial2 = 2; //p01.dat: 1029384756
		Integer serial3 = 3; //p01.dat: 987654321
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,2),
				new Pair<Integer,Integer>(cert3,2)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '59', 'Month': '12', 'Day': '31', 'Hour': '23', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}
		//and (2) means:
		//{'NotAfter': {'Minute': '59', 'Month': '12', 'Day': '31', 'Hour': '23', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1) //p01.dat: it is itself ('Text': 'User01', 'ID': '100000003')
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User03fp = 1; //p01.dat: User03fp
		machine.set_fingerprint(new BSet<Integer>(User03fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.guard)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1, machine.securityOfficer)
				));
		
		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User03fp)
				));

		//tokenID
		Integer tok = 1;
		machine.set_tokenID(new BSet<Integer>(tok));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,1)
						)
				);

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User03fp,machine.goodF)
				));
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.unmarked), 
						1)
				));*/

		//entryPeriod
		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.securityOfficer, s)));


		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(1);
		machine.set_latchTimeout2(1);


		machine.set_currentToken(-1);

		//Init parameters
		Integer token_user_to_read = 1;
		machine.set_entry_status1(machine.quiescent);
		machine.set_userTokenPresence(machine.present);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);
		Integer currentTime = 10;

		//FD.UserEntry.TISReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);
		
		//FD.UserEntry.ValidateUserTokenFail 
		Assert.assertTrue("Guard evt_ValidateUserTokenFail not satisfied.", 
				machine.evt_ValidateUserTokenFail.guard_ValidateUserTokenFail(currentTime));
		machine.evt_ValidateUserTokenFail.run_ValidateUserTokenFail(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.removeToken);

		machine.set_userTokenPresence(machine.absent);
		//FD.UserEntry.FailedAccessTokenRemoved
		Assert.assertTrue("Guard evt_FailedAccessTokenRemoved not satisfied.", machine.evt_FailedAccessTokenRemoved.guard_FailedAccessTokenRemoved());
		machine.evt_FailedAccessTokenRemoved.run_FailedAccessTokenRemoved();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.welcome);


		//FD.Stats.Update
		//FD.AuditLog.LogChange
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_UserEntry5(){
		//certificates user trying to enter
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		//certificates admin with guard role
		Integer cert1_adm = 4;
		Integer cert2_adm = 5;
		Integer cert3_adm = 6;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,
				cert1_adm, cert2_adm, cert3_adm ));

		//users for the user certificates
		Integer user1 = 1; //p01.dat: 4294967295
		Integer user2 = 2; //p01.dat: 0000032117
		Integer user3 = 3; //p04.dat: 0000032767
		//users for the admin certificates
		Integer user1_adm = 4;
		Integer user2_adm = 5;
		machine.set_user(new BSet<Integer>(user1,user2,user3,
				user1_adm, user2_adm));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2),
				new BSet<Integer>(user3),
				new BSet<Integer>(user1_adm),
				new BSet<Integer>(user2_adm)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user3)),
				new Pair<Integer,BSet<Integer>>(cert1_adm,new BSet<Integer>(user1_adm)),
				new Pair<Integer,BSet<Integer>>(cert2_adm,new BSet<Integer>(user2_adm)),
				new Pair<Integer,BSet<Integer>>(cert3_adm,new BSet<Integer>(user2_adm))
				));*/

		//serials for user certificates
		Integer serial1 = 1; //p01.dat: 100000004
		Integer serial2 = 2; //p01.dat: 987654321
		Integer serial3 = 3; //p01.dat: 1029384756
		//serials for admin certificates
		Integer serial1_adm = 4;
		Integer serial2_adm = 5;
		Integer serial3_adm = 6; //p01.dat: 1029384756
		/*machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,
				serial1_adm,serial2_adm,serial3_adm));*/

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert1_adm,serial1_adm),
				new Pair<Integer,Integer>(cert2_adm,serial2_adm),
				new Pair<Integer,Integer>(cert3_adm,serial3_adm)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,2),
				new Pair<Integer,Integer>(cert3,2),
				new Pair<Integer,Integer>(cert1_adm,1),
				new Pair<Integer,Integer>(cert2_adm,1),
				new Pair<Integer,Integer>(cert3_adm,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 
		//and (2) means
		//{'NotAfter': {'Minute': '59', 'Month': '12', 'Day': '31', 'Hour': '23', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 



		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		//publicKeys for admin certificates
		Integer pubKey1_adm = 5;
		Integer pubKey2_adm = 6;
		Integer pubKey3_adm = 7;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4,
				pubKey1_adm,pubKey2_adm,pubKey3_adm));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				//new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert1_adm,pubKey1_adm),
				new Pair<Integer,Integer>(cert2_adm,pubKey2_adm),
				new Pair<Integer,Integer>(cert3_adm,pubKey3_adm)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1,cert1_adm));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3,cert2_adm,cert3_adm));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1), //p01.dat: it is itself ('Text': 'User01', 'ID': '100000001')
				new Pair<Integer,Integer>(cert1_adm,user1_adm)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert2_adm,cert1_adm),
				new Pair<Integer,Integer>(cert3_adm,cert1_adm)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3,cert2_adm));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2,cert3_adm));

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User04fp = 1; //p01.dat: User04fp
		Integer User02fp = 2;
		machine.set_fingerprint(new BSet<Integer>(User04fp, User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.securityOfficer),
				new Pair<Integer,Integer>(cert2_adm,machine.guard)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret),
				new Pair<Integer,Integer>(cert2_adm,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User04fp),
				new Pair<Integer,Integer>(cert3_adm,User02fp)
				));

		//tokenID
		Integer tok = 1;
		Integer tok_adm = 2;
		machine.set_tokenID(new BSet<Integer>(tok, tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1),
				new Pair<Integer,Integer>(tok_adm,cert1_adm)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3),
				new Pair<Integer,Integer>(tok_adm,cert2_adm)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2),
				new Pair<Integer,Integer>(tok_adm,cert3_adm)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok),
				new Pair<Integer,Integer>(cert2_adm,tok_adm),
				new Pair<Integer,Integer>(cert3_adm,tok_adm)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1_adm),pubKey1_adm),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2_adm),pubKey1_adm)
				));*

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT),
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User04fp,machine.goodF),
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.unmarked), 
						1)
				));*/

		//entryPeriod
		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.securityOfficer, s)));

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(1);
		machine.set_latchTimeout2(1);


		machine.set_currentToken(-1);
		machine.set_currentAdminToken(-1);
		Integer admin1 = 1;
		Integer tokAdmin = 2;
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);

		//admin
		machine.set_admin(new BSet<Integer>(admin1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(admin1,tokAdmin)
				));

		machine.set_currentAdminToken(tokAdmin);

		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.guard)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.overrideLock)
						));

		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.overrideLock,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));
		
		machine.set_availableOps(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1, machine.updateConfigData)
						)
				);

		//Init parameters
		Integer token_user_to_read = 1;
		Integer currentTime = 10;
		Integer currentKeyedData = 1;
		machine.set_entry_status1(machine.quiescent);
		machine.set_entry_status2(machine.quiescent);
		machine.set_userTokenPresence(machine.present);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);


		//FD.UserEntry.TISReadUserToken 
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);


		//FD.UserEntry.ValidateUserTokenFail
		Assert.assertTrue("Guard evt_ValidateUserTokenFail not satisfied.", 
				machine.evt_ValidateUserTokenFail.guard_ValidateUserTokenFail(currentTime));
		machine.evt_ValidateUserTokenFail.run_ValidateUserTokenFail(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.removeToken);

		machine.set_userTokenPresence(machine.absent);

		//FD.UserEntry.FailedAccessTokenRemoved 
		Assert.assertTrue("Guard evt_FailedAccessTokenRemoved not satisfied.", machine.evt_FailedAccessTokenRemoved.guard_FailedAccessTokenRemoved());
		machine.evt_FailedAccessTokenRemoved.run_FailedAccessTokenRemoved();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.welcome);

		//FD.Stats.Update -> not modelled in EB
		machine.set_adminTokenPresence(machine.present); 
		machine.set_keyedDataPresence(machine.present);

		
		//FD.Enclave.TISStartAdminOp is defined as ValidateOpRequest
		Assert.assertTrue("Neither guard evt_ValidateOpRequestOK or evt_ValidateOpRequestFail were satisfied.", 
				machine.evt_ValidateOpRequestOK.guard_ValidateOpRequestOK(currentKeyedData)	||
				machine.evt_ValidateOpRequestFail.guard_ValidateOpRequestFail(currentKeyedData));
		if (machine.evt_ValidateOpRequestOK.guard_ValidateOpRequestOK(currentKeyedData)){
			machine.evt_ValidateOpRequestOK.run_ValidateOpRequestOK(currentKeyedData);
			Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);
		}

		else if (machine.evt_ValidateOpRequestFail.guard_ValidateOpRequestFail(currentKeyedData)){
			machine.evt_ValidateOpRequestFail.run_ValidateOpRequestFail(currentKeyedData);
			Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.invalidRequest);


		} 

		//FD.Enclave.OverrideDoorLockOK
		Assert.assertTrue("Guard evt_FailedAccessTokenRemoved not satisfied.", machine.evt_OverrideDoorLockOK.guard_OverrideDoorLockOK(currentTime));
		machine.evt_OverrideDoorLockOK.run_OverrideDoorLockOK(currentTime);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);
		Assert.assertEquals(machine.get_displayMessage3(), ref6_admin.doorUnlocked);

		machine.set_adminTokenPresence(machine.absent); 
		machine.set_enclaveStatus2(machine.waitingFinishAdminOp);
		//FD.Enclave.TISUnlockDoorOp
		Assert.assertTrue("Guard evt_BadAdminLogout not satisfied.", machine.evt_BadAdminLogout.guard_BadAdminLogout());
		machine.evt_BadAdminLogout.run_BadAdminLogout();
		//Assert.assertEquals(machine.get_currentAdminOp(), ref6_admin.NoOp);

		//FD.AuditLog.LogChange
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_UserEntry6(){
		//certificates user trying to enter
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		//certificates admin with guard role
		Integer cert1_adm = 4;
		Integer cert2_adm = 5;
		Integer cert3_adm = 6;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,
				cert1_adm, cert2_adm, cert3_adm ));

		//users for the user certificates
		Integer user1 = 1; //4294967295
		Integer user2 = 2; //0000032767
		//users for the admin certificates
		Integer user1_adm = 4;
		Integer user2_adm = 5;
		machine.set_user(new BSet<Integer>(user1,user2,
				user1_adm, user2_adm));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2),
				new BSet<Integer>(user1_adm),
				new BSet<Integer>(user2_adm)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert1_adm,new BSet<Integer>(user1_adm)),
				new Pair<Integer,BSet<Integer>>(cert2_adm,new BSet<Integer>(user2_adm)),
				new Pair<Integer,BSet<Integer>>(cert3_adm,new BSet<Integer>(user2_adm))
				));*/

		//serials for user certificates
		Integer serial1 = 1; //100000005
		Integer serial2 = 2; //'0000032767
		Integer serial3 = 3; //1029384756
		//serials for admin certificates
		Integer serial1_adm = 4;
		Integer serial2_adm = 5;
		Integer serial3_adm = 6; //p01.dat: 1029384756
		/*machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,
				serial1_adm,serial2_adm,serial3_adm));*/

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert1_adm,serial1_adm),
				new Pair<Integer,Integer>(cert2_adm,serial2_adm),
				new Pair<Integer,Integer>(cert3_adm,serial3_adm)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1),
				new Pair<Integer,Integer>(cert1_adm,1),
				new Pair<Integer,Integer>(cert2_adm,1),
				new Pair<Integer,Integer>(cert3_adm,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		//publicKeys for admin certificates
		Integer pubKey1_adm = 5;
		Integer pubKey2_adm = 6;
		Integer pubKey3_adm = 7;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4,
				pubKey1_adm,pubKey2_adm,pubKey3_adm));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert1_adm,pubKey1_adm),
				new Pair<Integer,Integer>(cert2_adm,pubKey2_adm),
				new Pair<Integer,Integer>(cert3_adm,pubKey3_adm)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1,cert1_adm));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3,cert2_adm,cert3_adm));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1), //p01.dat: it is itself ('Text': 'User01', 'ID': '100000001')
				new Pair<Integer,Integer>(cert1_adm,user1_adm)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert2_adm,cert1_adm),
				new Pair<Integer,Integer>(cert3_adm,cert1_adm)
				));
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3,cert2_adm));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2,cert3_adm));

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User05fp = 1; //p01.dat: User05fp
		Integer User02fp = 2;
		machine.set_fingerprint(new BSet<Integer>(User05fp, User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.userOnly),
				new Pair<Integer,Integer>(cert2_adm,machine.guard)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret),
				new Pair<Integer,Integer>(cert2_adm,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User05fp),
				new Pair<Integer,Integer>(cert3_adm,User02fp)
				));

		//tokenID
		Integer tok = 1;
		Integer tok_adm = 2;
		machine.set_tokenID(new BSet<Integer>(tok, tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1),
				new Pair<Integer,Integer>(tok_adm,cert1_adm)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3),
				new Pair<Integer,Integer>(tok_adm,cert2_adm)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2),
				new Pair<Integer,Integer>(tok_adm,cert3_adm)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok),
				new Pair<Integer,Integer>(cert2_adm,tok_adm),
				new Pair<Integer,Integer>(cert3_adm,tok_adm)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1_adm),pubKey1_adm),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2_adm),pubKey1_adm)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT),
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User05fp,machine.goodF),
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.unmarked), 
						1)
				));*/

		//entryPeriod
		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.securityOfficer, s)));

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(1);
		machine.set_latchTimeout2(1);


		machine.set_currentToken(-1);
		machine.set_currentAdminToken(-1);
		Integer admin1 = 1;
		Integer tokAdmin = 2;
		Integer token_user_to_read = 1;
		Integer currentTime = 1;
		Integer currentKeyedData = 1;
		machine.set_entry_status1(machine.quiescent);
		machine.set_entry_status2(machine.quiescent);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);


		//admin
		machine.set_admin(new BSet<Integer>(admin1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(admin1,tokAdmin)
				));

		machine.set_currentAdminToken(tokAdmin);

		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.guard)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.overrideLock)
						));

		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.overrideLock,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));


		machine.set_userTokenPresence(machine.present); 
		//FD.UserEntry.TISReadUserToken equivalent to ReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", 
				machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);


		//FD.UserEntry.BioCheckRequired 
		Assert.assertTrue("Guard evt_BioCheckRequired not satisfied.", machine.evt_BioCheckRequired.guard_BioCheckRequired(currentTime));
		machine.evt_BioCheckRequired.run_BioCheckRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.insertFinger);

		//FD.UserEntry.NoFinger
		machine.set_FingerPresence(machine.absent);

		//FD.UserEntry.FingerTimeout 
		Assert.assertTrue("Guard evt_FingerTimeout not satisfied.", machine.evt_FingerTimeout.guard_FingerTimeout());
		machine.evt_FingerTimeout.run_FingerTimeout();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.removeToken);

		machine.set_adminTokenPresence(machine.present);
		machine.set_keyedDataPresence(machine.present); 
		//FD.Enclave.TISStartAdminOp: It is defined as ValidateOpRequest
		Assert.assertTrue("Neither guard evt_ValidateOpRequestOK or evt_ValidateOpRequestFail were satisfied.",
				machine.evt_ValidateOpRequestOK.guard_ValidateOpRequestOK(currentKeyedData)	||
				machine.evt_ValidateOpRequestFail.guard_ValidateOpRequestFail(currentKeyedData));
		if (machine.evt_ValidateOpRequestOK.guard_ValidateOpRequestOK(currentKeyedData)){
			machine.evt_ValidateOpRequestOK.run_ValidateOpRequestOK(currentKeyedData);
			Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);
		}

		else if (machine.evt_ValidateOpRequestFail.guard_ValidateOpRequestFail(currentKeyedData)){
			machine.evt_ValidateOpRequestFail.run_ValidateOpRequestFail(currentKeyedData);
			Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.invalidRequest);
		} 

		//FD.Enclave.OverrideDoorLockOK 
		Assert.assertTrue("Guard evt_OverrideDoorLockOK not satisfied.", machine.evt_OverrideDoorLockOK.guard_OverrideDoorLockOK(currentTime));
		machine.evt_OverrideDoorLockOK.run_OverrideDoorLockOK(currentTime);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);
		Assert.assertEquals(machine.get_displayMessage3(), ref6_admin.doorUnlocked);


		//FD.Enclave.TISUnlockDoorOp 
		//FD.AuditLog.LogChange

		machine.set_userTokenPresence(machine.absent);
		//FD.UserEntry.FailedAccessTokenRemoved 
		Assert.assertTrue("Guard evt_FailedAccessTokenRemoved not satisfied.", machine.evt_FailedAccessTokenRemoved.guard_FailedAccessTokenRemoved());
		machine.evt_FailedAccessTokenRemoved.run_FailedAccessTokenRemoved();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.welcome);

	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_UserEntry7(){

		//certificates user trying to enter
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		//certificates admin with guard role
		Integer cert1_adm = 4;
		Integer cert2_adm = 5;
		Integer cert3_adm = 6;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,
				cert1_adm, cert2_adm, cert3_adm ));

		//users for the user certificates
		Integer user1 = 1; //4294967295
		Integer user2 = 2; //0000032767
		//users for the admin certificates
		Integer user1_adm = 4;
		Integer user2_adm = 5;
		machine.set_user(new BSet<Integer>(user1,user2,
				user1_adm, user2_adm));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2),
				new BSet<Integer>(user1_adm),
				new BSet<Integer>(user2_adm)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert1_adm,new BSet<Integer>(user1_adm)),
				new Pair<Integer,BSet<Integer>>(cert2_adm,new BSet<Integer>(user2_adm)),
				new Pair<Integer,BSet<Integer>>(cert3_adm,new BSet<Integer>(user2_adm))
				));*/

		//serials for user certificates
		Integer serial1 = 1; //100000005
		Integer serial2 = 2; //'0000032767
		Integer serial3 = 3; //1029384756
		//serials for admin certificates
		Integer serial1_adm = 4;
		Integer serial2_adm = 5;
		Integer serial3_adm = 6; //p01.dat: 1029384756
		/*machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,
				serial1_adm,serial2_adm,serial3_adm));*/

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert1_adm,serial1_adm),
				new Pair<Integer,Integer>(cert2_adm,serial2_adm),
				new Pair<Integer,Integer>(cert3_adm,serial3_adm)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1),
				new Pair<Integer,Integer>(cert1_adm,1),
				new Pair<Integer,Integer>(cert2_adm,1),
				new Pair<Integer,Integer>(cert3_adm,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		//publicKeys for admin certificates
		Integer pubKey1_adm = 5;
		Integer pubKey2_adm = 6;
		Integer pubKey3_adm = 7;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4,
				pubKey1_adm,pubKey2_adm,pubKey3_adm));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert1_adm,pubKey1_adm),
				new Pair<Integer,Integer>(cert2_adm,pubKey2_adm),
				new Pair<Integer,Integer>(cert3_adm,pubKey3_adm)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1,cert1_adm));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3,cert2_adm,cert3_adm));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1), //p01.dat: it is itself ('Text': 'User01', 'ID': '100000001')
				new Pair<Integer,Integer>(cert1_adm,user1_adm)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert2_adm,cert1_adm),
				new Pair<Integer,Integer>(cert3_adm,cert1_adm)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3,cert2_adm));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2,cert3_adm));

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User05fp = 1; //p01.dat: User05fp
		Integer User02fp = 2;
		machine.set_fingerprint(new BSet<Integer>(User05fp, User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.userOnly),
				new Pair<Integer,Integer>(cert2_adm,machine.guard)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret),
				new Pair<Integer,Integer>(cert2_adm,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User05fp),
				new Pair<Integer,Integer>(cert3_adm,User02fp)
				));

		//tokenID
		Integer tok = 1;
		Integer tok_adm = 2;
		machine.set_tokenID(new BSet<Integer>(tok, tok_adm));
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1),
				new Pair<Integer,Integer>(tok_adm,cert1_adm)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3),
				new Pair<Integer,Integer>(tok_adm,cert2_adm)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2),
				new Pair<Integer,Integer>(tok_adm,cert3_adm)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok),
				new Pair<Integer,Integer>(cert2_adm,tok_adm),
				new Pair<Integer,Integer>(cert3_adm,tok_adm)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1_adm),pubKey1_adm),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2_adm),pubKey1_adm)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT),
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User05fp,machine.goodF),
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.unmarked), 
						1)
				));*/

		//entryPeriod
		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.securityOfficer, s)));

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(1);
		machine.set_latchTimeout2(1);


		machine.set_currentToken(-1);
		machine.set_currentAdminToken(-1);
		Integer admin1 = 1;
		Integer tokAdmin = 2;
		Integer token_user_to_read = 1;
		Integer currentTime = 1;
		machine.set_entry_status1(machine.quiescent);
		machine.set_entry_status2(machine.quiescent);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);


		//admin
		machine.set_admin(new BSet<Integer>(admin1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(admin1,tokAdmin)
				));

		machine.set_currentAdminToken(tokAdmin);

		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.guard)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.NoOp)
						));

		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.overrideLock,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));

		machine.set_userTokenPresence(machine.present); 
		//FD.UserEntry.TISReadUserToken is defined as ReadUserToken 
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.BioCheckRequired 
		Assert.assertTrue("Guard evt_BioCheckRequired not satisfied.", machine.evt_BioCheckRequired.guard_BioCheckRequired(currentTime));
		machine.evt_BioCheckRequired.run_BioCheckRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.waitingFinger);

		machine.set_FingerPresence(machine.present); 
		//FD.UserEntry.ReadFingerOK 
		Assert.assertTrue("Guard evt_ReadFingerOK not satisfied.", machine.evt_ReadFingerOK.guard_ReadFingerOK());
		machine.evt_ReadFingerOK.run_ReadFingerOK();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.ValidateFingerFail
		Assert.assertTrue("Guard evt_ValidateFingerFail not satisfied.", machine.evt_ValidateFingerFail.guard_ValidateFingerFail());
		machine.evt_ValidateFingerFail.run_ValidateFingerFail();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.removeToken);

		machine.set_userTokenPresence(machine.absent);
		//FD.UserEntry.FailedAccessTokenRemoved
		Assert.assertTrue("Guard evt_FailedAccessTokenRemoved not satisfied.", machine.evt_FailedAccessTokenRemoved.guard_FailedAccessTokenRemoved());
		machine.evt_FailedAccessTokenRemoved.run_FailedAccessTokenRemoved();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.welcome);

			//FD.AuditLog.LogChange

	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_UserEntry8(){
		//certificates user trying to enter
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		//certificates admin with guard role
		Integer cert1_adm = 4;
		Integer cert2_adm = 5;
		Integer cert3_adm = 6;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,
				cert1_adm, cert2_adm, cert3_adm ));

		//users for the user certificates
		Integer user1 = 1; //4294967295
		Integer user2 = 2; //0000032767
		//users for the admin certificates
		Integer user1_adm = 4;
		Integer user2_adm = 5;
		machine.set_user(new BSet<Integer>(user1,user2,
				user1_adm, user2_adm));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2),
				new BSet<Integer>(user1_adm),
				new BSet<Integer>(user2_adm)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert1_adm,new BSet<Integer>(user1_adm)),
				new Pair<Integer,BSet<Integer>>(cert2_adm,new BSet<Integer>(user2_adm)),
				new Pair<Integer,BSet<Integer>>(cert3_adm,new BSet<Integer>(user2_adm))
				));*/

		//serials for user certificates
		Integer serial1 = 1; //100000005
		Integer serial2 = 2; //'0000032767
		Integer serial3 = 3; //1029384756
		//serials for admin certificates
		Integer serial1_adm = 4;
		Integer serial2_adm = 5;
		Integer serial3_adm = 6; //p01.dat: 1029384756
		/*machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,
				serial1_adm,serial2_adm,serial3_adm));*/

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert1_adm,serial1_adm),
				new Pair<Integer,Integer>(cert2_adm,serial2_adm),
				new Pair<Integer,Integer>(cert3_adm,serial3_adm)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1),
				new Pair<Integer,Integer>(cert1_adm,1),
				new Pair<Integer,Integer>(cert2_adm,1),
				new Pair<Integer,Integer>(cert3_adm,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		//publicKeys for admin certificates
		Integer pubKey1_adm = 5;
		Integer pubKey2_adm = 6;
		Integer pubKey3_adm = 7;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4,
				pubKey1_adm,pubKey2_adm,pubKey3_adm));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert1_adm,pubKey1_adm),
				new Pair<Integer,Integer>(cert2_adm,pubKey2_adm),
				new Pair<Integer,Integer>(cert3_adm,pubKey3_adm)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1,cert1_adm));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3,cert2_adm,cert3_adm));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1), //p01.dat: it is itself ('Text': 'User01', 'ID': '100000001')
				new Pair<Integer,Integer>(cert1_adm,user1_adm)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert2_adm,cert1_adm),
				new Pair<Integer,Integer>(cert3_adm,cert1_adm)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3,cert2_adm));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2,cert3_adm));

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User05fp = 1; //p01.dat: User05fp
		Integer User02fp = 2;
		machine.set_fingerprint(new BSet<Integer>(User05fp, User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.userOnly),
				new Pair<Integer,Integer>(cert2_adm,machine.guard)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret),
				new Pair<Integer,Integer>(cert2_adm,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User05fp),
				new Pair<Integer,Integer>(cert3_adm,User02fp)
				));

		//tokenID
		Integer tok = 1;
		Integer tok_adm = 2;
		machine.set_tokenID(new BSet<Integer>(tok, tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1),
				new Pair<Integer,Integer>(tok_adm,cert1_adm)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3),
				new Pair<Integer,Integer>(tok_adm,cert2_adm)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2),
				new Pair<Integer,Integer>(tok_adm,cert3_adm)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				//new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok),
				new Pair<Integer,Integer>(cert2_adm,tok_adm),
				new Pair<Integer,Integer>(cert3_adm,tok_adm)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1_adm),pubKey1_adm),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2_adm),pubKey1_adm)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT),
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User05fp,machine.goodF),
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						1)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(31))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(31))
												)
										)
						)
				);
		
		

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(31);
		machine.set_tokenRemovalTimeout(36);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(1);
		machine.set_latchTimeout2(1);


		machine.set_currentToken(-1);
		machine.set_currentAdminToken(-1);
		Integer admin1 = 1;
		Integer tokAdmin = 2;
		Integer token_user_to_read = 1;
		Integer currentTime = 31;
		Integer currentFinger = 1;
		machine.set_entry_status1(machine.quiescent);
		machine.set_entry_status2(machine.quiescent);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);


		//admin
		machine.set_admin(new BSet<Integer>(admin1));
		
		machine.set_availableOps(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1, machine.updateConfigData)
						)
				);

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(admin1,tokAdmin)
				));

		machine.set_currentAdminToken(tokAdmin);

		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.guard)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.NoOp)
						));

		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.overrideLock,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));
		
		machine.set_tokenAuthCert(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,1)
						)
				);
		
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1, machine.securityOfficer)
				));

		machine.set_userTokenPresence(machine.present);
		//FD.UserEntry.TISReadUserToken is defined as ReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.BioCheckRequired 
		Assert.assertTrue("Guard evt_BioCheckRequired not satisfied.", machine.evt_BioCheckRequired.guard_BioCheckRequired(currentTime));
		machine.evt_BioCheckRequired.run_BioCheckRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.insertFinger);

		machine.set_FingerPresence(machine.present); 
		//FD.UserEntry.ReadFingerOK
		Assert.assertTrue("Guard evt_ReadFingerOK not satisfied.", machine.evt_ReadFingerOK.guard_ReadFingerOK());
		machine.evt_ReadFingerOK.run_ReadFingerOK();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.ValidateFingerOK 
		Assert.assertTrue("Guard evt_ValidateFingerOK not satisfied.", machine.evt_ValidateFingerOK.guard_ValidateFingerOK(currentFinger));
		machine.evt_ValidateFingerOK.run_ValidateFingerOK(currentFinger);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.WriteUserTokenOK 
		Assert.assertTrue("Guard WriteUserTokenOK not satisfied.", machine.evt_WriteUserTokenOK.guard_WriteUserTokenOK());
		machine.evt_WriteUserTokenOK.run_WriteUserTokenOK();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//EntryOk -> it is not defined in the test, however it makes sense according to Figure 6.1. Besides
		//the description of the test says:
		//The user obtains a card with valid auth certificate. 
		//The user leaves their card in the reader too long 
		
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,31),
				new Pair<Integer,Integer>(cert2,31),
				new Pair<Integer,Integer>(cert3,31),
				new Pair<Integer,Integer>(cert1_adm,31),
				new Pair<Integer,Integer>(cert2_adm,31),
				new Pair<Integer,Integer>(cert3_adm,31)
				));
		
		//==> after being requested to enter so the user entry times out. <==
		Assert.assertTrue("Guard WriteUserTokenOK not satisfied.", machine.evt_EntryOK_1.guard_EntryOK_1(currentTime)
				|| machine.evt_EntryOK_2.guard_EntryOK_2(currentTime));
		machine.evt_EntryOK_1.run_EntryOK_1(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.openDoor);

		//FD.UserEntry.WaitingTokenRemoval -> not modelled in EB but it is checked by TokenRemovalTimeout
		currentTime = 55;

		Assert.assertTrue("Guard TokenRemovalTimeout not satisfied.", 
				machine.evt_TokenRemovalTimeout.guard_TokenRemovalTimeout(currentTime));
		machine.evt_TokenRemovalTimeout.run_TokenRemovalTimeout(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.removeToken);

		//FD.Enclave.TISAdminLogout 
		machine.set_adminTokenPresence(machine.absent); 
		//FD.Enclave.AdminLogout  => TokenRemovedAdminLogout
		Assert.assertTrue("Guard TokenRemovedAdminLogout not satisfied.", machine.evt_TokenRemovedAdminLogout.guard_TokenRemovedAdminLogout());
		machine.evt_TokenRemovedAdminLogout.run_TokenRemovedAdminLogout();
		Assert.assertEquals(machine.get_currentAdminOp().apply(
				machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())
				), ref6_admin.NoOp);
		//FD.AuditLog.LogChange
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_UserEntry9(){
		//certificates admin - guard: p02.dat
		//certificates user: p05.dat
		//adding auth cert for both

		//certificates user trying to enter
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		Integer cert4 = 4;
		//certificates admin with guard role
		Integer cert1_adm = 5;
		Integer cert2_adm = 6;
		Integer cert3_adm = 7;
		Integer cert4_adm = 8;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,cert4,
				cert1_adm, cert2_adm, cert3_adm, cert4_adm));

		//users for the user certificates
		Integer user1 = 1;
		Integer user2 = 2;
		//users for the admin certificates
		Integer user1_adm = 4;
		Integer user2_adm = 5;
		machine.set_user(new BSet<Integer>(user1,user2,
				user1_adm, user2_adm));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2),
				new BSet<Integer>(user1_adm),
				new BSet<Integer>(user2_adm)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert4,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert1_adm,new BSet<Integer>(user1_adm)),
				new Pair<Integer,BSet<Integer>>(cert2_adm,new BSet<Integer>(user2_adm)),
				new Pair<Integer,BSet<Integer>>(cert3_adm,new BSet<Integer>(user2_adm)),
				new Pair<Integer,BSet<Integer>>(cert4_adm,new BSet<Integer>(user2_adm))
				));*/

		//serials for user certificates
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		Integer serial4 = 4; 
		//serials for admin certificates
		Integer serial1_adm = 5;
		Integer serial2_adm = 6;
		Integer serial3_adm = 7;
		Integer serial4_adm = 8;
		/*machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,serial4,
				serial1_adm,serial2_adm,serial3_adm,serial4_adm));*/

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert4,serial4),
				new Pair<Integer,Integer>(cert1_adm,serial1_adm),
				new Pair<Integer,Integer>(cert2_adm,serial2_adm),
				new Pair<Integer,Integer>(cert3_adm,serial3_adm),
				new Pair<Integer,Integer>(cert4_adm,serial4_adm)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,31),
				new Pair<Integer,Integer>(cert2,31),
				new Pair<Integer,Integer>(cert3,31),
				new Pair<Integer,Integer>(cert4,31),
				new Pair<Integer,Integer>(cert1_adm,31),
				new Pair<Integer,Integer>(cert2_adm,31),
				new Pair<Integer,Integer>(cert3_adm,31),
				new Pair<Integer,Integer>(cert4_adm,31)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		//publicKeys for admin certificates
		Integer pubKey1_adm = 5;
		Integer pubKey2_adm = 6;
		Integer pubKey3_adm = 7;
		Integer pubKey4_adm = 8;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4,
				pubKey1_adm,pubKey2_adm,pubKey3_adm,pubKey4_adm));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert4,pubKey4),
				new Pair<Integer,Integer>(cert1_adm,pubKey1_adm),
				new Pair<Integer,Integer>(cert2_adm,pubKey2_adm),
				new Pair<Integer,Integer>(cert3_adm,pubKey3_adm),
				new Pair<Integer,Integer>(cert4_adm,pubKey4_adm)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1,cert1_adm));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3,cert4,cert2_adm,cert3_adm,cert4_adm));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1),
				new Pair<Integer,Integer>(cert1_adm,user1_adm)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert4,cert1),
				new Pair<Integer,Integer>(cert2_adm,cert1_adm),
				new Pair<Integer,Integer>(cert3_adm,cert1_adm),
				new Pair<Integer,Integer>(cert4_adm,cert1_adm)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3,cert2_adm));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2,cert3_adm));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert4,cert4_adm));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User05fp = 1; //p01.dat: User05fp
		Integer User02fp = 2;
		machine.set_fingerprint(new BSet<Integer>(User05fp, User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.userOnly),
				new Pair<Integer,Integer>(cert2_adm,machine.guard)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret),
				new Pair<Integer,Integer>(cert2_adm,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.secret),
				new Pair<Integer,Integer>(cert4_adm,machine.secret)
				));

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.secret),
				new Pair<Integer,Integer>(cert4_adm,machine.secret)
				));

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User05fp),
				new Pair<Integer,Integer>(cert3_adm,User02fp)
				));

		//tokenID
		Integer tok = 1;
		Integer tok_adm = 2;
		machine.set_tokenID(new BSet<Integer>(tok, tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1),
				new Pair<Integer,Integer>(tok_adm,cert1_adm)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3),
				new Pair<Integer,Integer>(tok_adm,cert2_adm)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2),
				new Pair<Integer,Integer>(tok_adm,cert3_adm)
				));

		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);
		
		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert4),
				new Pair<Integer,Integer>(tok_adm,cert4_adm)
				));

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok),
				new Pair<Integer,Integer>(cert4,tok),
				new Pair<Integer,Integer>(cert2_adm,tok_adm),
				new Pair<Integer,Integer>(cert3_adm,tok_adm),
				new Pair<Integer,Integer>(cert4_adm,tok_adm)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1_adm),pubKey1_adm),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2_adm),pubKey1_adm)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT),
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User05fp,machine.goodF),
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(31))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(31))
												)
										)
						)
				);

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(31);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);

		machine.set_currentToken(-1);
		machine.set_currentAdminToken(-1);
		Integer admin1 = 1;
		Integer tokAdmin = 2;
		Integer token_user_to_read = 1;
		Integer token_admin_to_read = 2;
		Integer currentTime = 31;
		machine.set_entry_status1(machine.quiescent);
		machine.set_entry_status2(machine.quiescent);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);


		//admin
		machine.set_admin(new BSet<Integer>(admin1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(admin1,tokAdmin)
				));

		machine.set_currentAdminToken(tokAdmin);

		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.NoRole)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.NoOp)
						));

		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.overrideLock,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));	

		machine.set_adminTokenPresence(machine.present);

		//FD.Enclave.TISAdminLogin ==> ReadAdminToken
		Assert.assertTrue("Guard evt_readAdminToken not satisfied.", machine.evt_readAdminToken.guard_readAdminToken(token_admin_to_read));
		machine.evt_readAdminToken.run_readAdminToken(token_admin_to_read);
		Assert.assertEquals(machine.get_currentAdminToken(), token_admin_to_read);

		//FD.Enclave.GetPresentAdminToken 
		machine.set_adminTokenPresence(machine.present);

		//FD.Enclave.ValidateAdminTokenOK 
		Assert.assertTrue("Guard evt_ValidateAdminTokenOK not satisfied.", machine.evt_ValidateAdminTokenOK.guard_ValidateAdminTokenOK(currentTime));
		machine.evt_ValidateAdminTokenOK.run_ValidateAdminTokenOK(currentTime);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);

		machine.set_userTokenPresence(machine.present);
		//FD.UserEntry.TISReadUserToken is defined as ReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);


		//FD.UserEntry.BioCheckNotRequired 
		Assert.assertTrue("Guard evt_BioCheckNotRequired not satisfied.", machine.evt_BioCheckNotRequired.guard_BioCheckNotRequired(currentTime));
		machine.evt_BioCheckNotRequired.run_BioCheckNotRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.EntryOK 
		Assert.assertTrue("Guard evt_EntryOK not satisfied.", machine.evt_EntryOK_1.guard_EntryOK_1(currentTime)
				|| machine.evt_EntryOK_2.guard_EntryOK_2(currentTime));
		machine.evt_EntryOK_1.run_EntryOK_1(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.openDoor);

		machine.set_userTokenPresence(machine.absent);

		//UnlockDoor
		Assert.assertTrue("Guard evt_unlockDoorOK not satisfied.", 
				machine.evt_unlockDoorOK.guard_unlockDoorOK(currentTime));
		machine.evt_unlockDoorOK.run_unlockDoorOK(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.doorUnlocked);

		//FD.AuditLog.LogChange 
		//FD.Stats.Update

	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_AdminLogin1(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		Integer cert4 = 4;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,cert4));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert4,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		Integer serial4 = 4; 
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,serial4));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert4,serial4)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1),
				new Pair<Integer,Integer>(cert4,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert4,pubKey4)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3,cert4));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert4,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert4));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User05fp = 1; //p01.dat: User05fp
		Integer User02fp = 2;
		machine.set_fingerprint(new BSet<Integer>(User05fp, User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.userOnly)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.userOnly)
				));

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.secret)
				));

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User05fp)
				));

		//tokenID
		Integer tok = 1;
		Integer tok_adm = 2;
		machine.set_tokenID(new BSet<Integer>(tok, tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert4)
				));

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,tok)//,
				//new Pair<Integer,Integer>(cert2,tok),
				//new Pair<Integer,Integer>(cert3,tok)
				
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT),
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User05fp,machine.goodF),
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
												)
										)
						)
				);

		//admin
		machine.set_admin(new BSet<Integer>(1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1,1)
				));

		machine.set_currentAdminToken(1);

		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoRole)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoOp)
						));

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);

		machine.set_currentAdminToken(-1);
		Integer token_admin_to_read = 1;
		Integer currentTime = 1;
		machine.set_entry_status2(machine.quiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);
		machine.set_adminTokenPresence(machine.present);

		//FD.Enclave.TISAdminLogin ==> ReadAdminToken
		Assert.assertTrue("Guard evt_readAdminToken not satisfied.", machine.evt_readAdminToken.guard_readAdminToken(token_admin_to_read));
		machine.evt_readAdminToken.run_readAdminToken(token_admin_to_read);
		Assert.assertEquals(machine.get_currentAdminToken(), token_admin_to_read);

		//FD.Enclave.ValidateAdminTokenFail 
		Assert.assertTrue("Guard evt_ValidateAdminTokenFail not satisfied.", 
				machine.evt_ValidateAdminTokenFail_1.guard_ValidateAdminTokenFail_1()
				|| machine.evt_ValidateAdminTokenFail_2.guard_ValidateAdminTokenFail_2()
				|| machine.evt_ValidateAdminTokenFail_3.guard_ValidateAdminTokenFail_3(currentTime)
				|| machine.evt_ValidateAdminTokenFail_4.guard_ValidateAdminTokenFail_4()
				);
		
		machine.evt_ValidateAdminTokenFail_1.run_ValidateAdminTokenFail_1();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.removeAdminToken);

		machine.set_adminTokenPresence(machine.absent);

		//FD.Enclave.FailedAdminTokenRemoved
		Assert.assertTrue("Guard evt_FailedAdminTokenRemoved not satisfied.", 
				machine.evt_FailedAdminTokenRemoved.guard_FailedAdminTokenRemoved());
		machine.evt_FailedAdminTokenRemoved.run_FailedAdminTokenRemoved();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.welcomeAdmin);

		//FD.AuditLog.LogChange

	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_AdminLogin2(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User06fp = 1; 
		machine.set_fingerprint(new BSet<Integer>(User06fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.auditManager)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User06fp)
				));

		//tokenID
		Integer tok_adm = 1;
		machine.set_tokenID(new BSet<Integer>(tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok_adm),
				new Pair<Integer,Integer>(cert3,tok_adm)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User06fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
												)
										)
						)
				);

		Integer adm = 1;
		//admin
		machine.set_admin(new BSet<Integer>(adm));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(adm,tok_adm)
				));

		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(adm,machine.NoRole)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(adm,machine.NoOp)
						));

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);

		machine.set_entry_status2(machine.quiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);
		machine.set_adminTokenPresence(machine.present);

		Integer token_admin_to_read = 1;
		machine.set_currentAdminToken(-1);
		Integer currentTime = 1;

		//FD.Enclave.TISAdminLogin  => ReadAdminToken
		Assert.assertTrue("Guard evt_readAdminToken not satisfied.", machine.evt_readAdminToken.guard_readAdminToken(token_admin_to_read));
		machine.evt_readAdminToken.run_readAdminToken(token_admin_to_read);
		Assert.assertEquals(machine.get_currentAdminToken(), token_admin_to_read);

		//FD.Enclave.ValidateAdminTokenFail 
		Assert.assertTrue("Guard evt_ValidateAdminTokenFail not satisfied.", 
				machine.evt_ValidateAdminTokenFail_1.guard_ValidateAdminTokenFail_1()
				|| machine.evt_ValidateAdminTokenFail_2.guard_ValidateAdminTokenFail_2()
				|| machine.evt_ValidateAdminTokenFail_3.guard_ValidateAdminTokenFail_3(currentTime)
				|| machine.evt_ValidateAdminTokenFail_4.guard_ValidateAdminTokenFail_4()
				);
		
		machine.evt_ValidateAdminTokenFail_1.run_ValidateAdminTokenFail_1();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.removeAdminToken);

		machine.set_adminTokenPresence(machine.absent);
		//FD.Enclave.FailedAdminTokenRemoved 
		Assert.assertTrue("Guard evt_FailedAdminTokenRemoved not satisfied.", 
				machine.evt_FailedAdminTokenRemoved.guard_FailedAdminTokenRemoved());
		machine.evt_FailedAdminTokenRemoved.run_FailedAdminTokenRemoved();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.welcomeAdmin);

		//FD.AuditLog.LogChange
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_UserEntry10(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1)
				));
		
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User06fp = 1; 
		machine.set_fingerprint(new BSet<Integer>(User06fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.auditManager)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User06fp)
				));

		//tokenID
		Integer tok_adm = 1;
		machine.set_tokenID(new BSet<Integer>(tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok_adm),
				new Pair<Integer,Integer>(cert3,tok_adm)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User06fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
												)
										)
						)
				);

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);

		Integer token_user_to_read = 1;
		Integer currentTime = 1;

		machine.set_entry_status1(machine.quiescent); 
		machine.set_userTokenPresence(machine.present); 
		machine.set_enclaveStatus1(machine.enclaveQuiescent);

		//FD.UserEntry.TISReadUserToken is defined as ReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.BioCheckRequired
		Assert.assertTrue("Guard evt_BioCheckRequired not satisfied.", machine.evt_BioCheckRequired.guard_BioCheckRequired(currentTime));
		machine.evt_BioCheckRequired.run_BioCheckRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.insertFinger);

		machine.set_userTokenPresence(machine.absent);

		//FD.UserEntry.UserTokenTorn 
		Assert.assertTrue("Guard evt_UserTokenTear not satisfied.", machine.evt_UserTokenTear.guard_UserTokenTear());
		machine.evt_UserTokenTear.run_UserTokenTear();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.welcome);

		//FD.AuditLog.LogChange

	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_UserEntry11(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3)
				));
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User06fp = 1; 
		machine.set_fingerprint(new BSet<Integer>(User06fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.auditManager)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User06fp)
				));

		//tokenID
		Integer tok_adm = 1;
		machine.set_tokenID(new BSet<Integer>(tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok_adm),
				new Pair<Integer,Integer>(cert3,tok_adm)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User06fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
												)
										)
						)
				);

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);

		Integer token_user_to_read = 1;
		Integer currentTime = 1;
		Integer currentFinger = 1;

		machine.set_entry_status1(machine.quiescent); 
		machine.set_userTokenPresence(machine.present); 
		machine.set_enclaveStatus1(machine.enclaveQuiescent);

		//FD.UserEntry.TISReadUserToken is defined as ReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.BioCheckRequired
		Assert.assertTrue("Guard evt_BioCheckRequired not satisfied.", machine.evt_BioCheckRequired.guard_BioCheckRequired(currentTime));
		machine.evt_BioCheckRequired.run_BioCheckRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.insertFinger);
		
		machine.set_FingerPresence(machine.present); 
		//FD.UserEntry.ReadFingerOK
		Assert.assertTrue("Guard evt_ReadFingerOK not satisfied.", machine.evt_ReadFingerOK.guard_ReadFingerOK());
		machine.evt_ReadFingerOK.run_ReadFingerOK();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);


		//FD.UserEntry.ValidateFingerOK
		Assert.assertTrue("Guard evt_ValidateFingerOK not satisfied.", machine.evt_ValidateFingerOK.guard_ValidateFingerOK(currentFinger));
		machine.evt_ValidateFingerOK.run_ValidateFingerOK(currentFinger);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		machine.set_userTokenPresence(machine.absent);
		//FD.UserEntry.UserTokenTorn
		Assert.assertTrue("Guard evt_UserTokenTear not satisfied.", machine.evt_UserTokenTear.guard_UserTokenTear());
		machine.evt_UserTokenTear.run_UserTokenTear();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.welcome);
		//FD.AuditLog.LogChange

	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_UserEntry12(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User06fp = 1; 
		machine.set_fingerprint(new BSet<Integer>(User06fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.auditManager)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User06fp)
				));

		//tokenID
		Integer tok_adm = 1;
		machine.set_tokenID(new BSet<Integer>(tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert3)
				));
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok_adm),
				new Pair<Integer,Integer>(cert3,tok_adm)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User06fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
												)
										),
										new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
												machine.auditManager, new BRelation<Integer,BSet<Integer>>(
														new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
														)
												)
						)
				);

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);

		Integer token_user_to_read = 1;
		Integer currentTime = 1;
		Integer currentFinger = 1;

		machine.set_entry_status1(machine.quiescent); 
		machine.set_userTokenPresence(machine.present); 
		machine.set_enclaveStatus1(machine.enclaveQuiescent);

		//FD.UserEntry.TISReadUserToken is defined as ReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);


		//FD.UserEntry.BioCheckRequired
		Assert.assertTrue("Guard evt_BioCheckRequired not satisfied.", machine.evt_BioCheckRequired.guard_BioCheckRequired(currentTime));
		machine.evt_BioCheckRequired.run_BioCheckRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.insertFinger);


		machine.set_FingerPresence(machine.present); 
		//FD.UserEntry.ReadFingerOK
		Assert.assertTrue("Guard evt_ReadFingerOK not satisfied.", machine.evt_ReadFingerOK.guard_ReadFingerOK());
		machine.evt_ReadFingerOK.run_ReadFingerOK();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.ValidateFingerOK
		Assert.assertTrue("Guard evt_ValidateFingerOK not satisfied.", machine.evt_ValidateFingerOK.guard_ValidateFingerOK(currentFinger));
		machine.evt_ValidateFingerOK.run_ValidateFingerOK(currentFinger);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.WriteUserTokenOK 
		Assert.assertTrue("Guard evt_WriteUserTokenOK not satisfied.", machine.evt_WriteUserTokenOK.guard_WriteUserTokenOK());
		machine.evt_WriteUserTokenOK.run_WriteUserTokenOK();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.EntryOK 
		Assert.assertTrue("Guard evt_EntryOK not satisfied.", machine.evt_EntryOK_1.guard_EntryOK_1(currentTime)
				|| machine.evt_EntryOK_2.guard_EntryOK_2(currentTime));
		machine.evt_EntryOK_1.run_EntryOK_1(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.openDoor);

		//FD.AuditLog.LogChange

	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_UpdateConfig2(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		Integer cert4 = 4;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,cert4));

		//users
		Integer user1 = 1; //p01.dat: 4294967295
		Integer user2 = 2; //p01.dat: 0000032767
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert4,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1;
		Integer serial2 = 2;
		Integer serial3 = 3;
		Integer serial4 = 4;
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,serial4));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert4,serial4)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1),
				new Pair<Integer,Integer>(cert4,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '59', 'Month': '12', 'Day': '31', 'Hour': '23', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}



		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert4,pubKey4)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3,cert4));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert4,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert4));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User01fp = 1;
		machine.set_fingerprint(new BSet<Integer>(User01fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.securityOfficer)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.securityOfficer)
				));

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User01fp)
				));

		//tokenID
		Integer tok = 1;
		machine.set_tokenID(new BSet<Integer>(tok));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert4)
				));

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok),
				new Pair<Integer,Integer>(cert4,tok)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User01fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						1)
				));*/

		//entryPeriod
		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.securityOfficer, s)));

		Integer admin1 = 1;
		Integer tokAdmin = 1;

		//admin
		machine.set_admin(new BSet<Integer>(admin1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(admin1,tokAdmin)
				));

		machine.set_currentAdminToken(tokAdmin);

		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.NoRole)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.updateConfigData)
						));

		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.updateConfigData,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 2)
				));

		machine.set_entry_status2(machine.quiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);
		machine.set_adminTokenPresence(machine.present);

		Integer token_admin_to_read = 1;
		Integer currentTime = 1;
		Integer currentKeyedData = 1;
		Integer currentFloppy = 1;

		//FD.Enclave.TISAdminLogin => TISReadAdminToken defined as ReadAdminToken
		Assert.assertTrue("Guard evt_readAdminToken not satisfied.", 
				machine.evt_readAdminToken.guard_readAdminToken(token_admin_to_read));
		machine.evt_readAdminToken.run_readAdminToken(token_admin_to_read);
		Assert.assertEquals(machine.get_currentAdminToken(), token_admin_to_read);


		//FD.Enclave.GetPresentAdminToken 
		machine.set_keyedDataPresence(machine.present);
		//FD.Enclave.ValidateAdminTokenOK 
		Assert.assertTrue("Guard evt_ValidateAdminTokenOK not satisfied.", machine.evt_ValidateAdminTokenOK.guard_ValidateAdminTokenOK(currentTime));
		machine.evt_ValidateAdminTokenOK.run_ValidateAdminTokenOK(currentTime);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);

		//FD.Enclave.TISStartAdminOp => ValidateOpRequest -> ValidateOpRequestOK
		machine.set_keyedDataPresence(machine.present);
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.updateConfigData)
						));
		//FD.Enclave.ValidateOpRequestOK 
		Assert.assertTrue("Guard evt_ValidateOpRequestOK not satisfied.", 
				machine.evt_ValidateOpRequestOK.guard_ValidateOpRequestOK(currentKeyedData));
		machine.evt_ValidateOpRequestOK.run_ValidateOpRequestOK(currentKeyedData);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		machine.set_floppyPresence(machine.absent);

		//FD.Enclave.StartUpdateConfigDataWaitingFloppy
		Assert.assertTrue("Guard evt_StartUpdateConfigWaitingFloppy not satisfied.", machine.evt_StartUpdateConfigWaitingFloppy.guard_StartUpdateConfigWaitingFloppy());
		machine.evt_StartUpdateConfigWaitingFloppy.run_StartUpdateConfigWaitingFloppy();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.insertConfigData);

		machine.set_floppyPresence(machine.present);
		//FD.Enclave.StartUpdateConfigDataOK 
		Assert.assertTrue("Guard evt_StartUpdateConfigOK not satisfied.", machine.evt_StartUpdateConfigOK.guard_StartUpdateConfigOK());
		machine.evt_StartUpdateConfigOK.run_StartUpdateConfigOK();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);
		
		//FD.Enclave.FinishUpdateConfigDataFail
		Assert.assertTrue("Guard evt_FinishUpdateConfigDataFail not satisfied.", machine.evt_FinishUpdateConfigDataFail.guard_FinishUpdateConfigDataFail(currentFloppy));
		machine.evt_FinishUpdateConfigDataFail.run_FinishUpdateConfigDataFail(currentFloppy);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.invalidData);

		//FD.AuditLog.LogChange
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_UpdateConfig3(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		Integer cert4 = 4;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,cert4));

		//users
		Integer user1 = 1; //p01.dat: 4294967295
		Integer user2 = 2; //p01.dat: 0000032767
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert4,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1;
		Integer serial2 = 2;
		Integer serial3 = 3;
		Integer serial4 = 4;
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,serial4));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert4,serial4)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1),
				new Pair<Integer,Integer>(cert4,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '59', 'Month': '12', 'Day': '31', 'Hour': '23', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}



		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert4,pubKey4)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3,cert4));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert4,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert4));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User01fp = 1;
		machine.set_fingerprint(new BSet<Integer>(User01fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.securityOfficer)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.securityOfficer)
				));

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User01fp)
				));

		//tokenID
		Integer tok = 1;
		machine.set_tokenID(new BSet<Integer>(tok));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert4)
				));

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok),
				new Pair<Integer,Integer>(cert4,tok)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User01fp,machine.goodF)
				));
		
		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						1)
				));*/

		//entryPeriod
		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.securityOfficer, s)));

		Integer admin1 = 1;
		Integer tokAdmin = 1;

		//admin
		machine.set_admin(new BSet<Integer>(admin1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(admin1,tokAdmin)
				));

		machine.set_currentAdminToken(tokAdmin);

		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.securityOfficer)
						));
		
		machine.set_availableOps(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1, machine.updateConfigData)
						)
				);

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.NoOp)
						));

		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.updateConfigData,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));

		Integer currentKeyedData = 1;
		Integer floppy_to_be_read = 1;

		machine.set_enclaveStatus2(machine.enclaveQuiescent);
		machine.set_adminTokenPresence(machine.present); 
		machine.set_entry_status2(machine.quiescent);
		machine.set_keyedDataPresence(machine.present); 
		machine.set_floppyPresence(machine.absent);

		//FD.Enclave.TISStartAdminOp is defined as ValidateOpRequest => ValidateOpRequestOK 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.updateConfigData)
						));
		//FD.Enclave.ValidateOpRequestOK 
		Assert.assertTrue("Guard evt_ValidateOpRequestOK not satisfied.", 
				machine.evt_ValidateOpRequestOK.guard_ValidateOpRequestOK(currentKeyedData));
		machine.evt_ValidateOpRequestOK.run_ValidateOpRequestOK(currentKeyedData);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		//FD.Enclave.StartUpdateConfigDataWaitingFloppy 
		Assert.assertTrue("Guard evt_StartUpdateConfigWaitingFloppy not satisfied.", machine.evt_StartUpdateConfigWaitingFloppy.guard_StartUpdateConfigWaitingFloppy());
		machine.evt_StartUpdateConfigWaitingFloppy.run_StartUpdateConfigWaitingFloppy();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.insertConfigData);

		machine.set_floppyPresence(machine.present);
		//FD.Enclave.StartUpdateConfigDataOK 
		Assert.assertTrue("Guard evt_StartUpdateConfigOK not satisfied.", machine.evt_StartUpdateConfigOK.guard_StartUpdateConfigOK());
		machine.evt_StartUpdateConfigOK.run_StartUpdateConfigOK();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		//FD.Enclave.FinishUpdateConfigDataOK 
		Assert.assertTrue("Guard evt_FinishUpdateConfigDataOK not satisfied.", machine.evt_FinishUpdateConfigDataOK.guard_FinishUpdateConfigDataOK(floppy_to_be_read));
		machine.evt_FinishUpdateConfigDataOK.run_FinishUpdateConfigDataOK(floppy_to_be_read);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);
		
		//FD.Enclave.TISAdminLogout 
		machine.set_adminTokenPresence(machine.absent); 
		//FD.Enclave.AdminLogout  => TokenRemovedAdminLogout
		Assert.assertTrue("Guard TokenRemovedAdminLogout not satisfied.", machine.evt_TokenRemovedAdminLogout.guard_TokenRemovedAdminLogout());
		machine.evt_TokenRemovedAdminLogout.run_TokenRemovedAdminLogout();
		Assert.assertEquals(machine.get_currentAdminOp().apply(
				machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())), ref6_admin.NoOp);
		
		//FD.AuditLog.LogChange

	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_AdminLogout1(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1;
		Integer serial2 = 2;
		Integer serial3 = 3;
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,35),
				new Pair<Integer,Integer>(cert2,30),
				new Pair<Integer,Integer>(cert3,30)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '59', 'Month': '12', 'Day': '31', 'Hour': '23', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}



		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User07fp = 1;
		machine.set_fingerprint(new BSet<Integer>(User07fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.guard)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User07fp)
				));

		//tokenID
		Integer tok = 1;
		machine.set_tokenID(new BSet<Integer>(tok));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);
		
		machine.set_tokenAuthCert(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,1)
						)
				);
		
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1, machine.securityOfficer)
				));

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User07fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						1)
				));*/
		
		//entryPeriod
		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(30)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.guard, s)));

		Integer admin1 = 1;
		Integer tokAdmin = 1;

		//admin
		machine.set_admin(new BSet<Integer>(admin1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(admin1,tokAdmin)
				));

		machine.set_currentAdminToken(tokAdmin);
		
		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.NoRole)
						));
		
		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.NoOp)
						));

		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.updateConfigData,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalDuration(15);

		machine.set_tokenRemovalTimeout(0);
		machine.set_latchTimeout1(30);
		machine.set_latchTimeout2(30);

		Integer token_user_to_read = 1;
		Integer token_admin_to_read = 1;
		Integer currentTime = 30;
		Integer currentFinger = 1;

		machine.set_entry_status1(machine.quiescent);
		machine.set_entry_status2(machine.quiescent);
		machine.set_userTokenPresence(machine.present); 
		machine.set_enclaveStatus1(machine.enclaveQuiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);

		//FD.UserEntry.TISReadUserToken is defined as ReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.BioCheckRequired 
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", 
				machine.evt_BioCheckRequired.guard_BioCheckRequired(currentTime));
		machine.evt_BioCheckRequired.run_BioCheckRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.insertFinger);

		machine.set_FingerPresence(machine.present); 
		//FD.UserEntry.ReadFingerOK
		Assert.assertTrue("Guard evt_ReadFingerOK not satisfied.", machine.evt_ReadFingerOK.guard_ReadFingerOK());
		machine.evt_ReadFingerOK.run_ReadFingerOK();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);
		
		//FD.UserEntry.ValidateFingerOK 
		Assert.assertTrue("Guard evt_ValidateFingerOK not satisfied.", machine.evt_ValidateFingerOK.guard_ValidateFingerOK(currentFinger));
		machine.evt_ValidateFingerOK.run_ValidateFingerOK(currentFinger);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);
		
		//FD.UserEntry.WriteUserTokenOK
		Assert.assertTrue("Guard evt_WriteUserTokenOK not satisfied.", machine.evt_WriteUserTokenOK.guard_WriteUserTokenOK());
		machine.evt_WriteUserTokenOK.run_WriteUserTokenOK();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.EntryOK 
		Assert.assertTrue("Guard evt_EntryOK not satisfied.", machine.evt_EntryOK_1.guard_EntryOK_1(currentTime)
				|| machine.evt_EntryOK_2.guard_EntryOK_2(currentTime));
		machine.evt_EntryOK_1.run_EntryOK_1(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.openDoor);

		machine.set_userTokenPresence(machine.absent);
		//FD.UserEntry.UnlockDoorOK 
		Assert.assertTrue("Guard evt_unlockDoorOK not satisfied.", 
				machine.evt_unlockDoorOK.guard_unlockDoorOK(currentTime));
		machine.evt_unlockDoorOK.run_unlockDoorOK(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.doorUnlocked);

		machine.set_adminTokenPresence(machine.present);
		//FD.Enclave.readAdminToken
		Assert.assertTrue("Guard evt_readAdminToken not satisfied.", 
				machine.evt_readAdminToken.guard_readAdminToken(token_admin_to_read));
		machine.evt_readAdminToken.run_readAdminToken(token_admin_to_read);
		Assert.assertEquals(machine.get_currentAdminToken(), token_admin_to_read);
		
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,30),
				new Pair<Integer,Integer>(cert2,30),
				new Pair<Integer,Integer>(cert3,30)
				)); 
		//FD.Enclave.GetPresentAdminToken 
		machine.set_keyedDataPresence(machine.present);
		//FD.Enclave.ValidateAdminTokenOK
		Assert.assertTrue("Guard evt_ValidateAdminTokenOK not satisfied.", 
				machine.evt_ValidateAdminTokenOK.guard_ValidateAdminTokenOK(currentTime));
		machine.evt_ValidateAdminTokenOK.run_ValidateAdminTokenOK(currentTime);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);

		//wait for 5 mins
		
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(admin1,machine.guard)
						));
		currentTime = 5;
		//FD.Enclave.AdminTokenTimeout
		Assert.assertTrue("Guard evt_AdminTokenTimeout not satisfied.", 
				machine.evt_AdminTokenTimeout_1.guard_AdminTokenTimeout_1()
				||machine.evt_AdminTokenTimeout_2.guard_AdminTokenTimeout_2()
				||machine.evt_AdminTokenTimeout_3.guard_AdminTokenTimeout_3(currentTime)
				||machine.evt_AdminTokenTimeout_4.guard_AdminTokenTimeout_4()
				);
		machine.evt_AdminTokenTimeout_1.run_AdminTokenTimeout_1();
		
		Assert.assertEquals(machine.get_enclaveStatus2(), 
				ref6_admin.waitingRemoveAdminTokenFail);

		machine.set_adminTokenPresence(machine.absent);
		//FD.Enclave.TISCompleteTimeoutAdminLogout => FailedAdminTokenRemoved
		Assert.assertTrue("Guard evt_FailedAdminTokenRemoved not satisfied.", 
				machine.evt_FailedAdminTokenRemoved.guard_FailedAdminTokenRemoved());
		machine.evt_FailedAdminTokenRemoved.run_FailedAdminTokenRemoved();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.welcomeAdmin);

		//FD.AuditLog.LogChange

	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_AdminLogin3(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		Integer cert4 = 4;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,cert4));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert4,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		Integer serial4 = 4; 
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,serial4));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert4,serial4)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1),
				new Pair<Integer,Integer>(cert4,10)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert4,pubKey4)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3,cert4));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert4,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert4));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User05fp = 1; //p01.dat: User05fp
		Integer User02fp = 2;
		machine.set_fingerprint(new BSet<Integer>(User05fp, User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.userOnly)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.userOnly)
				));

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.secret)
				));

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User05fp)
				));

		//tokenID
		Integer tok = 1;
		Integer tok_adm = 2;
		machine.set_tokenID(new BSet<Integer>(tok, tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert4)
				));

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok),
				new Pair<Integer,Integer>(cert4,tok)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT),
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User05fp,machine.goodF),
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
												)
										)
						)
				);

		//admin
		machine.set_admin(new BSet<Integer>(1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1,1)
				));

		machine.set_currentAdminToken(1);

		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoRole)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoOp)
						));

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);

		machine.set_currentAdminToken(-1);
		Integer token_admin_to_read = 1;
		machine.set_entry_status2(machine.quiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);
		machine.set_adminTokenPresence(machine.present);
		Integer currentTime = 1;

		//FD.Enclave.GetPresentAdminToken => readAdminToken
		Assert.assertTrue("Guard evt_readAdminToken not satisfied.", machine.evt_readAdminToken.guard_readAdminToken(token_admin_to_read));
		machine.evt_readAdminToken.run_readAdminToken(token_admin_to_read);
		Assert.assertEquals(machine.get_currentAdminToken(), token_admin_to_read);
		
		//FD.Enclave.ValidateAdminTokenFailed 
		Assert.assertTrue("Guard evt_ValidateAdminTokenFail not satisfied.", 
				machine.evt_ValidateAdminTokenFail_1.guard_ValidateAdminTokenFail_1()
				|| machine.evt_ValidateAdminTokenFail_2.guard_ValidateAdminTokenFail_2()
				|| machine.evt_ValidateAdminTokenFail_3.guard_ValidateAdminTokenFail_3(currentTime)
				|| machine.evt_ValidateAdminTokenFail_4.guard_ValidateAdminTokenFail_4()
				);
		
		machine.evt_ValidateAdminTokenFail_1.run_ValidateAdminTokenFail_1();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.removeAdminToken);

		machine.set_adminTokenPresence(machine.absent);
		//FD.Enclave.AdminLogout => FailedAdminTokenRemoved
		Assert.assertTrue("Guard evt_FailedAdminTokenRemoved not satisfied.", 
				machine.evt_FailedAdminTokenRemoved.guard_FailedAdminTokenRemoved());
		machine.evt_FailedAdminTokenRemoved.run_FailedAdminTokenRemoved();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.welcomeAdmin);
		
		//FD.AuditLog.LogChange
	}
	
	@SuppressWarnings({ "static-access", "unchecked" })
	@Test
	public void test_UpdateConfig4(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		Integer cert4 = 4;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,cert4));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert4,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		Integer serial4 = 4; 
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,serial4));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert4,serial4)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1),
				new Pair<Integer,Integer>(cert4,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert4,pubKey4)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3,cert4));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert4,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert4));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User05fp = 1; //p01.dat: User05fp
		Integer User02fp = 2;
		machine.set_fingerprint(new BSet<Integer>(User05fp, User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.securityOfficer)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.securityOfficer)
				));

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.secret)
				));

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User05fp)
				));

		//tokenID
		Integer tok = 1;
		Integer tok_adm = 2;
		machine.set_tokenID(new BSet<Integer>(tok, tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert4)
				));

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok),
				new Pair<Integer,Integer>(cert4,tok)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT),
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User05fp,machine.goodF),
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
												)
										)
						)
				);

		//admin
		machine.set_admin(new BSet<Integer>(1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1,1)
				));

		machine.set_currentAdminToken(1);
		
		machine.set_availableOps(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1, machine.updateConfigData)
						)
				);

		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoRole)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoOp)
						));
		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.updateConfigData,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);

		machine.set_currentAdminToken(1);
		Integer currentKeyedData = 1;
		Integer token_admin_toread = 1;
		Integer currentTime = 1;
		machine.set_enclaveStatus2(machine.enclaveQuiescent);
		machine.set_entry_status2(machine.quiescent); 

		machine.set_adminTokenPresence(machine.present);
		//readAdminToken
		Assert.assertTrue("Guard evt_readAdminToken not satisfied.", machine.evt_readAdminToken.guard_readAdminToken(token_admin_toread));
		machine.evt_readAdminToken.run_readAdminToken(token_admin_toread);
		Assert.assertEquals(machine.get_currentAdminToken(), token_admin_toread);

		//ValidateAdminTokenOK
		Assert.assertTrue("Guard evt_ValidateAdminTokenOK not satisfied.", machine.evt_ValidateAdminTokenOK.guard_ValidateAdminTokenOK(currentTime));
		machine.evt_ValidateAdminTokenOK.run_ValidateAdminTokenOK(currentTime);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);

		//FD.Enclave.TISStartAdminOp => ValidateOpRequest -> ValidateOpRequestOK
		machine.set_keyedDataPresence(machine.present);
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.securityOfficer)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.updateConfigData)
						));

		//FD.Enclave.ValidateOpRequestOK
		Assert.assertTrue("Guard evt_ValidateOpRequestOK not satisfied.", 
				machine.evt_ValidateOpRequestOK.guard_ValidateOpRequestOK(currentKeyedData));
		machine.evt_ValidateOpRequestOK.run_ValidateOpRequestOK(currentKeyedData);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		machine.set_floppyPresence(machine.absent);
		//FD.Enclave.StartUpdateConfigDataWaitingFloppy 
		Assert.assertTrue("Guard evt_StartUpdateConfigWaitingFloppy not satisfied.", machine.evt_StartUpdateConfigWaitingFloppy.guard_StartUpdateConfigWaitingFloppy());
		machine.evt_StartUpdateConfigWaitingFloppy.run_StartUpdateConfigWaitingFloppy();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.insertConfigData);
		
		machine.set_adminTokenPresence(machine.absent); 
		//FD.Enclave.BadAdminLogout 
		Assert.assertTrue("Guard evt_BadAdminLogout not satisfied.", machine.evt_BadAdminLogout.guard_BadAdminLogout());
		machine.evt_BadAdminLogout.run_BadAdminLogout();
		Assert.assertEquals(machine.get_currentAdminOp().apply(
				machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())), ref6_admin.NoOp);


		//FD.Enclave.TISAdminLogout 
		//FD.AuditLog.LogChange
	}
	
	@SuppressWarnings({ "static-access", "unchecked" })
	@Test
	public void test_UpdateConfig5(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		Integer cert4 = 4;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,cert4));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert4,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		Integer serial4 = 4; 
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,serial4));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert4,serial4)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1),
				new Pair<Integer,Integer>(cert4,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert4,pubKey4)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3,cert4));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert4,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert4));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User05fp = 1; //p01.dat: User05fp
		Integer User02fp = 2;
		machine.set_fingerprint(new BSet<Integer>(User05fp, User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.securityOfficer)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.securityOfficer)
				));

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.secret)
				));

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User05fp)
				));

		//tokenID
		Integer tok = 1;
		Integer tok_adm = 2;
		machine.set_tokenID(new BSet<Integer>(tok, tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert4)
				));

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok),
				new Pair<Integer,Integer>(cert4,tok)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT),
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User05fp,machine.goodF),
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
												)
										)
						)
				);

		//admin
		machine.set_admin(new BSet<Integer>(1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1,1)
				));


		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoRole)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.updateConfigData)
						));

		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.updateConfigData,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);

		machine.set_currentAdminToken(-1);

		Integer currentKeyedData = 1;
		Integer token_admin_toread = 1;
		Integer currentTime = 1;
		Integer currentFloppy = 1;
		machine.set_enclaveStatus2(machine.enclaveQuiescent);
		machine.set_entry_status2(machine.quiescent); 

		machine.set_adminTokenPresence(machine.present);
		//readAdminToken
		Assert.assertTrue("Guard evt_readAdminToken not satisfied.", machine.evt_readAdminToken.guard_readAdminToken(token_admin_toread));
		machine.evt_readAdminToken.run_readAdminToken(token_admin_toread);
		Assert.assertEquals(machine.get_currentAdminToken(), token_admin_toread);

		//ValidateAdminTokenOK
		Assert.assertTrue("Guard evt_ValidateAdminTokenOK not satisfied.", machine.evt_ValidateAdminTokenOK.guard_ValidateAdminTokenOK(currentTime));
		machine.evt_ValidateAdminTokenOK.run_ValidateAdminTokenOK(currentTime);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);

		//FD.Enclave.TISStartAdminOp => ValidateOpRequest -> ValidateOpRequestOK
		machine.set_keyedDataPresence(machine.present);
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.guard)
						));

		//FD.Enclave.ValidateOpRequestOK
		Assert.assertTrue("Guard evt_ValidateOpRequestOK not satisfied.", 
				machine.evt_ValidateOpRequestOK.guard_ValidateOpRequestOK(currentKeyedData));
		machine.evt_ValidateOpRequestOK.run_ValidateOpRequestOK(currentKeyedData);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		machine.set_floppyPresence(machine.present);
		//FD.Enclave.StartUpdateConfigDataOK 
		Assert.assertTrue("Guard evt_StartUpdateConfigOK not satisfied.", machine.evt_StartUpdateConfigOK.guard_StartUpdateConfigOK());
		machine.evt_StartUpdateConfigOK.run_StartUpdateConfigOK();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		//FD.Enclave.FinishUpdateConfigDataOK
		Assert.assertTrue("Guard evt_FinishUpdateConfigDataOK not satisfied.", machine.evt_FinishUpdateConfigDataOK.guard_FinishUpdateConfigDataOK(currentFloppy));
		machine.evt_FinishUpdateConfigDataOK.run_FinishUpdateConfigDataOK(currentFloppy);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);

		//FD.AuditLog.LogChange

	}
	
	@SuppressWarnings({ "static-access", "unchecked" })
	@Test
	public void test_Shutdown1(){
		//certificates for the user
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		Integer cert4 = 4;
		Integer cert1_adm = 5;
		Integer cert2_adm = 6;
		Integer cert3_adm = 7;
		Integer cert4_adm = 8;
		//certificates for the admin
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,cert4,
				cert1_adm,cert2_adm,cert3_adm,cert4_adm));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		Integer user3 = 3;
		Integer user4 = 4;
		machine.set_user(new BSet<Integer>(user1,user2,user3,user4));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2),
				new BSet<Integer>(user3),
				new BSet<Integer>(user4)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert4,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert1_adm,new BSet<Integer>(user3)),
				new Pair<Integer,BSet<Integer>>(cert2_adm,new BSet<Integer>(user3)),
				new Pair<Integer,BSet<Integer>>(cert3_adm,new BSet<Integer>(user4)),
				new Pair<Integer,BSet<Integer>>(cert4_adm,new BSet<Integer>(user4))
				));*/

		//serials
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		Integer serial4 = 4; 
		Integer serial5 = 5; 
		Integer serial6 = 6; 
		Integer serial7 = 7; 
		Integer serial8 = 8; 
		/*machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,serial4,
				serial5,serial6,serial7,serial8));*/

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert4,serial4),
				new Pair<Integer,Integer>(cert1_adm,serial5),
				new Pair<Integer,Integer>(cert2_adm,serial6),
				new Pair<Integer,Integer>(cert3_adm,serial7),
				new Pair<Integer,Integer>(cert4_adm,serial8)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,30),
				new Pair<Integer,Integer>(cert2,30),
				new Pair<Integer,Integer>(cert3,30),
				new Pair<Integer,Integer>(cert4,30),
				new Pair<Integer,Integer>(cert1_adm,30),
				new Pair<Integer,Integer>(cert2_adm,30),
				new Pair<Integer,Integer>(cert3_adm,30),
				new Pair<Integer,Integer>(cert4_adm,30)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		Integer pubKey5 = 5;
		Integer pubKey6 = 6;
		Integer pubKey7 = 7;
		Integer pubKey8 = 8;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4, 
				pubKey5, pubKey6, pubKey7, pubKey8));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert4,pubKey4),
				new Pair<Integer,Integer>(cert1_adm,pubKey5),
				new Pair<Integer,Integer>(cert2_adm,pubKey6),
				new Pair<Integer,Integer>(cert3_adm,pubKey7),
				new Pair<Integer,Integer>(cert4_adm,pubKey8)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1, cert1_adm));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3,cert4,cert2_adm,cert3_adm,cert4_adm));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1),
				new Pair<Integer,Integer>(cert1_adm,user3)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert4,cert1),
				new Pair<Integer,Integer>(cert2_adm,cert1_adm),
				new Pair<Integer,Integer>(cert3_adm,cert1_adm),
				new Pair<Integer,Integer>(cert4_adm,cert1_adm)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3,cert3_adm));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2,cert2_adm));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert4,cert4_adm));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User05fp = 1;
		Integer User02fp = 2;
		machine.set_fingerprint(new BSet<Integer>(User05fp, User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.auditManager),
				new Pair<Integer,Integer>(cert3_adm,machine.securityOfficer)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret),
				new Pair<Integer,Integer>(cert3_adm,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.auditManager),
				new Pair<Integer,Integer>(cert4_adm,machine.securityOfficer)
				));

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.secret),
				new Pair<Integer,Integer>(cert4_adm,machine.secret)
				));

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User05fp),
				new Pair<Integer,Integer>(cert2_adm,User02fp)
				));

		//tokenID
		Integer tok = 1;
		Integer tok_adm = 2;
		machine.set_tokenID(new BSet<Integer>(tok, tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1),
				new Pair<Integer,Integer>(tok_adm,cert1_adm)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3),
				new Pair<Integer,Integer>(tok_adm,cert3_adm)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2),
				new Pair<Integer,Integer>(tok_adm,cert2_adm)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert4),
				new Pair<Integer,Integer>(tok_adm,cert4_adm)
				));

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				
				new Pair<Integer,Integer>(cert3,tok),
				//new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert4,tok),
				new Pair<Integer,Integer>(cert2_adm,tok_adm),
				new Pair<Integer,Integer>(cert3_adm,tok_adm),
				new Pair<Integer,Integer>(cert4_adm,tok_adm)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user3),pubKey4),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user4),pubKey4)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT),
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User05fp,machine.goodF),
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(30))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(30))
												)
										),
										new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
												machine.auditManager, new BRelation<Integer,BSet<Integer>>(
														new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(30))
														)
												)
						)
				);

		//admin
		machine.set_admin(new BSet<Integer>(1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1,2)
				));


		//rolePresent
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoRole)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.shutdownOp)
						));

		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.shutdownOp,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));
		
		machine.set_availableOps(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1, machine.updateConfigData)
						)
				);

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(30);
		machine.set_latchTimeout2(30);

		machine.set_currentAdminToken(2);
		Integer token_user_to_read = 1;
		Integer currentTime = 30;
		Integer currentKeyedData = 1;
		Integer token_admin_to_read = 2;

		machine.set_entry_status1(machine.quiescent);
		machine.set_entry_status2(machine.quiescent);
		machine.set_userTokenPresence(machine.present); 
		machine.set_enclaveStatus1(machine.enclaveQuiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);

		//FD.UserEntry.TISReadUserToken is defined as ReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", 
				machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.BioCheckNotRequired
		Assert.assertTrue("Guard evt_BioCheckNotRequired not satisfied.", 
				machine.evt_BioCheckNotRequired.guard_BioCheckNotRequired(currentTime));
		machine.evt_BioCheckNotRequired.run_BioCheckNotRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.EntryOK 
		Assert.assertTrue("Guard evt_EntryOK not satisfied.", machine.evt_EntryOK_1.guard_EntryOK_1(currentTime)
				|| machine.evt_EntryOK_2.guard_EntryOK_2(currentTime));
		machine.evt_EntryOK_1.run_EntryOK_1(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.openDoor);

		machine.set_userTokenPresence(machine.absent);
		//FD.UserEntry.unlockDoorOK 
		Assert.assertTrue("Guard evt_unlockDoorOK not satisfied.", 
				machine.evt_unlockDoorOK.guard_unlockDoorOK(currentTime));
		machine.evt_unlockDoorOK.run_unlockDoorOK(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.doorUnlocked);

		machine.set_adminTokenPresence(machine.present);
		//readAdminToken
		Assert.assertTrue("Guard evt_readAdminToken not satisfied.", 
				machine.evt_readAdminToken.guard_readAdminToken(token_admin_to_read));
		machine.evt_readAdminToken.run_readAdminToken(token_admin_to_read);
		Assert.assertEquals(machine.get_currentAdminToken(), token_admin_to_read);

		//ValidateAdminTokenOK
		Assert.assertTrue("Guard evt_ValidateAdminTokenOK not satisfied.", machine.evt_ValidateAdminTokenOK.guard_ValidateAdminTokenOK(currentTime));
		machine.evt_ValidateAdminTokenOK.run_ValidateAdminTokenOK(currentTime);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);

		machine.set_keyedDataPresence(machine.present);
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.securityOfficer)
						));
		//FD.Enclave.ValidateOpRequestOK 
		Assert.assertTrue("Guard evt_ValidateOpRequestOK not satisfied.", machine.evt_ValidateOpRequestOK.guard_ValidateOpRequestOK(currentKeyedData));
		machine.evt_ValidateOpRequestOK.run_ValidateOpRequestOK(currentKeyedData);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		machine.set_currentDoor(machine.open);
		//FD.Enclave.ShutdownWaitingDoor 
		Assert.assertTrue("Guard evt_ShutdownWaitingDoor not satisfied.", machine.evt_ShutdownWaitingDoor.guard_ShutdownWaitingDoor());
		machine.evt_ShutdownWaitingDoor.run_ShutdownWaitingDoor();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.closeDoor);

		machine.set_currentDoor(machine.closed);
		//FD.Enclave.TIS.ShutDownOp => ShutdownOK 
		//FD.Enclave.ShutdownOK 
		Assert.assertTrue("Guard evt_ShutdownOK not satisfied.", machine.evt_ShutdownOK.guard_ShutdownOK());
		machine.evt_ShutdownOK.run_ShutdownOK();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.clear);
		Assert.assertEquals(machine.get_displayMessage3(), ref6_admin.blank);

		//FD.AuditLog.LogChange

	}
	
	@SuppressWarnings({ "static-access", "unchecked" })
	@Test
	public void test_ArchiveLog1(){
		//certificates for the user
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		Integer cert4 = 4;
		Integer cert1_adm = 5;
		Integer cert2_adm = 6;
		Integer cert3_adm = 7;
		Integer cert4_adm = 8;
		//certificates for the admin
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,cert4,
				cert1_adm,cert2_adm,cert3_adm,cert4_adm));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		Integer user3 = 3;
		Integer user4 = 4;
		machine.set_user(new BSet<Integer>(user1,user2,user3,user4));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2),
				new BSet<Integer>(user3),
				new BSet<Integer>(user4)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert4,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert1_adm,new BSet<Integer>(user3)),
				new Pair<Integer,BSet<Integer>>(cert2_adm,new BSet<Integer>(user3)),
				new Pair<Integer,BSet<Integer>>(cert3_adm,new BSet<Integer>(user4)),
				new Pair<Integer,BSet<Integer>>(cert4_adm,new BSet<Integer>(user4))
				));*/

		//serials
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		Integer serial4 = 4; 
		Integer serial5 = 5; 
		Integer serial6 = 6; 
		Integer serial7 = 7; 
		Integer serial8 = 8; 
		/*machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,serial4,
				serial5,serial6,serial7,serial8));*/

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert4,serial4),
				new Pair<Integer,Integer>(cert1_adm,serial5),
				new Pair<Integer,Integer>(cert2_adm,serial6),
				new Pair<Integer,Integer>(cert3_adm,serial7),
				new Pair<Integer,Integer>(cert4_adm,serial8)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1),
				new Pair<Integer,Integer>(cert4,1),
				new Pair<Integer,Integer>(cert1_adm,1),
				new Pair<Integer,Integer>(cert2_adm,1),
				new Pair<Integer,Integer>(cert3_adm,1),
				new Pair<Integer,Integer>(cert4_adm,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		Integer pubKey5 = 5;
		Integer pubKey6 = 6;
		Integer pubKey7 = 7;
		Integer pubKey8 = 8;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4, 
				pubKey5, pubKey6, pubKey7, pubKey8));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert4,pubKey4),
				new Pair<Integer,Integer>(cert1_adm,pubKey5),
				new Pair<Integer,Integer>(cert2_adm,pubKey6),
				new Pair<Integer,Integer>(cert3_adm,pubKey7),
				new Pair<Integer,Integer>(cert4_adm,pubKey8)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1, cert1_adm));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3,cert4,cert2_adm,cert3_adm,cert4_adm));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1),
				new Pair<Integer,Integer>(cert1_adm,user3)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert4,cert1),
				new Pair<Integer,Integer>(cert2_adm,cert1_adm),
				new Pair<Integer,Integer>(cert3_adm,cert1_adm),
				new Pair<Integer,Integer>(cert4_adm,cert1_adm)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3,cert3_adm));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2,cert2_adm));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert4,cert4_adm));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User05fp = 1;
		Integer User02fp = 2;
		machine.set_fingerprint(new BSet<Integer>(User05fp, User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.auditManager),
				new Pair<Integer,Integer>(cert3_adm,machine.securityOfficer)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret),
				new Pair<Integer,Integer>(cert3_adm,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.auditManager),
				new Pair<Integer,Integer>(cert4_adm,machine.auditManager)
				));

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.secret),
				new Pair<Integer,Integer>(cert4_adm,machine.secret)
				));

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User05fp),
				new Pair<Integer,Integer>(cert2_adm,User02fp)
				));

		//tokenID
		Integer tok = 1;
		Integer tok_adm = 2;
		machine.set_tokenID(new BSet<Integer>(tok, tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1),
				new Pair<Integer,Integer>(tok_adm,cert1_adm)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3),
				new Pair<Integer,Integer>(tok_adm,cert3_adm)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2),
				new Pair<Integer,Integer>(tok_adm,cert2_adm)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert4),
				new Pair<Integer,Integer>(tok_adm,cert4_adm)
				));

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok),
				new Pair<Integer,Integer>(cert4,tok),
				new Pair<Integer,Integer>(cert2_adm,tok_adm),
				new Pair<Integer,Integer>(cert3_adm,tok_adm),
				new Pair<Integer,Integer>(cert4_adm,tok_adm)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user3),pubKey4),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user4),pubKey4)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT),
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User05fp,machine.goodF),
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));*/
		/*
		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
												)
										),
										new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
												machine.auditManager, new BRelation<Integer,BSet<Integer>>(
														new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
														)
												)
						)
				);

		//admin
		machine.set_admin(new BSet<Integer>(1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1,2)
				));


		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoRole)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoOp)
						));

		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.archiveLog,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);

		Integer token_admin_to_read = 2;
		Integer currentTime = 1;
		Integer currentKeyedData = 1;

		//FD.TIS.TISStartup is defined as StartEnrolledStation
		//StartEnrolledStation -> not modeled in EB
		machine.set_screenMsg1(machine.welcomeAdmin);
		machine.set_screenMsg2(machine.welcomeAdmin);
		machine.set_displayMessage1(machine.welcome);
		machine.set_displayMessage2(machine.welcome);
		machine.set_displayMessage3(machine.welcome);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);
		machine.set_entry_status1(machine.quiescent);
		machine.set_entry_status2(machine.quiescent);

		machine.set_adminTokenPresence(machine.present);
		//FD.Enclave.TISAdminLogin => TISReadAdminToken defined as ReadAdminToken
		Assert.assertTrue("Guard evt_readAdminToken not satisfied.", machine.evt_readAdminToken.guard_readAdminToken(token_admin_to_read));
		machine.evt_readAdminToken.run_readAdminToken(token_admin_to_read);
		Assert.assertEquals(machine.get_currentAdminToken(), token_admin_to_read);

		//FD.Enclave.GetPresentAdminToken 
		machine.set_keyedDataPresence(machine.present);
		//FD.Enclave.ValidateAdminTokenOK 
		Assert.assertTrue("Guard evt_ValidateAdminTokenOK not satisfied.", machine.evt_ValidateAdminTokenOK.guard_ValidateAdminTokenOK(currentTime));
		machine.evt_ValidateAdminTokenOK.run_ValidateAdminTokenOK(currentTime);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);

		//FD.Enclave.TISStartAdminOp => ValidateOpRequest -> ValidateOpRequestOK
		machine.set_keyedDataPresence(machine.present);
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.auditManager)
						));

		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.archiveLog)
						));
		//FD.Enclave.ValidateOpRequestOK 
		Assert.assertTrue("Guard evt_ValidateOpRequestOK not satisfied.", 
				machine.evt_ValidateOpRequestOK.guard_ValidateOpRequestOK(currentKeyedData));
		machine.evt_ValidateOpRequestOK.run_ValidateOpRequestOK(currentKeyedData);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		machine.set_floppyPresence(machine.absent);
		//FD.Enclave.StartArchiveLogWaitingFloppy 
		Assert.assertTrue("Guard evt_StartArchiveLogWaitingFloppy not satisfied.", machine.evt_StartArchiveLogWaitingFloppy.guard_StartArchiveLogWaitingFloppy());
		machine.evt_StartArchiveLogWaitingFloppy.run_StartArchiveLogWaitingFloppy();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.insertBlankFloppy);

		machine.set_floppyPresence(machine.present);
		//FD.Enclave.StartArchiveLogOK 
		Assert.assertTrue("Guard evt_StartArchiveLogOK not satisfied.", machine.evt_StartArchiveLogOK.guard_StartArchiveLogOK());
		machine.evt_StartArchiveLogOK.run_StartArchiveLogOK();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		//FD.Enclave.FinishArchiveLogNoFloppy
		//FD.Enclave.TISArchiveLogOp
		Assert.assertTrue("Guard evt_FinishArchiveLogFail not satisfied.", 
				machine.evt_FinishArchiveLogBadMatch.guard_FinishArchiveLogBadMatch()
			||
				machine.evt_FinishArchiveLogNoFloppy.guard_FinishArchiveLogNoFloppy()
				);
		if (machine.evt_FinishArchiveLogBadMatch.guard_FinishArchiveLogBadMatch()){
			machine.evt_FinishArchiveLogBadMatch.run_FinishArchiveLogBadMatch();
		}else{
			machine.evt_FinishArchiveLogNoFloppy.run_FinishArchiveLogNoFloppy();
		}
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.archiveFailed);

		//FD.AuditLog.LogChange

	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_ArchiveLog2(){
		/**
		 * This is the same test as test_ArchiveLog1:
		 *  FinishArchiveLogFail is defined as FinishArchiveLogBadMatch or FinishArchiveLogNoFloppy we
		 *  modelled both scenarios in the same event
		 */
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		Integer cert4 = 4;
		Integer cert1_adm = 5;
		Integer cert2_adm = 6;
		Integer cert3_adm = 7;
		Integer cert4_adm = 8;
		//certificates for the admin
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,cert4,
				cert1_adm,cert2_adm,cert3_adm,cert4_adm));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		Integer user3 = 3;
		Integer user4 = 4;
		machine.set_user(new BSet<Integer>(user1,user2,user3,user4));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2),
				new BSet<Integer>(user3),
				new BSet<Integer>(user4)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert4,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert1_adm,new BSet<Integer>(user3)),
				new Pair<Integer,BSet<Integer>>(cert2_adm,new BSet<Integer>(user3)),
				new Pair<Integer,BSet<Integer>>(cert3_adm,new BSet<Integer>(user4)),
				new Pair<Integer,BSet<Integer>>(cert4_adm,new BSet<Integer>(user4))
				));*/

		//serials
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		Integer serial4 = 4; 
		Integer serial5 = 5; 
		Integer serial6 = 6; 
		Integer serial7 = 7; 
		Integer serial8 = 8; 
		/*machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,serial4,
				serial5,serial6,serial7,serial8));*/

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert4,serial4),
				new Pair<Integer,Integer>(cert1_adm,serial5),
				new Pair<Integer,Integer>(cert2_adm,serial6),
				new Pair<Integer,Integer>(cert3_adm,serial7),
				new Pair<Integer,Integer>(cert4_adm,serial8)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1),
				new Pair<Integer,Integer>(cert4,1),
				new Pair<Integer,Integer>(cert1_adm,1),
				new Pair<Integer,Integer>(cert2_adm,1),
				new Pair<Integer,Integer>(cert3_adm,1),
				new Pair<Integer,Integer>(cert4_adm,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		Integer pubKey5 = 5;
		Integer pubKey6 = 6;
		Integer pubKey7 = 7;
		Integer pubKey8 = 8;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4, 
				pubKey5, pubKey6, pubKey7, pubKey8));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert4,pubKey4),
				new Pair<Integer,Integer>(cert1_adm,pubKey5),
				new Pair<Integer,Integer>(cert2_adm,pubKey6),
				new Pair<Integer,Integer>(cert3_adm,pubKey7),
				new Pair<Integer,Integer>(cert4_adm,pubKey8)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1, cert1_adm));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3,cert4,cert2_adm,cert3_adm,cert4_adm));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1),
				new Pair<Integer,Integer>(cert1_adm,user3)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert4,cert1),
				new Pair<Integer,Integer>(cert2_adm,cert1_adm),
				new Pair<Integer,Integer>(cert3_adm,cert1_adm),
				new Pair<Integer,Integer>(cert4_adm,cert1_adm)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3,cert3_adm));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2,cert2_adm));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert4,cert4_adm));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User05fp = 1;
		Integer User02fp = 2;
		machine.set_fingerprint(new BSet<Integer>(User05fp, User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.auditManager),
				new Pair<Integer,Integer>(cert3_adm,machine.securityOfficer)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret),
				new Pair<Integer,Integer>(cert3_adm,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.auditManager),
				new Pair<Integer,Integer>(cert4_adm,machine.auditManager)
				));

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.secret),
				new Pair<Integer,Integer>(cert4_adm,machine.secret)
				));

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User05fp),
				new Pair<Integer,Integer>(cert2_adm,User02fp)
				));

		//tokenID
		Integer tok = 1;
		Integer tok_adm = 2;
		machine.set_tokenID(new BSet<Integer>(tok, tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1),
				new Pair<Integer,Integer>(tok_adm,cert1_adm)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3),
				new Pair<Integer,Integer>(tok_adm,cert3_adm)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2),
				new Pair<Integer,Integer>(tok_adm,cert2_adm)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert4),
				new Pair<Integer,Integer>(tok_adm,cert4_adm)
				));

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok),
				new Pair<Integer,Integer>(cert4,tok),
				new Pair<Integer,Integer>(cert2_adm,tok_adm),
				new Pair<Integer,Integer>(cert3_adm,tok_adm),
				new Pair<Integer,Integer>(cert4_adm,tok_adm)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user3),pubKey4),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user4),pubKey4)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT),
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User05fp,machine.goodF),
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
												)
										),
										new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
												machine.auditManager, new BRelation<Integer,BSet<Integer>>(
														new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
														)
												)
						)
				);

		//admin
		machine.set_admin(new BSet<Integer>(1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1,2)
				));


		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoRole)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoOp)
						));

		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.archiveLog,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);

		Integer token_admin_to_read = 2;
		Integer currentTime = 1;
		Integer currentKeyedData = 1;

		//FD.TIS.TISStartup is defined as StartEnrolledStation
		//StartEnrolledStation -> not modeled in EB
		machine.set_screenMsg1(machine.welcomeAdmin);
		machine.set_screenMsg2(machine.welcomeAdmin);
		machine.set_displayMessage1(machine.welcome);
		machine.set_displayMessage2(machine.welcome);
		machine.set_displayMessage3(machine.welcome);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);
		machine.set_entry_status1(machine.quiescent);
		machine.set_entry_status2(machine.quiescent);

		machine.set_adminTokenPresence(machine.present);
		//FD.Enclave.TISAdminLogin => TISReadAdminToken defined as ReadAdminToken
		Assert.assertTrue("Guard evt_readAdminToken not satisfied.", machine.evt_readAdminToken.guard_readAdminToken(token_admin_to_read));
		machine.evt_readAdminToken.run_readAdminToken(token_admin_to_read);
		Assert.assertEquals(machine.get_currentAdminToken(), token_admin_to_read);

		//FD.Enclave.GetPresentAdminToken 
		machine.set_keyedDataPresence(machine.present);
		//FD.Enclave.ValidateAdminTokenOK 
		Assert.assertTrue("Guard evt_ValidateAdminTokenOK not satisfied.", machine.evt_ValidateAdminTokenOK.guard_ValidateAdminTokenOK(currentTime));
		machine.evt_ValidateAdminTokenOK.run_ValidateAdminTokenOK(currentTime);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);

		//FD.Enclave.TISStartAdminOp => ValidateOpRequest -> ValidateOpRequestOK
		machine.set_keyedDataPresence(machine.present);
		//rolePresent 
				machine.set_rolePresent(
						new BRelation<Integer,Integer>(
								new Pair<Integer,Integer>(1,machine.auditManager)
								));

				//currentAdminOp 
				machine.set_currentAdminOp(
						new BRelation<Integer,Integer>(
								new Pair<Integer,Integer>(1,machine.archiveLog)
								));
		//FD.Enclave.ValidateOpRequestOK 
		Assert.assertTrue("Guard evt_ValidateOpRequestOK not satisfied.", 
				machine.evt_ValidateOpRequestOK.guard_ValidateOpRequestOK(currentKeyedData));
		machine.evt_ValidateOpRequestOK.run_ValidateOpRequestOK(currentKeyedData);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		machine.set_floppyPresence(machine.absent);
		//FD.Enclave.StartArchiveLogWaitingFloppy 
		Assert.assertTrue("Guard evt_StartArchiveLogWaitingFloppy not satisfied.", machine.evt_StartArchiveLogWaitingFloppy.guard_StartArchiveLogWaitingFloppy());
		machine.evt_StartArchiveLogWaitingFloppy.run_StartArchiveLogWaitingFloppy();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.insertBlankFloppy);

		machine.set_floppyPresence(machine.present);
		//FD.Enclave.StartArchiveLogOK 
		Assert.assertTrue("Guard evt_StartArchiveLogOK not satisfied.", machine.evt_StartArchiveLogOK.guard_StartArchiveLogOK());
		machine.evt_StartArchiveLogOK.run_StartArchiveLogOK();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		//FD.Enclave.FinishArchiveLogNoFloppy
		//FD.Enclave.TISArchiveLogOp
		Assert.assertTrue("Guard evt_FinishArchiveLogFail not satisfied.", 
				machine.evt_FinishArchiveLogBadMatch.guard_FinishArchiveLogBadMatch()
			||
				machine.evt_FinishArchiveLogNoFloppy.guard_FinishArchiveLogNoFloppy()
				);
		if (machine.evt_FinishArchiveLogBadMatch.guard_FinishArchiveLogBadMatch()){
			machine.evt_FinishArchiveLogBadMatch.run_FinishArchiveLogBadMatch();
		}else{
			machine.evt_FinishArchiveLogNoFloppy.run_FinishArchiveLogNoFloppy();
		}
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.archiveFailed);

		//FD.AuditLog.LogChange
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_ArchiveLog3(){
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		Integer cert4 = 4;
		Integer cert1_adm = 5;
		Integer cert2_adm = 6;
		Integer cert3_adm = 7;
		Integer cert4_adm = 8;
		//certificates for the admin
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,cert4,
				cert1_adm,cert2_adm,cert3_adm,cert4_adm));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		Integer user3 = 3;
		Integer user4 = 4;
		machine.set_user(new BSet<Integer>(user1,user2,user3,user4));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2),
				new BSet<Integer>(user3),
				new BSet<Integer>(user4)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert4,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert1_adm,new BSet<Integer>(user3)),
				new Pair<Integer,BSet<Integer>>(cert2_adm,new BSet<Integer>(user3)),
				new Pair<Integer,BSet<Integer>>(cert3_adm,new BSet<Integer>(user4)),
				new Pair<Integer,BSet<Integer>>(cert4_adm,new BSet<Integer>(user4))
				));*/

		//serials
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		Integer serial4 = 4; 
		Integer serial5 = 5; 
		Integer serial6 = 6; 
		Integer serial7 = 7; 
		Integer serial8 = 8; 
		/*machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,serial4,
				serial5,serial6,serial7,serial8));*/

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert4,serial4),
				new Pair<Integer,Integer>(cert1_adm,serial5),
				new Pair<Integer,Integer>(cert2_adm,serial6),
				new Pair<Integer,Integer>(cert3_adm,serial7),
				new Pair<Integer,Integer>(cert4_adm,serial8)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1),
				new Pair<Integer,Integer>(cert4,1),
				new Pair<Integer,Integer>(cert1_adm,1),
				new Pair<Integer,Integer>(cert2_adm,1),
				new Pair<Integer,Integer>(cert3_adm,1),
				new Pair<Integer,Integer>(cert4_adm,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		Integer pubKey5 = 5;
		Integer pubKey6 = 6;
		Integer pubKey7 = 7;
		Integer pubKey8 = 8;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4, 
				pubKey5, pubKey6, pubKey7, pubKey8));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert4,pubKey4),
				new Pair<Integer,Integer>(cert1_adm,pubKey5),
				new Pair<Integer,Integer>(cert2_adm,pubKey6),
				new Pair<Integer,Integer>(cert3_adm,pubKey7),
				new Pair<Integer,Integer>(cert4_adm,pubKey8)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1, cert1_adm));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3,cert4,cert2_adm,cert3_adm,cert4_adm));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1),
				new Pair<Integer,Integer>(cert1_adm,user3)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert4,cert1),
				new Pair<Integer,Integer>(cert2_adm,cert1_adm),
				new Pair<Integer,Integer>(cert3_adm,cert1_adm),
				new Pair<Integer,Integer>(cert4_adm,cert1_adm)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3,cert3_adm));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2,cert2_adm));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert4,cert4_adm));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User05fp = 1;
		Integer User02fp = 2;
		machine.set_fingerprint(new BSet<Integer>(User05fp, User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.auditManager),
				new Pair<Integer,Integer>(cert3_adm,machine.securityOfficer)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret),
				new Pair<Integer,Integer>(cert3_adm,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.auditManager),
				new Pair<Integer,Integer>(cert4_adm,machine.auditManager)
				));

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4,machine.secret),
				new Pair<Integer,Integer>(cert4_adm,machine.secret)
				));

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User05fp),
				new Pair<Integer,Integer>(cert2_adm,User02fp)
				));

		//tokenID
		Integer tok = 1;
		Integer tok_adm = 2;
		machine.set_tokenID(new BSet<Integer>(tok, tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1),
				new Pair<Integer,Integer>(tok_adm,cert1_adm)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3),
				new Pair<Integer,Integer>(tok_adm,cert3_adm)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2),
				new Pair<Integer,Integer>(tok_adm,cert2_adm)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert4),
				new Pair<Integer,Integer>(tok_adm,cert4_adm)
				));

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok),
				new Pair<Integer,Integer>(cert4,tok),
				new Pair<Integer,Integer>(cert2_adm,tok_adm),
				new Pair<Integer,Integer>(cert3_adm,tok_adm),
				new Pair<Integer,Integer>(cert4_adm,tok_adm)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user3),pubKey4),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user4),pubKey4)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT),
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User05fp,machine.goodF),
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
												)
										),
										new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
												machine.auditManager, new BRelation<Integer,BSet<Integer>>(
														new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
														)
												)
						)
				);

		//admin
		machine.set_admin(new BSet<Integer>(1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1,2)
				));


		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoRole)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoOp)
						));

		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.archiveLog,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));
		
		machine.set_availableOps(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1, machine.updateConfigData)
						)
				);

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);

		Integer token_admin_to_read = 2;
		Integer currentTime = 1;
		Integer currentKeyedData = 1;

		//FD.TIS.TISStartup is defined as StartEnrolledStation
		//StartEnrolledStation -> not modeled in EB
		machine.set_screenMsg1(machine.welcomeAdmin);
		machine.set_screenMsg2(machine.welcomeAdmin);
		machine.set_displayMessage1(machine.welcome);
		machine.set_displayMessage2(machine.welcome);
		machine.set_displayMessage3(machine.welcome);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);
		machine.set_entry_status1(machine.quiescent);
		machine.set_entry_status2(machine.quiescent);

		machine.set_adminTokenPresence(machine.present);
		//FD.Enclave.TISAdminLogin => TISReadAdminToken defined as ReadAdminToken
		Assert.assertTrue("Guard evt_readAdminToken not satisfied.", machine.evt_readAdminToken.guard_readAdminToken(token_admin_to_read));
		machine.evt_readAdminToken.run_readAdminToken(token_admin_to_read);
		Assert.assertEquals(machine.get_currentAdminToken(), token_admin_to_read);

		//FD.Enclave.GetPresentAdminToken 
		machine.set_keyedDataPresence(machine.present);
		//FD.Enclave.ValidateAdminTokenOK 
		Assert.assertTrue("Guard evt_ValidateAdminTokenOK not satisfied.", machine.evt_ValidateAdminTokenOK.guard_ValidateAdminTokenOK(currentTime));
		machine.evt_ValidateAdminTokenOK.run_ValidateAdminTokenOK(currentTime);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);

		//FD.Enclave.TISStartAdminOp => ValidateOpRequest -> ValidateOpRequestOK
		machine.set_keyedDataPresence(machine.present);
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.auditManager)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.archiveLog)
						));
		//FD.Enclave.ValidateOpRequestOK 
		Assert.assertTrue("Guard evt_ValidateOpRequestOK not satisfied.", 
				machine.evt_ValidateOpRequestOK.guard_ValidateOpRequestOK(currentKeyedData));
		machine.evt_ValidateOpRequestOK.run_ValidateOpRequestOK(currentKeyedData);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		machine.set_floppyPresence(machine.absent);
		//FD.Enclave.StartArchiveLogWaitingFloppy 
		Assert.assertTrue("Guard evt_StartArchiveLogWaitingFloppy not satisfied.", machine.evt_StartArchiveLogWaitingFloppy.guard_StartArchiveLogWaitingFloppy());
		machine.evt_StartArchiveLogWaitingFloppy.run_StartArchiveLogWaitingFloppy();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.insertBlankFloppy);

		machine.set_floppyPresence(machine.present);
		//FD.Enclave.StartArchiveLogOK 
		Assert.assertTrue("Guard evt_StartArchiveLogOK not satisfied.", machine.evt_StartArchiveLogOK.guard_StartArchiveLogOK());
		machine.evt_StartArchiveLogOK.run_StartArchiveLogOK();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		//FD.Enclave.FinishArchiveLogOK
		Assert.assertTrue("Guard evt_FinishArchiveLogOK not satisfied.", machine.evt_FinishArchiveLogOK.guard_FinishArchiveLogOK());
		machine.evt_FinishArchiveLogOK.run_FinishArchiveLogOK();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);

		//FD.Enclave.TISAdminLogout 
		machine.set_adminTokenPresence(machine.absent); 
		//FD.Enclave.AdminLogout  => TokenRemovedAdminLogout
		Assert.assertTrue("Guard TokenRemovedAdminLogout not satisfied.", machine.evt_TokenRemovedAdminLogout.guard_TokenRemovedAdminLogout());
		machine.evt_TokenRemovedAdminLogout.run_TokenRemovedAdminLogout();
		Assert.assertEquals(machine.get_currentAdminOp().apply(
				machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())), ref6_admin.NoOp);

		//FD.AuditLog.LogChange
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_UserEntry13(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User07fp = 1; 
		machine.set_fingerprint(new BSet<Integer>(User07fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.userOnly)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User07fp)
				));

		//tokenID
		Integer tok = 1;
		machine.set_tokenID(new BSet<Integer>(tok));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok)
				));

		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);
		
		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User07fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(31))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(31))
												)
										),
										new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
												machine.auditManager, new BRelation<Integer,BSet<Integer>>(
														new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(31))
														)
												)
						)
				);

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(31);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);

		machine.set_entry_status1(machine.quiescent); 
		machine.set_userTokenPresence(machine.present); 
		machine.set_enclaveStatus1(machine.enclaveQuiescent);

		Integer token_user_to_read = 1;
		Integer currentTime = 31;
		Integer currentFinger = 1;

		//FD.UserEntry.TISReadUserToken is defined as ReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", 
				machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);
		
		//FD.UserEntry.BioCheckRequired 
		Assert.assertTrue("Guard evt_BioCheckRequired not satisfied.", machine.evt_BioCheckRequired.guard_BioCheckRequired(currentTime));
		machine.evt_BioCheckRequired.run_BioCheckRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.insertFinger);

		machine.set_FingerPresence(machine.present);
		//FD.UserEntry.ReadFingerOK 
		Assert.assertTrue("Guard evt_ReadFingerOK not satisfied.", machine.evt_ReadFingerOK.guard_ReadFingerOK());
		machine.evt_ReadFingerOK.run_ReadFingerOK();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.ValidateFingerOK 
		Assert.assertTrue("Guard evt_ValidateFingerOK not satisfied.", machine.evt_ValidateFingerOK.guard_ValidateFingerOK(currentFinger));
		machine.evt_ValidateFingerOK.run_ValidateFingerOK(currentFinger);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.WriteUserTokenFail
		Assert.assertTrue("Guard evt_WriteUserTokenFail not satisfied.", machine.evt_WriteUserTokenFail.guard_WriteUserTokenFail());
		machine.evt_WriteUserTokenFail.run_WriteUserTokenFail();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.tokenUpdateFailed);

		//FD.UserEntry.EntryOK 
		Assert.assertTrue("Guard evt_EntryOK not satisfied.", machine.evt_EntryOK_1.guard_EntryOK_1(currentTime)
				|| machine.evt_EntryOK_2.guard_EntryOK_2(currentTime));
		machine.evt_EntryOK_1.run_EntryOK_1(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.openDoor);

		machine.set_userTokenPresence(machine.absent);
		//FD.UserEntry.UnlockDoorOK 
		Assert.assertTrue("Guard evt_unlockDoorOK not satisfied.", 
				machine.evt_unlockDoorOK.guard_unlockDoorOK(currentTime));
		machine.evt_unlockDoorOK.run_unlockDoorOK(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.doorUnlocked);

		//FD.AuditLog.LogChange
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_UserEntry14(){
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		Integer cert1_adm = 5;
		Integer cert2_adm = 6;
		Integer cert3_adm = 7;
		Integer cert4_adm = 8;
		//certificates for the admin
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,
				cert1_adm,cert2_adm,cert3_adm,cert4_adm));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		Integer user3 = 3;
		Integer user4 = 4;
		machine.set_user(new BSet<Integer>(user1,user2,user3,user4));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2),
				new BSet<Integer>(user3),
				new BSet<Integer>(user4)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert1_adm,new BSet<Integer>(user3)),
				new Pair<Integer,BSet<Integer>>(cert2_adm,new BSet<Integer>(user3)),
				new Pair<Integer,BSet<Integer>>(cert3_adm,new BSet<Integer>(user4)),
				new Pair<Integer,BSet<Integer>>(cert4_adm,new BSet<Integer>(user4))
				));*/

		//serials
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		Integer serial5 = 5; 
		Integer serial6 = 6; 
		Integer serial7 = 7; 
		Integer serial8 = 8; 
		/*machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,
				serial5,serial6,serial7,serial8));*/

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert1_adm,serial5),
				new Pair<Integer,Integer>(cert2_adm,serial6),
				new Pair<Integer,Integer>(cert3_adm,serial7),
				new Pair<Integer,Integer>(cert4_adm,serial8)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1),
				new Pair<Integer,Integer>(cert1_adm,1),
				new Pair<Integer,Integer>(cert2_adm,1),
				new Pair<Integer,Integer>(cert3_adm,1),
				new Pair<Integer,Integer>(cert4_adm,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		Integer pubKey5 = 5;
		Integer pubKey6 = 6;
		Integer pubKey7 = 7;
		Integer pubKey8 = 8;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4, 
				pubKey5, pubKey6, pubKey7, pubKey8));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert1_adm,pubKey5),
				new Pair<Integer,Integer>(cert2_adm,pubKey6),
				new Pair<Integer,Integer>(cert3_adm,pubKey7),
				new Pair<Integer,Integer>(cert4_adm,pubKey8)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1, cert1_adm));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3,cert2_adm,cert3_adm,cert4_adm));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1),
				new Pair<Integer,Integer>(cert1_adm,user3)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert2_adm,cert1_adm),
				new Pair<Integer,Integer>(cert3_adm,cert1_adm),
				new Pair<Integer,Integer>(cert4_adm,cert1_adm)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3,cert3_adm));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2,cert2_adm));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert4_adm));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User05fp = 1;
		Integer User02fp = 2;
		machine.set_fingerprint(new BSet<Integer>(User05fp, User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.auditManager),
				new Pair<Integer,Integer>(cert3_adm,machine.securityOfficer)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret),
				new Pair<Integer,Integer>(cert3_adm,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4_adm,machine.securityOfficer)
				));

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4_adm,machine.secret)
				));

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User05fp),
				new Pair<Integer,Integer>(cert2_adm,User02fp)
				));

		//tokenID
		Integer tok = 1;
		Integer tok_adm = 2;
		machine.set_tokenID(new BSet<Integer>(tok, tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1),
				new Pair<Integer,Integer>(tok_adm,cert1_adm)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3),
				new Pair<Integer,Integer>(tok_adm,cert3_adm)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2),
				new Pair<Integer,Integer>(tok_adm,cert2_adm)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert4_adm)
				));

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok),
				new Pair<Integer,Integer>(cert2_adm,tok_adm),
				new Pair<Integer,Integer>(cert3_adm,tok_adm),
				new Pair<Integer,Integer>(cert4_adm,tok_adm)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user3),pubKey8),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user4),pubKey8)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT),
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User05fp,machine.goodF),
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
												)
										),
										new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
												machine.auditManager, new BRelation<Integer,BSet<Integer>>(
														new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
														)
												)
						)
				);

		//admin
		machine.set_admin(new BSet<Integer>(1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1,2)
				));


		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoRole)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoOp)
						));

		machine.set_availableOps(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1, machine.updateConfigData)
						)
				);
		
		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.updateConfigData,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess));

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);
		
		machine.set_entry_status1(machine.quiescent);
		machine.set_entry_status2(machine.quiescent);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);
		machine.set_adminTokenPresence(machine.present);

		Integer token_admin_to_read = 2;
		Integer currentTime = 1;
		Integer currentKeyedData = 1;
		Integer currentFloppy = 1;
		Integer token_user_to_read = 1;
		Integer currentFinger = 1;

		machine.set_adminTokenPresence(machine.present);
		//FD.Enclave.TISAdminLogin => TISReadAdminToken defined as ReadAdminToken
		Assert.assertTrue("Guard evt_readAdminToken not satisfied.", machine.evt_readAdminToken.guard_readAdminToken(token_admin_to_read));
		machine.evt_readAdminToken.run_readAdminToken(token_admin_to_read);
		Assert.assertEquals(machine.get_currentAdminToken(), token_admin_to_read);

		//FD.Enclave.GetPresentAdminToken 
		machine.set_keyedDataPresence(machine.present);
		//FD.Enclave.ValidateAdminTokenOK 
		Assert.assertTrue("Guard evt_ValidateAdminTokenOK not satisfied.", machine.evt_ValidateAdminTokenOK.guard_ValidateAdminTokenOK(currentTime));
		machine.evt_ValidateAdminTokenOK.run_ValidateAdminTokenOK(currentTime);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);

		//FD.Enclave.TISStartAdminOp => ValidateOpRequest -> ValidateOpRequestOK
		machine.set_keyedDataPresence(machine.present);
		//rolePresent 
				machine.set_rolePresent(
						new BRelation<Integer,Integer>(
								new Pair<Integer,Integer>(1,machine.securityOfficer)
								));

				//currentAdminOp 
				machine.set_currentAdminOp(
						new BRelation<Integer,Integer>(
								new Pair<Integer,Integer>(1,machine.updateConfigData)
								));
		//FD.Enclave.ValidateOpRequestOK 
		Assert.assertTrue("Guard evt_ValidateOpRequestOK not satisfied.", 
				machine.evt_ValidateOpRequestOK.guard_ValidateOpRequestOK(currentKeyedData));
		machine.evt_ValidateOpRequestOK.run_ValidateOpRequestOK(currentKeyedData);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		machine.set_floppyPresence(machine.present);
		//FD.Enclave.StartUpdateConfigDataOK 
		Assert.assertTrue("Guard evt_StartUpdateConfigOK not satisfied.", machine.evt_StartUpdateConfigOK.guard_StartUpdateConfigOK());
		machine.evt_StartUpdateConfigOK.run_StartUpdateConfigOK();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);

		//FD.Enclave.FinishUpdateConfigDataOK
		Assert.assertTrue("Guard evt_FinishUpdateConfigDataOK not satisfied.", machine.evt_FinishUpdateConfigDataOK.guard_FinishUpdateConfigDataOK(currentFloppy));
		machine.evt_FinishUpdateConfigDataOK.run_FinishUpdateConfigDataOK(currentFloppy);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);

		machine.set_userTokenPresence(machine.present); 
		//FD.UserEntry.TISReadUserToken is defined as ReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", 
				machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.BioCheckRequired 
		Assert.assertTrue("Guard evt_BioCheckRequired not satisfied.", machine.evt_BioCheckRequired.guard_BioCheckRequired(currentTime));
		machine.evt_BioCheckRequired.run_BioCheckRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.insertFinger);

		machine.set_FingerPresence(machine.present);
		//FD.UserEntry.ReadFingerOK 
		Assert.assertTrue("Guard evt_ReadFingerOK not satisfied.", machine.evt_ReadFingerOK.guard_ReadFingerOK());
		machine.evt_ReadFingerOK.run_ReadFingerOK();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.ValidateFingerOK 
		Assert.assertTrue("Guard evt_ValidateFingerOK not satisfied.", machine.evt_ValidateFingerOK.guard_ValidateFingerOK(currentFinger));
		machine.evt_ValidateFingerOK.run_ValidateFingerOK(currentFinger);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.WriteUserTokenFail
		Assert.assertTrue("Guard evt_WriteUserTokenFail not satisfied.", machine.evt_WriteUserTokenFail.guard_WriteUserTokenFail());
		machine.evt_WriteUserTokenFail.run_WriteUserTokenFail();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.tokenUpdateFailed);

		currentTime = 5;
		//EntryNotAllowed
		Assert.assertTrue("Guard evt_EntryNotAllowed not satisfied.", 
				machine.evt_EntryNotAllowed_1.guard_EntryNotAllowed_1(currentTime)
			||
				machine.evt_EntryNotAllowed_2.guard_EntryNotAllowed_2(currentTime)
				);
		machine.evt_EntryNotAllowed_1.run_EntryNotAllowed_1(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.removeToken);
		
		machine.set_userTokenPresence(machine.absent);
		//FD.UserEntry.FailedAccessTokenRemoved
		Assert.assertTrue("Guard evt_FailedAccessTokenRemoved not satisfied.", 
				machine.evt_FailedAccessTokenRemoved.guard_FailedAccessTokenRemoved());
		machine.evt_FailedAccessTokenRemoved.run_FailedAccessTokenRemoved();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.welcome);
		 
		//FD.Enclave.TISAdminLogout 
		machine.set_adminTokenPresence(machine.absent); 
		//FD.Enclave.AdminLogout  => TokenRemovedAdminLogout
		Assert.assertTrue("Guard TokenRemovedAdminLogout not satisfied.", machine.evt_TokenRemovedAdminLogout.guard_TokenRemovedAdminLogout());
		machine.evt_TokenRemovedAdminLogout.run_TokenRemovedAdminLogout();
		Assert.assertEquals(machine.get_currentAdminOp().apply(
				machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())), ref6_admin.NoOp);

		//FD.AuditLog.LogChange

	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_Shutdown2(){
		Integer cert1_adm = 5;
		Integer cert2_adm = 6;
		Integer cert3_adm = 7;
		Integer cert4_adm = 8;
		//certificates for the admin
		machine.set_certificates(new BSet<Integer>(cert1_adm,cert2_adm,cert3_adm,cert4_adm));

		//users
		Integer user3 = 3;
		Integer user4 = 4;
		machine.set_user(new BSet<Integer>(user3,user4));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user3),
				new BSet<Integer>(user4)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1_adm,new BSet<Integer>(user3)),
				new Pair<Integer,BSet<Integer>>(cert2_adm,new BSet<Integer>(user3)),
				new Pair<Integer,BSet<Integer>>(cert3_adm,new BSet<Integer>(user4)),
				new Pair<Integer,BSet<Integer>>(cert4_adm,new BSet<Integer>(user4))
				));*/

		//serials 
		Integer serial5 = 5; 
		Integer serial6 = 6; 
		Integer serial7 = 7; 
		Integer serial8 = 8; 
		//machine.set_serial(new BSet<Integer>(serial5,serial6,serial7,serial8));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1_adm,serial5),
				new Pair<Integer,Integer>(cert2_adm,serial6),
				new Pair<Integer,Integer>(cert3_adm,serial7),
				new Pair<Integer,Integer>(cert4_adm,serial8)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1_adm,1),
				new Pair<Integer,Integer>(cert2_adm,1),
				new Pair<Integer,Integer>(cert3_adm,1),
				new Pair<Integer,Integer>(cert4_adm,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		Integer pubKey5 = 5;
		Integer pubKey6 = 6;
		Integer pubKey7 = 7;
		Integer pubKey8 = 8;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4, 
				pubKey5, pubKey6, pubKey7, pubKey8));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1_adm,pubKey5),
				new Pair<Integer,Integer>(cert2_adm,pubKey6),
				new Pair<Integer,Integer>(cert3_adm,pubKey7),
				new Pair<Integer,Integer>(cert4_adm,pubKey8)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1_adm));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2_adm,cert3_adm,cert4_adm));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1_adm,user3)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2_adm,cert1_adm),
				new Pair<Integer,Integer>(cert3_adm,cert1_adm),
				new Pair<Integer,Integer>(cert4_adm,cert1_adm)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3_adm));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2_adm));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert4_adm));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User05fp = 1;
		Integer User02fp = 2;
		machine.set_fingerprint(new BSet<Integer>(User05fp, User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3_adm,machine.securityOfficer)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3_adm,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4_adm,machine.securityOfficer)
				));

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert4_adm,machine.secret)
				));

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2_adm,User02fp)
				));

		//tokenID
		Integer tok = 1;
		Integer tok_adm = 2;
		machine.set_tokenID(new BSet<Integer>(tok, tok_adm));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert1_adm)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert3_adm)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert2_adm)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok_adm,cert4_adm)
				));

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2_adm,tok_adm),
				new Pair<Integer,Integer>(cert3_adm,tok_adm),
				new Pair<Integer,Integer>(cert4_adm,tok_adm)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user3),pubKey8),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user4),pubKey8)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT),
				new Pair<Integer,Integer>(tok_adm,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User05fp,machine.goodF),
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));
		
		machine.set_availableOps(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1, machine.updateConfigData)
						)
				);

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(1))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
												)
										),
										new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
												machine.auditManager, new BRelation<Integer,BSet<Integer>>(
														new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1))
														)
												)
						)
				);

		//admin
		machine.set_admin(new BSet<Integer>(1));

		//adminToken
		machine.set_adminToken(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1,2)
				));


		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoRole)
						));

		//currentAdminOp 
		machine.set_currentAdminOp(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoOp)
						));

		//keyedOps 
		machine.set_keyedOps(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.shutdownOp,1)
				));

		//configFile
		machine.set_configFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.good1, 1)
				));

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);
		
		machine.set_entry_status2(machine.quiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);
		
		Integer currentTime = 1;
		Integer token_admin_to_read = 2;
		Integer currentKeyedData = 1;
		
		machine.set_adminTokenPresence(machine.present);
		//FD.Enclave.TISAdminLogin => TISReadAdminToken defined as ReadAdminToken
		Assert.assertTrue("Guard evt_readAdminToken not satisfied.", machine.evt_readAdminToken.guard_readAdminToken(token_admin_to_read));
		machine.evt_readAdminToken.run_readAdminToken(token_admin_to_read);
		Assert.assertEquals(machine.get_currentAdminToken(), token_admin_to_read);

		//FD.Enclave.GetPresentAdminToken 
		machine.set_keyedDataPresence(machine.present);
		//FD.Enclave.ValidateAdminTokenOK 
		Assert.assertTrue("Guard evt_ValidateAdminTokenOK not satisfied.", machine.evt_ValidateAdminTokenOK.guard_ValidateAdminTokenOK(currentTime));
		machine.evt_ValidateAdminTokenOK.run_ValidateAdminTokenOK(currentTime);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.requestAdminOp);

		//FD.Enclave.TISStartAdminOp => ValidateOpRequest -> ValidateOpRequestOK
		machine.set_keyedDataPresence(machine.present);
		//rolePresent 
				machine.set_rolePresent(
						new BRelation<Integer,Integer>(
								new Pair<Integer,Integer>(1,machine.securityOfficer)
								));

				//currentAdminOp 
				machine.set_currentAdminOp(
						new BRelation<Integer,Integer>(
								new Pair<Integer,Integer>(1,machine.shutdownOp)
								));
		//FD.Enclave.ValidateOpRequestOK 
		Assert.assertTrue("Guard evt_ValidateOpRequestOK not satisfied.", 
				machine.evt_ValidateOpRequestOK.guard_ValidateOpRequestOK(currentKeyedData));
		machine.evt_ValidateOpRequestOK.run_ValidateOpRequestOK(currentKeyedData);
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.doingOp);
		
		machine.set_currentDoor(machine.closed);
		//FD.Enclave.TISShutDownOp => ShutdownOK 
		//FD.Enclave.ShutdownOK 
		Assert.assertTrue("Guard evt_ShutdownOK not satisfied.", machine.evt_ShutdownOK.guard_ShutdownOK());
		machine.evt_ShutdownOK.run_ShutdownOK();
		Assert.assertEquals(machine.get_screenMsg2(), ref6_admin.clear);
		Assert.assertEquals(machine.get_displayMessage3(), ref6_admin.blank);
		
		//FD.AuditLog.LogChange
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_1_property(){
		//certificates
		Integer cert_id_cert = 1;
		Integer cert_priv = 2;
		Integer cert_ianda = 3;
		Integer cert_auth = 4;
		machine.set_certificates(new BSet<Integer>(cert_id_cert,cert_priv,cert_ianda, cert_auth));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		Integer user3 = 3;
		Integer user4 = 4;
		machine.set_user(new BSet<Integer>(user1,user2,user3, user4));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2),
				new BSet<Integer>(user3)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert_id_cert,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert_priv,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert_ianda,new BSet<Integer>(user3))
				));*/

		//serials
		Integer serial1 = 1;
		Integer serial2 = 2;
		Integer serial3 = 3;
		Integer serial4 = 4;
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert_id_cert,serial1),
				new Pair<Integer,Integer>(cert_priv,serial2),
				new Pair<Integer,Integer>(cert_ianda,serial3),
				new Pair<Integer,Integer>(cert_auth,serial4)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert_id_cert,1),
				new Pair<Integer,Integer>(cert_priv,1),
				new Pair<Integer,Integer>(cert_ianda,1),
				new Pair<Integer,Integer>(cert_auth,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '59', 'Month': '12', 'Day': '31', 'Hour': '23', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}



		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert_id_cert,pubKey1),
				new Pair<Integer,Integer>(cert_priv,pubKey2),
				new Pair<Integer,Integer>(cert_ianda,pubKey3),
				new Pair<Integer,Integer>(cert_auth,pubKey4)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert_id_cert));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert_priv, cert_ianda, cert_auth));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert_id_cert,user1),
				new Pair<Integer,Integer>(cert_priv,user2),
				new Pair<Integer,Integer>(cert_ianda,user3),
				new Pair<Integer,Integer>(cert_auth,user4)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>());

		//privCert
		machine.set_privCert(new BSet<Integer>(cert_priv));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert_ianda));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert_auth));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		
		//fingerprint
		machine.set_fingerprint(new BSet<Integer>());

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert_priv,machine.NoRole)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert_priv,machine.unmarked)
				));
		
		//rolePresent 
		machine.set_rolePresent(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(1,machine.NoRole)
						));
		
		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert_auth, machine.guard)
				));
		
		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>());

		Integer adm_tok = 10;
		
		//tokenID
		machine.set_tokenID(new BSet<Integer>(adm_tok));

		//tokenIDCert
		//machine.(new BRelation<Integer,Integer>());

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(adm_tok,cert_priv)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(adm_tok,cert_ianda)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(adm_tok,cert_auth)
						)
				);

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert_auth,adm_tok)
				
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(adm_tok, machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>());

		machine.set_currentAdminToken(adm_tok);
		machine.set_currentToken(-1);

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						1)
				));*/

		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(1)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.guard, s)));

		machine.set_validEnrol(new BSet<Integer>(1,2,3));
		machine.set_enrolmentFile(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(1,1),
				new Pair<Integer,Integer>(1,2),
				new Pair<Integer,Integer>(1,3)
				)); 

		//FD.TIS.TISStartup is defined as StartEnrolledStation
		//StartEnrolledStation -> not modeled in EB
		machine.set_screenMsg1(machine.welcomeAdmin);
		machine.set_screenMsg2(machine.welcomeAdmin);
		machine.set_displayMessage1(machine.welcome);
		machine.set_displayMessage2(machine.welcome);
		machine.set_displayMessage3(machine.welcome);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);
		machine.set_enclaveStatus2(machine.enclaveQuiescent);
		machine.set_entry_status1(machine.quiescent);
		machine.set_entry_status2(machine.quiescent);

		Integer currentFloppy = 1;

		machine.set_enclaveStatus1(machine.notEnrolled); 
		machine.set_floppyPresence(machine.present);

		//FD.Enclave.TISEnrolOp => ReadEnrolmentData => ReadEnrolmentFloppy 
		//FD.Enclave.ReadEnrolmentFloppy 
		Assert.assertTrue("Guard evt_ReadEnrolmentFloppy not satisfied.", machine.evt_ReadEnrolmentFloppy.guard_ReadEnrolmentFloppy());
		machine.evt_ReadEnrolmentFloppy.run_ReadEnrolmentFloppy();
		Assert.assertEquals(machine.get_screenMsg1(), ref6_admin.validatingEnrolmentData);
		Assert.assertEquals(machine.get_displayMessage2(), ref6_admin.blank);

		//FD.Enclave.ValidateEnrolmentDataFail
		Assert.assertTrue("Guard evt_ValidateEnrolmentDataOK not satisfied.", machine.evt_ValidateEnrolmentDataOK.guard_ValidateEnrolmentDataOK(currentFloppy));
		machine.evt_ValidateEnrolmentDataOK.run_ValidateEnrolmentDataOK(currentFloppy);
		Assert.assertEquals(machine.get_screenMsg1(), ref6_admin.welcomeAdmin);
		Assert.assertEquals(machine.get_displayMessage2(), ref6_admin.welcome);
		
		//FD.AuditLog.LogChange
		
		//Testing Property number one
		Assert.assertTrue("Property one not satified: guard_SecurityPropertyOne_Unlock_with_token", machine.evt_SecurityPropertyOne_Unlock_with_token.guard_SecurityPropertyOne_Unlock_with_token(1));
		machine.evt_SecurityPropertyOne_Unlock_with_token.run_SecurityPropertyOne_Unlock_with_token(1);

	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_2_property(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3));

		//users
		Integer user1 = 1; //p01.dat: 4294967295
		Integer user2 = 2; //p01.dat: 0000032767
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1; //p01.dat: 100000001
		Integer serial2 = 2; //p01.dat: 1029384756
		Integer serial3 = 3; //p01.dat: 987654321
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '59', 'Month': '12', 'Day': '31', 'Hour': '23', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}



		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1) //p01.dat: it is itself ('Text': 'User01', 'ID': '100000001')
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User01fp = 1; //p01.dat: User01fp
		machine.set_fingerprint(new BSet<Integer>(User01fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.securityOfficer)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User01fp)
				));

		//tokenID
		Integer tok = 1;
		machine.set_tokenID(new BSet<Integer>(tok));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User01fp,machine.goodF)
				));
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						1)
				));*/

		//entryPeriod
		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(31)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.securityOfficer, s)));


		machine.set_tokenRemovalTimeout(0);
		machine.set_tokenRemovalDuration(0);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);
		machine.set_latchUnlockDuration(31);
		machine.set_alarmSilentDuration(10);

		machine.set_currentToken(-1);

		//Init parameters
		Integer token_user_to_read = 1;
		machine.set_entry_status1(machine.quiescent);
		machine.set_userTokenPresence(machine.present);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);
		Integer currentTime = 31;

		//FD.UserEntry.TISReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.BioCheckRequired 
		Assert.assertTrue("Guard evt_BioCheckRequired not satisfied.", machine.evt_BioCheckRequired.guard_BioCheckRequired(currentTime));
		machine.evt_BioCheckRequired.run_BioCheckRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.waitingFinger);
	
		machine.set_FingerPresence(machine.present);
		//FD.UserEntry.ReadFingerOK
		Assert.assertTrue("Guard evt_ReadFingerOK not satisfied.", machine.evt_ReadFingerOK.guard_ReadFingerOK());
		machine.evt_ReadFingerOK.run_ReadFingerOK();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		Integer fingerPrint = User01fp;
		//FD.UserEntry.ValidateFingerOK
		Assert.assertTrue("Guard evt_ValidateFingerOK not satisfied.", machine.evt_ValidateFingerOK.guard_ValidateFingerOK(fingerPrint));
		machine.evt_ValidateFingerOK.run_ValidateFingerOK(fingerPrint);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.ConstructAuthCert -> built-in writeUserTokenOK

		//FD.UserEntry.WriteUserTokenOK
		Assert.assertTrue("Guard evt_WriteUserTokenOK not satisfied.", machine.evt_WriteUserTokenOK.guard_WriteUserTokenOK());
		machine.evt_WriteUserTokenOK.run_WriteUserTokenOK();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.EntryOK
		Assert.assertTrue("Guard evt_EntryOK not satisfied.", machine.evt_EntryOK_1.guard_EntryOK_1(currentTime)
															|| machine.evt_EntryOK_2.guard_EntryOK_2(currentTime));
		machine.evt_EntryOK_1.run_EntryOK_1(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.openDoor);


		machine.set_userTokenPresence(machine.absent);
		//FD.UserEntry.UnlockDoorOK
		Assert.assertTrue("Guard evt_UnlockDoorOK not satisfied.", 
				machine.evt_unlockDoorOK.guard_unlockDoorOK(currentTime));
		machine.evt_unlockDoorOK.run_unlockDoorOK(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.doorUnlocked);
		//FD.AuditLog.LogChange
		
		//Testing Property number one
		Assert.assertTrue("Property one not satified: guard_SecurityPropertyOne_Unlock_with_token", machine.evt_SecurityPropertyOne_Unlock_with_token.guard_SecurityPropertyOne_Unlock_with_token(1));
		machine.evt_SecurityPropertyOne_Unlock_with_token.run_SecurityPropertyOne_Unlock_with_token(1);
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_3_sec_property(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3));

		//users
		Integer user1 = 1; //p01.dat: 4294967295
		Integer user2 = 2; //p01.dat: 0000032767
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1; //p01.dat: 100000002
		Integer serial2 = 2; //p01.dat: 1029384756
		Integer serial3 = 3; //p01.dat: 987654321
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '59', 'Month': '12', 'Day': '31', 'Hour': '23', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}



		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1) //p01.dat: it is itself ('Text': 'User02', 'ID': '100000002')
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>());


		
		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User02fp = 1; //p01.dat: User02fp
		machine.set_fingerprint(new BSet<Integer>(User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.guard) //p02.dat: 'Role': '1'
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret) //p02.dat 'Class': '4'
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User02fp)
				));

		//tokenID
		Integer tok = 1;
		machine.set_tokenID(new BSet<Integer>(tok));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						1)
				));*/

		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(31)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.guard, s)));

		machine.set_tokenRemovalTimeout(0);
		machine.set_tokenRemovalDuration(0);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);
		machine.set_latchUnlockDuration(31);
		machine.set_alarmSilentDuration(10);

		Integer currentTime = 31;
		Integer fingerPrint = User02fp;

		machine.set_currentToken(-1);

		//Init parameters
		Integer token_user_to_read = 1;
		machine.set_entry_status1(machine.quiescent);
		machine.set_userTokenPresence(machine.present);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);

		//FD.UserEntry.TISReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", 
				machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), machine.wait);
		
		//FD.UserEntry.BioCheckRequired 
		Assert.assertTrue("Guard evt_BioCheckRequired not satisfied.", machine.evt_BioCheckRequired.guard_BioCheckRequired(currentTime));
		machine.evt_BioCheckRequired.run_BioCheckRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), machine.waitingFinger);

		machine.set_FingerPresence(machine.present);
		//FD.UserEntry.ReadFingerOK
		Assert.assertTrue("Guard evt_ReadFingerOK not satisfied.", machine.evt_ReadFingerOK.guard_ReadFingerOK());
		machine.evt_ReadFingerOK.run_ReadFingerOK();
		Assert.assertEquals(machine.get_displayMessage1(), machine.wait);

		//FD.UserEntry.ValidateFingerOK
		Assert.assertTrue("Guard evt_ValidateFingerOK not satisfied.", machine.evt_ValidateFingerOK.guard_ValidateFingerOK(fingerPrint));
		machine.evt_ValidateFingerOK.run_ValidateFingerOK(fingerPrint);
		Assert.assertEquals(machine.get_displayMessage1(), machine.wait);
	
		//FD.UserEntry.ConstructAuthCert -> built-in writeUserTokenOK

		//FD.UserEntry.WriteUserTokenOK
		Assert.assertTrue("Guard evt_WriteUserTokenOK not satisfied.", machine.evt_WriteUserTokenOK.guard_WriteUserTokenOK());
		machine.evt_WriteUserTokenOK.run_WriteUserTokenOK();
		Assert.assertEquals(machine.get_displayMessage1(), machine.wait);

		//FD.UserEntry.EntryOK
		Assert.assertTrue("Guard evt_EntryOK not satisfied.", machine.evt_EntryOK_1.guard_EntryOK_1(currentTime)
				|| machine.evt_EntryOK_2.guard_EntryOK_2(currentTime));
		machine.evt_EntryOK_1.run_EntryOK_1(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), machine.openDoor);

		machine.set_userTokenPresence(machine.absent);
		//FD.UserEntry.UnlockDoorOK
		Assert.assertTrue("Guard evt_UnlockDoorOK not satisfied.", machine.evt_unlockDoorOK.guard_unlockDoorOK(currentTime));
		machine.evt_unlockDoorOK.run_unlockDoorOK(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), machine.doorUnlocked);
		//FD.AuditLog.LogChange
		
		//Testing Property number one
		Assert.assertTrue("Property one not satified: guard_SecurityPropertyOne_Unlock_with_token", machine.evt_SecurityPropertyOne_Unlock_with_token.guard_SecurityPropertyOne_Unlock_with_token(1));
		machine.evt_SecurityPropertyOne_Unlock_with_token.run_SecurityPropertyOne_Unlock_with_token(1);
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_4_sec_property(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3)
				));


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User07fp = 1; 
		machine.set_fingerprint(new BSet<Integer>(User07fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.userOnly)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User07fp)
				));

		//tokenID
		Integer tok = 1;
		machine.set_tokenID(new BSet<Integer>(tok));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,tok),
				new Pair<Integer,Integer>(cert3,tok)
				));

		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);
		
		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User07fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(31))
										)
								),
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(31))
												)
										),
										new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
												machine.auditManager, new BRelation<Integer,BSet<Integer>>(
														new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(31))
														)
												)
						)
				);

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(31);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);

		machine.set_entry_status1(machine.quiescent); 
		machine.set_userTokenPresence(machine.present); 
		machine.set_enclaveStatus1(machine.enclaveQuiescent);

		Integer token_user_to_read = 1;
		Integer currentTime = 31;
		Integer currentFinger = 1;

		//FD.UserEntry.TISReadUserToken is defined as ReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", 
				machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);
		
		//FD.UserEntry.BioCheckRequired 
		Assert.assertTrue("Guard evt_BioCheckRequired not satisfied.", machine.evt_BioCheckRequired.guard_BioCheckRequired(currentTime));
		machine.evt_BioCheckRequired.run_BioCheckRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.insertFinger);

		machine.set_FingerPresence(machine.present);
		//FD.UserEntry.ReadFingerOK 
		Assert.assertTrue("Guard evt_ReadFingerOK not satisfied.", machine.evt_ReadFingerOK.guard_ReadFingerOK());
		machine.evt_ReadFingerOK.run_ReadFingerOK();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.ValidateFingerOK 
		Assert.assertTrue("Guard evt_ValidateFingerOK not satisfied.", machine.evt_ValidateFingerOK.guard_ValidateFingerOK(currentFinger));
		machine.evt_ValidateFingerOK.run_ValidateFingerOK(currentFinger);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.WriteUserTokenFail
		Assert.assertTrue("Guard evt_WriteUserTokenFail not satisfied.", machine.evt_WriteUserTokenFail.guard_WriteUserTokenFail());
		machine.evt_WriteUserTokenFail.run_WriteUserTokenFail();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.tokenUpdateFailed);

		//FD.UserEntry.EntryOK 
		Assert.assertTrue("Guard evt_EntryOK not satisfied.", machine.evt_EntryOK_1.guard_EntryOK_1(currentTime)
				|| machine.evt_EntryOK_2.guard_EntryOK_2(currentTime));
		machine.evt_EntryOK_1.run_EntryOK_1(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.openDoor);

		machine.set_userTokenPresence(machine.absent);
		//FD.UserEntry.UnlockDoorOK 
		Assert.assertTrue("Guard evt_unlockDoorOK not satisfied.", 
				machine.evt_unlockDoorOK.guard_unlockDoorOK(currentTime));
		machine.evt_unlockDoorOK.run_unlockDoorOK(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.doorUnlocked);

		//FD.AuditLog.LogChange
		

		//Testing Property number one
		Assert.assertTrue("Property one not satified: guard_SecurityPropertyOne_Unlock_with_token", machine.evt_SecurityPropertyOne_Unlock_with_token.guard_SecurityPropertyOne_Unlock_with_token(1));
		machine.evt_SecurityPropertyOne_Unlock_with_token.run_SecurityPropertyOne_Unlock_with_token(1);
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Test
	public void test_5_sec_property(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3));

		//users
		Integer user1 = 1;
		Integer user2 = 2;
		machine.set_user(new BSet<Integer>(user1,user2));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1; 
		Integer serial2 = 2; 
		Integer serial3 = 3; 
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,1),
				new Pair<Integer,Integer>(cert2,1),
				new Pair<Integer,Integer>(cert3,1)
				)); 
		//p01.dat: where (1) means: 
		//{'NotAfter': {'Minute': '00', 'Month': '2', 'Day': '12', 'Hour': '22', 'Year': '2010'}, 
		//'NotBefore': {'Minute': '00', 	'Month': '01', 'Day': '01', 'Hour': '00', 'Year': '2001'}}, 

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3)
				));
		
		machine.set_timesRecentTo(
				new BRelation<Integer,BSet<Integer>>(
						new Pair<Integer, BSet<Integer>>(1, new BSet<Integer>(10,20,31)),
						new Pair<Integer, BSet<Integer>>(31, new BSet<Integer>(10,20,31))
						)
				);


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1)
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>());

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User07fp = 1; 
		machine.set_fingerprint(new BSet<Integer>(User07fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.userOnly)
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>());

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>());

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User07fp)
				));

		//tokenID
		Integer tok = 1;
		machine.set_tokenID(new BSet<Integer>(tok));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>());

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,tok)
				//new Pair<Integer,Integer>(cert2,tok)
				));

		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);
		
		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User07fp,machine.goodF)
				));

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						machine.secret),
						new Pair<Pair<Integer,Integer>,Integer>(
								new Pair<Integer,Integer>(machine.userOnly,machine.secret), 
								machine.secret),
								new Pair<Pair<Integer,Integer>,Integer>(
										new Pair<Integer,Integer>(machine.guard,machine.secret), 
										machine.secret)
				));*/

		//entryPeriod
		machine.set_entryPeriod(
				new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
						new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
								machine.securityOfficer, new BRelation<Integer,BSet<Integer>>(
										new Pair<Integer,BSet<Integer>>(machine.unmarked,new BSet<Integer>(31))
										)
								),
								
								new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
										machine.userOnly, new BRelation<Integer,BSet<Integer>>(
												new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(31))
												)
										),
										new Pair<Integer,BRelation<Integer,BSet<Integer>>>(
												machine.auditManager, new BRelation<Integer,BSet<Integer>>(
														new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(31))
														)
												)
						)
				);

		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(31);
		machine.set_tokenRemovalTimeout(15);
		machine.set_tokenRemovalDuration(15);
		machine.set_latchTimeout1(31);
		machine.set_latchTimeout2(31);

		machine.set_entry_status1(machine.quiescent); 
		machine.set_userTokenPresence(machine.present); 
		machine.set_enclaveStatus1(machine.enclaveQuiescent);

		Integer token_user_to_read = 1;
		Integer currentTime = 31;
		Integer currentFinger = 1;

		//FD.UserEntry.TISReadUserToken is defined as ReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", 
				machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);
		
		//FD.UserEntry.BioCheckRequired 
		Assert.assertTrue("Guard evt_BioCheckRequired not satisfied.", machine.evt_BioCheckRequired.guard_BioCheckRequired(currentTime));
		machine.evt_BioCheckRequired.run_BioCheckRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.insertFinger);

		machine.set_FingerPresence(machine.present);
		//FD.UserEntry.ReadFingerOK 
		Assert.assertTrue("Guard evt_ReadFingerOK not satisfied.", machine.evt_ReadFingerOK.guard_ReadFingerOK());
		machine.evt_ReadFingerOK.run_ReadFingerOK();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.ValidateFingerOK 
		Assert.assertTrue("Guard evt_ValidateFingerOK not satisfied.", machine.evt_ValidateFingerOK.guard_ValidateFingerOK(currentFinger));
		machine.evt_ValidateFingerOK.run_ValidateFingerOK(currentFinger);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.WriteUserTokenFail
		Assert.assertTrue("Guard evt_WriteUserTokenFail not satisfied.", machine.evt_WriteUserTokenFail.guard_WriteUserTokenFail());
		machine.evt_WriteUserTokenFail.run_WriteUserTokenFail();
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.tokenUpdateFailed);

		//FD.UserEntry.EntryOK 
		Assert.assertTrue("Guard evt_EntryOK not satisfied.", machine.evt_EntryOK_1.guard_EntryOK_1(currentTime)
				|| machine.evt_EntryOK_2.guard_EntryOK_2(currentTime));
		machine.evt_EntryOK_1.run_EntryOK_1(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.openDoor);

		machine.set_userTokenPresence(machine.absent);
		//FD.UserEntry.UnlockDoorOK 
		Assert.assertTrue("Guard evt_unlockDoorOK not satisfied.", 
				machine.evt_unlockDoorOK.guard_unlockDoorOK(currentTime));
		machine.evt_unlockDoorOK.run_unlockDoorOK(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.doorUnlocked);

		//FD.AuditLog.LogChange
		
		BSet<Integer> recentTime = new BSet<Integer>(31);
				
		//Testing Property number two
		Assert.assertTrue("Property two not satified: guard_SecurityPropertyTwo_Unlock_at_allowed_time", machine.evt_SecurityPropertyTwo_Unlock_at_allowed_time.guard_SecurityPropertyTwo_Unlock_at_allowed_time(
				currentTime, recentTime));
		machine.evt_SecurityPropertyTwo_Unlock_at_allowed_time.run_SecurityPropertyTwo_Unlock_at_allowed_time(
				currentTime, recentTime);
	}
	
	@SuppressWarnings({ "static-access", "unchecked" })
	@Test
	public void test_6_sec_property(){
		//certificates
		Integer cert1 = 1;
		Integer cert2 = 2;
		Integer cert3 = 3;
		Integer cert4 = 4;
		machine.set_certificates(new BSet<Integer>(cert1,cert2,cert3,cert4));

		//users
		Integer user1 = 1; 
		Integer user2 = 2; 
		Integer user3 = 3; 
		machine.set_user(new BSet<Integer>(user1,user2,user3));

		//issuer
		/*machine.set_issuer(new BSet<BSet<Integer>>(
				new BSet<Integer>(user1),
				new BSet<Integer>(user2),
				new BSet<Integer>(user3)
				));*/

		//certificateIssuer
		/*machine.set_certificateIssuer(new BRelation<Integer,BSet<Integer>>(
				new Pair<Integer,BSet<Integer>>(cert1,new BSet<Integer>(user1)),
				new Pair<Integer,BSet<Integer>>(cert2,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert3,new BSet<Integer>(user2)),
				new Pair<Integer,BSet<Integer>>(cert4,new BSet<Integer>(user2))
				));*/

		//serials
		Integer serial1 = 1;
		Integer serial2 = 2;
		Integer serial3 = 3;
		Integer serial4 = 4;
		//machine.set_serial(new BSet<Integer>(serial1,serial2,serial3,serial4));

		//certificateID 
		machine.set_certificateID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,serial1),
				new Pair<Integer,Integer>(cert2,serial2),
				new Pair<Integer,Integer>(cert3,serial3),
				new Pair<Integer,Integer>(cert4,serial4)
				));

		//validityPeriods
		machine.set_validityPeriods(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,100),
				new Pair<Integer,Integer>(cert2,100),
				new Pair<Integer,Integer>(cert3,100),
				new Pair<Integer,Integer>(cert4,100)
				));

		//publicKeys
		Integer pubKey1 = 1;
		Integer pubKey2 = 2;
		Integer pubKey3 = 3;
		Integer pubKey4 = 4;
		machine.set_publicKeys(new BSet<Integer>(pubKey1,pubKey2,pubKey3,pubKey4));

		//isValidatedBy
		machine.set_isValidatedBy(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,pubKey1),
				new Pair<Integer,Integer>(cert2,pubKey2),
				new Pair<Integer,Integer>(cert3,pubKey3),
				new Pair<Integer,Integer>(cert4,pubKey4)
				));
		
		machine.set_timesRecentTo(
				new BRelation<Integer,BSet<Integer>>(
						new Pair<Integer, BSet<Integer>>(100, new BSet<Integer>(100,120,131)),
						new Pair<Integer, BSet<Integer>>(31, new BSet<Integer>(10,20,31))
						)
				);


		//IDCert
		machine.set_idCert(new BSet<Integer>(cert1));

		//attCert
		machine.set_attCert(new BSet<Integer>(cert2,cert3));

		//idcert_subject
		machine.set_subject(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert1,user1) 
				));

		//idcert_subjectPubK

		//attcert_baseCertID
		machine.set_baseCertID(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,cert1),
				new Pair<Integer,Integer>(cert3,cert1),
				new Pair<Integer,Integer>(cert4,cert1)
				));

		//privCert
		machine.set_privCert(new BSet<Integer>(cert3));

		//iandaCert
		machine.set_iandaCert(new BSet<Integer>(cert2));

		//authCert
		machine.set_authCert(new BSet<Integer>(cert4));

		//privilege 
		machine.set_privilege(new BSet<Integer>(
				machine.userOnly, machine.guard, machine.securityOfficer, machine.auditManager, machine.NoRole
				));

		//clearence
		machine.set_clearence(new BSet<Integer>(
				machine.unmarked, machine.unclassified, machine.restricted, machine.confidential, machine.secret, machine.topsecret
				));

		//fingerprint
		Integer User02fp = 1; //p01.dat: User02fp
		machine.set_fingerprint(new BSet<Integer>(User02fp));

		//privCertRole
		machine.set_privCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.guard) 
				));

		//privCertClearence
		machine.set_privCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//authCertRole
		machine.set_authCertRole(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.guard) 
				));

		//authCertClearence
		machine.set_authCertClearence(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert3,machine.secret)
				));

		//iandaCertfpTemplate
		machine.set_fpTemplate(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(cert2,User02fp)
				));

		//tokenID
		Integer tok = 1;
		machine.set_tokenID(new BSet<Integer>(tok));

		//tokenIDCert
		/*machine.set_tokenIDCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert1)
				));*/

		//tokenPrivCert 
		machine.set_tokenPrivCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert3)
				));

		//tokenIandaCert 
		machine.set_tokenIandaCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert2)
				));

		//tokenAuthCert
		machine.set_tokenAuthCert(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,cert4)
				));

		//attcert_tokenID 
		machine.set_attCertTokID(new BRelation<Integer,Integer>(
				//new Pair<Integer,Integer>(cert2,tok)
				new Pair<Integer,Integer>(cert3,tok)
				//new Pair<Integer,Integer>(cert4,tok)
				));

		//issuerKey 
		/*machine.set_issuerKey(new BRelation<BSet<Integer>,Integer>(
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user1),pubKey1),
				new Pair<BSet<Integer>,Integer>(new BSet<Integer>(user2),pubKey1)
				));*/

		//goodTok 
		machine.set_goodTok(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(tok,machine.goodT)
				));

		machine.set_goodFP(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(User02fp,machine.goodF)
				));
		
		machine.set_status_sec(
				new BSet<Integer>(
						machine.quiescent, machine.gotUserToken, machine.waitingEntry, 
						machine.waitingRemoveTokenSuccess)
				);

		//authPeriod
		/*machine.set_authPeriod(new BRelation<Integer,Integer>(
				new Pair<Integer,Integer>(machine.userOnly,1),
				new Pair<Integer,Integer>(machine.guard,1),
				new Pair<Integer,Integer>(machine.securityOfficer,1),
				new Pair<Integer,Integer>(machine.auditManager,1),
				new Pair<Integer,Integer>(machine.NoRole,1)
				));

		//minClearance
		machine.set_minClearance(new BRelation<Pair<Integer,Integer>,Integer>(
				new Pair<Pair<Integer,Integer>,Integer>(
						new Pair<Integer,Integer>(machine.unmarked,machine.secret), 
						1)
				));*/

		BRelation<Integer,BSet<Integer>> s = new BRelation<Integer,BSet<Integer>>();
		s.add(new Pair<Integer,BSet<Integer>>(machine.secret,new BSet<Integer>(100)));

		machine.set_entryPeriod(new BRelation<Integer,BRelation<Integer,BSet<Integer>>>(
				new Pair<Integer,BRelation<Integer,BSet<Integer>>>(machine.guard, s)));


		machine.set_alarmSilentDuration(20);
		machine.set_latchUnlockDuration(30);
		machine.set_tokenRemovalDuration(15);

		machine.set_tokenRemovalTimeout(0);
		machine.set_latchTimeout1(40);
		machine.set_latchTimeout2(40);



		machine.set_currentToken(-1);

		//Init parameters
		Integer token_user_to_read = 1;
		Integer currentTime = 100;
		machine.set_entry_status1(machine.quiescent);
		machine.set_userTokenPresence(machine.present);
		machine.set_enclaveStatus1(machine.enclaveQuiescent);

		//FD.UserEntry.TISReadUserToken
		Assert.assertTrue("Guard evt_ReadUserToken not satisfied.", machine.evt_ReadUserToken.guard_ReadUserToken(token_user_to_read));
		machine.evt_ReadUserToken.run_ReadUserToken(token_user_to_read);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);
		
		//FD.UserEntry.BioCheckNotRequired 
		Assert.assertTrue("Guard evt_BioCheckNotRequired not satisfied.", 
				machine.evt_BioCheckNotRequired.guard_BioCheckNotRequired(currentTime));
		machine.evt_BioCheckNotRequired.run_BioCheckNotRequired(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.wait);

		//FD.UserEntry.EntryOK
		Assert.assertTrue("Guard evt_EntryOK not satisfied.", machine.evt_EntryOK_1.guard_EntryOK_1(currentTime)
				|| machine.evt_EntryOK_2.guard_EntryOK_2(currentTime));
		machine.evt_EntryOK_1.run_EntryOK_1(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.openDoor);

		machine.set_userTokenPresence(machine.absent);

		//FD.UserEntry.UnlockDoorOK
		Assert.assertTrue("Guard evt_UnlockDoorOK not satisfied.", 
				machine.evt_unlockDoorOK.guard_unlockDoorOK(currentTime));
		machine.evt_unlockDoorOK.run_unlockDoorOK(currentTime);
		Assert.assertEquals(machine.get_displayMessage1(), ref6_admin.doorUnlocked);
		//FD.AuditLog.LogChange
		
		
		BSet<Integer> recentTime = new BSet<Integer>(100);
		
		//Testing Property number two
		Assert.assertTrue("Property two not satified: guard_SecurityPropertyTwo_Unlock_at_allowed_time", machine.evt_SecurityPropertyTwo_Unlock_at_allowed_time.guard_SecurityPropertyTwo_Unlock_at_allowed_time(
				currentTime, recentTime));
		machine.evt_SecurityPropertyTwo_Unlock_at_allowed_time.run_SecurityPropertyTwo_Unlock_at_allowed_time(
				currentTime, recentTime);
	}
	

}


/************
Type definitions in Spark ADA (can be found in <praxis-docs>/code/core/*type*)

type StatusT is (Alarming, Silent);
type PresenceT is (Present, Absent);
type CertificateT is (IDCert, AuthCert, PrivCert, IandACert);
type PrivilegeT is (
      UserOnly,
      Guard,
      AuditManager,
      SecurityOfficer
      );
subtype AdminPrivilegeT is PrivilegeT range Guard .. SecurityOfficer;
type ClassT is (
      Unmarked,
      Unclassified,
      Restricted,
      Confidential,
      Secret,
      Topsecret
      );
/************/