package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class OverrideDoorLockOK{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public OverrideDoorLockOK(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.overrideLock) && machine.get_enclaveStatus2().equals(machine.waitingStartAdminOp) && machine.get_adminTokenPresence().equals(machine.present) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && !machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoOp) && NAT.instance.has(currentTime)); */
	public /*@ pure */ boolean guard_OverrideDoorLockOK( Integer currentTime) {
		return (machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.overrideLock) && machine.get_enclaveStatus2().equals(machine.waitingStartAdminOp) && machine.get_adminTokenPresence().equals(machine.present) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && !machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoOp) && NAT.instance.has(currentTime));
	}

	/*@ public normal_behavior
		requires guard_OverrideDoorLockOK(currentTime);
		assignable machine.screenMsg2, machine.displayMessage3, machine.enclaveStatus2, machine.latchTimeout2, machine.currentAdminOp, machine.alarmTimeout2;
		ensures guard_OverrideDoorLockOK(currentTime) &&  machine.get_screenMsg2() == \old(machine.requestAdminOp) &&  machine.get_displayMessage3() == \old(machine.doorUnlocked) &&  machine.get_enclaveStatus2() == \old(machine.enclaveQuiescent) &&  machine.get_latchTimeout2() == \old(new Integer(currentTime + machine.get_latchUnlockDuration())) &&  machine.get_currentAdminOp().equals(\old((machine.get_currentAdminOp().override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))))) &&  machine.get_alarmTimeout2() == \old(new Integer(currentTime + machine.get_latchUnlockDuration() + machine.get_alarmSilentDuration())); 
	 also
		requires !guard_OverrideDoorLockOK(currentTime);
		assignable \nothing;
		ensures true; */
	public void run_OverrideDoorLockOK( Integer currentTime){
		if(guard_OverrideDoorLockOK(currentTime)) {
			Integer screenMsg2_tmp = machine.get_screenMsg2();
			Integer displayMessage3_tmp = machine.get_displayMessage3();
			Integer enclaveStatus2_tmp = machine.get_enclaveStatus2();
			Integer latchTimeout2_tmp = machine.get_latchTimeout2();
			BRelation<Integer,Integer> currentAdminOp_tmp = machine.get_currentAdminOp();
			Integer alarmTimeout2_tmp = machine.get_alarmTimeout2();

			machine.set_screenMsg2(machine.requestAdminOp);
			machine.set_displayMessage3(machine.doorUnlocked);
			machine.set_enclaveStatus2(machine.enclaveQuiescent);
			machine.set_latchTimeout2(new Integer(currentTime + machine.get_latchUnlockDuration()));
			machine.set_currentAdminOp((currentAdminOp_tmp.override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))));
			machine.set_alarmTimeout2(new Integer(currentTime + machine.get_latchUnlockDuration() + machine.get_alarmSilentDuration()));

			System.out.println("OverrideDoorLockOK executed currentTime: " + currentTime + " ");
		}
	}

}
