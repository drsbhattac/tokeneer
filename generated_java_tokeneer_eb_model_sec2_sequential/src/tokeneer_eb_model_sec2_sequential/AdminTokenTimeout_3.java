package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class AdminTokenTimeout_3{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public AdminTokenTimeout_3(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (NAT.instance.has(currentTime) && machine.get_enclaveStatus2().equals(machine.enclaveQuiescent) && machine.get_adminTokenPresence().equals(machine.present) && machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && machine.get_tokenAuthCert().domain().has(machine.get_currentAdminToken()) && !machine.get_validityPeriods().image(new BSet<Integer>(machine.get_tokenAuthCert().apply(machine.get_currentAdminToken()))).has(currentTime)); */
	public /*@ pure */ boolean guard_AdminTokenTimeout_3( Integer currentTime) {
		return (NAT.instance.has(currentTime) && machine.get_enclaveStatus2().equals(machine.enclaveQuiescent) && machine.get_adminTokenPresence().equals(machine.present) && machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && machine.get_tokenAuthCert().domain().has(machine.get_currentAdminToken()) && !machine.get_validityPeriods().image(new BSet<Integer>(machine.get_tokenAuthCert().apply(machine.get_currentAdminToken()))).has(currentTime));
	}

	/*@ public normal_behavior
		requires guard_AdminTokenTimeout_3(currentTime);
		assignable machine.enclaveStatus2;
		ensures guard_AdminTokenTimeout_3(currentTime) &&  machine.get_enclaveStatus2() == \old(machine.waitingRemoveAdminTokenFail); 
	 also
		requires !guard_AdminTokenTimeout_3(currentTime);
		assignable \nothing;
		ensures true; */
	public void run_AdminTokenTimeout_3( Integer currentTime){
		if(guard_AdminTokenTimeout_3(currentTime)) {
			Integer enclaveStatus2_tmp = machine.get_enclaveStatus2();

			machine.set_enclaveStatus2(machine.waitingRemoveAdminTokenFail);

			System.out.println("AdminTokenTimeout_3 executed currentTime: " + currentTime + " ");
		}
	}

}
