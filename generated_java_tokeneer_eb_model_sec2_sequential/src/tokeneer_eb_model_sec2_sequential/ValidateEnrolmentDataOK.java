package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class ValidateEnrolmentDataOK{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public ValidateEnrolmentDataOK(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.FLOPPY.has(ValidateEnrolmentDataOK_currentFloppy) && machine.get_validEnrol().has(machine.get_enrolmentFile().apply(ValidateEnrolmentDataOK_currentFloppy)) && machine.get_enclaveStatus1().equals(machine.waitingEnrol)); */
	public /*@ pure */ boolean guard_ValidateEnrolmentDataOK( Integer ValidateEnrolmentDataOK_currentFloppy) {
		return (machine.FLOPPY.has(ValidateEnrolmentDataOK_currentFloppy) && machine.get_validEnrol().has(machine.get_enrolmentFile().apply(ValidateEnrolmentDataOK_currentFloppy)) && machine.get_enclaveStatus1().equals(machine.waitingEnrol));
	}

	/*@ public normal_behavior
		requires guard_ValidateEnrolmentDataOK(ValidateEnrolmentDataOK_currentFloppy);
		assignable machine.screenMsg1, machine.enclaveStatus1, machine.entry_status2, machine.displayMessage2;
		ensures guard_ValidateEnrolmentDataOK(ValidateEnrolmentDataOK_currentFloppy) &&  machine.get_screenMsg1() == \old(machine.welcomeAdmin) &&  machine.get_enclaveStatus1() == \old(machine.enclaveQuiescent) &&  machine.get_entry_status2() == \old(machine.quiescent) &&  machine.get_displayMessage2() == \old(machine.welcome); 
	 also
		requires !guard_ValidateEnrolmentDataOK(ValidateEnrolmentDataOK_currentFloppy);
		assignable \nothing;
		ensures true; */
	public void run_ValidateEnrolmentDataOK( Integer ValidateEnrolmentDataOK_currentFloppy){
		if(guard_ValidateEnrolmentDataOK(ValidateEnrolmentDataOK_currentFloppy)) {
			Integer screenMsg1_tmp = machine.get_screenMsg1();
			Integer enclaveStatus1_tmp = machine.get_enclaveStatus1();
			Integer entry_status2_tmp = machine.get_entry_status2();
			Integer displayMessage2_tmp = machine.get_displayMessage2();

			machine.set_screenMsg1(machine.welcomeAdmin);
			machine.set_enclaveStatus1(machine.enclaveQuiescent);
			machine.set_entry_status2(machine.quiescent);
			machine.set_displayMessage2(machine.welcome);

			System.out.println("ValidateEnrolmentDataOK executed ValidateEnrolmentDataOK_currentFloppy: " + ValidateEnrolmentDataOK_currentFloppy + " ");
		}
	}

}
