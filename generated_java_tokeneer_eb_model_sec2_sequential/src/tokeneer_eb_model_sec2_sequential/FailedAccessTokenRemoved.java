package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class FailedAccessTokenRemoved{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public FailedAccessTokenRemoved(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_entry_status1().equals(machine.waitingRemoveTokenFail) && machine.get_userTokenPresence().equals(machine.absent)); */
	public /*@ pure */ boolean guard_FailedAccessTokenRemoved() {
		return (
				machine.get_entry_status1().equals(machine.waitingRemoveTokenFail) && 
				machine.get_userTokenPresence().equals(machine.absent));
	}

	/*@ public normal_behavior
		requires guard_FailedAccessTokenRemoved();
		assignable machine.entry_status1, machine.displayMessage1, machine.status_sec;
		ensures guard_FailedAccessTokenRemoved() &&  machine.get_entry_status1() == \old(machine.quiescent) &&  machine.get_displayMessage1() == \old(machine.welcome) &&  machine.get_status_sec().equals(\old(new BSet<Integer>(machine.quiescent,machine.gotUserToken,machine.waitingEntry,machine.waitingRemoveTokenSuccess))); 
	 also
		requires !guard_FailedAccessTokenRemoved();
		assignable \nothing;
		ensures true; */
	public void run_FailedAccessTokenRemoved(){
		if(guard_FailedAccessTokenRemoved()) {
			Integer entry_status1_tmp = machine.get_entry_status1();
			Integer displayMessage1_tmp = machine.get_displayMessage1();
			BSet<Integer> status_sec_tmp = machine.get_status_sec();

			machine.set_entry_status1(machine.quiescent);
			machine.set_displayMessage1(machine.welcome);
			machine.set_status_sec(new BSet<Integer>(machine.quiescent,machine.gotUserToken,machine.waitingEntry,machine.waitingRemoveTokenSuccess));

			System.out.println("FailedAccessTokenRemoved executed ");
		}
	}

}
