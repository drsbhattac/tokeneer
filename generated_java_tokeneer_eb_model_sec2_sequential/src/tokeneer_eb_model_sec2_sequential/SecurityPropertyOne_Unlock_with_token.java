package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class SecurityPropertyOne_Unlock_with_token{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public SecurityPropertyOne_Unlock_with_token(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> machine.get_fingerprint().has(currentFinger) && machine.get_goodTok().domain().has(machine.get_currentToken()) && machine.get_attCertTokID().range().has(machine.get_currentToken()) && machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) && machine.get_tokenPrivCert().domain().has(machine.get_currentToken()) && machine.get_isValidatedBy().domain().has(machine.get_tokenPrivCert().apply(machine.get_currentToken())) && machine.get_tokenIandaCert().domain().has(machine.get_currentToken()) && machine.get_isValidatedBy().domain().has(machine.get_tokenIandaCert().apply(machine.get_currentToken())) && machine.get_goodFP().apply(currentFinger).equals(machine.goodF) || machine.get_goodTok().domain().has(machine.get_currentToken()) && machine.get_tokenAuthCert().domain().has(machine.get_currentToken()) && machine.get_attCertTokID().range().has(machine.get_currentToken()) && machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) || machine.get_goodTok().domain().has(machine.get_currentAdminToken()) && machine.get_tokenAuthCert().domain().has(machine.get_currentAdminToken()) && machine.get_attCertTokID().range().has(machine.get_currentAdminToken()) && machine.get_authCertRole().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentAdminToken())) && machine.get_authCertRole().apply(machine.get_attCertTokID().inverse().apply(machine.get_currentAdminToken())).equals(machine.guard); */
	public /*@ pure */ boolean guard_SecurityPropertyOne_Unlock_with_token( Integer currentFinger) {
		return 
				machine.get_fingerprint().has(currentFinger) && 
				machine.get_goodTok().domain().has(machine.get_currentToken()) && 
				machine.get_attCertTokID().range().has(machine.get_currentToken()) && 
				machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) && 
				machine.get_tokenPrivCert().domain().has(machine.get_currentToken()) && 
				machine.get_isValidatedBy().domain().has(machine.get_tokenPrivCert().apply(machine.get_currentToken())) && 
				machine.get_tokenIandaCert().domain().has(machine.get_currentToken()) && 
				machine.get_isValidatedBy().domain().has(machine.get_tokenIandaCert().apply(machine.get_currentToken())) &&
				machine.get_goodFP().apply(currentFinger).equals(machine.goodF) 
			|| 
				machine.get_goodTok().domain().has(machine.get_currentToken()) && 
				machine.get_tokenAuthCert().domain().has(machine.get_currentToken()) && 
				machine.get_attCertTokID().range().has(machine.get_currentToken()) && 
				machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) 
			|| 
				machine.get_goodTok().domain().has(machine.get_currentAdminToken()) && 
				machine.get_tokenAuthCert().domain().has(machine.get_currentAdminToken()) && 
				machine.get_attCertTokID().range().has(machine.get_currentAdminToken()) && 
				machine.get_authCertRole().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentAdminToken())) && 
				machine.get_authCertRole().apply(machine.get_attCertTokID().inverse().apply(machine.get_currentAdminToken())).	
						equals(machine.guard);
	}

	/*@ public normal_behavior
		requires guard_SecurityPropertyOne_Unlock_with_token(currentFinger);
		assignable \nothing;
		ensures guard_SecurityPropertyOne_Unlock_with_token(currentFinger) && true; 
	 also
		requires !guard_SecurityPropertyOne_Unlock_with_token(currentFinger);
		assignable \nothing;
		ensures true; */
	public void run_SecurityPropertyOne_Unlock_with_token( Integer currentFinger){
		if(guard_SecurityPropertyOne_Unlock_with_token(currentFinger)) {


			System.out.println("SecurityPropertyOne_Unlock_with_token executed currentFinger: " + currentFinger + " ");
		}
	}

}
