package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class FinishArchiveLogOK{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public FinishArchiveLogOK(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.archiveLog) && machine.get_floppyPresence().equals(machine.present) && machine.get_enclaveStatus2().equals(machine.waitingFinishAdminOp) && machine.get_adminTokenPresence().equals(machine.present) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && !machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoOp)); */
	public /*@ pure */ boolean guard_FinishArchiveLogOK() {
		return (machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.archiveLog) && machine.get_floppyPresence().equals(machine.present) && machine.get_enclaveStatus2().equals(machine.waitingFinishAdminOp) && machine.get_adminTokenPresence().equals(machine.present) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && !machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoOp));
	}

	/*@ public normal_behavior
		requires guard_FinishArchiveLogOK();
		assignable machine.screenMsg2, machine.enclaveStatus2, machine.currentAdminOp;
		ensures guard_FinishArchiveLogOK() &&  machine.get_screenMsg2() == \old(machine.requestAdminOp) &&  machine.get_enclaveStatus2() == \old(machine.enclaveQuiescent) &&  machine.get_currentAdminOp().equals(\old((machine.get_currentAdminOp().override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))))); 
	 also
		requires !guard_FinishArchiveLogOK();
		assignable \nothing;
		ensures true; */
	public void run_FinishArchiveLogOK(){
		if(guard_FinishArchiveLogOK()) {
			Integer screenMsg2_tmp = machine.get_screenMsg2();
			Integer enclaveStatus2_tmp = machine.get_enclaveStatus2();
			BRelation<Integer,Integer> currentAdminOp_tmp = machine.get_currentAdminOp();

			machine.set_screenMsg2(machine.requestAdminOp);
			machine.set_enclaveStatus2(machine.enclaveQuiescent);
			machine.set_currentAdminOp((currentAdminOp_tmp.override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))));

			System.out.println("FinishArchiveLogOK executed ");
		}
	}

}
