package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class RequestEnrolment{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public RequestEnrolment(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_floppyPresence().equals(machine.absent) && machine.get_enclaveStatus1().equals(machine.notEnrolled)); */
	public /*@ pure */ boolean guard_RequestEnrolment() {
		return (machine.get_floppyPresence().equals(machine.absent) && machine.get_enclaveStatus1().equals(machine.notEnrolled));
	}

	/*@ public normal_behavior
		requires guard_RequestEnrolment();
		assignable machine.screenMsg1, machine.displayMessage2;
		ensures guard_RequestEnrolment() &&  machine.get_screenMsg1() == \old(machine.insertEnrolmentData) &&  machine.get_displayMessage2() == \old(machine.blank); 
	 also
		requires !guard_RequestEnrolment();
		assignable \nothing;
		ensures true; */
	public void run_RequestEnrolment(){
		if(guard_RequestEnrolment()) {
			Integer screenMsg1_tmp = machine.get_screenMsg1();
			Integer displayMessage2_tmp = machine.get_displayMessage2();

			machine.set_screenMsg1(machine.insertEnrolmentData);
			machine.set_displayMessage2(machine.blank);

			System.out.println("RequestEnrolment executed ");
		}
	}

}
