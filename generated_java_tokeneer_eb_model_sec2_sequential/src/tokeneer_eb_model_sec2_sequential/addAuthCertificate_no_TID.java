package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class addAuthCertificate_no_TID{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public addAuthCertificate_no_TID(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.CERTIFICATES.difference(machine.get_certificates()).has(addCertificate_ce) && NAT.instance.has(addCertificate_period) && machine.get_publicKeys().has(addCertificate_pubkey) && !machine.get_isValidatedBy().range().has(addCertificate_pubkey) && machine.get_idCert().has(addAttCertificate_idc) && !machine.get_baseCertID().range().has(addAttCertificate_idc) && machine.get_privilege().has(addAuthCertificate_priv) && machine.get_clearence().has(addAuthCertificate_class) && !machine.get_authCertRole().has(new Pair<Integer,Integer>(addCertificate_ce,addAuthCertificate_priv)) && !machine.get_authCertRole().domain().has(addCertificate_ce) && !machine.get_authCertClearence().has(new Pair<Integer,Integer>(addCertificate_ce,addAuthCertificate_class)) && !machine.get_authCertClearence().domain().has(addCertificate_ce) && machine.get_tokenID().difference(machine.get_attCertTokID().range()).has(addPrivCertificate_tid) && !machine.get_attCertTokID().domain().has(addCertificate_ce)); */
	public /*@ pure */ boolean guard_addAuthCertificate_no_TID( Integer addCertificate_ce, Integer addCertificate_period, Integer addCertificate_pubkey, Integer addAttCertificate_idc, Integer addAuthCertificate_class, Integer addAuthCertificate_priv, Integer addPrivCertificate_tid) {
		return (machine.CERTIFICATES.difference(machine.get_certificates()).has(addCertificate_ce) && NAT.instance.has(addCertificate_period) && machine.get_publicKeys().has(addCertificate_pubkey) && !machine.get_isValidatedBy().range().has(addCertificate_pubkey) && machine.get_idCert().has(addAttCertificate_idc) && !machine.get_baseCertID().range().has(addAttCertificate_idc) && machine.get_privilege().has(addAuthCertificate_priv) && machine.get_clearence().has(addAuthCertificate_class) && !machine.get_authCertRole().has(new Pair<Integer,Integer>(addCertificate_ce,addAuthCertificate_priv)) && !machine.get_authCertRole().domain().has(addCertificate_ce) && !machine.get_authCertClearence().has(new Pair<Integer,Integer>(addCertificate_ce,addAuthCertificate_class)) && !machine.get_authCertClearence().domain().has(addCertificate_ce) && machine.get_tokenID().difference(machine.get_attCertTokID().range()).has(addPrivCertificate_tid) && !machine.get_attCertTokID().domain().has(addCertificate_ce));
	}

	/*@ public normal_behavior
		requires guard_addAuthCertificate_no_TID(addCertificate_ce,addCertificate_period,addCertificate_pubkey,addAttCertificate_idc,addAuthCertificate_class,addAuthCertificate_priv,addPrivCertificate_tid);
		assignable machine.certificates, machine.validityPeriods, machine.isValidatedBy, machine.attCert, machine.baseCertID, machine.authCert, machine.authCertRole, machine.authCertClearence, machine.attCertTokID;
		ensures guard_addAuthCertificate_no_TID(addCertificate_ce,addCertificate_period,addCertificate_pubkey,addAttCertificate_idc,addAuthCertificate_class,addAuthCertificate_priv,addPrivCertificate_tid) &&  machine.get_certificates().equals(\old((machine.get_certificates().union(new BSet<Integer>(addCertificate_ce))))) &&  machine.get_validityPeriods().equals(\old((machine.get_validityPeriods().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addCertificate_period)))))) &&  machine.get_isValidatedBy().equals(\old((machine.get_isValidatedBy().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addCertificate_pubkey)))))) &&  machine.get_attCert().equals(\old((machine.get_attCert().union(new BSet<Integer>(addCertificate_ce))))) &&  machine.get_baseCertID().equals(\old((machine.get_baseCertID().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addAttCertificate_idc)))))) &&  machine.get_authCert().equals(\old((machine.get_authCert().union(new BSet<Integer>(addCertificate_ce))))) &&  machine.get_authCertRole().equals(\old((machine.get_authCertRole().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addAuthCertificate_priv)))))) &&  machine.get_authCertClearence().equals(\old((machine.get_authCertClearence().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addAuthCertificate_class)))))) &&  machine.get_attCertTokID().equals(\old((machine.get_attCertTokID().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addPrivCertificate_tid)))))); 
	 also
		requires !guard_addAuthCertificate_no_TID(addCertificate_ce,addCertificate_period,addCertificate_pubkey,addAttCertificate_idc,addAuthCertificate_class,addAuthCertificate_priv,addPrivCertificate_tid);
		assignable \nothing;
		ensures true; */
	public void run_addAuthCertificate_no_TID( Integer addCertificate_ce, Integer addCertificate_period, Integer addCertificate_pubkey, Integer addAttCertificate_idc, Integer addAuthCertificate_class, Integer addAuthCertificate_priv, Integer addPrivCertificate_tid){
		if(guard_addAuthCertificate_no_TID(addCertificate_ce,addCertificate_period,addCertificate_pubkey,addAttCertificate_idc,addAuthCertificate_class,addAuthCertificate_priv,addPrivCertificate_tid)) {
			BSet<Integer> certificates_tmp = machine.get_certificates();
			BRelation<Integer,Integer> validityPeriods_tmp = machine.get_validityPeriods();
			BRelation<Integer,Integer> isValidatedBy_tmp = machine.get_isValidatedBy();
			BSet<Integer> attCert_tmp = machine.get_attCert();
			BRelation<Integer,Integer> baseCertID_tmp = machine.get_baseCertID();
			BSet<Integer> authCert_tmp = machine.get_authCert();
			BRelation<Integer,Integer> authCertRole_tmp = machine.get_authCertRole();
			BRelation<Integer,Integer> authCertClearence_tmp = machine.get_authCertClearence();
			BRelation<Integer,Integer> attCertTokID_tmp = machine.get_attCertTokID();

			machine.set_certificates((certificates_tmp.union(new BSet<Integer>(addCertificate_ce))));
			machine.set_validityPeriods((validityPeriods_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addCertificate_period)))));
			machine.set_isValidatedBy((isValidatedBy_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addCertificate_pubkey)))));
			machine.set_attCert((attCert_tmp.union(new BSet<Integer>(addCertificate_ce))));
			machine.set_baseCertID((baseCertID_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addAttCertificate_idc)))));
			machine.set_authCert((authCert_tmp.union(new BSet<Integer>(addCertificate_ce))));
			machine.set_authCertRole((authCertRole_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addAuthCertificate_priv)))));
			machine.set_authCertClearence((authCertClearence_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addAuthCertificate_class)))));
			machine.set_attCertTokID((attCertTokID_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addPrivCertificate_tid)))));

			System.out.println("addAuthCertificate_no_TID executed addCertificate_ce: " + addCertificate_ce + " addCertificate_period: " + addCertificate_period + " addCertificate_pubkey: " + addCertificate_pubkey + " addAttCertificate_idc: " + addAttCertificate_idc + " addAuthCertificate_class: " + addAuthCertificate_class + " addAuthCertificate_priv: " + addAuthCertificate_priv + " addPrivCertificate_tid: " + addPrivCertificate_tid + " ");
		}
	}

}
