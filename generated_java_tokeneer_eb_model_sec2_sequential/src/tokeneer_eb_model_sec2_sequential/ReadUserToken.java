package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class ReadUserToken{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public ReadUserToken(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.TOKENID.has(ReadUserToken_currentUserToken) && machine.get_entry_status1().equals(machine.quiescent) && machine.get_userTokenPresence().equals(machine.present) && new BSet<Integer>(machine.enclaveQuiescent,machine.waitingRemoveAdminTokenFail).has(machine.get_enclaveStatus1()) && machine.get_status_sec().has(machine.quiescent)) && (machine.variant() >= 0); */
	public /*@ pure */ boolean guard_ReadUserToken( Integer ReadUserToken_currentUserToken) {
		return (
				machine.TOKENID.has(ReadUserToken_currentUserToken) && 
				machine.get_entry_status1().equals(machine.quiescent) && 
				machine.get_userTokenPresence().equals(machine.present) && 
				new BSet<Integer>(machine.enclaveQuiescent,machine.waitingRemoveAdminTokenFail).has(machine.get_enclaveStatus1()) && 
				machine.get_status_sec().has(machine.quiescent)) && 
				(machine.variant() >= 0);
	}

	/*@ public normal_behavior
		requires guard_ReadUserToken(ReadUserToken_currentUserToken);
		assignable machine.entry_status1, machine.currentToken, machine.displayMessage1, machine.status_sec;
		ensures guard_ReadUserToken(ReadUserToken_currentUserToken) &&  machine.get_entry_status1() == \old(machine.gotUserToken) &&  machine.get_currentToken() == \old(ReadUserToken_currentUserToken) &&  machine.get_displayMessage1() == \old(machine.wait) &&  machine.get_status_sec().equals(\old(machine.get_status_sec().difference(new BSet<Integer>(machine.quiescent))))
			 && machine.variant() < \old(machine.variant()); 
	 also
		requires !guard_ReadUserToken(ReadUserToken_currentUserToken);
		assignable \nothing;
		ensures true; */
	public void run_ReadUserToken( Integer ReadUserToken_currentUserToken){
		if(guard_ReadUserToken(ReadUserToken_currentUserToken)) {
			Integer entry_status1_tmp = machine.get_entry_status1();
			Integer currentToken_tmp = machine.get_currentToken();
			Integer displayMessage1_tmp = machine.get_displayMessage1();
			BSet<Integer> status_sec_tmp = machine.get_status_sec();

			machine.set_entry_status1(machine.gotUserToken);
			machine.set_currentToken(ReadUserToken_currentUserToken);
			machine.set_displayMessage1(machine.wait);
			machine.set_status_sec(status_sec_tmp.difference(new BSet<Integer>(machine.quiescent)));

			System.out.println("ReadUserToken executed ReadUserToken_currentUserToken: " + ReadUserToken_currentUserToken + " ");
		}
	}

}
