package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class BioCheckRequired{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public BioCheckRequired(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_entry_status1().equals(machine.gotUserToken) && NAT.instance.has(currentTime) && machine.get_tokenAuthCert().domain().has(machine.get_currentToken()) && !machine.get_validityPeriods().image(new BSet<Integer>(machine.get_tokenAuthCert().apply(machine.get_currentToken()))).has(currentTime) || !machine.get_tokenAuthCert().domain().has(machine.get_currentToken()) && machine.get_attCertTokID().range().has(machine.get_currentToken()) && machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) && machine.get_tokenPrivCert().domain().has(machine.get_currentToken()) && machine.get_isValidatedBy().domain().has(machine.get_tokenPrivCert().apply(machine.get_currentToken())) && machine.get_tokenIandaCert().domain().has(machine.get_currentToken()) && machine.get_isValidatedBy().domain().has(machine.get_tokenIandaCert().apply(machine.get_currentToken())) && machine.get_userTokenPresence().equals(machine.present)); */
	public /*@ pure */ boolean guard_BioCheckRequired( Integer currentTime) {
		return (
			machine.get_entry_status1().equals(machine.gotUserToken) && 
			NAT.instance.has(currentTime) && 
			machine.get_tokenAuthCert().domain().has(machine.get_currentToken()) && 
			!machine.get_validityPeriods().image(new BSet<Integer>(machine.get_tokenAuthCert().apply(
							machine.get_currentToken()))).has(currentTime) 
				|| 
							!machine.get_tokenAuthCert().domain().has(machine.get_currentToken()) && 
			machine.get_attCertTokID().range().has(machine.get_currentToken()) && 
			machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) && 
			machine.get_tokenPrivCert().domain().has(machine.get_currentToken()) && 
			machine.get_isValidatedBy().domain().has(machine.get_tokenPrivCert().apply(machine.get_currentToken())) && 
			machine.get_tokenIandaCert().domain().has(machine.get_currentToken()) && 
			machine.get_isValidatedBy().domain().has(machine.get_tokenIandaCert().apply(machine.get_currentToken())) && 
			machine.get_userTokenPresence().equals(machine.present));
	}

	/*@ public normal_behavior
		requires guard_BioCheckRequired(currentTime);
		assignable machine.entry_status1, machine.displayMessage1;
		ensures guard_BioCheckRequired(currentTime) &&  machine.get_entry_status1() == \old(machine.waitingFinger) &&  machine.get_displayMessage1() == \old(machine.insertFinger); 
	 also
		requires !guard_BioCheckRequired(currentTime);
		assignable \nothing;
		ensures true; */
	public void run_BioCheckRequired( Integer currentTime){
		if(guard_BioCheckRequired(currentTime)) {
			Integer entry_status1_tmp = machine.get_entry_status1();
			Integer displayMessage1_tmp = machine.get_displayMessage1();

			machine.set_entry_status1(machine.waitingFinger);
			machine.set_displayMessage1(machine.insertFinger);

			System.out.println("BioCheckRequired executed currentTime: " + currentTime + " ");
		}
	}

}
