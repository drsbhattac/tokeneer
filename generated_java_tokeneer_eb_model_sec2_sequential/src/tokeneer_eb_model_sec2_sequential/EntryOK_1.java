package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class EntryOK_1{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public EntryOK_1(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_entry_status1().equals(machine.waitingEntry) && machine.get_goodTok().domain().has(machine.get_currentToken()) && machine.get_goodTok().apply(machine.get_currentToken()).equals(machine.goodT) && machine.get_userTokenPresence().equals(machine.present) && NAT.instance.has(currentTime) && machine.get_tokenID().has(machine.get_currentToken()) && machine.get_attCertTokID().inverse().domain().has(machine.get_currentToken()) && machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) && machine.get_isValidatedBy().domain().has(machine.get_tokenPrivCert().apply(machine.get_currentToken())) && machine.get_isValidatedBy().domain().has(machine.get_tokenIandaCert().apply(machine.get_currentToken())) && machine.get_entryPeriod().apply(machine.get_privCertRole().apply(machine.get_tokenPrivCert().apply(machine.get_currentToken()))).apply(machine.get_privCertClearence().apply(machine.get_tokenPrivCert().apply(machine.get_currentToken()))).has(currentTime) && machine.get_status_sec().has(machine.waitingEntry)) && (machine.variant() >= 0); */
	public /*@ pure */ boolean guard_EntryOK_1( Integer currentTime) {
		return (
				machine.get_entry_status1().equals(machine.waitingEntry) && 
				machine.get_goodTok().domain().has(machine.get_currentToken()) && 
				machine.get_goodTok().apply(machine.get_currentToken()).equals(machine.goodT) && 
				machine.get_userTokenPresence().equals(machine.present) && 
				NAT.instance.has(currentTime) && 
				machine.get_tokenID().has(machine.get_currentToken()) && 
				machine.get_attCertTokID().inverse().domain().has(machine.get_currentToken()) && 
				machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) && machine.get_isValidatedBy().domain().has(machine.get_tokenPrivCert().apply(machine.get_currentToken())) && machine.get_isValidatedBy().domain().has(machine.get_tokenIandaCert().apply(machine.get_currentToken())) && machine.get_entryPeriod().apply(machine.get_privCertRole().apply(machine.get_tokenPrivCert().apply(machine.get_currentToken()))).apply(machine.get_privCertClearence().apply(machine.get_tokenPrivCert().apply(machine.get_currentToken()))).has(currentTime) && machine.get_status_sec().has(machine.waitingEntry)) && (machine.variant() >= 0);
	}

	/*@ public normal_behavior
		requires guard_EntryOK_1(currentTime);
		assignable machine.entry_status1, machine.displayMessage1, machine.tokenRemovalTimeout, machine.status_sec;
		ensures guard_EntryOK_1(currentTime) &&  machine.get_entry_status1() == \old(machine.waitingRemoveTokenSuccess) &&  machine.get_displayMessage1() == \old(machine.openDoor) &&  machine.get_tokenRemovalTimeout() == \old(new Integer(currentTime + machine.get_tokenRemovalDuration())) &&  machine.get_status_sec().equals(\old(machine.get_status_sec().difference(new BSet<Integer>(machine.waitingEntry))))
			 && machine.variant() < \old(machine.variant()); 
	 also
		requires !guard_EntryOK_1(currentTime);
		assignable \nothing;
		ensures true; */
	public void run_EntryOK_1( Integer currentTime){
		if(guard_EntryOK_1(currentTime)) {
			Integer entry_status1_tmp = machine.get_entry_status1();
			Integer displayMessage1_tmp = machine.get_displayMessage1();
			Integer tokenRemovalTimeout_tmp = machine.get_tokenRemovalTimeout();
			BSet<Integer> status_sec_tmp = machine.get_status_sec();

			machine.set_entry_status1(machine.waitingRemoveTokenSuccess);
			machine.set_displayMessage1(machine.openDoor);
			machine.set_tokenRemovalTimeout(new Integer(currentTime + machine.get_tokenRemovalDuration()));
			machine.set_status_sec(status_sec_tmp.difference(new BSet<Integer>(machine.waitingEntry)));

			System.out.println("EntryOK_1 executed currentTime: " + currentTime + " ");
		}
	}

}
