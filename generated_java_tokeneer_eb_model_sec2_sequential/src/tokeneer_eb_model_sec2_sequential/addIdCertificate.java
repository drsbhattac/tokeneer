package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class addIdCertificate{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public addIdCertificate(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.CERTIFICATES.difference(machine.get_certificates()).has(addCertificate_ce) && NAT.instance.has(addCertificate_period) && machine.get_publicKeys().has(addCertificate_pubkey) && !machine.get_isValidatedBy().range().has(addCertificate_pubkey) && !machine.get_subject().range().has(addIdCertificate_sub) && machine.get_user().has(addIdCertificate_sub) && machine.get_publicKeys().has(addIdCertificate_usrPubKey) && !machine.get_subjectPubKey().domain().has(addIdCertificate_sub) && !machine.get_subjectPubKey().range().has(addIdCertificate_usrPubKey)); */
	public /*@ pure */ boolean guard_addIdCertificate( Integer addCertificate_ce, Integer addCertificate_period, Integer addCertificate_pubkey, Integer addIdCertificate_sub, Integer addIdCertificate_usrPubKey) {
		return (machine.CERTIFICATES.difference(machine.get_certificates()).has(addCertificate_ce) && NAT.instance.has(addCertificate_period) && machine.get_publicKeys().has(addCertificate_pubkey) && !machine.get_isValidatedBy().range().has(addCertificate_pubkey) && !machine.get_subject().range().has(addIdCertificate_sub) && machine.get_user().has(addIdCertificate_sub) && machine.get_publicKeys().has(addIdCertificate_usrPubKey) && !machine.get_subjectPubKey().domain().has(addIdCertificate_sub) && !machine.get_subjectPubKey().range().has(addIdCertificate_usrPubKey));
	}

	/*@ public normal_behavior
		requires guard_addIdCertificate(addCertificate_ce,addCertificate_period,addCertificate_pubkey,addIdCertificate_sub,addIdCertificate_usrPubKey);
		assignable machine.certificates, machine.validityPeriods, machine.isValidatedBy, machine.idCert, machine.subject, machine.subjectPubKey;
		ensures guard_addIdCertificate(addCertificate_ce,addCertificate_period,addCertificate_pubkey,addIdCertificate_sub,addIdCertificate_usrPubKey) &&  machine.get_certificates().equals(\old((machine.get_certificates().union(new BSet<Integer>(addCertificate_ce))))) &&  machine.get_validityPeriods().equals(\old((machine.get_validityPeriods().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addCertificate_period)))))) &&  machine.get_isValidatedBy().equals(\old((machine.get_isValidatedBy().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addCertificate_pubkey)))))) &&  machine.get_idCert().equals(\old((machine.get_idCert().union(new BSet<Integer>(addCertificate_ce))))) &&  machine.get_subject().equals(\old((machine.get_subject().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addIdCertificate_sub)))))) &&  machine.get_subjectPubKey().equals(\old((machine.get_subjectPubKey().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addIdCertificate_sub,addIdCertificate_usrPubKey)))))); 
	 also
		requires !guard_addIdCertificate(addCertificate_ce,addCertificate_period,addCertificate_pubkey,addIdCertificate_sub,addIdCertificate_usrPubKey);
		assignable \nothing;
		ensures true; */
	public void run_addIdCertificate( Integer addCertificate_ce, Integer addCertificate_period, Integer addCertificate_pubkey, Integer addIdCertificate_sub, Integer addIdCertificate_usrPubKey){
		if(guard_addIdCertificate(addCertificate_ce,addCertificate_period,addCertificate_pubkey,addIdCertificate_sub,addIdCertificate_usrPubKey)) {
			BSet<Integer> certificates_tmp = machine.get_certificates();
			BRelation<Integer,Integer> validityPeriods_tmp = machine.get_validityPeriods();
			BRelation<Integer,Integer> isValidatedBy_tmp = machine.get_isValidatedBy();
			BSet<Integer> idCert_tmp = machine.get_idCert();
			BRelation<Integer,Integer> subject_tmp = machine.get_subject();
			BRelation<Integer,Integer> subjectPubKey_tmp = machine.get_subjectPubKey();

			machine.set_certificates((certificates_tmp.union(new BSet<Integer>(addCertificate_ce))));
			machine.set_validityPeriods((validityPeriods_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addCertificate_period)))));
			machine.set_isValidatedBy((isValidatedBy_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addCertificate_pubkey)))));
			machine.set_idCert((idCert_tmp.union(new BSet<Integer>(addCertificate_ce))));
			machine.set_subject((subject_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addIdCertificate_sub)))));
			machine.set_subjectPubKey((subjectPubKey_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addIdCertificate_sub,addIdCertificate_usrPubKey)))));

			System.out.println("addIdCertificate executed addCertificate_ce: " + addCertificate_ce + " addCertificate_period: " + addCertificate_period + " addCertificate_pubkey: " + addCertificate_pubkey + " addIdCertificate_sub: " + addIdCertificate_sub + " addIdCertificate_usrPubKey: " + addIdCertificate_usrPubKey + " ");
		}
	}

}
