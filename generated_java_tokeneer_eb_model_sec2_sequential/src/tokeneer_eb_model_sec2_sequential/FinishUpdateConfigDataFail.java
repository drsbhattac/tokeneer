package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class FinishUpdateConfigDataFail{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public FinishUpdateConfigDataFail(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.updateConfigData) && machine.get_enclaveStatus2().equals(machine.waitingFinishAdminOp) && machine.get_adminTokenPresence().equals(machine.present) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && !machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoOp) && machine.FLOPPY.has(FinishUpdateConfigDataFail_currentFloppy) && !machine.get_configFile().range().has(FinishUpdateConfigDataFail_currentFloppy)); */
	public /*@ pure */ boolean guard_FinishUpdateConfigDataFail( Integer FinishUpdateConfigDataFail_currentFloppy) {
		return (machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.updateConfigData) && machine.get_enclaveStatus2().equals(machine.waitingFinishAdminOp) && machine.get_adminTokenPresence().equals(machine.present) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && !machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoOp) && machine.FLOPPY.has(FinishUpdateConfigDataFail_currentFloppy) && !machine.get_configFile().range().has(FinishUpdateConfigDataFail_currentFloppy));
	}

	/*@ public normal_behavior
		requires guard_FinishUpdateConfigDataFail(FinishUpdateConfigDataFail_currentFloppy);
		assignable machine.screenMsg2, machine.enclaveStatus2, machine.currentAdminOp;
		ensures guard_FinishUpdateConfigDataFail(FinishUpdateConfigDataFail_currentFloppy) &&  machine.get_screenMsg2() == \old(machine.invalidData) &&  machine.get_enclaveStatus2() == \old(machine.enclaveQuiescent) &&  machine.get_currentAdminOp().equals(\old((machine.get_currentAdminOp().override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))))); 
	 also
		requires !guard_FinishUpdateConfigDataFail(FinishUpdateConfigDataFail_currentFloppy);
		assignable \nothing;
		ensures true; */
	public void run_FinishUpdateConfigDataFail( Integer FinishUpdateConfigDataFail_currentFloppy){
		if(guard_FinishUpdateConfigDataFail(FinishUpdateConfigDataFail_currentFloppy)) {
			Integer screenMsg2_tmp = machine.get_screenMsg2();
			Integer enclaveStatus2_tmp = machine.get_enclaveStatus2();
			BRelation<Integer,Integer> currentAdminOp_tmp = machine.get_currentAdminOp();

			machine.set_screenMsg2(machine.invalidData);
			machine.set_enclaveStatus2(machine.enclaveQuiescent);
			machine.set_currentAdminOp((currentAdminOp_tmp.override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))));

			System.out.println("FinishUpdateConfigDataFail executed FinishUpdateConfigDataFail_currentFloppy: " + FinishUpdateConfigDataFail_currentFloppy + " ");
		}
	}

}
