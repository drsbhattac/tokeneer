package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class doNothing{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public doNothing(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> machine.get_entry_status1().equals(machine.waitingRemoveTokenSuccess) || machine.get_entry_status1().equals(machine.waitingRemoveTokenFail) || machine.get_entry_status1().equals(machine.gotFinger); */
	public /*@ pure */ boolean guard_doNothing() {
		return machine.get_entry_status1().equals(machine.waitingRemoveTokenSuccess) || machine.get_entry_status1().equals(machine.waitingRemoveTokenFail) || machine.get_entry_status1().equals(machine.gotFinger);
	}

	/*@ public normal_behavior
		requires guard_doNothing();
		assignable machine.displayMessage1;
		ensures guard_doNothing() &&  machine.get_displayMessage1() == \old(machine.blank); 
	 also
		requires !guard_doNothing();
		assignable \nothing;
		ensures true; */
	public void run_doNothing(){
		if(guard_doNothing()) {
			Integer displayMessage1_tmp = machine.get_displayMessage1();

			machine.set_displayMessage1(machine.blank);

			System.out.println("doNothing executed ");
		}
	}

}
