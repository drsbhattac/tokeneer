package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class ValidateUserTokenFail{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public ValidateUserTokenFail(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_entry_status1().equals(machine.gotUserToken) && NAT.instance.has(currentTime) && machine.get_tokenAuthCert().domain().has(machine.get_currentToken()) && !machine.get_validityPeriods().image(new BSet<Integer>(machine.get_tokenAuthCert().apply(machine.get_currentToken()))).has(currentTime) || !machine.get_tokenAuthCert().domain().has(machine.get_currentToken()) && machine.get_attCertTokID().range().has(machine.get_currentToken()) && !machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) || machine.get_tokenPrivCert().domain().has(machine.get_currentToken()) && !machine.get_isValidatedBy().domain().has(machine.get_tokenPrivCert().apply(machine.get_currentToken())) || machine.get_tokenIandaCert().domain().has(machine.get_currentToken()) && !machine.get_isValidatedBy().domain().has(machine.get_tokenIandaCert().apply(machine.get_currentToken())) && machine.get_userTokenPresence().equals(machine.present)); */
	public /*@ pure */ boolean guard_ValidateUserTokenFail( Integer currentTime) {
		return (
			machine.get_entry_status1().equals(machine.gotUserToken) && 
			NAT.instance.has(currentTime) && 
			machine.get_tokenAuthCert().domain().has(machine.get_currentToken()) && 
			!machine.get_validityPeriods().image(new BSet<Integer>(machine.get_tokenAuthCert()
					.apply(machine.get_currentToken()))).has(currentTime) 
				|| 
					!machine.get_tokenAuthCert().domain().has(machine.get_currentToken()) && 
			machine.get_attCertTokID().range().has(machine.get_currentToken()) && 
			!machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse()
					.apply(machine.get_currentToken())) 
				|| 
				machine.get_tokenPrivCert().domain().has(machine.get_currentToken()) && 
			!machine.get_isValidatedBy().domain().has(machine.get_tokenPrivCert().apply(machine.get_currentToken())) || machine.get_tokenIandaCert().domain().has(machine.get_currentToken()) && 
			!machine.get_isValidatedBy().domain().has(machine.get_tokenIandaCert().apply(machine.get_currentToken())) && 
			machine.get_userTokenPresence().equals(machine.present));
	}

	/*@ public normal_behavior
		requires guard_ValidateUserTokenFail(currentTime);
		assignable machine.entry_status1, machine.displayMessage1;
		ensures guard_ValidateUserTokenFail(currentTime) &&  machine.get_entry_status1() == \old(machine.waitingRemoveTokenFail) &&  machine.get_displayMessage1() == \old(machine.removeToken); 
	 also
		requires !guard_ValidateUserTokenFail(currentTime);
		assignable \nothing;
		ensures true; */
	public void run_ValidateUserTokenFail( Integer currentTime){
		if(guard_ValidateUserTokenFail(currentTime)) {
			Integer entry_status1_tmp = machine.get_entry_status1();
			Integer displayMessage1_tmp = machine.get_displayMessage1();

			machine.set_entry_status1(machine.waitingRemoveTokenFail);
			machine.set_displayMessage1(machine.removeToken);

			System.out.println("ValidateUserTokenFail executed currentTime: " + currentTime + " ");
		}
	}

}
