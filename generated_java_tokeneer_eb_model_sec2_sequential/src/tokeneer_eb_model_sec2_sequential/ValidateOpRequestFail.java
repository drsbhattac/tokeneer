package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class ValidateOpRequestFail{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public ValidateOpRequestFail(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_enclaveStatus2().equals(machine.enclaveQuiescent) && machine.get_adminTokenPresence().equals(machine.present) && machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && new BSet<Integer>(machine.quiescent,machine.waitingRemoveTokenFail).has(machine.get_entry_status2()) && machine.get_keyedDataPresence().equals(machine.present) && !machine.get_keyedOps().inverse().domain().has(ValidateOpRequestFail_currentKeyedData)); */
	public /*@ pure */ boolean guard_ValidateOpRequestFail( Integer ValidateOpRequestFail_currentKeyedData) {
		return (
			machine.get_enclaveStatus2().equals(machine.enclaveQuiescent) && 
			machine.get_adminTokenPresence().equals(machine.present) && 
			machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && 
			!machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()))
					.equals(machine.NoRole) && 
			new BSet<Integer>(machine.quiescent,machine.waitingRemoveTokenFail).has(machine.get_entry_status2()) && 
			machine.get_keyedDataPresence().equals(machine.present) && 
			!machine.get_keyedOps().inverse().domain().has(ValidateOpRequestFail_currentKeyedData));
	}

	/*@ public normal_behavior
		requires guard_ValidateOpRequestFail(ValidateOpRequestFail_currentKeyedData);
		assignable machine.screenMsg2;
		ensures guard_ValidateOpRequestFail(ValidateOpRequestFail_currentKeyedData) &&  machine.get_screenMsg2() == \old(machine.invalidRequest); 
	 also
		requires !guard_ValidateOpRequestFail(ValidateOpRequestFail_currentKeyedData);
		assignable \nothing;
		ensures true; */
	public void run_ValidateOpRequestFail( Integer ValidateOpRequestFail_currentKeyedData){
		if(guard_ValidateOpRequestFail(ValidateOpRequestFail_currentKeyedData)) {
			Integer screenMsg2_tmp = machine.get_screenMsg2();

			machine.set_screenMsg2(machine.invalidRequest);

			System.out.println("ValidateOpRequestFail executed ValidateOpRequestFail_currentKeyedData: " + ValidateOpRequestFail_currentKeyedData + " ");
		}
	}

}
