package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class ShutdownOK{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public ShutdownOK(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_enclaveStatus2().equals(machine.waitingStartAdminOp) && machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.shutdownOp) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && machine.get_currentDoor().equals(machine.closed)); */
	public /*@ pure */ boolean guard_ShutdownOK() {
		return (machine.get_enclaveStatus2().equals(machine.waitingStartAdminOp) && machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.shutdownOp) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && machine.get_currentDoor().equals(machine.closed));
	}

	/*@ public normal_behavior
		requires guard_ShutdownOK();
		assignable machine.screenMsg2, machine.enclaveStatus2, machine.displayMessage3, machine.rolePresent, machine.currentAdminOp, machine.availableOps;
		ensures guard_ShutdownOK() &&  machine.get_screenMsg2() == \old(machine.clear) &&  machine.get_enclaveStatus2() == \old(machine.shutdown) &&  machine.get_displayMessage3() == \old(machine.blank) &&  machine.get_rolePresent().equals(\old((machine.get_rolePresent().override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoRole)))))) &&  machine.get_currentAdminOp().equals(\old((machine.get_currentAdminOp().override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))))) &&  machine.get_availableOps().equals(\old((machine.get_availableOps().override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))))); 
	 also
		requires !guard_ShutdownOK();
		assignable \nothing;
		ensures true; */
	public void run_ShutdownOK(){
		if(guard_ShutdownOK()) {
			Integer screenMsg2_tmp = machine.get_screenMsg2();
			Integer enclaveStatus2_tmp = machine.get_enclaveStatus2();
			Integer displayMessage3_tmp = machine.get_displayMessage3();
			BRelation<Integer,Integer> rolePresent_tmp = machine.get_rolePresent();
			BRelation<Integer,Integer> currentAdminOp_tmp = machine.get_currentAdminOp();
			BRelation<Integer,Integer> availableOps_tmp = machine.get_availableOps();

			machine.set_screenMsg2(machine.clear);
			machine.set_enclaveStatus2(machine.shutdown);
			machine.set_displayMessage3(machine.blank);
			machine.set_rolePresent((rolePresent_tmp.override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoRole)))));
			machine.set_currentAdminOp((currentAdminOp_tmp.override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))));
			machine.set_availableOps((availableOps_tmp.override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))));

			System.out.println("ShutdownOK executed ");
		}
	}

}
