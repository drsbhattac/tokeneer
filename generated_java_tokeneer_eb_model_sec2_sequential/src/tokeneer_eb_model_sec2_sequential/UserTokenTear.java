package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class UserTokenTear{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public UserTokenTear(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> new BSet<Integer>(machine.gotUserToken,machine.waitingUpdateToken,machine.waitingFinger,machine.gotFinger,machine.waitingEntry).has(machine.get_entry_status1()); */
	public /*@ pure */ boolean guard_UserTokenTear() {
		return new BSet<Integer>(machine.gotUserToken,machine.waitingUpdateToken,machine.waitingFinger,machine.gotFinger,machine.waitingEntry).has(machine.get_entry_status1());
	}

	/*@ public normal_behavior
		requires guard_UserTokenTear();
		assignable machine.entry_status1, machine.displayMessage1, machine.userTokenPresence, machine.status_sec;
		ensures guard_UserTokenTear() &&  machine.get_entry_status1() == \old(machine.quiescent) &&  machine.get_displayMessage1() == \old(machine.welcome) &&  machine.get_userTokenPresence() == \old(machine.absent) &&  machine.get_status_sec().equals(\old(new BSet<Integer>(machine.quiescent,machine.gotUserToken,machine.waitingEntry,machine.waitingRemoveTokenSuccess))); 
	 also
		requires !guard_UserTokenTear();
		assignable \nothing;
		ensures true; */
	public void run_UserTokenTear(){
		if(guard_UserTokenTear()) {
			Integer entry_status1_tmp = machine.get_entry_status1();
			Integer displayMessage1_tmp = machine.get_displayMessage1();
			Integer userTokenPresence_tmp = machine.get_userTokenPresence();
			BSet<Integer> status_sec_tmp = machine.get_status_sec();

			machine.set_entry_status1(machine.quiescent);
			machine.set_displayMessage1(machine.welcome);
			machine.set_userTokenPresence(machine.absent);
			machine.set_status_sec(new BSet<Integer>(machine.quiescent,machine.gotUserToken,machine.waitingEntry,machine.waitingRemoveTokenSuccess));

			System.out.println("UserTokenTear executed ");
		}
	}

}
