package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class StartArchiveLogOK{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public StartArchiveLogOK(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.archiveLog) && machine.get_floppyPresence().equals(machine.present) && machine.get_enclaveStatus2().equals(machine.waitingStartAdminOp) && machine.get_adminTokenPresence().equals(machine.present)); */
	public /*@ pure */ boolean guard_StartArchiveLogOK() {
		return (machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.archiveLog) && machine.get_floppyPresence().equals(machine.present) && machine.get_enclaveStatus2().equals(machine.waitingStartAdminOp) && machine.get_adminTokenPresence().equals(machine.present));
	}

	/*@ public normal_behavior
		requires guard_StartArchiveLogOK();
		assignable machine.screenMsg2, machine.enclaveStatus2;
		ensures guard_StartArchiveLogOK() &&  machine.get_screenMsg2() == \old(machine.doingOp) &&  machine.get_enclaveStatus2() == \old(machine.waitingFinishAdminOp); 
	 also
		requires !guard_StartArchiveLogOK();
		assignable \nothing;
		ensures true; */
	public void run_StartArchiveLogOK(){
		if(guard_StartArchiveLogOK()) {
			Integer screenMsg2_tmp = machine.get_screenMsg2();
			Integer enclaveStatus2_tmp = machine.get_enclaveStatus2();

			machine.set_screenMsg2(machine.doingOp);
			machine.set_enclaveStatus2(machine.waitingFinishAdminOp);

			System.out.println("StartArchiveLogOK executed ");
		}
	}

}
