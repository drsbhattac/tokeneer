package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class SecurityPropertyTwo_Unlock_at_allowed_time{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public SecurityPropertyTwo_Unlock_at_allowed_time(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.time.has(currentTime) && ((machine.time).pow()).has(recentTime) && recentTime.isSubset(machine.get_timesRecentTo().apply(currentTime)) && machine.get_goodTok().domain().has(machine.get_currentToken()) && machine.get_attCertTokID().range().has(machine.get_currentToken()) && machine.get_tokenPrivCert().domain().has(machine.get_currentToken()) && machine.get_tokenIandaCert().domain().has(machine.get_currentToken()) && machine.get_privCertRole().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) && machine.get_privCertClearence().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) && recentTime.isSubset(machine.get_entryPeriod().apply(machine.get_privCertRole().apply(machine.get_attCertTokID().inverse().apply(machine.get_currentToken()))).apply(machine.get_privCertClearence().apply(machine.get_attCertTokID().inverse().apply(machine.get_currentToken()))))); */
	public /*@ pure */ boolean guard_SecurityPropertyTwo_Unlock_at_allowed_time( Integer currentTime, BSet<Integer> recentTime) {
		return (
			machine.time.has(currentTime) && 
			(recentTime.isSubset(machine.time)) && 
			recentTime.isSubset(machine.get_timesRecentTo().apply(currentTime)) && 
			machine.get_goodTok().domain().has(machine.get_currentToken()) && 
			machine.get_attCertTokID().range().has(machine.get_currentToken()) && 
			machine.get_tokenPrivCert().domain().has(machine.get_currentToken()) && 
			machine.get_tokenIandaCert().domain().has(machine.get_currentToken()) && 
			machine.get_privCertRole().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) && 
			machine.get_privCertClearence().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) && 
			recentTime.isSubset(machine.get_entryPeriod().apply(machine.get_privCertRole().apply(
					machine.get_attCertTokID().inverse().apply(machine.get_currentToken()))).apply(
								machine.get_privCertClearence().apply(machine.get_attCertTokID().inverse().apply(
										machine.get_currentToken())))));
	}

	/*@ public normal_behavior
		requires guard_SecurityPropertyTwo_Unlock_at_allowed_time(currentTime,recentTime);
		assignable \nothing;
		ensures guard_SecurityPropertyTwo_Unlock_at_allowed_time(currentTime,recentTime) && true; 
	 also
		requires !guard_SecurityPropertyTwo_Unlock_at_allowed_time(currentTime,recentTime);
		assignable \nothing;
		ensures true; */
	public void run_SecurityPropertyTwo_Unlock_at_allowed_time( Integer currentTime, BSet<Integer> recentTime){
		if(guard_SecurityPropertyTwo_Unlock_at_allowed_time(currentTime,recentTime)) {


			System.out.println("SecurityPropertyTwo_Unlock_at_allowed_time executed currentTime: " + currentTime + " recentTime: " + recentTime + " ");
		}
	}

}
