package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class WriteUserTokenFail{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public WriteUserTokenFail(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_entry_status1().equals(machine.waitingUpdateToken) && machine.get_userTokenPresence().equals(machine.present)); */
	public /*@ pure */ boolean guard_WriteUserTokenFail() {
		return (machine.get_entry_status1().equals(machine.waitingUpdateToken) && machine.get_userTokenPresence().equals(machine.present));
	}

	/*@ public normal_behavior
		requires guard_WriteUserTokenFail();
		assignable machine.entry_status1, machine.displayMessage1;
		ensures guard_WriteUserTokenFail() &&  machine.get_entry_status1() == \old(machine.waitingEntry) &&  machine.get_displayMessage1() == \old(machine.tokenUpdateFailed); 
	 also
		requires !guard_WriteUserTokenFail();
		assignable \nothing;
		ensures true; */
	public void run_WriteUserTokenFail(){
		if(guard_WriteUserTokenFail()) {
			Integer entry_status1_tmp = machine.get_entry_status1();
			Integer displayMessage1_tmp = machine.get_displayMessage1();

			machine.set_entry_status1(machine.waitingEntry);
			machine.set_displayMessage1(machine.tokenUpdateFailed);

			System.out.println("WriteUserTokenFail executed ");
		}
	}

}
