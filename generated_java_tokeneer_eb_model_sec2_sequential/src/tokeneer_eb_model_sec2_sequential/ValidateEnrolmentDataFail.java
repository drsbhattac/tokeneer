package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class ValidateEnrolmentDataFail{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public ValidateEnrolmentDataFail(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.FLOPPY.has(ValidateEnrolmentDataFail_currentFloppy) && !machine.get_validEnrol().has(machine.get_enrolmentFile().apply(ValidateEnrolmentDataFail_currentFloppy)) && machine.get_enclaveStatus1().equals(machine.waitingEnrol)); */
	public /*@ pure */ boolean guard_ValidateEnrolmentDataFail( Integer ValidateEnrolmentDataFail_currentFloppy) {
		return (
				machine.FLOPPY.has(ValidateEnrolmentDataFail_currentFloppy) && 
				!machine.get_validEnrol().has(machine.get_enrolmentFile().apply(ValidateEnrolmentDataFail_currentFloppy)) && 
				machine.get_enclaveStatus1().equals(machine.waitingEnrol));
	}

	/*@ public normal_behavior
		requires guard_ValidateEnrolmentDataFail(ValidateEnrolmentDataFail_currentFloppy);
		assignable machine.screenMsg1, machine.enclaveStatus1, machine.displayMessage2;
		ensures guard_ValidateEnrolmentDataFail(ValidateEnrolmentDataFail_currentFloppy) &&  machine.get_screenMsg1() == \old(machine.enrolmentFailed) &&  machine.get_enclaveStatus1() == \old(machine.waitingEndEnrol) &&  machine.get_displayMessage2() == \old(machine.blank); 
	 also
		requires !guard_ValidateEnrolmentDataFail(ValidateEnrolmentDataFail_currentFloppy);
		assignable \nothing;
		ensures true; */
	public void run_ValidateEnrolmentDataFail( Integer ValidateEnrolmentDataFail_currentFloppy){
		if(guard_ValidateEnrolmentDataFail(ValidateEnrolmentDataFail_currentFloppy)) {
			Integer screenMsg1_tmp = machine.get_screenMsg1();
			Integer enclaveStatus1_tmp = machine.get_enclaveStatus1();
			Integer displayMessage2_tmp = machine.get_displayMessage2();

			machine.set_screenMsg1(machine.enrolmentFailed);
			machine.set_enclaveStatus1(machine.waitingEndEnrol);
			machine.set_displayMessage2(machine.blank);

			System.out.println("ValidateEnrolmentDataFail executed ValidateEnrolmentDataFail_currentFloppy: " + ValidateEnrolmentDataFail_currentFloppy + " ");
		}
	}

}
