package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class ValidateOpRequestOK{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public ValidateOpRequestOK(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_keyedDataPresence().equals(machine.present) && machine.get_keyedOps().inverse().domain().has(ValidateOpRequestOK_currentKeyedData) && machine.get_enclaveStatus2().equals(machine.enclaveQuiescent) && machine.get_adminTokenPresence().equals(machine.present) && machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && new BSet<Integer>(machine.quiescent,machine.waitingRemoveTokenFail).has(machine.get_entry_status2()) && !machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoOp)); */
	public /*@ pure */ boolean guard_ValidateOpRequestOK( Integer ValidateOpRequestOK_currentKeyedData) {
		
		return (
			machine.get_keyedDataPresence().equals(machine.present) && 
			machine.get_keyedOps().inverse().domain().has(ValidateOpRequestOK_currentKeyedData) && 
			machine.get_enclaveStatus2().equals(machine.enclaveQuiescent) && 
			machine.get_adminTokenPresence().equals(machine.present) && 
			machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && 
			!machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()))
						.equals(machine.NoRole) && 
			new BSet<Integer>(machine.quiescent,machine.waitingRemoveTokenFail).has(machine.get_entry_status2()) && 
			!machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()))
							.equals(machine.NoOp));
	}

	/*@ public normal_behavior
		requires guard_ValidateOpRequestOK(ValidateOpRequestOK_currentKeyedData);
		assignable machine.screenMsg2, machine.enclaveStatus2, machine.currentAdminOp;
		ensures guard_ValidateOpRequestOK(ValidateOpRequestOK_currentKeyedData) &&  machine.get_screenMsg2() == \old(machine.doingOp) &&  machine.get_enclaveStatus2() == \old(machine.waitingStartAdminOp) &&  machine.get_currentAdminOp().equals(\old((machine.get_currentAdminOp().override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.get_keyedOps().inverse().apply(ValidateOpRequestOK_currentKeyedData))))))); 
	 also
		requires !guard_ValidateOpRequestOK(ValidateOpRequestOK_currentKeyedData);
		assignable \nothing;
		ensures true; */
	public void run_ValidateOpRequestOK( Integer ValidateOpRequestOK_currentKeyedData){
		if(guard_ValidateOpRequestOK(ValidateOpRequestOK_currentKeyedData)) {
			Integer screenMsg2_tmp = machine.get_screenMsg2();
			Integer enclaveStatus2_tmp = machine.get_enclaveStatus2();
			BRelation<Integer,Integer> currentAdminOp_tmp = machine.get_currentAdminOp();

			machine.set_screenMsg2(machine.doingOp);
			machine.set_enclaveStatus2(machine.waitingStartAdminOp);
			machine.set_currentAdminOp((currentAdminOp_tmp.override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.get_keyedOps().inverse().apply(ValidateOpRequestOK_currentKeyedData))))));

			System.out.println("ValidateOpRequestOK executed ValidateOpRequestOK_currentKeyedData: " + ValidateOpRequestOK_currentKeyedData + " ");
		}
	}

}
