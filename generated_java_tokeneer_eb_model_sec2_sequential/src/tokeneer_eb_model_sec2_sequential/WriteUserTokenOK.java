package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class WriteUserTokenOK{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public WriteUserTokenOK(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_entry_status1().equals(machine.waitingUpdateToken) && machine.get_userTokenPresence().equals(machine.present)); */
	public /*@ pure */ boolean guard_WriteUserTokenOK() {
		return (machine.get_entry_status1().equals(machine.waitingUpdateToken) && machine.get_userTokenPresence().equals(machine.present));
	}

	/*@ public normal_behavior
		requires guard_WriteUserTokenOK();
		assignable machine.entry_status1, machine.displayMessage1;
		ensures guard_WriteUserTokenOK() &&  machine.get_entry_status1() == \old(machine.waitingEntry) &&  machine.get_displayMessage1() == \old(machine.wait); 
	 also
		requires !guard_WriteUserTokenOK();
		assignable \nothing;
		ensures true; */
	public void run_WriteUserTokenOK(){
		if(guard_WriteUserTokenOK()) {
			Integer entry_status1_tmp = machine.get_entry_status1();
			Integer displayMessage1_tmp = machine.get_displayMessage1();

			machine.set_entry_status1(machine.waitingEntry);
			machine.set_displayMessage1(machine.wait);

			System.out.println("WriteUserTokenOK executed ");
		}
	}

}
