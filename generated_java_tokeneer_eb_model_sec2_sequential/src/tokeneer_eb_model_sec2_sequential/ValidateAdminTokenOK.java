package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class ValidateAdminTokenOK{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public ValidateAdminTokenOK(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_enclaveStatus2().equals(machine.gotAdminToken) && machine.get_adminTokenPresence().equals(machine.present) && NAT.instance.has(currentTime) && machine.get_attCertTokID().range().has(machine.get_currentAdminToken()) && machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentAdminToken())) && machine.get_tokenAuthCert().domain().has(machine.get_currentAdminToken()) && machine.get_validityPeriods().image(new BSet<Integer>(machine.get_tokenAuthCert().apply(machine.get_currentAdminToken()))).has(currentTime) && machine.AdminPrivilege.has(machine.get_authCertRole().apply(machine.get_tokenAuthCert().apply(machine.get_currentAdminToken())))); */
	public /*@ pure */ boolean guard_ValidateAdminTokenOK( Integer currentTime) {
		return (
				machine.get_enclaveStatus2().equals(machine.gotAdminToken) && 
				machine.get_adminTokenPresence().equals(machine.present) && 
				NAT.instance.has(currentTime) && 
				machine.get_attCertTokID().range().has(machine.get_currentAdminToken()) && 
				machine.get_isValidatedBy().domain().has(machine.get_attCertTokID()
						.inverse().apply(machine.get_currentAdminToken())) && 
				machine.get_tokenAuthCert().domain().has(machine.get_currentAdminToken()) && 
				machine.get_validityPeriods().image(new BSet<Integer>(
						machine.get_tokenAuthCert().apply(machine.get_currentAdminToken()))).has(currentTime) && 
				machine.AdminPrivilege.has(machine.get_authCertRole().apply(machine.get_tokenAuthCert().apply(
						machine.get_currentAdminToken()))));
	}

	/*@ public normal_behavior
		requires guard_ValidateAdminTokenOK(currentTime);
		assignable machine.screenMsg2, machine.enclaveStatus2;
		ensures guard_ValidateAdminTokenOK(currentTime) &&  machine.get_screenMsg2() == \old(machine.requestAdminOp) &&  machine.get_enclaveStatus2() == \old(machine.enclaveQuiescent); 
	 also
		requires !guard_ValidateAdminTokenOK(currentTime);
		assignable \nothing;
		ensures true; */
	public void run_ValidateAdminTokenOK( Integer currentTime){
		if(guard_ValidateAdminTokenOK(currentTime)) {
			Integer screenMsg2_tmp = machine.get_screenMsg2();
			Integer enclaveStatus2_tmp = machine.get_enclaveStatus2();

			machine.set_screenMsg2(machine.requestAdminOp);
			machine.set_enclaveStatus2(machine.enclaveQuiescent);

			System.out.println("ValidateAdminTokenOK executed currentTime: " + currentTime + " ");
		}
	}

}
