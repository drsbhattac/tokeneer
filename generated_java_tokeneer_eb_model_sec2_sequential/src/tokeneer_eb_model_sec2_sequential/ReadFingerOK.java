package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class ReadFingerOK{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public ReadFingerOK(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_entry_status1().equals(machine.waitingFinger) && machine.get_FingerPresence().equals(machine.present) && machine.get_userTokenPresence().equals(machine.present)); */
	public /*@ pure */ boolean guard_ReadFingerOK() {
		return (machine.get_entry_status1().equals(machine.waitingFinger) && machine.get_FingerPresence().equals(machine.present) && machine.get_userTokenPresence().equals(machine.present));
	}

	/*@ public normal_behavior
		requires guard_ReadFingerOK();
		assignable machine.entry_status1, machine.displayMessage1;
		ensures guard_ReadFingerOK() &&  machine.get_entry_status1() == \old(machine.gotFinger) &&  machine.get_displayMessage1() == \old(machine.wait); 
	 also
		requires !guard_ReadFingerOK();
		assignable \nothing;
		ensures true; */
	public void run_ReadFingerOK(){
		if(guard_ReadFingerOK()) {
			Integer entry_status1_tmp = machine.get_entry_status1();
			Integer displayMessage1_tmp = machine.get_displayMessage1();

			machine.set_entry_status1(machine.gotFinger);
			machine.set_displayMessage1(machine.wait);

			System.out.println("ReadFingerOK executed ");
		}
	}

}
