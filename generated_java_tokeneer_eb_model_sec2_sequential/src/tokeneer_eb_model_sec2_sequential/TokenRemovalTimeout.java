package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class TokenRemovalTimeout{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public TokenRemovalTimeout(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_entry_status1().equals(machine.waitingRemoveTokenSuccess) && machine.get_userTokenPresence().equals(machine.present) && NAT.instance.has(currentTime) && (currentTime).compareTo(machine.get_tokenRemovalTimeout()) > 0); */
	public /*@ pure */ boolean guard_TokenRemovalTimeout( Integer currentTime) {
		return (
			machine.get_entry_status1().equals(machine.waitingRemoveTokenSuccess) && 
			machine.get_userTokenPresence().equals(machine.present) && 
			NAT.instance.has(currentTime) && 
			(currentTime).compareTo(machine.get_tokenRemovalTimeout()) > 0);
	}

	/*@ public normal_behavior
		requires guard_TokenRemovalTimeout(currentTime);
		assignable machine.entry_status1, machine.displayMessage1;
		ensures guard_TokenRemovalTimeout(currentTime) &&  machine.get_entry_status1() == \old(machine.waitingRemoveTokenFail) &&  machine.get_displayMessage1() == \old(machine.removeToken); 
	 also
		requires !guard_TokenRemovalTimeout(currentTime);
		assignable \nothing;
		ensures true; */
	public void run_TokenRemovalTimeout( Integer currentTime){
		if(guard_TokenRemovalTimeout(currentTime)) {
			Integer entry_status1_tmp = machine.get_entry_status1();
			Integer displayMessage1_tmp = machine.get_displayMessage1();

			machine.set_entry_status1(machine.waitingRemoveTokenFail);
			machine.set_displayMessage1(machine.removeToken);

			System.out.println("TokenRemovalTimeout executed currentTime: " + currentTime + " ");
		}
	}

}
