package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class deleteIandaCertificate_no_TID{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public deleteIandaCertificate_no_TID(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_certificates().has(deleteCertificate_ce) && machine.get_isValidatedBy().domain().has(deleteCertificate_ce) && machine.get_validityPeriods().domain().has(deleteCertificate_ce) && machine.get_attCert().has(deleteCertificate_ce) && machine.get_iandaCert().has(deleteCertificate_ce) && !machine.get_authCert().has(deleteCertificate_ce) && !machine.get_privCert().has(deleteCertificate_ce) && machine.get_tokenIandaCert().range().has(deleteCertificate_ce) && !machine.get_attCertTokID().domain().has(deleteCertificate_ce)); */
	public /*@ pure */ boolean guard_deleteIandaCertificate_no_TID( Integer deleteCertificate_ce) {
		return (machine.get_certificates().has(deleteCertificate_ce) && machine.get_isValidatedBy().domain().has(deleteCertificate_ce) && machine.get_validityPeriods().domain().has(deleteCertificate_ce) && machine.get_attCert().has(deleteCertificate_ce) && machine.get_iandaCert().has(deleteCertificate_ce) && !machine.get_authCert().has(deleteCertificate_ce) && !machine.get_privCert().has(deleteCertificate_ce) && machine.get_tokenIandaCert().range().has(deleteCertificate_ce) && !machine.get_attCertTokID().domain().has(deleteCertificate_ce));
	}

	/*@ public normal_behavior
		requires guard_deleteIandaCertificate_no_TID(deleteCertificate_ce);
		assignable machine.certificates, machine.isValidatedBy, machine.validityPeriods, machine.certificateID, machine.attCert, machine.baseCertID, machine.iandaCert, machine.fpTemplate, machine.tokenIandaCert, machine.tokenPrivCert, machine.tokenAuthCert, machine.tokenID, machine.goodTok;
		ensures guard_deleteIandaCertificate_no_TID(deleteCertificate_ce) &&  machine.get_certificates().equals(\old(machine.get_certificates().difference(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_isValidatedBy().equals(\old(machine.get_isValidatedBy().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_validityPeriods().equals(\old(machine.get_validityPeriods().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_certificateID().equals(\old(machine.get_certificateID().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_attCert().equals(\old(machine.get_attCert().difference(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_baseCertID().equals(\old(machine.get_baseCertID().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_iandaCert().equals(\old(machine.get_iandaCert().difference(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_fpTemplate().equals(\old(machine.get_fpTemplate().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_tokenIandaCert().equals(\old(machine.get_tokenIandaCert().rangeSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_tokenPrivCert().equals(\old(machine.get_tokenPrivCert().domainSubtraction(new BSet<Integer>(machine.get_tokenIandaCert().inverse().apply(deleteCertificate_ce))))) &&  machine.get_tokenAuthCert().equals(\old(machine.get_tokenAuthCert().domainSubtraction(new BSet<Integer>(machine.get_tokenIandaCert().inverse().apply(deleteCertificate_ce))))) &&  machine.get_tokenID().equals(\old(machine.get_tokenID().difference(new BSet<Integer>(machine.get_tokenIandaCert().inverse().apply(deleteCertificate_ce))))) &&  machine.get_goodTok().equals(\old(machine.get_goodTok().domainSubtraction(new BSet<Integer>(machine.get_tokenIandaCert().inverse().apply(deleteCertificate_ce))))); 
	 also
		requires !guard_deleteIandaCertificate_no_TID(deleteCertificate_ce);
		assignable \nothing;
		ensures true; */
	public void run_deleteIandaCertificate_no_TID( Integer deleteCertificate_ce){
		if(guard_deleteIandaCertificate_no_TID(deleteCertificate_ce)) {
			BSet<Integer> certificates_tmp = machine.get_certificates();
			BRelation<Integer,Integer> isValidatedBy_tmp = machine.get_isValidatedBy();
			BRelation<Integer,Integer> validityPeriods_tmp = machine.get_validityPeriods();
			BRelation<Integer,Integer> certificateID_tmp = machine.get_certificateID();
			BSet<Integer> attCert_tmp = machine.get_attCert();
			BRelation<Integer,Integer> baseCertID_tmp = machine.get_baseCertID();
			BSet<Integer> iandaCert_tmp = machine.get_iandaCert();
			BRelation<Integer,Integer> fpTemplate_tmp = machine.get_fpTemplate();
			BRelation<Integer,Integer> tokenIandaCert_tmp = machine.get_tokenIandaCert();
			BRelation<Integer,Integer> tokenPrivCert_tmp = machine.get_tokenPrivCert();
			BRelation<Integer,Integer> tokenAuthCert_tmp = machine.get_tokenAuthCert();
			BSet<Integer> tokenID_tmp = machine.get_tokenID();
			BRelation<Integer,Integer> goodTok_tmp = machine.get_goodTok();

			machine.set_certificates(certificates_tmp.difference(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_isValidatedBy(isValidatedBy_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_validityPeriods(validityPeriods_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_certificateID(certificateID_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_attCert(attCert_tmp.difference(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_baseCertID(baseCertID_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_iandaCert(iandaCert_tmp.difference(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_fpTemplate(fpTemplate_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_tokenIandaCert(tokenIandaCert_tmp.rangeSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_tokenPrivCert(tokenPrivCert_tmp.domainSubtraction(new BSet<Integer>(tokenIandaCert_tmp.inverse().apply(deleteCertificate_ce))));
			machine.set_tokenAuthCert(tokenAuthCert_tmp.domainSubtraction(new BSet<Integer>(tokenIandaCert_tmp.inverse().apply(deleteCertificate_ce))));
			machine.set_tokenID(tokenID_tmp.difference(new BSet<Integer>(tokenIandaCert_tmp.inverse().apply(deleteCertificate_ce))));
			machine.set_goodTok(goodTok_tmp.domainSubtraction(new BSet<Integer>(tokenIandaCert_tmp.inverse().apply(deleteCertificate_ce))));

			System.out.println("deleteIandaCertificate_no_TID executed deleteCertificate_ce: " + deleteCertificate_ce + " ");
		}
	}

}
