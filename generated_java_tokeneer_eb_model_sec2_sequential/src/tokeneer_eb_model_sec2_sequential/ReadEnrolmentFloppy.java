package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class ReadEnrolmentFloppy{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public ReadEnrolmentFloppy(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_enclaveStatus1().equals(machine.notEnrolled) && machine.get_floppyPresence().equals(machine.present)); */
	public /*@ pure */ boolean guard_ReadEnrolmentFloppy() {
		return (machine.get_enclaveStatus1().equals(machine.notEnrolled) && machine.get_floppyPresence().equals(machine.present));
	}

	/*@ public normal_behavior
		requires guard_ReadEnrolmentFloppy();
		assignable machine.screenMsg1, machine.enclaveStatus1, machine.displayMessage2;
		ensures guard_ReadEnrolmentFloppy() &&  machine.get_screenMsg1() == \old(machine.validatingEnrolmentData) &&  machine.get_enclaveStatus1() == \old(machine.waitingEnrol) &&  machine.get_displayMessage2() == \old(machine.blank); 
	 also
		requires !guard_ReadEnrolmentFloppy();
		assignable \nothing;
		ensures true; */
	public void run_ReadEnrolmentFloppy(){
		if(guard_ReadEnrolmentFloppy()) {
			Integer screenMsg1_tmp = machine.get_screenMsg1();
			Integer enclaveStatus1_tmp = machine.get_enclaveStatus1();
			Integer displayMessage2_tmp = machine.get_displayMessage2();

			machine.set_screenMsg1(machine.validatingEnrolmentData);
			machine.set_enclaveStatus1(machine.waitingEnrol);
			machine.set_displayMessage2(machine.blank);

			System.out.println("ReadEnrolmentFloppy executed ");
		}
	}

}
