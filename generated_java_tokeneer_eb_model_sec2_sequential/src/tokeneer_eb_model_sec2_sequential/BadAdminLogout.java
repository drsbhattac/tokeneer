package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class BadAdminLogout{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public BadAdminLogout(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (new BSet<Integer>(machine.waitingStartAdminOp,machine.waitingFinishAdminOp,machine.gotAdminToken).has(machine.get_enclaveStatus2()) && machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && machine.get_adminTokenPresence().equals(machine.absent)); */
	public /*@ pure */ boolean guard_BadAdminLogout() {
		return (new BSet<Integer>(machine.waitingStartAdminOp,machine.waitingFinishAdminOp,machine.gotAdminToken).has(machine.get_enclaveStatus2()) && machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && machine.get_adminTokenPresence().equals(machine.absent));
	}

	/*@ public normal_behavior
		requires guard_BadAdminLogout();
		assignable machine.rolePresent, machine.currentAdminOp, machine.enclaveStatus2, machine.availableOps;
		ensures guard_BadAdminLogout() &&  machine.get_rolePresent().equals(\old((machine.get_rolePresent().override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoRole)))))) &&  machine.get_currentAdminOp().equals(\old((machine.get_currentAdminOp().override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))))) &&  machine.get_enclaveStatus2() == \old(machine.enclaveQuiescent) &&  machine.get_availableOps().equals(\old((machine.get_availableOps().override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))))); 
	 also
		requires !guard_BadAdminLogout();
		assignable \nothing;
		ensures true; */
	public void run_BadAdminLogout(){
		if(guard_BadAdminLogout()) {
			BRelation<Integer,Integer> rolePresent_tmp = machine.get_rolePresent();
			BRelation<Integer,Integer> currentAdminOp_tmp = machine.get_currentAdminOp();
			Integer enclaveStatus2_tmp = machine.get_enclaveStatus2();
			BRelation<Integer,Integer> availableOps_tmp = machine.get_availableOps();

			machine.set_rolePresent((rolePresent_tmp.override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoRole)))));
			machine.set_currentAdminOp((currentAdminOp_tmp.override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))));
			machine.set_enclaveStatus2(machine.enclaveQuiescent);
			
			machine.set_availableOps((
					availableOps_tmp.override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))));
			
			System.out.println("BadAdminLogout executed ");
		}
	}

}
