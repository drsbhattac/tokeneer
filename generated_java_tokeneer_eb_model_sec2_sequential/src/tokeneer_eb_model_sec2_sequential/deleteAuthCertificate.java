package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class deleteAuthCertificate{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public deleteAuthCertificate(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_certificates().has(deleteCertificate_ce) && machine.get_isValidatedBy().domain().has(deleteCertificate_ce) && machine.get_validityPeriods().domain().has(deleteCertificate_ce) && machine.get_attCert().has(deleteCertificate_ce) && machine.get_authCert().has(deleteCertificate_ce) && !machine.get_privCert().has(deleteCertificate_ce) && !machine.get_iandaCert().has(deleteCertificate_ce) && machine.get_tokenAuthCert().range().has(deleteCertificate_ce)); */
	public /*@ pure */ boolean guard_deleteAuthCertificate( Integer deleteCertificate_ce) {
		return (machine.get_certificates().has(deleteCertificate_ce) && machine.get_isValidatedBy().domain().has(deleteCertificate_ce) && machine.get_validityPeriods().domain().has(deleteCertificate_ce) && machine.get_attCert().has(deleteCertificate_ce) && machine.get_authCert().has(deleteCertificate_ce) && !machine.get_privCert().has(deleteCertificate_ce) && !machine.get_iandaCert().has(deleteCertificate_ce) && machine.get_tokenAuthCert().range().has(deleteCertificate_ce));
	}

	/*@ public normal_behavior
		requires guard_deleteAuthCertificate(deleteCertificate_ce);
		assignable machine.certificates, machine.isValidatedBy, machine.validityPeriods, machine.certificateID, machine.attCert, machine.baseCertID, machine.authCert, machine.authCertRole, machine.authCertClearence, machine.tokenAuthCert, machine.attCertTokID;
		ensures guard_deleteAuthCertificate(deleteCertificate_ce) &&  machine.get_certificates().equals(\old(machine.get_certificates().difference(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_isValidatedBy().equals(\old(machine.get_isValidatedBy().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_validityPeriods().equals(\old(machine.get_validityPeriods().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_certificateID().equals(\old(machine.get_certificateID().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_attCert().equals(\old(machine.get_attCert().difference(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_baseCertID().equals(\old(machine.get_baseCertID().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_authCert().equals(\old(machine.get_authCert().difference(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_authCertRole().equals(\old(machine.get_authCertRole().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_authCertClearence().equals(\old(machine.get_authCertClearence().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_tokenAuthCert().equals(\old(machine.get_tokenAuthCert().rangeSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_attCertTokID().equals(\old(machine.get_attCertTokID().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))); 
	 also
		requires !guard_deleteAuthCertificate(deleteCertificate_ce);
		assignable \nothing;
		ensures true; */
	public void run_deleteAuthCertificate( Integer deleteCertificate_ce){
		if(guard_deleteAuthCertificate(deleteCertificate_ce)) {
			BSet<Integer> certificates_tmp = machine.get_certificates();
			BRelation<Integer,Integer> isValidatedBy_tmp = machine.get_isValidatedBy();
			BRelation<Integer,Integer> validityPeriods_tmp = machine.get_validityPeriods();
			BRelation<Integer,Integer> certificateID_tmp = machine.get_certificateID();
			BSet<Integer> attCert_tmp = machine.get_attCert();
			BRelation<Integer,Integer> baseCertID_tmp = machine.get_baseCertID();
			BSet<Integer> authCert_tmp = machine.get_authCert();
			BRelation<Integer,Integer> authCertRole_tmp = machine.get_authCertRole();
			BRelation<Integer,Integer> authCertClearence_tmp = machine.get_authCertClearence();
			BRelation<Integer,Integer> tokenAuthCert_tmp = machine.get_tokenAuthCert();
			BRelation<Integer,Integer> attCertTokID_tmp = machine.get_attCertTokID();

			machine.set_certificates(certificates_tmp.difference(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_isValidatedBy(isValidatedBy_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_validityPeriods(validityPeriods_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_certificateID(certificateID_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_attCert(attCert_tmp.difference(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_baseCertID(baseCertID_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_authCert(authCert_tmp.difference(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_authCertRole(authCertRole_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_authCertClearence(authCertClearence_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_tokenAuthCert(tokenAuthCert_tmp.rangeSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_attCertTokID(attCertTokID_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));

			System.out.println("deleteAuthCertificate executed deleteCertificate_ce: " + deleteCertificate_ce + " ");
		}
	}

}
