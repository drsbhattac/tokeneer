package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class ValidateAdminTokenFail_1{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public ValidateAdminTokenFail_1(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_enclaveStatus2().equals(machine.gotAdminToken) && machine.get_adminTokenPresence().equals(machine.present) && machine.get_attCertTokID().range().has(machine.get_currentAdminToken()) && !machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentAdminToken()))); */
	public /*@ pure */ boolean guard_ValidateAdminTokenFail_1() {
		return (
			machine.get_enclaveStatus2().equals(machine.gotAdminToken) && 
			machine.get_adminTokenPresence().equals(machine.present) && 
			machine.get_attCertTokID().range().has(machine.get_currentAdminToken()) && 
			!machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentAdminToken())));
	}

	/*@ public normal_behavior
		requires guard_ValidateAdminTokenFail_1();
		assignable machine.screenMsg2, machine.enclaveStatus2;
		ensures guard_ValidateAdminTokenFail_1() &&  machine.get_screenMsg2() == \old(machine.removeAdminToken) &&  machine.get_enclaveStatus2() == \old(machine.waitingRemoveAdminTokenFail); 
	 also
		requires !guard_ValidateAdminTokenFail_1();
		assignable \nothing;
		ensures true; */
	public void run_ValidateAdminTokenFail_1(){
		Integer screenMsg2_tmp = machine.get_screenMsg2();
		Integer enclaveStatus2_tmp = machine.get_enclaveStatus2();

		machine.set_screenMsg2(machine.removeAdminToken);
		machine.set_enclaveStatus2(machine.waitingRemoveAdminTokenFail);

		System.out.println("ValidateAdminTokenFail_1 executed ");
	}

}
