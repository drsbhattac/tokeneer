package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class ShutdownWaitingDoor{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public ShutdownWaitingDoor(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_enclaveStatus2().equals(machine.waitingStartAdminOp) && machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.shutdownOp) && machine.get_currentDoor().equals(machine.open)); */
	public /*@ pure */ boolean guard_ShutdownWaitingDoor() {
		return (machine.get_enclaveStatus2().equals(machine.waitingStartAdminOp) && machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.shutdownOp) && machine.get_currentDoor().equals(machine.open));
	}

	/*@ public normal_behavior
		requires guard_ShutdownWaitingDoor();
		assignable machine.screenMsg2;
		ensures guard_ShutdownWaitingDoor() &&  machine.get_screenMsg2() == \old(machine.closeDoor); 
	 also
		requires !guard_ShutdownWaitingDoor();
		assignable \nothing;
		ensures true; */
	public void run_ShutdownWaitingDoor(){
		if(guard_ShutdownWaitingDoor()) {
			Integer screenMsg2_tmp = machine.get_screenMsg2();

			machine.set_screenMsg2(machine.closeDoor);

			System.out.println("ShutdownWaitingDoor executed ");
		}
	}

}
