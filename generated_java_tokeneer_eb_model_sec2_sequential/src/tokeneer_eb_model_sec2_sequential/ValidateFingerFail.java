package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class ValidateFingerFail{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public ValidateFingerFail(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_entry_status1().equals(machine.gotFinger) && machine.get_userTokenPresence().equals(machine.present)); */
	public /*@ pure */ boolean guard_ValidateFingerFail() {
		return (machine.get_entry_status1().equals(machine.gotFinger) && machine.get_userTokenPresence().equals(machine.present));
	}

	/*@ public normal_behavior
		requires guard_ValidateFingerFail();
		assignable machine.entry_status1, machine.displayMessage1;
		ensures guard_ValidateFingerFail() &&  machine.get_entry_status1() == \old(machine.waitingRemoveTokenFail) &&  machine.get_displayMessage1() == \old(machine.removeToken); 
	 also
		requires !guard_ValidateFingerFail();
		assignable \nothing;
		ensures true; */
	public void run_ValidateFingerFail(){
		if(guard_ValidateFingerFail()) {
			Integer entry_status1_tmp = machine.get_entry_status1();
			Integer displayMessage1_tmp = machine.get_displayMessage1();

			machine.set_entry_status1(machine.waitingRemoveTokenFail);
			machine.set_displayMessage1(machine.removeToken);

			System.out.println("ValidateFingerFail executed ");
		}
	}

}
