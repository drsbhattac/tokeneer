package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class FailedEnrolFloppyRemoved{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public FailedEnrolFloppyRemoved(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_enclaveStatus1().equals(machine.waitingEndEnrol) && machine.get_floppyPresence().equals(machine.absent)); */
	public /*@ pure */ boolean guard_FailedEnrolFloppyRemoved() {
		return (machine.get_enclaveStatus1().equals(machine.waitingEndEnrol) && machine.get_floppyPresence().equals(machine.absent));
	}

	/*@ public normal_behavior
		requires guard_FailedEnrolFloppyRemoved();
		assignable machine.screenMsg1, machine.enclaveStatus1, machine.displayMessage2;
		ensures guard_FailedEnrolFloppyRemoved() &&  machine.get_screenMsg1() == \old(machine.insertEnrolmentData) &&  machine.get_enclaveStatus1() == \old(machine.notEnrolled) &&  machine.get_displayMessage2() == \old(machine.blank); 
	 also
		requires !guard_FailedEnrolFloppyRemoved();
		assignable \nothing;
		ensures true; */
	public void run_FailedEnrolFloppyRemoved(){
		if(guard_FailedEnrolFloppyRemoved()) {
			Integer screenMsg1_tmp = machine.get_screenMsg1();
			Integer enclaveStatus1_tmp = machine.get_enclaveStatus1();
			Integer displayMessage2_tmp = machine.get_displayMessage2();

			machine.set_screenMsg1(machine.insertEnrolmentData);
			machine.set_enclaveStatus1(machine.notEnrolled);
			machine.set_displayMessage2(machine.blank);

			System.out.println("FailedEnrolFloppyRemoved executed ");
		}
	}

}
