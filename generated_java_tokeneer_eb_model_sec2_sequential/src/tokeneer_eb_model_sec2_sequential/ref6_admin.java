package tokeneer_eb_model_sec2_sequential;

import eventb_prelude.*;
import Util.*;
//@ model import org.jmlspecs.models.JMLObjectSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ref6_admin{
	private static final Integer max_integer = Utilities.max_integer;
	private static final Integer min_integer = Utilities.min_integer;

	public ShutdownOK evt_ShutdownOK = new ShutdownOK(this);
	public addIandaCertificate_TID evt_addIandaCertificate_TID = new addIandaCertificate_TID(this);
	public ValidateUserTokenFail evt_ValidateUserTokenFail = new ValidateUserTokenFail(this);
	public SecurityPropertyOne_Unlock_with_token evt_SecurityPropertyOne_Unlock_with_token = new SecurityPropertyOne_Unlock_with_token(this);
	public ValidateOpRequestOK evt_ValidateOpRequestOK = new ValidateOpRequestOK(this);
	public ValidateFingerOK evt_ValidateFingerOK = new ValidateFingerOK(this);
	public FailedAccessTokenRemoved evt_FailedAccessTokenRemoved = new FailedAccessTokenRemoved(this);
	public SecurityPropertyTwo_Unlock_at_allowed_time evt_SecurityPropertyTwo_Unlock_at_allowed_time = new SecurityPropertyTwo_Unlock_at_allowed_time(this);
	public addPrivCertificate_TID evt_addPrivCertificate_TID = new addPrivCertificate_TID(this);
	public FailedEnrolFloppyRemoved evt_FailedEnrolFloppyRemoved = new FailedEnrolFloppyRemoved(this);
	public deletePrivCertificate_no_TID evt_deletePrivCertificate_no_TID = new deletePrivCertificate_no_TID(this);
	public ValidateAdminTokenOK evt_ValidateAdminTokenOK = new ValidateAdminTokenOK(this);
	public ValidateOpRequestFail evt_ValidateOpRequestFail = new ValidateOpRequestFail(this);
	public ReadFingerOK evt_ReadFingerOK = new ReadFingerOK(this);
	public StartArchiveLogWaitingFloppy evt_StartArchiveLogWaitingFloppy = new StartArchiveLogWaitingFloppy(this);
	public addIandaCertificate_no_TID evt_addIandaCertificate_no_TID = new addIandaCertificate_no_TID(this);
	public FinishArchiveLogOK evt_FinishArchiveLogOK = new FinishArchiveLogOK(this);
	public OverrideDoorLockOK evt_OverrideDoorLockOK = new OverrideDoorLockOK(this);
	public FinishArchiveLogBadMatch evt_FinishArchiveLogBadMatch = new FinishArchiveLogBadMatch(this);
	public ValidateEnrolmentDataOK evt_ValidateEnrolmentDataOK = new ValidateEnrolmentDataOK(this);
	public unlockDoorOK evt_unlockDoorOK = new unlockDoorOK(this);
	public ShutdownWaitingDoor evt_ShutdownWaitingDoor = new ShutdownWaitingDoor(this);
	public addAuthCertificate_TID evt_addAuthCertificate_TID = new addAuthCertificate_TID(this);
	public ReadUserToken evt_ReadUserToken = new ReadUserToken(this);
	public StartArchiveLogOK evt_StartArchiveLogOK = new StartArchiveLogOK(this);
	public deleteIandaCertificate_no_TID evt_deleteIandaCertificate_no_TID = new deleteIandaCertificate_no_TID(this);
	public FingerTimeout evt_FingerTimeout = new FingerTimeout(this);
	public ValidateEnrolmentDataFail evt_ValidateEnrolmentDataFail = new ValidateEnrolmentDataFail(this);
	public addAuthCertificate_no_TID evt_addAuthCertificate_no_TID = new addAuthCertificate_no_TID(this);
	public deleteIandaCertificate_TID evt_deleteIandaCertificate_TID = new deleteIandaCertificate_TID(this);
	public AdminTokenTimeout_1 evt_AdminTokenTimeout_1 = new AdminTokenTimeout_1(this);
	public FinishArchiveLogNoFloppy evt_FinishArchiveLogNoFloppy = new FinishArchiveLogNoFloppy(this);
	public AdminTokenTimeout_3 evt_AdminTokenTimeout_3 = new AdminTokenTimeout_3(this);
	public deletePrivCertificate_TID evt_deletePrivCertificate_TID = new deletePrivCertificate_TID(this);
	public TokenRemovalTimeout evt_TokenRemovalTimeout = new TokenRemovalTimeout(this);
	public AdminTokenTimeout_2 evt_AdminTokenTimeout_2 = new AdminTokenTimeout_2(this);
	public WriteUserTokenFail evt_WriteUserTokenFail = new WriteUserTokenFail(this);
	public EntryOK_1 evt_EntryOK_1 = new EntryOK_1(this);
	public EntryOK_2 evt_EntryOK_2 = new EntryOK_2(this);
	public AdminTokenTimeout_4 evt_AdminTokenTimeout_4 = new AdminTokenTimeout_4(this);
	public lockDoor evt_lockDoor = new lockDoor(this);
	public RequestEnrolment evt_RequestEnrolment = new RequestEnrolment(this);
	public doNothing evt_doNothing = new doNothing(this);
	public TokenRemovedAdminLogout evt_TokenRemovedAdminLogout = new TokenRemovedAdminLogout(this);
	public FailedAdminTokenRemoved evt_FailedAdminTokenRemoved = new FailedAdminTokenRemoved(this);
	public UserTokenTear evt_UserTokenTear = new UserTokenTear(this);
	public ValidateFingerFail evt_ValidateFingerFail = new ValidateFingerFail(this);
	public StartUpdateConfigOK evt_StartUpdateConfigOK = new StartUpdateConfigOK(this);
	public deleteAuthCertificate evt_deleteAuthCertificate = new deleteAuthCertificate(this);
	public deleteIdCertificate evt_deleteIdCertificate = new deleteIdCertificate(this);
	public StartUpdateConfigWaitingFloppy evt_StartUpdateConfigWaitingFloppy = new StartUpdateConfigWaitingFloppy(this);
	public BioCheckRequired evt_BioCheckRequired = new BioCheckRequired(this);
	public FinishUpdateConfigDataOK evt_FinishUpdateConfigDataOK = new FinishUpdateConfigDataOK(this);
	public ReadEnrolmentFloppy evt_ReadEnrolmentFloppy = new ReadEnrolmentFloppy(this);
	public BioCheckNotRequired evt_BioCheckNotRequired = new BioCheckNotRequired(this);
	public ValidateAdminTokenFail_4 evt_ValidateAdminTokenFail_4 = new ValidateAdminTokenFail_4(this);
	public FinishUpdateConfigDataFail evt_FinishUpdateConfigDataFail = new FinishUpdateConfigDataFail(this);
	public ValidateAdminTokenFail_3 evt_ValidateAdminTokenFail_3 = new ValidateAdminTokenFail_3(this);
	public addPrivCertificate_no_TID evt_addPrivCertificate_no_TID = new addPrivCertificate_no_TID(this);
	public ValidateAdminTokenFail_2 evt_ValidateAdminTokenFail_2 = new ValidateAdminTokenFail_2(this);
	public ValidateAdminTokenFail_1 evt_ValidateAdminTokenFail_1 = new ValidateAdminTokenFail_1(this);
	public EntryNotAllowed_2 evt_EntryNotAllowed_2 = new EntryNotAllowed_2(this);
	public EntryNotAllowed_1 evt_EntryNotAllowed_1 = new EntryNotAllowed_1(this);
	public readAdminToken evt_readAdminToken = new readAdminToken(this);
	public BadAdminLogout evt_BadAdminLogout = new BadAdminLogout(this);
	public WriteUserTokenOK evt_WriteUserTokenOK = new WriteUserTokenOK(this);
	public addIdCertificate evt_addIdCertificate = new addIdCertificate(this);
	

	/******Set definitions******/
	//@ public static constraint CERTIFICATEID.equals(\old(CERTIFICATEID)); 
	public static final BSet<Integer> CERTIFICATEID = new Enumerated(min_integer,max_integer);

	//@ public static constraint CERTIFICATES.equals(\old(CERTIFICATES)); 
	public static final BSet<Integer> CERTIFICATES = new Enumerated(min_integer,max_integer);

	//@ public static constraint KEYS.equals(\old(KEYS)); 
	public static final BSet<Integer> KEYS = new Enumerated(min_integer,max_integer);

	//@ public static constraint USER.equals(\old(USER)); 
	public static final BSet<Integer> USER = new Enumerated(min_integer,max_integer);

	//@ public static constraint CLEARENCE.equals(\old(CLEARENCE)); 
	public static final BSet<Integer> CLEARENCE = new Enumerated(min_integer,max_integer);

	//@ public static constraint FINGERPRINTTEMPLATE.equals(\old(FINGERPRINTTEMPLATE)); 
	public static final BSet<Integer> FINGERPRINTTEMPLATE = new Enumerated(min_integer,max_integer);

	//@ public static constraint PRIVILEGE.equals(\old(PRIVILEGE)); 
	public static final BSet<Integer> PRIVILEGE = new Enumerated(min_integer,max_integer);

	//@ public static constraint ENTRY_STATUS.equals(\old(ENTRY_STATUS)); 
	public static final BSet<Integer> ENTRY_STATUS = new Enumerated(min_integer,max_integer);

	//@ public static constraint TOKENID.equals(\old(TOKENID)); 
	public static final BSet<Integer> TOKENID = new Enumerated(min_integer,max_integer);

	//@ public static constraint DISPLAYMESSAGE.equals(\old(DISPLAYMESSAGE)); 
	public static final BSet<Integer> DISPLAYMESSAGE = new Enumerated(min_integer,max_integer);

	//@ public static constraint DOORALARM.equals(\old(DOORALARM)); 
	public static final BSet<Integer> DOORALARM = new Enumerated(min_integer,max_integer);

	//@ public static constraint FINGERPRINTTRY.equals(\old(FINGERPRINTTRY)); 
	public static final BSet<Integer> FINGERPRINTTRY = new Enumerated(min_integer,max_integer);

	//@ public static constraint LOCKING.equals(\old(LOCKING)); 
	public static final BSet<Integer> LOCKING = new Enumerated(min_integer,max_integer);

	//@ public static constraint PRESENCE.equals(\old(PRESENCE)); 
	public static final BSet<Integer> PRESENCE = new Enumerated(min_integer,max_integer);

	//@ public static constraint TOKENTRY.equals(\old(TOKENTRY)); 
	public static final BSet<Integer> TOKENTRY = new Enumerated(min_integer,max_integer);

	//@ public static constraint ENCLAVESTATUS.equals(\old(ENCLAVESTATUS)); 
	public static final BSet<Integer> ENCLAVESTATUS = new Enumerated(min_integer,max_integer);

	//@ public static constraint ENROL.equals(\old(ENROL)); 
	public static final BSet<Integer> ENROL = new Enumerated(min_integer,max_integer);

	//@ public static constraint FLOPPY.equals(\old(FLOPPY)); 
	public static final BSet<Integer> FLOPPY = new Enumerated(min_integer,max_integer);

	//@ public static constraint SCREENTEXT.equals(\old(SCREENTEXT)); 
	public static final BSet<Integer> SCREENTEXT = new Enumerated(min_integer,max_integer);

	//@ public static constraint ADMIN.equals(\old(ADMIN)); 
	public static final BSet<Integer> ADMIN = new Enumerated(min_integer,max_integer);

	//@ public static constraint ADMINOP.equals(\old(ADMINOP)); 
	public static final BSet<Integer> ADMINOP = new Enumerated(min_integer,max_integer);

	//@ public static constraint DOOR.equals(\old(DOOR)); 
	public static final BSet<Integer> DOOR = new Enumerated(min_integer,max_integer);

	//@ public static constraint KEYBOARD.equals(\old(KEYBOARD)); 
	public static final BSet<Integer> KEYBOARD = new Enumerated(min_integer,max_integer);


	/******Constant definitions******/
	//@ public static constraint NoRole.equals(\old(NoRole)); 
	public static final Integer NoRole = Test_ref6_admin.random_NoRole;

	//@ public static constraint auditManager.equals(\old(auditManager)); 
	public static final Integer auditManager = Test_ref6_admin.random_auditManager;

	//@ public static constraint confidential.equals(\old(confidential)); 
	public static final Integer confidential = Test_ref6_admin.random_confidential;

	//@ public static constraint guard.equals(\old(guard)); 
	public static final Integer guard = Test_ref6_admin.random_guard;

	//@ public static constraint restricted.equals(\old(restricted)); 
	public static final Integer restricted = Test_ref6_admin.random_restricted;

	//@ public static constraint secret.equals(\old(secret)); 
	public static final Integer secret = Test_ref6_admin.random_secret;

	//@ public static constraint securityOfficer.equals(\old(securityOfficer)); 
	public static final Integer securityOfficer = Test_ref6_admin.random_securityOfficer;

	//@ public static constraint topsecret.equals(\old(topsecret)); 
	public static final Integer topsecret = Test_ref6_admin.random_topsecret;

	//@ public static constraint unclassified.equals(\old(unclassified)); 
	public static final Integer unclassified = Test_ref6_admin.random_unclassified;

	//@ public static constraint unmarked.equals(\old(unmarked)); 
	public static final Integer unmarked = Test_ref6_admin.random_unmarked;

	//@ public static constraint userOnly.equals(\old(userOnly)); 
	public static final Integer userOnly = Test_ref6_admin.random_userOnly;

	//@ public static constraint gotFinger.equals(\old(gotFinger)); 
	public static final Integer gotFinger = Test_ref6_admin.random_gotFinger;

	//@ public static constraint gotUserToken.equals(\old(gotUserToken)); 
	public static final Integer gotUserToken = Test_ref6_admin.random_gotUserToken;

	//@ public static constraint quiescent.equals(\old(quiescent)); 
	public static final Integer quiescent = Test_ref6_admin.random_quiescent;

	//@ public static constraint waitingEntry.equals(\old(waitingEntry)); 
	public static final Integer waitingEntry = Test_ref6_admin.random_waitingEntry;

	//@ public static constraint waitingFinger.equals(\old(waitingFinger)); 
	public static final Integer waitingFinger = Test_ref6_admin.random_waitingFinger;

	//@ public static constraint waitingRemoveTokenFail.equals(\old(waitingRemoveTokenFail)); 
	public static final Integer waitingRemoveTokenFail = Test_ref6_admin.random_waitingRemoveTokenFail;

	//@ public static constraint waitingRemoveTokenSuccess.equals(\old(waitingRemoveTokenSuccess)); 
	public static final Integer waitingRemoveTokenSuccess = Test_ref6_admin.random_waitingRemoveTokenSuccess;

	//@ public static constraint waitingUpdateToken.equals(\old(waitingUpdateToken)); 
	public static final Integer waitingUpdateToken = Test_ref6_admin.random_waitingUpdateToken;

	//@ public static constraint MAXTIME.equals(\old(MAXTIME)); 
	public static final Integer MAXTIME = Test_ref6_admin.random_MAXTIME;

	//@ public static constraint absent.equals(\old(absent)); 
	public static final Integer absent = Test_ref6_admin.random_absent;

	//@ public static constraint badFP.equals(\old(badFP)); 
	public static final Integer badFP = Test_ref6_admin.random_badFP;

	//@ public static constraint badT.equals(\old(badT)); 
	public static final Integer badT = Test_ref6_admin.random_badT;

	//@ public static constraint blank.equals(\old(blank)); 
	public static final Integer blank = Test_ref6_admin.random_blank;

	//@ public static constraint d_alarming.equals(\old(d_alarming)); 
	public static final Integer d_alarming = Test_ref6_admin.random_d_alarming;

	//@ public static constraint d_silent.equals(\old(d_silent)); 
	public static final Integer d_silent = Test_ref6_admin.random_d_silent;

	//@ public static constraint doorUnlocked.equals(\old(doorUnlocked)); 
	public static final Integer doorUnlocked = Test_ref6_admin.random_doorUnlocked;

	//@ public static constraint goodF.equals(\old(goodF)); 
	public static final Integer goodF = Test_ref6_admin.random_goodF;

	//@ public static constraint goodT.equals(\old(goodT)); 
	public static final Integer goodT = Test_ref6_admin.random_goodT;

	//@ public static constraint initFPTry.equals(\old(initFPTry)); 
	public static final Integer initFPTry = Test_ref6_admin.random_initFPTry;

	//@ public static constraint initTokenTry.equals(\old(initTokenTry)); 
	public static final Integer initTokenTry = Test_ref6_admin.random_initTokenTry;

	//@ public static constraint insertFinger.equals(\old(insertFinger)); 
	public static final Integer insertFinger = Test_ref6_admin.random_insertFinger;

	//@ public static constraint noFP.equals(\old(noFP)); 
	public static final Integer noFP = Test_ref6_admin.random_noFP;

	//@ public static constraint noT.equals(\old(noT)); 
	public static final Integer noT = Test_ref6_admin.random_noT;

	//@ public static constraint openDoor.equals(\old(openDoor)); 
	public static final Integer openDoor = Test_ref6_admin.random_openDoor;

	//@ public static constraint present.equals(\old(present)); 
	public static final Integer present = Test_ref6_admin.random_present;

	//@ public static constraint removeToken.equals(\old(removeToken)); 
	public static final Integer removeToken = Test_ref6_admin.random_removeToken;

	//@ public static constraint time.equals(\old(time)); 
	public static final BSet<Integer> time = Test_ref6_admin.random_time;

	//@ public static constraint tokenUpdateFailed.equals(\old(tokenUpdateFailed)); 
	public static final Integer tokenUpdateFailed = Test_ref6_admin.random_tokenUpdateFailed;

	//@ public static constraint wait.equals(\old(wait)); 
	public static final Integer wait = Test_ref6_admin.random_wait;

	//@ public static constraint welcome.equals(\old(welcome)); 
	public static final Integer welcome = Test_ref6_admin.random_welcome;

	//@ public static constraint archiveFailed.equals(\old(archiveFailed)); 
	public static final Integer archiveFailed = Test_ref6_admin.random_archiveFailed;

	//@ public static constraint badFloppy.equals(\old(badFloppy)); 
	public static final Integer badFloppy = Test_ref6_admin.random_badFloppy;

	//@ public static constraint busy.equals(\old(busy)); 
	public static final Integer busy = Test_ref6_admin.random_busy;

	//@ public static constraint clear.equals(\old(clear)); 
	public static final Integer clear = Test_ref6_admin.random_clear;

	//@ public static constraint closeDoor.equals(\old(closeDoor)); 
	public static final Integer closeDoor = Test_ref6_admin.random_closeDoor;

	//@ public static constraint doingOp.equals(\old(doingOp)); 
	public static final Integer doingOp = Test_ref6_admin.random_doingOp;

	//@ public static constraint emptyFloppy.equals(\old(emptyFloppy)); 
	public static final Integer emptyFloppy = Test_ref6_admin.random_emptyFloppy;

	//@ public static constraint enclaveQuiescent.equals(\old(enclaveQuiescent)); 
	public static final Integer enclaveQuiescent = Test_ref6_admin.random_enclaveQuiescent;

	//@ public static constraint enrolmentFailed.equals(\old(enrolmentFailed)); 
	public static final Integer enrolmentFailed = Test_ref6_admin.random_enrolmentFailed;

	//@ public static constraint gotAdminToken.equals(\old(gotAdminToken)); 
	public static final Integer gotAdminToken = Test_ref6_admin.random_gotAdminToken;

	//@ public static constraint insertBlankFloppy.equals(\old(insertBlankFloppy)); 
	public static final Integer insertBlankFloppy = Test_ref6_admin.random_insertBlankFloppy;

	//@ public static constraint insertConfigData.equals(\old(insertConfigData)); 
	public static final Integer insertConfigData = Test_ref6_admin.random_insertConfigData;

	//@ public static constraint insertEnrolmentData.equals(\old(insertEnrolmentData)); 
	public static final Integer insertEnrolmentData = Test_ref6_admin.random_insertEnrolmentData;

	//@ public static constraint invalidData.equals(\old(invalidData)); 
	public static final Integer invalidData = Test_ref6_admin.random_invalidData;

	//@ public static constraint invalidRequest.equals(\old(invalidRequest)); 
	public static final Integer invalidRequest = Test_ref6_admin.random_invalidRequest;

	//@ public static constraint noFloppy.equals(\old(noFloppy)); 
	public static final Integer noFloppy = Test_ref6_admin.random_noFloppy;

	//@ public static constraint notEnrolled.equals(\old(notEnrolled)); 
	public static final Integer notEnrolled = Test_ref6_admin.random_notEnrolled;

	//@ public static constraint removeAdminToken.equals(\old(removeAdminToken)); 
	public static final Integer removeAdminToken = Test_ref6_admin.random_removeAdminToken;

	//@ public static constraint requestAdminOp.equals(\old(requestAdminOp)); 
	public static final Integer requestAdminOp = Test_ref6_admin.random_requestAdminOp;

	//@ public static constraint shutdown.equals(\old(shutdown)); 
	public static final Integer shutdown = Test_ref6_admin.random_shutdown;

	//@ public static constraint validatingEnrolmentData.equals(\old(validatingEnrolmentData)); 
	public static final Integer validatingEnrolmentData = Test_ref6_admin.random_validatingEnrolmentData;

	//@ public static constraint waitingEndEnrol.equals(\old(waitingEndEnrol)); 
	public static final Integer waitingEndEnrol = Test_ref6_admin.random_waitingEndEnrol;

	//@ public static constraint waitingEnrol.equals(\old(waitingEnrol)); 
	public static final Integer waitingEnrol = Test_ref6_admin.random_waitingEnrol;

	//@ public static constraint waitingFinishAdminOp.equals(\old(waitingFinishAdminOp)); 
	public static final Integer waitingFinishAdminOp = Test_ref6_admin.random_waitingFinishAdminOp;

	//@ public static constraint waitingRemoveAdminTokenFail.equals(\old(waitingRemoveAdminTokenFail)); 
	public static final Integer waitingRemoveAdminTokenFail = Test_ref6_admin.random_waitingRemoveAdminTokenFail;

	//@ public static constraint waitingStartAdminOp.equals(\old(waitingStartAdminOp)); 
	public static final Integer waitingStartAdminOp = Test_ref6_admin.random_waitingStartAdminOp;

	//@ public static constraint welcomeAdmin.equals(\old(welcomeAdmin)); 
	public static final Integer welcomeAdmin = Test_ref6_admin.random_welcomeAdmin;

	//@ public static constraint AdminPrivilege.equals(\old(AdminPrivilege)); 
	public static final BSet<Integer> AdminPrivilege = Test_ref6_admin.random_AdminPrivilege;

	//@ public static constraint NoOp.equals(\old(NoOp)); 
	public static final Integer NoOp = Test_ref6_admin.random_NoOp;

	//@ public static constraint archiveLog.equals(\old(archiveLog)); 
	public static final Integer archiveLog = Test_ref6_admin.random_archiveLog;

	//@ public static constraint badKB.equals(\old(badKB)); 
	public static final Integer badKB = Test_ref6_admin.random_badKB;

	//@ public static constraint closed.equals(\old(closed)); 
	public static final Integer closed = Test_ref6_admin.random_closed;

	//@ public static constraint good1.equals(\old(good1)); 
	public static final Integer good1 = Test_ref6_admin.random_good1;

	//@ public static constraint good2.equals(\old(good2)); 
	public static final Integer good2 = Test_ref6_admin.random_good2;

	//@ public static constraint good3.equals(\old(good3)); 
	public static final Integer good3 = Test_ref6_admin.random_good3;

	//@ public static constraint noKB.equals(\old(noKB)); 
	public static final Integer noKB = Test_ref6_admin.random_noKB;

	//@ public static constraint open.equals(\old(open)); 
	public static final Integer open = Test_ref6_admin.random_open;

	//@ public static constraint overrideLock.equals(\old(overrideLock)); 
	public static final Integer overrideLock = Test_ref6_admin.random_overrideLock;

	//@ public static constraint shutdownOp.equals(\old(shutdownOp)); 
	public static final Integer shutdownOp = Test_ref6_admin.random_shutdownOp;

	//@ public static constraint ss.equals(\old(ss)); 
	public static final BSet<Integer> ss = Test_ref6_admin.random_ss;

	//@ public static constraint updateConfigData.equals(\old(updateConfigData)); 
	public static final Integer updateConfigData = Test_ref6_admin.random_updateConfigData;



	/******Axiom definitions******/
	/*@ public static invariant PRIVILEGE.equals(new BSet<Integer>(userOnly,guard,securityOfficer,auditManager,NoRole)); */
	/*@ public static invariant CLEARENCE.equals(new BSet<Integer>(unmarked,unclassified,restricted,confidential,secret,topsecret)); */
	/*@ public static invariant !userOnly.equals(guard); */
	/*@ public static invariant !userOnly.equals(securityOfficer); */
	/*@ public static invariant !userOnly.equals(auditManager); */
	/*@ public static invariant !userOnly.equals(NoRole); */
	/*@ public static invariant !guard.equals(securityOfficer); */
	/*@ public static invariant !guard.equals(auditManager); */
	/*@ public static invariant !guard.equals(NoRole); */
	/*@ public static invariant !securityOfficer.equals(auditManager); */
	/*@ public static invariant !securityOfficer.equals(NoRole); */
	/*@ public static invariant !auditManager.equals(NoRole); */
	/*@ public static invariant !unmarked.equals(unclassified); */
	/*@ public static invariant !unmarked.equals(restricted); */
	/*@ public static invariant !unmarked.equals(confidential); */
	/*@ public static invariant !unmarked.equals(secret); */
	/*@ public static invariant !unmarked.equals(topsecret); */
	/*@ public static invariant !unclassified.equals(restricted); */
	/*@ public static invariant !unclassified.equals(confidential); */
	/*@ public static invariant !unclassified.equals(secret); */
	/*@ public static invariant !unclassified.equals(topsecret); */
	/*@ public static invariant !restricted.equals(confidential); */
	/*@ public static invariant !restricted.equals(secret); */
	/*@ public static invariant !restricted.equals(topsecret); */
	/*@ public static invariant !confidential.equals(secret); */
	/*@ public static invariant !confidential.equals(topsecret); */
	/*@ public static invariant !secret.equals(topsecret); */
	/*@ public static invariant ENTRY_STATUS.equals(new BSet<Integer>(quiescent,gotUserToken,waitingFinger,gotFinger,waitingUpdateToken,waitingEntry,waitingRemoveTokenSuccess,waitingRemoveTokenFail)); */
	/*@ public static invariant !quiescent.equals(gotUserToken); */
	/*@ public static invariant !quiescent.equals(waitingFinger); */
	/*@ public static invariant !quiescent.equals(gotFinger); */
	/*@ public static invariant !quiescent.equals(waitingUpdateToken); */
	/*@ public static invariant !quiescent.equals(waitingEntry); */
	/*@ public static invariant !quiescent.equals(waitingRemoveTokenSuccess); */
	/*@ public static invariant !quiescent.equals(waitingRemoveTokenFail); */
	/*@ public static invariant !gotUserToken.equals(waitingFinger); */
	/*@ public static invariant !gotUserToken.equals(gotFinger); */
	/*@ public static invariant !gotUserToken.equals(waitingUpdateToken); */
	/*@ public static invariant !gotUserToken.equals(waitingEntry); */
	/*@ public static invariant !gotUserToken.equals(waitingRemoveTokenSuccess); */
	/*@ public static invariant !gotUserToken.equals(waitingRemoveTokenFail); */
	/*@ public static invariant !waitingFinger.equals(gotFinger); */
	/*@ public static invariant !waitingFinger.equals(waitingUpdateToken); */
	/*@ public static invariant !waitingFinger.equals(waitingEntry); */
	/*@ public static invariant !waitingFinger.equals(waitingRemoveTokenSuccess); */
	/*@ public static invariant !waitingFinger.equals(waitingRemoveTokenFail); */
	/*@ public static invariant !gotFinger.equals(waitingUpdateToken); */
	/*@ public static invariant !gotFinger.equals(waitingEntry); */
	/*@ public static invariant !gotFinger.equals(waitingRemoveTokenSuccess); */
	/*@ public static invariant !gotFinger.equals(waitingRemoveTokenFail); */
	/*@ public static invariant !waitingUpdateToken.equals(waitingEntry); */
	/*@ public static invariant !waitingUpdateToken.equals(waitingRemoveTokenSuccess); */
	/*@ public static invariant !waitingUpdateToken.equals(waitingRemoveTokenFail); */
	/*@ public static invariant !waitingEntry.equals(waitingRemoveTokenSuccess); */
	/*@ public static invariant !waitingEntry.equals(waitingRemoveTokenFail); */
	/*@ public static invariant !waitingRemoveTokenSuccess.equals(waitingRemoveTokenFail); */
	/*@ public static invariant FINGERPRINTTRY.equals(new BSet<Integer>(badFP,noFP,initFPTry,goodF)); */
	/*@ public static invariant !badFP.equals(noFP); */
	/*@ public static invariant !badFP.equals(initFPTry); */
	/*@ public static invariant !badFP.equals(goodF); */
	/*@ public static invariant !noFP.equals(initFPTry); */
	/*@ public static invariant !noFP.equals(goodF); */
	/*@ public static invariant !initFPTry.equals(goodF); */
	/*@ public static invariant DISPLAYMESSAGE.equals(new BSet<Integer>(blank,welcome,insertFinger,openDoor,wait,removeToken,tokenUpdateFailed,doorUnlocked)); */
	/*@ public static invariant !blank.equals(welcome); */
	/*@ public static invariant !blank.equals(insertFinger); */
	/*@ public static invariant !blank.equals(openDoor); */
	/*@ public static invariant !blank.equals(wait); */
	/*@ public static invariant !blank.equals(removeToken); */
	/*@ public static invariant !blank.equals(tokenUpdateFailed); */
	/*@ public static invariant !blank.equals(doorUnlocked); */
	/*@ public static invariant !welcome.equals(insertFinger); */
	/*@ public static invariant !welcome.equals(openDoor); */
	/*@ public static invariant !welcome.equals(wait); */
	/*@ public static invariant !welcome.equals(removeToken); */
	/*@ public static invariant !welcome.equals(tokenUpdateFailed); */
	/*@ public static invariant !welcome.equals(doorUnlocked); */
	/*@ public static invariant !insertFinger.equals(openDoor); */
	/*@ public static invariant !insertFinger.equals(wait); */
	/*@ public static invariant !insertFinger.equals(removeToken); */
	/*@ public static invariant !insertFinger.equals(tokenUpdateFailed); */
	/*@ public static invariant !insertFinger.equals(doorUnlocked); */
	/*@ public static invariant !openDoor.equals(wait); */
	/*@ public static invariant !openDoor.equals(removeToken); */
	/*@ public static invariant !openDoor.equals(tokenUpdateFailed); */
	/*@ public static invariant !openDoor.equals(doorUnlocked); */
	/*@ public static invariant !wait.equals(removeToken); */
	/*@ public static invariant !wait.equals(tokenUpdateFailed); */
	/*@ public static invariant !wait.equals(doorUnlocked); */
	/*@ public static invariant !removeToken.equals(tokenUpdateFailed); */
	/*@ public static invariant !removeToken.equals(doorUnlocked); */
	/*@ public static invariant !tokenUpdateFailed.equals(doorUnlocked); */
	/*@ public static invariant TOKENTRY.equals(new BSet<Integer>(goodT,badT,noT,initTokenTry)); */
	/*@ public static invariant !goodT.equals(badT); */
	/*@ public static invariant !goodT.equals(noT); */
	/*@ public static invariant !goodT.equals(initTokenTry); */
	/*@ public static invariant !badT.equals(noT); */
	/*@ public static invariant !badT.equals(initTokenTry); */
	/*@ public static invariant !noT.equals(initTokenTry); */
	/*@ public static invariant time.equals(NAT.instance); */
	/*@ public static invariant NAT1.instance.has(MAXTIME); */
	/*@ public static invariant PRESENCE.equals(new BSet<Integer>(absent,present)); */
	/*@ public static invariant DOORALARM.equals(new BSet<Integer>(d_alarming,d_silent)); */
	/*@ public static invariant !d_alarming.equals(d_silent); */
	/*@ public static invariant ENCLAVESTATUS.equals(new BSet<Integer>(notEnrolled,waitingEnrol,waitingEndEnrol,enclaveQuiescent,gotAdminToken,waitingRemoveAdminTokenFail,waitingStartAdminOp,waitingFinishAdminOp,shutdown)); */
	/*@ public static invariant !notEnrolled.equals(waitingEnrol); */
	/*@ public static invariant !notEnrolled.equals(waitingEndEnrol); */
	/*@ public static invariant !notEnrolled.equals(enclaveQuiescent); */
	/*@ public static invariant !notEnrolled.equals(gotAdminToken); */
	/*@ public static invariant !notEnrolled.equals(waitingRemoveAdminTokenFail); */
	/*@ public static invariant !notEnrolled.equals(waitingStartAdminOp); */
	/*@ public static invariant !notEnrolled.equals(waitingFinishAdminOp); */
	/*@ public static invariant !notEnrolled.equals(shutdown); */
	/*@ public static invariant !waitingEnrol.equals(waitingEndEnrol); */
	/*@ public static invariant !waitingEnrol.equals(enclaveQuiescent); */
	/*@ public static invariant !waitingEnrol.equals(gotAdminToken); */
	/*@ public static invariant !waitingEnrol.equals(waitingRemoveAdminTokenFail); */
	/*@ public static invariant !waitingEnrol.equals(waitingStartAdminOp); */
	/*@ public static invariant !waitingEnrol.equals(waitingFinishAdminOp); */
	/*@ public static invariant !waitingEnrol.equals(shutdown); */
	/*@ public static invariant !waitingEndEnrol.equals(enclaveQuiescent); */
	/*@ public static invariant !waitingEndEnrol.equals(gotAdminToken); */
	/*@ public static invariant !waitingEndEnrol.equals(waitingRemoveAdminTokenFail); */
	/*@ public static invariant !waitingEndEnrol.equals(waitingStartAdminOp); */
	/*@ public static invariant !waitingEndEnrol.equals(waitingFinishAdminOp); */
	/*@ public static invariant !waitingEndEnrol.equals(shutdown); */
	/*@ public static invariant !enclaveQuiescent.equals(gotAdminToken); */
	/*@ public static invariant !enclaveQuiescent.equals(waitingRemoveAdminTokenFail); */
	/*@ public static invariant !enclaveQuiescent.equals(waitingStartAdminOp); */
	/*@ public static invariant !enclaveQuiescent.equals(waitingFinishAdminOp); */
	/*@ public static invariant !enclaveQuiescent.equals(shutdown); */
	/*@ public static invariant !gotAdminToken.equals(waitingRemoveAdminTokenFail); */
	/*@ public static invariant !gotAdminToken.equals(waitingStartAdminOp); */
	/*@ public static invariant !gotAdminToken.equals(waitingFinishAdminOp); */
	/*@ public static invariant !gotAdminToken.equals(shutdown); */
	/*@ public static invariant !waitingRemoveAdminTokenFail.equals(waitingStartAdminOp); */
	/*@ public static invariant !waitingRemoveAdminTokenFail.equals(waitingFinishAdminOp); */
	/*@ public static invariant !waitingRemoveAdminTokenFail.equals(shutdown); */
	/*@ public static invariant !waitingStartAdminOp.equals(waitingFinishAdminOp); */
	/*@ public static invariant !waitingStartAdminOp.equals(shutdown); */
	/*@ public static invariant !waitingFinishAdminOp.equals(shutdown); */
	/*@ public static invariant FLOPPY.equals(new BSet<Integer>(noFloppy,emptyFloppy,badFloppy)); */
	/*@ public static invariant !noFloppy.equals(emptyFloppy); */
	/*@ public static invariant !noFloppy.equals(badFloppy); */
	/*@ public static invariant !emptyFloppy.equals(badFloppy); */
	/*@ public static invariant SCREENTEXT.equals(new BSet<Integer>(clear,welcomeAdmin,busy,removeAdminToken,closeDoor,requestAdminOp,doingOp,invalidRequest,invalidData,insertEnrolmentData,validatingEnrolmentData,enrolmentFailed,archiveFailed,insertBlankFloppy,insertConfigData)); */
	/*@ public static invariant !clear.equals(welcomeAdmin); */
	/*@ public static invariant !clear.equals(busy); */
	/*@ public static invariant !clear.equals(removeAdminToken); */
	/*@ public static invariant !clear.equals(closeDoor); */
	/*@ public static invariant !clear.equals(requestAdminOp); */
	/*@ public static invariant !clear.equals(doingOp); */
	/*@ public static invariant !clear.equals(invalidRequest); */
	/*@ public static invariant !clear.equals(invalidData); */
	/*@ public static invariant !clear.equals(insertEnrolmentData); */
	/*@ public static invariant !clear.equals(validatingEnrolmentData); */
	/*@ public static invariant !clear.equals(enrolmentFailed); */
	/*@ public static invariant !clear.equals(archiveFailed); */
	/*@ public static invariant !clear.equals(insertBlankFloppy); */
	/*@ public static invariant !clear.equals(insertConfigData); */
	/*@ public static invariant !welcomeAdmin.equals(busy); */
	/*@ public static invariant !welcomeAdmin.equals(removeAdminToken); */
	/*@ public static invariant !welcomeAdmin.equals(closeDoor); */
	/*@ public static invariant !welcomeAdmin.equals(requestAdminOp); */
	/*@ public static invariant !welcomeAdmin.equals(doingOp); */
	/*@ public static invariant !welcomeAdmin.equals(invalidRequest); */
	/*@ public static invariant !welcomeAdmin.equals(invalidData); */
	/*@ public static invariant !welcomeAdmin.equals(insertEnrolmentData); */
	/*@ public static invariant !welcomeAdmin.equals(validatingEnrolmentData); */
	/*@ public static invariant !welcomeAdmin.equals(enrolmentFailed); */
	/*@ public static invariant !welcomeAdmin.equals(archiveFailed); */
	/*@ public static invariant !welcomeAdmin.equals(insertBlankFloppy); */
	/*@ public static invariant !welcomeAdmin.equals(insertConfigData); */
	/*@ public static invariant !busy.equals(removeAdminToken); */
	/*@ public static invariant !busy.equals(closeDoor); */
	/*@ public static invariant !busy.equals(requestAdminOp); */
	/*@ public static invariant !busy.equals(doingOp); */
	/*@ public static invariant !busy.equals(invalidRequest); */
	/*@ public static invariant !busy.equals(invalidData); */
	/*@ public static invariant !busy.equals(insertEnrolmentData); */
	/*@ public static invariant !busy.equals(validatingEnrolmentData); */
	/*@ public static invariant !busy.equals(enrolmentFailed); */
	/*@ public static invariant !busy.equals(archiveFailed); */
	/*@ public static invariant !busy.equals(insertBlankFloppy); */
	/*@ public static invariant !busy.equals(insertConfigData); */
	/*@ public static invariant !removeAdminToken.equals(closeDoor); */
	/*@ public static invariant !removeAdminToken.equals(requestAdminOp); */
	/*@ public static invariant !removeAdminToken.equals(doingOp); */
	/*@ public static invariant !removeAdminToken.equals(invalidRequest); */
	/*@ public static invariant !removeAdminToken.equals(invalidData); */
	/*@ public static invariant !removeAdminToken.equals(insertEnrolmentData); */
	/*@ public static invariant !removeAdminToken.equals(validatingEnrolmentData); */
	/*@ public static invariant !removeAdminToken.equals(enrolmentFailed); */
	/*@ public static invariant !removeAdminToken.equals(archiveFailed); */
	/*@ public static invariant !removeAdminToken.equals(insertBlankFloppy); */
	/*@ public static invariant !removeAdminToken.equals(insertConfigData); */
	/*@ public static invariant !closeDoor.equals(requestAdminOp); */
	/*@ public static invariant !closeDoor.equals(doingOp); */
	/*@ public static invariant !closeDoor.equals(invalidRequest); */
	/*@ public static invariant !closeDoor.equals(invalidData); */
	/*@ public static invariant !closeDoor.equals(insertEnrolmentData); */
	/*@ public static invariant !closeDoor.equals(validatingEnrolmentData); */
	/*@ public static invariant !closeDoor.equals(enrolmentFailed); */
	/*@ public static invariant !closeDoor.equals(archiveFailed); */
	/*@ public static invariant !closeDoor.equals(insertBlankFloppy); */
	/*@ public static invariant !closeDoor.equals(insertConfigData); */
	/*@ public static invariant !requestAdminOp.equals(doingOp); */
	/*@ public static invariant !requestAdminOp.equals(invalidRequest); */
	/*@ public static invariant !requestAdminOp.equals(invalidData); */
	/*@ public static invariant !requestAdminOp.equals(insertEnrolmentData); */
	/*@ public static invariant !requestAdminOp.equals(validatingEnrolmentData); */
	/*@ public static invariant !requestAdminOp.equals(enrolmentFailed); */
	/*@ public static invariant !requestAdminOp.equals(archiveFailed); */
	/*@ public static invariant !requestAdminOp.equals(insertBlankFloppy); */
	/*@ public static invariant !requestAdminOp.equals(insertConfigData); */
	/*@ public static invariant !doingOp.equals(invalidRequest); */
	/*@ public static invariant !doingOp.equals(invalidData); */
	/*@ public static invariant !doingOp.equals(insertEnrolmentData); */
	/*@ public static invariant !doingOp.equals(validatingEnrolmentData); */
	/*@ public static invariant !doingOp.equals(enrolmentFailed); */
	/*@ public static invariant !doingOp.equals(archiveFailed); */
	/*@ public static invariant !doingOp.equals(insertBlankFloppy); */
	/*@ public static invariant !doingOp.equals(insertConfigData); */
	/*@ public static invariant !invalidRequest.equals(invalidData); */
	/*@ public static invariant !invalidRequest.equals(insertEnrolmentData); */
	/*@ public static invariant !invalidRequest.equals(validatingEnrolmentData); */
	/*@ public static invariant !invalidRequest.equals(enrolmentFailed); */
	/*@ public static invariant !invalidRequest.equals(archiveFailed); */
	/*@ public static invariant !invalidRequest.equals(insertBlankFloppy); */
	/*@ public static invariant !invalidRequest.equals(insertConfigData); */
	/*@ public static invariant !invalidData.equals(insertEnrolmentData); */
	/*@ public static invariant !invalidData.equals(validatingEnrolmentData); */
	/*@ public static invariant !invalidData.equals(enrolmentFailed); */
	/*@ public static invariant !invalidData.equals(archiveFailed); */
	/*@ public static invariant !invalidData.equals(insertBlankFloppy); */
	/*@ public static invariant !invalidData.equals(insertConfigData); */
	/*@ public static invariant !insertEnrolmentData.equals(validatingEnrolmentData); */
	/*@ public static invariant !insertEnrolmentData.equals(enrolmentFailed); */
	/*@ public static invariant !insertEnrolmentData.equals(archiveFailed); */
	/*@ public static invariant !insertEnrolmentData.equals(insertBlankFloppy); */
	/*@ public static invariant !insertEnrolmentData.equals(insertConfigData); */
	/*@ public static invariant !validatingEnrolmentData.equals(enrolmentFailed); */
	/*@ public static invariant !validatingEnrolmentData.equals(archiveFailed); */
	/*@ public static invariant !validatingEnrolmentData.equals(insertBlankFloppy); */
	/*@ public static invariant !validatingEnrolmentData.equals(insertConfigData); */
	/*@ public static invariant !enrolmentFailed.equals(archiveFailed); */
	/*@ public static invariant !enrolmentFailed.equals(insertBlankFloppy); */
	/*@ public static invariant !enrolmentFailed.equals(insertConfigData); */
	/*@ public static invariant !archiveFailed.equals(insertBlankFloppy); */
	/*@ public static invariant !archiveFailed.equals(insertConfigData); */
	/*@ public static invariant !insertBlankFloppy.equals(insertConfigData); */
	/*@ public static invariant ADMINOP.equals(new BSet<Integer>(archiveLog,updateConfigData,overrideLock,shutdownOp,NoOp)); */
	/*@ public static invariant !archiveLog.equals(updateConfigData); */
	/*@ public static invariant !archiveLog.equals(overrideLock); */
	/*@ public static invariant !archiveLog.equals(shutdownOp); */
	/*@ public static invariant !archiveLog.equals(NoOp); */
	/*@ public static invariant !updateConfigData.equals(overrideLock); */
	/*@ public static invariant !updateConfigData.equals(shutdownOp); */
	/*@ public static invariant !updateConfigData.equals(NoOp); */
	/*@ public static invariant !overrideLock.equals(shutdownOp); */
	/*@ public static invariant !overrideLock.equals(NoOp); */
	/*@ public static invariant !shutdownOp.equals(NoOp); */
	/*@ public static invariant AdminPrivilege.equals(new BSet<Integer>(guard,securityOfficer,auditManager,NoRole)); */
	/*@ public static invariant KEYBOARD.equals(new BSet<Integer>(noKB,badKB,good1,good2,good3)); */
	/*@ public static invariant !noKB.equals(badKB); */
	/*@ public static invariant !noKB.equals(good1); */
	/*@ public static invariant !noKB.equals(good2); */
	/*@ public static invariant !noKB.equals(good3); */
	/*@ public static invariant !badKB.equals(good1); */
	/*@ public static invariant !badKB.equals(good2); */
	/*@ public static invariant !badKB.equals(good3); */
	/*@ public static invariant !good1.equals(good2); */
	/*@ public static invariant !good1.equals(good3); */
	/*@ public static invariant !good2.equals(good3); */
	/*@ public static invariant DOOR.equals(new BSet<Integer>(open,closed)); */
	/*@ public static invariant !open.equals(closed); */
	/*@ public static invariant ss.equals(new BSet<Integer>(quiescent,gotUserToken,waitingEntry,waitingRemoveTokenSuccess)); */
	/*@ public static invariant ss.finite(); */


	/******Variable definitions******/
	/*@ spec_public */ private Integer FingerPresence;

	/*@ spec_public */ private BSet<Integer> admin;

	/*@ spec_public */ private BRelation<Integer,Integer> adminToken;

	/*@ spec_public */ private Integer adminTokenPresence;

	/*@ spec_public */ private Integer alarmSilentDuration;

	/*@ spec_public */ private Integer alarmTimeout1;

	/*@ spec_public */ private Integer alarmTimeout2;

	/*@ spec_public */ private BSet<Integer> attCert;

	/*@ spec_public */ private BRelation<Integer,Integer> attCertTokID;

	/*@ spec_public */ private BSet<Integer> authCert;

	/*@ spec_public */ private BRelation<Integer,Integer> authCertClearence;

	/*@ spec_public */ private BRelation<Integer,Integer> authCertRole;

	/*@ spec_public */ private BRelation<Integer,Integer> availableOps;

	/*@ spec_public */ private BRelation<Integer,Integer> baseCertID;

	/*@ spec_public */ private BRelation<Integer,Integer> certificateID;

	/*@ spec_public */ private BSet<Integer> certificates;

	/*@ spec_public */ private BSet<Integer> clearence;

	/*@ spec_public */ private BRelation<Integer,Integer> configFile;

	/*@ spec_public */ private BRelation<Integer,Integer> currentAdminOp;

	/*@ spec_public */ private Integer currentAdminToken;

	/*@ spec_public */ private Integer currentDoor;

	/*@ spec_public */ private Integer currentToken;

	/*@ spec_public */ private Integer displayMessage1;

	/*@ spec_public */ private Integer displayMessage2;

	/*@ spec_public */ private Integer displayMessage3;

	/*@ spec_public */ private Integer doorAlarm;

	/*@ spec_public */ private Integer enclaveStatus1;

	/*@ spec_public */ private Integer enclaveStatus2;

	/*@ spec_public */ private BRelation<Integer,Integer> enrolmentFile;

	/*@ spec_public */ private BRelation<Integer,BRelation<Integer,BSet<Integer>>> entryPeriod;

	/*@ spec_public */ private Integer entry_status1;

	/*@ spec_public */ private Integer entry_status2;

	/*@ spec_public */ private BSet<Integer> fingerprint;

	/*@ spec_public */ private BSet<Integer> fingerprintTry;

	/*@ spec_public */ private Integer floppyPresence;

	/*@ spec_public */ private BRelation<Integer,Integer> fpTemplate;

	/*@ spec_public */ private BRelation<Integer,Integer> goodFP;

	/*@ spec_public */ private BRelation<Integer,Integer> goodTok;

	/*@ spec_public */ private BSet<Integer> iandaCert;

	/*@ spec_public */ private BSet<Integer> idCert;

	/*@ spec_public */ private BRelation<Integer,Integer> isValidatedBy;

	/*@ spec_public */ private Integer keyedDataPresence;

	/*@ spec_public */ private BRelation<Integer,Integer> keyedOps;

	/*@ spec_public */ private Integer latchTimeout1;

	/*@ spec_public */ private Integer latchTimeout2;

	/*@ spec_public */ private Integer latchUnlockDuration;

	/*@ spec_public */ private Boolean locked;

	/*@ spec_public */ private BSet<Integer> privCert;

	/*@ spec_public */ private BRelation<Integer,Integer> privCertClearence;

	/*@ spec_public */ private BRelation<Integer,Integer> privCertRole;

	/*@ spec_public */ private BSet<Integer> privilege;

	/*@ spec_public */ private BSet<Integer> publicKeys;

	/*@ spec_public */ private BRelation<Integer,Integer> rolePresent;

	/*@ spec_public */ private Integer screenMsg1;

	/*@ spec_public */ private Integer screenMsg2;

	/*@ spec_public */ private BSet<Integer> status_sec;

	/*@ spec_public */ private BRelation<Integer,Integer> subject;

	/*@ spec_public */ private BRelation<Integer,Integer> subjectPubKey;

	/*@ spec_public */ private BRelation<Integer,BSet<Integer>> timesRecentTo;

	/*@ spec_public */ private BRelation<Integer,Integer> tokenAuthCert;

	/*@ spec_public */ private BSet<Integer> tokenID;

	/*@ spec_public */ private BRelation<Integer,Integer> tokenIandaCert;

	/*@ spec_public */ private BRelation<Integer,Integer> tokenPrivCert;

	/*@ spec_public */ private Integer tokenRemovalDuration;

	/*@ spec_public */ private Integer tokenRemovalTimeout;

	/*@ spec_public */ private BSet<Integer> tokenTry;

	/*@ spec_public */ private BSet<Integer> user;

	/*@ spec_public */ private Integer userTokenPresence;

	/*@ spec_public */ private BSet<Integer> validEnrol;

	/*@ spec_public */ private BRelation<Integer,Integer> validityPeriods;



	/*@ public normal_behavior
		requires true;
		assignable \nothing;
		ensures \result == status_sec.intValue(); */
	public /*@ pure */ int variant(){
		return status_sec.int_size();
	}


	/******Invariant definition******/
	/*@ public invariant
		certificates.isSubset(CERTIFICATES) &&
		publicKeys.isSubset(KEYS) &&
		 certificateID.domain().isSubset(certificates) && certificateID.range().isSubset(CERTIFICATEID) && certificateID.isaFunction() && BRelation.cross(certificates,CERTIFICATEID).has(certificateID) &&
		 validityPeriods.domain().isSubset(certificates) && validityPeriods.range().isSubset(NAT.instance) && BRelation.cross(certificates,NAT.instance).has(validityPeriods) &&
		 isValidatedBy.domain().isSubset(certificates) && isValidatedBy.range().isSubset(publicKeys) && isValidatedBy.isaFunction() && isValidatedBy.inverse().isaFunction() && BRelation.cross(certificates,publicKeys).has(isValidatedBy) &&
		user.isSubset(USER) &&
		idCert.isSubset(certificates) &&
		attCert.isSubset(certificates) &&
		(idCert.union(attCert)).equals(certificates) &&
		(idCert.intersection(attCert)).equals(BSet.EMPTY) &&
		 subject.domain().equals(idCert) && subject.range().isSubset(user) && subject.isaFunction() && subject.inverse().isaFunction() && BRelation.cross(idCert,user).has(subject) &&
		 subjectPubKey.domain().isSubset(user) && subjectPubKey.range().isSubset(publicKeys) && subjectPubKey.isaFunction() && subjectPubKey.inverse().isaFunction() && BRelation.cross(user,publicKeys).has(subjectPubKey) &&
		 baseCertID.domain().isSubset(attCert) && baseCertID.range().isSubset(idCert) && baseCertID.isaFunction() && baseCertID.inverse().isaFunction() && BRelation.cross(attCert,idCert).has(baseCertID) &&
		privCert.isSubset(attCert) &&
		iandaCert.isSubset(attCert) &&
		authCert.isSubset(attCert) &&
		(privCert.union(iandaCert.union(authCert))).equals(attCert) &&
		(privCert.intersection(iandaCert)).equals(BSet.EMPTY) &&
		(privCert.intersection(authCert)).equals(BSet.EMPTY) &&
		(privCert.intersection(iandaCert)).equals(BSet.EMPTY) &&
		privilege.isSubset(PRIVILEGE) &&
		clearence.isSubset(CLEARENCE) &&
		fingerprint.isSubset(FINGERPRINTTEMPLATE) &&
		 privCertRole.domain().equals(privCert) && privCertRole.range().isSubset(privilege) && privCertRole.isaFunction() && BRelation.cross(privCert,privilege).has(privCertRole) &&
		 privCertClearence.domain().equals(privCert) && privCertClearence.range().isSubset(clearence) && privCertClearence.isaFunction() && BRelation.cross(privCert,clearence).has(privCertClearence) &&
		 authCertRole.domain().equals(authCert) && authCertRole.range().isSubset(privilege) && authCertRole.isaFunction() && BRelation.cross(authCert,privilege).has(authCertRole) &&
		 authCertClearence.domain().equals(authCert) && authCertClearence.range().isSubset(clearence) && authCertClearence.isaFunction() && BRelation.cross(authCert,clearence).has(authCertClearence) &&
		 fpTemplate.domain().equals(iandaCert) && fpTemplate.range().isSubset(fingerprint) && fpTemplate.isaFunction() && fpTemplate.inverse().isaFunction() && BRelation.cross(iandaCert,fingerprint).has(fpTemplate) &&
		tokenID.isSubset(TOKENID) &&
		 tokenPrivCert.domain().equals(tokenID) && tokenPrivCert.range().isSubset(privCert) && tokenPrivCert.isaFunction() && tokenPrivCert.inverse().isaFunction() && BRelation.cross(tokenID,privCert).has(tokenPrivCert) &&
		 tokenIandaCert.domain().equals(tokenID) && tokenIandaCert.range().isSubset(iandaCert) && tokenIandaCert.isaFunction() && tokenIandaCert.inverse().isaFunction() && BRelation.cross(tokenID,iandaCert).has(tokenIandaCert) &&
		 tokenAuthCert.domain().isSubset(tokenID) && tokenAuthCert.range().isSubset(authCert) && tokenAuthCert.isaFunction() && tokenAuthCert.inverse().isaFunction() && BRelation.cross(tokenID,authCert).has(tokenAuthCert) &&
		ENTRY_STATUS.has(entry_status1) &&
		TOKENID.has(currentToken) &&
		 attCertTokID.domain().equals(attCert) && attCertTokID.range().isSubset(tokenID) && attCertTokID.isaFunction() && attCertTokID.inverse().isaFunction() && BRelation.cross(attCert,tokenID).has(attCertTokID) &&
		fingerprintTry.isSubset(FINGERPRINTTRY) &&
		 goodFP.domain().equals(fingerprint) && goodFP.range().isSubset(fingerprintTry) && goodFP.isaFunction() && BRelation.cross(fingerprint,fingerprintTry).has(goodFP) &&
		tokenTry.isSubset(TOKENTRY) &&
		 goodTok.domain().equals(tokenID) && goodTok.range().isSubset(tokenTry) && goodTok.isaFunction() && BRelation.cross(tokenID,tokenTry).has(goodTok) &&
		DISPLAYMESSAGE.has(displayMessage1) &&
		NAT.instance.has(latchUnlockDuration) &&
		NAT.instance.has(latchTimeout1) &&
		BOOL.instance.has(locked) &&
		DOORALARM.has(doorAlarm) &&
		 (\exists Integer currentTime;((NAT.instance.has(currentTime) && (currentTime).compareTo(MAXTIME) <= 0 && locked.equals(true)) <==> ((currentTime).compareTo(new Integer(latchTimeout1 - latchUnlockDuration)) <= 0 || (currentTime).compareTo(latchTimeout1) >= 0))) &&
		(latchTimeout1).compareTo(MAXTIME) <= 0 &&
		(new Integer(latchTimeout1 - latchUnlockDuration)).compareTo(new Integer(0)) >= 0 &&
		PRESENCE.has(userTokenPresence) &&
		PRESENCE.has(FingerPresence) &&
		 entryPeriod.domain().equals(PRIVILEGE) && entryPeriod.range().isSubset(BRelation.cross(CLEARENCE,((time).pow()))) && entryPeriod.isaFunction() && BRelation.cross(PRIVILEGE,BRelation.cross(CLEARENCE,((time).pow()))).has(entryPeriod) &&
		NAT.instance.has(tokenRemovalTimeout) &&
		NAT.instance.has(alarmTimeout1) &&
		NAT.instance.has(alarmSilentDuration) &&
		NAT.instance.has(tokenRemovalDuration) &&
		ENCLAVESTATUS.has(enclaveStatus1) &&
		SCREENTEXT.has(screenMsg1) &&
		validEnrol.isSubset(ENROL) &&
		 enrolmentFile.domain().equals(FLOPPY) && enrolmentFile.range().isSubset(ENROL) && enrolmentFile.isaFunction() && BRelation.cross(FLOPPY,ENROL).has(enrolmentFile) &&
		PRESENCE.has(floppyPresence) &&
		ENTRY_STATUS.has(entry_status2) &&
		DISPLAYMESSAGE.has(displayMessage2) &&
		admin.isSubset(ADMIN) &&
		 adminToken.domain().equals(admin) && adminToken.range().isSubset(TOKENID) && adminToken.isaFunction() && adminToken.inverse().isaFunction() && BRelation.cross(admin,TOKENID).has(adminToken) &&
		 rolePresent.domain().equals(admin) && rolePresent.range().isSubset(AdminPrivilege) && rolePresent.isaFunction() && BRelation.cross(admin,AdminPrivilege).has(rolePresent) &&
		 availableOps.domain().equals(admin) && availableOps.range().isSubset(ADMINOP) && availableOps.isaFunction() && BRelation.cross(admin,ADMINOP).has(availableOps) &&
		 currentAdminOp.domain().equals(admin) && currentAdminOp.range().isSubset(ADMINOP) && currentAdminOp.isaFunction() && BRelation.cross(admin,ADMINOP).has(currentAdminOp) &&
		 keyedOps.domain().equals(ADMINOP) && keyedOps.range().isSubset(KEYBOARD) && keyedOps.isaFunction() && keyedOps.inverse().isaFunction() && BRelation.cross(ADMINOP,KEYBOARD).has(keyedOps) &&
		 (\forall Integer admn;((admin.has(admn) && rolePresent.domain().has(admn) && rolePresent.apply(admn).equals(NoRole)) ==> (availableOps.apply(admn).equals(NoOp)))) &&
		 (\forall Integer admn;((admin.has(admn) && rolePresent.domain().has(admn) && !rolePresent.apply(admn).equals(NoRole) && rolePresent.apply(admn).equals(guard)) ==> (availableOps.apply(admn).equals(overrideLock)))) &&
		 (\forall Integer admn;((admin.has(admn) && !rolePresent.apply(admn).equals(NoRole) && rolePresent.apply(admn).equals(auditManager) && rolePresent.domain().has(admn)) ==> (availableOps.apply(admn).equals(archiveLog)))) &&
		 (\forall Integer admn;((admin.has(admn) && !rolePresent.apply(admn).equals(NoRole) && rolePresent.apply(admn).equals(securityOfficer) && rolePresent.domain().has(admn)) ==> (availableOps.apply(admn).equals(updateConfigData) || availableOps.apply(admn).equals(shutdownOp)))) &&
		TOKENID.has(currentAdminToken) &&
		PRESENCE.has(adminTokenPresence) &&
		PRESENCE.has(keyedDataPresence) &&
		 configFile.domain().isSubset(KEYBOARD) && configFile.range().isSubset(FLOPPY) && configFile.isaFunction() && BRelation.cross(KEYBOARD,FLOPPY).has(configFile) &&
		DOOR.has(currentDoor) &&
		NAT.instance.has(alarmTimeout2) &&
		NAT.instance.has(latchTimeout2) &&
		ENCLAVESTATUS.has(enclaveStatus2) &&
		SCREENTEXT.has(screenMsg2) &&
		DISPLAYMESSAGE.has(displayMessage3) &&
		 timesRecentTo.domain().equals(time) && timesRecentTo.range().isSubset(((time).pow())) && timesRecentTo.isaFunction() && BRelation.cross(time,((time).pow())).has(timesRecentTo) &&
		 (\exists Integer currentTime;((NAT.instance.has(currentTime) && locked.equals(true) && currentDoor.equals(open) && (currentTime).compareTo(alarmTimeout2) > 0) ==> (doorAlarm.equals(d_alarming)))) &&
		status_sec.isSubset(ss); */


	/******Getter and Mutator methods definition******/
	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.tokenPrivCert;*/
	public /*@ pure */ BRelation<Integer,Integer> get_tokenPrivCert(){
		return this.tokenPrivCert;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.tokenPrivCert;
	    ensures this.tokenPrivCert == tokenPrivCert;*/
	public void set_tokenPrivCert(BRelation<Integer,Integer> tokenPrivCert){
		this.tokenPrivCert = tokenPrivCert;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.entry_status1;*/
	public /*@ pure */ Integer get_entry_status1(){
		return this.entry_status1;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.entry_status1;
	    ensures this.entry_status1 == entry_status1;*/
	public void set_entry_status1(Integer entry_status1){
		this.entry_status1 = entry_status1;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.publicKeys;*/
	public /*@ pure */ BSet<Integer> get_publicKeys(){
		return this.publicKeys;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.publicKeys;
	    ensures this.publicKeys == publicKeys;*/
	public void set_publicKeys(BSet<Integer> publicKeys){
		this.publicKeys = publicKeys;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.enrolmentFile;*/
	public /*@ pure */ BRelation<Integer,Integer> get_enrolmentFile(){
		return this.enrolmentFile;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.enrolmentFile;
	    ensures this.enrolmentFile == enrolmentFile;*/
	public void set_enrolmentFile(BRelation<Integer,Integer> enrolmentFile){
		this.enrolmentFile = enrolmentFile;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.entry_status2;*/
	public /*@ pure */ Integer get_entry_status2(){
		return this.entry_status2;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.entry_status2;
	    ensures this.entry_status2 == entry_status2;*/
	public void set_entry_status2(Integer entry_status2){
		this.entry_status2 = entry_status2;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.latchUnlockDuration;*/
	public /*@ pure */ Integer get_latchUnlockDuration(){
		return this.latchUnlockDuration;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.latchUnlockDuration;
	    ensures this.latchUnlockDuration == latchUnlockDuration;*/
	public void set_latchUnlockDuration(Integer latchUnlockDuration){
		this.latchUnlockDuration = latchUnlockDuration;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.tokenID;*/
	public /*@ pure */ BSet<Integer> get_tokenID(){
		return this.tokenID;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.tokenID;
	    ensures this.tokenID == tokenID;*/
	public void set_tokenID(BSet<Integer> tokenID){
		this.tokenID = tokenID;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.goodTok;*/
	public /*@ pure */ BRelation<Integer,Integer> get_goodTok(){
		return this.goodTok;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.goodTok;
	    ensures this.goodTok == goodTok;*/
	public void set_goodTok(BRelation<Integer,Integer> goodTok){
		this.goodTok = goodTok;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.subject;*/
	public /*@ pure */ BRelation<Integer,Integer> get_subject(){
		return this.subject;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.subject;
	    ensures this.subject == subject;*/
	public void set_subject(BRelation<Integer,Integer> subject){
		this.subject = subject;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.currentDoor;*/
	public /*@ pure */ Integer get_currentDoor(){
		return this.currentDoor;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.currentDoor;
	    ensures this.currentDoor == currentDoor;*/
	public void set_currentDoor(Integer currentDoor){
		this.currentDoor = currentDoor;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.tokenAuthCert;*/
	public /*@ pure */ BRelation<Integer,Integer> get_tokenAuthCert(){
		return this.tokenAuthCert;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.tokenAuthCert;
	    ensures this.tokenAuthCert == tokenAuthCert;*/
	public void set_tokenAuthCert(BRelation<Integer,Integer> tokenAuthCert){
		this.tokenAuthCert = tokenAuthCert;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.adminToken;*/
	public /*@ pure */ BRelation<Integer,Integer> get_adminToken(){
		return this.adminToken;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.adminToken;
	    ensures this.adminToken == adminToken;*/
	public void set_adminToken(BRelation<Integer,Integer> adminToken){
		this.adminToken = adminToken;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.isValidatedBy;*/
	public /*@ pure */ BRelation<Integer,Integer> get_isValidatedBy(){
		return this.isValidatedBy;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.isValidatedBy;
	    ensures this.isValidatedBy == isValidatedBy;*/
	public void set_isValidatedBy(BRelation<Integer,Integer> isValidatedBy){
		this.isValidatedBy = isValidatedBy;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.idCert;*/
	public /*@ pure */ BSet<Integer> get_idCert(){
		return this.idCert;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.idCert;
	    ensures this.idCert == idCert;*/
	public void set_idCert(BSet<Integer> idCert){
		this.idCert = idCert;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.fingerprint;*/
	public /*@ pure */ BSet<Integer> get_fingerprint(){
		return this.fingerprint;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.fingerprint;
	    ensures this.fingerprint == fingerprint;*/
	public void set_fingerprint(BSet<Integer> fingerprint){
		this.fingerprint = fingerprint;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.currentAdminOp;*/
	public /*@ pure */ BRelation<Integer,Integer> get_currentAdminOp(){
		return this.currentAdminOp;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.currentAdminOp;
	    ensures this.currentAdminOp == currentAdminOp;*/
	public void set_currentAdminOp(BRelation<Integer,Integer> currentAdminOp){
		this.currentAdminOp = currentAdminOp;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.privCertRole;*/
	public /*@ pure */ BRelation<Integer,Integer> get_privCertRole(){
		return this.privCertRole;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.privCertRole;
	    ensures this.privCertRole == privCertRole;*/
	public void set_privCertRole(BRelation<Integer,Integer> privCertRole){
		this.privCertRole = privCertRole;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.keyedDataPresence;*/
	public /*@ pure */ Integer get_keyedDataPresence(){
		return this.keyedDataPresence;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.keyedDataPresence;
	    ensures this.keyedDataPresence == keyedDataPresence;*/
	public void set_keyedDataPresence(Integer keyedDataPresence){
		this.keyedDataPresence = keyedDataPresence;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.locked;*/
	public /*@ pure */ Boolean get_locked(){
		return this.locked;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.locked;
	    ensures this.locked == locked;*/
	public void set_locked(Boolean locked){
		this.locked = locked;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.availableOps;*/
	public /*@ pure */ BRelation<Integer,Integer> get_availableOps(){
		return this.availableOps;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.availableOps;
	    ensures this.availableOps == availableOps;*/
	public void set_availableOps(BRelation<Integer,Integer> availableOps){
		this.availableOps = availableOps;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.privCert;*/
	public /*@ pure */ BSet<Integer> get_privCert(){
		return this.privCert;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.privCert;
	    ensures this.privCert == privCert;*/
	public void set_privCert(BSet<Integer> privCert){
		this.privCert = privCert;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.tokenIandaCert;*/
	public /*@ pure */ BRelation<Integer,Integer> get_tokenIandaCert(){
		return this.tokenIandaCert;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.tokenIandaCert;
	    ensures this.tokenIandaCert == tokenIandaCert;*/
	public void set_tokenIandaCert(BRelation<Integer,Integer> tokenIandaCert){
		this.tokenIandaCert = tokenIandaCert;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.fpTemplate;*/
	public /*@ pure */ BRelation<Integer,Integer> get_fpTemplate(){
		return this.fpTemplate;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.fpTemplate;
	    ensures this.fpTemplate == fpTemplate;*/
	public void set_fpTemplate(BRelation<Integer,Integer> fpTemplate){
		this.fpTemplate = fpTemplate;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.tokenRemovalTimeout;*/
	public /*@ pure */ Integer get_tokenRemovalTimeout(){
		return this.tokenRemovalTimeout;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.tokenRemovalTimeout;
	    ensures this.tokenRemovalTimeout == tokenRemovalTimeout;*/
	public void set_tokenRemovalTimeout(Integer tokenRemovalTimeout){
		this.tokenRemovalTimeout = tokenRemovalTimeout;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.validEnrol;*/
	public /*@ pure */ BSet<Integer> get_validEnrol(){
		return this.validEnrol;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.validEnrol;
	    ensures this.validEnrol == validEnrol;*/
	public void set_validEnrol(BSet<Integer> validEnrol){
		this.validEnrol = validEnrol;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.certificates;*/
	public /*@ pure */ BSet<Integer> get_certificates(){
		return this.certificates;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.certificates;
	    ensures this.certificates == certificates;*/
	public void set_certificates(BSet<Integer> certificates){
		this.certificates = certificates;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.enclaveStatus2;*/
	public /*@ pure */ Integer get_enclaveStatus2(){
		return this.enclaveStatus2;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.enclaveStatus2;
	    ensures this.enclaveStatus2 == enclaveStatus2;*/
	public void set_enclaveStatus2(Integer enclaveStatus2){
		this.enclaveStatus2 = enclaveStatus2;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.userTokenPresence;*/
	public /*@ pure */ Integer get_userTokenPresence(){
		return this.userTokenPresence;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.userTokenPresence;
	    ensures this.userTokenPresence == userTokenPresence;*/
	public void set_userTokenPresence(Integer userTokenPresence){
		this.userTokenPresence = userTokenPresence;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.enclaveStatus1;*/
	public /*@ pure */ Integer get_enclaveStatus1(){
		return this.enclaveStatus1;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.enclaveStatus1;
	    ensures this.enclaveStatus1 == enclaveStatus1;*/
	public void set_enclaveStatus1(Integer enclaveStatus1){
		this.enclaveStatus1 = enclaveStatus1;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.tokenTry;*/
	public /*@ pure */ BSet<Integer> get_tokenTry(){
		return this.tokenTry;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.tokenTry;
	    ensures this.tokenTry == tokenTry;*/
	public void set_tokenTry(BSet<Integer> tokenTry){
		this.tokenTry = tokenTry;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.validityPeriods;*/
	public /*@ pure */ BRelation<Integer,Integer> get_validityPeriods(){
		return this.validityPeriods;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.validityPeriods;
	    ensures this.validityPeriods == validityPeriods;*/
	public void set_validityPeriods(BRelation<Integer,Integer> validityPeriods){
		this.validityPeriods = validityPeriods;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.authCert;*/
	public /*@ pure */ BSet<Integer> get_authCert(){
		return this.authCert;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.authCert;
	    ensures this.authCert == authCert;*/
	public void set_authCert(BSet<Integer> authCert){
		this.authCert = authCert;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.authCertRole;*/
	public /*@ pure */ BRelation<Integer,Integer> get_authCertRole(){
		return this.authCertRole;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.authCertRole;
	    ensures this.authCertRole == authCertRole;*/
	public void set_authCertRole(BRelation<Integer,Integer> authCertRole){
		this.authCertRole = authCertRole;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.keyedOps;*/
	public /*@ pure */ BRelation<Integer,Integer> get_keyedOps(){
		return this.keyedOps;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.keyedOps;
	    ensures this.keyedOps == keyedOps;*/
	public void set_keyedOps(BRelation<Integer,Integer> keyedOps){
		this.keyedOps = keyedOps;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.rolePresent;*/
	public /*@ pure */ BRelation<Integer,Integer> get_rolePresent(){
		return this.rolePresent;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.rolePresent;
	    ensures this.rolePresent == rolePresent;*/
	public void set_rolePresent(BRelation<Integer,Integer> rolePresent){
		this.rolePresent = rolePresent;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.screenMsg1;*/
	public /*@ pure */ Integer get_screenMsg1(){
		return this.screenMsg1;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.screenMsg1;
	    ensures this.screenMsg1 == screenMsg1;*/
	public void set_screenMsg1(Integer screenMsg1){
		this.screenMsg1 = screenMsg1;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.screenMsg2;*/
	public /*@ pure */ Integer get_screenMsg2(){
		return this.screenMsg2;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.screenMsg2;
	    ensures this.screenMsg2 == screenMsg2;*/
	public void set_screenMsg2(Integer screenMsg2){
		this.screenMsg2 = screenMsg2;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.adminTokenPresence;*/
	public /*@ pure */ Integer get_adminTokenPresence(){
		return this.adminTokenPresence;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.adminTokenPresence;
	    ensures this.adminTokenPresence == adminTokenPresence;*/
	public void set_adminTokenPresence(Integer adminTokenPresence){
		this.adminTokenPresence = adminTokenPresence;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.doorAlarm;*/
	public /*@ pure */ Integer get_doorAlarm(){
		return this.doorAlarm;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.doorAlarm;
	    ensures this.doorAlarm == doorAlarm;*/
	public void set_doorAlarm(Integer doorAlarm){
		this.doorAlarm = doorAlarm;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.attCertTokID;*/
	public /*@ pure */ BRelation<Integer,Integer> get_attCertTokID(){
		return this.attCertTokID;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.attCertTokID;
	    ensures this.attCertTokID == attCertTokID;*/
	public void set_attCertTokID(BRelation<Integer,Integer> attCertTokID){
		this.attCertTokID = attCertTokID;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.admin;*/
	public /*@ pure */ BSet<Integer> get_admin(){
		return this.admin;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.admin;
	    ensures this.admin == admin;*/
	public void set_admin(BSet<Integer> admin){
		this.admin = admin;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.alarmTimeout2;*/
	public /*@ pure */ Integer get_alarmTimeout2(){
		return this.alarmTimeout2;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.alarmTimeout2;
	    ensures this.alarmTimeout2 == alarmTimeout2;*/
	public void set_alarmTimeout2(Integer alarmTimeout2){
		this.alarmTimeout2 = alarmTimeout2;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.status_sec;*/
	public /*@ pure */ BSet<Integer> get_status_sec(){
		return this.status_sec;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.status_sec;
	    ensures this.status_sec == status_sec;*/
	public void set_status_sec(BSet<Integer> status_sec){
		this.status_sec = status_sec;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.alarmTimeout1;*/
	public /*@ pure */ Integer get_alarmTimeout1(){
		return this.alarmTimeout1;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.alarmTimeout1;
	    ensures this.alarmTimeout1 == alarmTimeout1;*/
	public void set_alarmTimeout1(Integer alarmTimeout1){
		this.alarmTimeout1 = alarmTimeout1;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.authCertClearence;*/
	public /*@ pure */ BRelation<Integer,Integer> get_authCertClearence(){
		return this.authCertClearence;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.authCertClearence;
	    ensures this.authCertClearence == authCertClearence;*/
	public void set_authCertClearence(BRelation<Integer,Integer> authCertClearence){
		this.authCertClearence = authCertClearence;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.privilege;*/
	public /*@ pure */ BSet<Integer> get_privilege(){
		return this.privilege;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.privilege;
	    ensures this.privilege == privilege;*/
	public void set_privilege(BSet<Integer> privilege){
		this.privilege = privilege;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.baseCertID;*/
	public /*@ pure */ BRelation<Integer,Integer> get_baseCertID(){
		return this.baseCertID;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.baseCertID;
	    ensures this.baseCertID == baseCertID;*/
	public void set_baseCertID(BRelation<Integer,Integer> baseCertID){
		this.baseCertID = baseCertID;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.tokenRemovalDuration;*/
	public /*@ pure */ Integer get_tokenRemovalDuration(){
		return this.tokenRemovalDuration;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.tokenRemovalDuration;
	    ensures this.tokenRemovalDuration == tokenRemovalDuration;*/
	public void set_tokenRemovalDuration(Integer tokenRemovalDuration){
		this.tokenRemovalDuration = tokenRemovalDuration;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.FingerPresence;*/
	public /*@ pure */ Integer get_FingerPresence(){
		return this.FingerPresence;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.FingerPresence;
	    ensures this.FingerPresence == FingerPresence;*/
	public void set_FingerPresence(Integer FingerPresence){
		this.FingerPresence = FingerPresence;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.attCert;*/
	public /*@ pure */ BSet<Integer> get_attCert(){
		return this.attCert;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.attCert;
	    ensures this.attCert == attCert;*/
	public void set_attCert(BSet<Integer> attCert){
		this.attCert = attCert;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.displayMessage1;*/
	public /*@ pure */ Integer get_displayMessage1(){
		return this.displayMessage1;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.displayMessage1;
	    ensures this.displayMessage1 == displayMessage1;*/
	public void set_displayMessage1(Integer displayMessage1){
		this.displayMessage1 = displayMessage1;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.fingerprintTry;*/
	public /*@ pure */ BSet<Integer> get_fingerprintTry(){
		return this.fingerprintTry;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.fingerprintTry;
	    ensures this.fingerprintTry == fingerprintTry;*/
	public void set_fingerprintTry(BSet<Integer> fingerprintTry){
		this.fingerprintTry = fingerprintTry;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.goodFP;*/
	public /*@ pure */ BRelation<Integer,Integer> get_goodFP(){
		return this.goodFP;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.goodFP;
	    ensures this.goodFP == goodFP;*/
	public void set_goodFP(BRelation<Integer,Integer> goodFP){
		this.goodFP = goodFP;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.latchTimeout1;*/
	public /*@ pure */ Integer get_latchTimeout1(){
		return this.latchTimeout1;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.latchTimeout1;
	    ensures this.latchTimeout1 == latchTimeout1;*/
	public void set_latchTimeout1(Integer latchTimeout1){
		this.latchTimeout1 = latchTimeout1;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.displayMessage2;*/
	public /*@ pure */ Integer get_displayMessage2(){
		return this.displayMessage2;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.displayMessage2;
	    ensures this.displayMessage2 == displayMessage2;*/
	public void set_displayMessage2(Integer displayMessage2){
		this.displayMessage2 = displayMessage2;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.latchTimeout2;*/
	public /*@ pure */ Integer get_latchTimeout2(){
		return this.latchTimeout2;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.latchTimeout2;
	    ensures this.latchTimeout2 == latchTimeout2;*/
	public void set_latchTimeout2(Integer latchTimeout2){
		this.latchTimeout2 = latchTimeout2;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.displayMessage3;*/
	public /*@ pure */ Integer get_displayMessage3(){
		return this.displayMessage3;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.displayMessage3;
	    ensures this.displayMessage3 == displayMessage3;*/
	public void set_displayMessage3(Integer displayMessage3){
		this.displayMessage3 = displayMessage3;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.configFile;*/
	public /*@ pure */ BRelation<Integer,Integer> get_configFile(){
		return this.configFile;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.configFile;
	    ensures this.configFile == configFile;*/
	public void set_configFile(BRelation<Integer,Integer> configFile){
		this.configFile = configFile;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.privCertClearence;*/
	public /*@ pure */ BRelation<Integer,Integer> get_privCertClearence(){
		return this.privCertClearence;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.privCertClearence;
	    ensures this.privCertClearence == privCertClearence;*/
	public void set_privCertClearence(BRelation<Integer,Integer> privCertClearence){
		this.privCertClearence = privCertClearence;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.clearence;*/
	public /*@ pure */ BSet<Integer> get_clearence(){
		return this.clearence;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.clearence;
	    ensures this.clearence == clearence;*/
	public void set_clearence(BSet<Integer> clearence){
		this.clearence = clearence;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.alarmSilentDuration;*/
	public /*@ pure */ Integer get_alarmSilentDuration(){
		return this.alarmSilentDuration;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.alarmSilentDuration;
	    ensures this.alarmSilentDuration == alarmSilentDuration;*/
	public void set_alarmSilentDuration(Integer alarmSilentDuration){
		this.alarmSilentDuration = alarmSilentDuration;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.subjectPubKey;*/
	public /*@ pure */ BRelation<Integer,Integer> get_subjectPubKey(){
		return this.subjectPubKey;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.subjectPubKey;
	    ensures this.subjectPubKey == subjectPubKey;*/
	public void set_subjectPubKey(BRelation<Integer,Integer> subjectPubKey){
		this.subjectPubKey = subjectPubKey;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.certificateID;*/
	public /*@ pure */ BRelation<Integer,Integer> get_certificateID(){
		return this.certificateID;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.certificateID;
	    ensures this.certificateID == certificateID;*/
	public void set_certificateID(BRelation<Integer,Integer> certificateID){
		this.certificateID = certificateID;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.timesRecentTo;*/
	public /*@ pure */ BRelation<Integer,BSet<Integer>> get_timesRecentTo(){
		return this.timesRecentTo;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.timesRecentTo;
	    ensures this.timesRecentTo == timesRecentTo;*/
	public void set_timesRecentTo(BRelation<Integer,BSet<Integer>> timesRecentTo){
		this.timesRecentTo = timesRecentTo;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.currentToken;*/
	public /*@ pure */ Integer get_currentToken(){
		return this.currentToken;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.currentToken;
	    ensures this.currentToken == currentToken;*/
	public void set_currentToken(Integer currentToken){
		this.currentToken = currentToken;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.currentAdminToken;*/
	public /*@ pure */ Integer get_currentAdminToken(){
		return this.currentAdminToken;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.currentAdminToken;
	    ensures this.currentAdminToken == currentAdminToken;*/
	public void set_currentAdminToken(Integer currentAdminToken){
		this.currentAdminToken = currentAdminToken;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.entryPeriod;*/
	public /*@ pure */ BRelation<Integer,BRelation<Integer,BSet<Integer>>> get_entryPeriod(){
		return this.entryPeriod;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.entryPeriod;
	    ensures this.entryPeriod == entryPeriod;*/
	public void set_entryPeriod(BRelation<Integer,BRelation<Integer,BSet<Integer>>> entryPeriod){
		this.entryPeriod = entryPeriod;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.iandaCert;*/
	public /*@ pure */ BSet<Integer> get_iandaCert(){
		return this.iandaCert;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.iandaCert;
	    ensures this.iandaCert == iandaCert;*/
	public void set_iandaCert(BSet<Integer> iandaCert){
		this.iandaCert = iandaCert;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.user;*/
	public /*@ pure */ BSet<Integer> get_user(){
		return this.user;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.user;
	    ensures this.user == user;*/
	public void set_user(BSet<Integer> user){
		this.user = user;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable \nothing;
	    ensures \result == this.floppyPresence;*/
	public /*@ pure */ Integer get_floppyPresence(){
		return this.floppyPresence;
	}

	/*@ public normal_behavior
	    requires true;
	    assignable this.floppyPresence;
	    ensures this.floppyPresence == floppyPresence;*/
	public void set_floppyPresence(Integer floppyPresence){
		this.floppyPresence = floppyPresence;
	}



	/*@ public normal_behavior
	    requires true;
	    assignable \everything;
	    ensures
		certificates.isEmpty() &&
		isValidatedBy.isEmpty() &&
		validityPeriods.isEmpty() &&
		publicKeys.isEmpty() &&
		certificateID.isEmpty() &&
		idCert.isEmpty() &&
		attCert.isEmpty() &&
		subject.isEmpty() &&
		subjectPubKey.isEmpty() &&
		user.isEmpty() &&
		baseCertID.isEmpty() &&
		privCert.isEmpty() &&
		iandaCert.isEmpty() &&
		authCert.isEmpty() &&
		privCertRole.isEmpty() &&
		privCertClearence.isEmpty() &&
		authCertRole.isEmpty() &&
		authCertClearence.isEmpty() &&
		fpTemplate.isEmpty() &&
		privilege.isEmpty() &&
		clearence.isEmpty() &&
		fingerprint.isEmpty() &&
		tokenPrivCert.isEmpty() &&
		tokenIandaCert.isEmpty() &&
		tokenAuthCert.isEmpty() &&
		entry_status1 == quiescent &&
		(\exists Integer currentToken_localVar; TOKENID.has(currentToken_localVar); currentToken.equals(currentToken_localVar)) &&
		tokenID.isEmpty() &&
		attCertTokID.isEmpty() &&
		fingerprintTry.isEmpty() &&
		goodFP.isEmpty() &&
		goodTok.isEmpty() &&
		tokenTry.isEmpty() &&
		displayMessage1 == blank &&
		latchUnlockDuration == 0 &&
		latchTimeout1 == 0 &&
		doorAlarm == d_silent &&
		locked == true &&
		userTokenPresence == absent &&
		FingerPresence == absent &&
		(\exists BRelation<Integer,BRelation<Integer,BSet<Integer>>> entryPeriod_localVar; BRelation.cross(PRIVILEGE,BRelation.cross(CLEARENCE,new BSet<BSet<Integer>>(new BSet<Integer>(1))).pow()).pow().has(entryPeriod_localVar); entryPeriod.equals(entryPeriod_localVar)) &&
		tokenRemovalTimeout == 0 &&
		alarmTimeout1 == 0 &&
		alarmSilentDuration == 0 &&
		tokenRemovalDuration == 0 &&
		enclaveStatus1 == notEnrolled &&
		screenMsg1 == clear &&
		validEnrol.isEmpty() &&
		(\exists BRelation<Integer,Integer> enrolmentFile_localVar; BRelation.cross(FLOPPY,ENROL).pow().has(enrolmentFile_localVar); enrolmentFile.equals(enrolmentFile_localVar)) &&
		floppyPresence == absent &&
		entry_status2 == quiescent &&
		displayMessage2 == blank &&
		admin.isEmpty() &&
		rolePresent.isEmpty() &&
		availableOps.isEmpty() &&
		currentAdminOp.isEmpty() &&
		(\exists BRelation<Integer,Integer> keyedOps_localVar; BRelation.cross(ADMINOP,KEYBOARD).pow().has(keyedOps_localVar); keyedOps.equals(keyedOps_localVar)) &&
		(\exists Integer currentAdminToken_localVar; TOKENID.has(currentAdminToken_localVar); currentAdminToken.equals(currentAdminToken_localVar)) &&
		adminTokenPresence == absent &&
		adminToken.isEmpty() &&
		keyedDataPresence == absent &&
		(\exists BRelation<Integer,Integer> configFile_localVar; BRelation.cross(KEYBOARD,FLOPPY).pow().has(configFile_localVar); configFile.equals(configFile_localVar)) &&
		currentDoor == closed &&
		alarmTimeout2 == 0 &&
		latchTimeout2 == 0 &&
		enclaveStatus2 == notEnrolled &&
		screenMsg2 == clear &&
		displayMessage3 == blank &&
		(\exists BRelation<Integer,BSet<Integer>> timesRecentTo_localVar; BRelation.cross(time,(time).pow()).pow().has(timesRecentTo_localVar); timesRecentTo.equals(timesRecentTo_localVar)) &&
		status_sec.equals(new BSet<Integer>(quiescent,gotUserToken,waitingEntry,waitingRemoveTokenSuccess));*/
	public ref6_admin(){
	}
}