package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class ValidateAdminTokenFail_3{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public ValidateAdminTokenFail_3(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (NAT.instance.has(currentTime) && machine.get_enclaveStatus2().equals(machine.gotAdminToken) && machine.get_adminTokenPresence().equals(machine.present) && machine.get_tokenAuthCert().domain().has(machine.get_currentAdminToken()) && !machine.get_validityPeriods().image(new BSet<Integer>(machine.get_tokenAuthCert().apply(machine.get_currentAdminToken()))).has(currentTime)); */
	public /*@ pure */ boolean guard_ValidateAdminTokenFail_3( Integer currentTime) {
		return (
			NAT.instance.has(currentTime) && 
			machine.get_enclaveStatus2().equals(machine.gotAdminToken) && 
			machine.get_adminTokenPresence().equals(machine.present) && 
			machine.get_tokenAuthCert().domain().has(machine.get_currentAdminToken()) && 
			!machine.get_validityPeriods().image(new BSet<Integer>(machine.get_tokenAuthCert().apply(
						machine.get_currentAdminToken()))).has(currentTime));
	}

	/*@ public normal_behavior
		requires guard_ValidateAdminTokenFail_3(currentTime);
		assignable machine.screenMsg2, machine.enclaveStatus2;
		ensures guard_ValidateAdminTokenFail_3(currentTime) &&  machine.get_screenMsg2() == \old(machine.removeAdminToken) &&  machine.get_enclaveStatus2() == \old(machine.waitingRemoveAdminTokenFail); 
	 also
		requires !guard_ValidateAdminTokenFail_3(currentTime);
		assignable \nothing;
		ensures true; */
	public void run_ValidateAdminTokenFail_3( Integer currentTime){
		if(guard_ValidateAdminTokenFail_3(currentTime)) {
			Integer screenMsg2_tmp = machine.get_screenMsg2();
			Integer enclaveStatus2_tmp = machine.get_enclaveStatus2();

			machine.set_screenMsg2(machine.removeAdminToken);
			machine.set_enclaveStatus2(machine.waitingRemoveAdminTokenFail);

			System.out.println("ValidateAdminTokenFail_3 executed currentTime: " + currentTime + " ");
		}
	}

}
