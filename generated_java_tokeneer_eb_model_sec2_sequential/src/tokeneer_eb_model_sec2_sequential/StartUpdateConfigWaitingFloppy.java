package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class StartUpdateConfigWaitingFloppy{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public StartUpdateConfigWaitingFloppy(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.updateConfigData) && machine.get_floppyPresence().equals(machine.absent) && machine.get_enclaveStatus2().equals(machine.waitingStartAdminOp) && machine.get_adminTokenPresence().equals(machine.present)); */
	public /*@ pure */ boolean guard_StartUpdateConfigWaitingFloppy() {
		return (machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && machine.get_currentAdminOp().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.updateConfigData) && machine.get_floppyPresence().equals(machine.absent) && machine.get_enclaveStatus2().equals(machine.waitingStartAdminOp) && machine.get_adminTokenPresence().equals(machine.present));
	}

	/*@ public normal_behavior
		requires guard_StartUpdateConfigWaitingFloppy();
		assignable machine.screenMsg2;
		ensures guard_StartUpdateConfigWaitingFloppy() &&  machine.get_screenMsg2() == \old(machine.insertConfigData); 
	 also
		requires !guard_StartUpdateConfigWaitingFloppy();
		assignable \nothing;
		ensures true; */
	public void run_StartUpdateConfigWaitingFloppy(){
		if(guard_StartUpdateConfigWaitingFloppy()) {
			Integer screenMsg2_tmp = machine.get_screenMsg2();

			machine.set_screenMsg2(machine.insertConfigData);

			System.out.println("StartUpdateConfigWaitingFloppy executed ");
		}
	}

}
