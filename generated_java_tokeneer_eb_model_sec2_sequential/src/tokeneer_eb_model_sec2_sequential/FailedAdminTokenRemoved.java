package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class FailedAdminTokenRemoved{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public FailedAdminTokenRemoved(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_enclaveStatus2().equals(machine.waitingRemoveAdminTokenFail) && machine.get_adminTokenPresence().equals(machine.absent)); */
	public /*@ pure */ boolean guard_FailedAdminTokenRemoved() {
		return (
			machine.get_enclaveStatus2().equals(machine.waitingRemoveAdminTokenFail) && 
			machine.get_adminTokenPresence().equals(machine.absent));
	}

	/*@ public normal_behavior
		requires guard_FailedAdminTokenRemoved();
		assignable machine.screenMsg2, machine.enclaveStatus2;
		ensures guard_FailedAdminTokenRemoved() &&  machine.get_screenMsg2() == \old(machine.welcomeAdmin) &&  machine.get_enclaveStatus2() == \old(machine.enclaveQuiescent); 
	 also
		requires !guard_FailedAdminTokenRemoved();
		assignable \nothing;
		ensures true; */
	public void run_FailedAdminTokenRemoved(){
		if(guard_FailedAdminTokenRemoved()) {
			Integer screenMsg2_tmp = machine.get_screenMsg2();
			Integer enclaveStatus2_tmp = machine.get_enclaveStatus2();

			machine.set_screenMsg2(machine.welcomeAdmin);
			machine.set_enclaveStatus2(machine.enclaveQuiescent);

			System.out.println("FailedAdminTokenRemoved executed ");
		}
	}

}
