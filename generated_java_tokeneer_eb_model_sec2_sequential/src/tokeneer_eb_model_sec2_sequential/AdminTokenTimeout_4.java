package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class AdminTokenTimeout_4{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public AdminTokenTimeout_4(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_enclaveStatus2().equals(machine.enclaveQuiescent) && machine.get_adminTokenPresence().equals(machine.present) && machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && machine.get_attCertTokID().inverse().domain().has(machine.get_currentAdminToken()) && machine.get_authCertRole().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentAdminToken())) && !machine.AdminPrivilege.has(machine.get_authCertRole().apply(machine.get_attCertTokID().inverse().apply(machine.get_currentAdminToken())))); */
	public /*@ pure */ boolean guard_AdminTokenTimeout_4() {
		return (machine.get_enclaveStatus2().equals(machine.enclaveQuiescent) && machine.get_adminTokenPresence().equals(machine.present) && machine.get_adminToken().inverse().domain().has(machine.get_currentAdminToken()) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && machine.get_attCertTokID().inverse().domain().has(machine.get_currentAdminToken()) && machine.get_authCertRole().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentAdminToken())) && !machine.AdminPrivilege.has(machine.get_authCertRole().apply(machine.get_attCertTokID().inverse().apply(machine.get_currentAdminToken()))));
	}

	/*@ public normal_behavior
		requires guard_AdminTokenTimeout_4();
		assignable machine.enclaveStatus2;
		ensures guard_AdminTokenTimeout_4() &&  machine.get_enclaveStatus2() == \old(machine.waitingRemoveAdminTokenFail); 
	 also
		requires !guard_AdminTokenTimeout_4();
		assignable \nothing;
		ensures true; */
	public void run_AdminTokenTimeout_4(){
		if(guard_AdminTokenTimeout_4()) {
			Integer enclaveStatus2_tmp = machine.get_enclaveStatus2();

			machine.set_enclaveStatus2(machine.waitingRemoveAdminTokenFail);

			System.out.println("AdminTokenTimeout_4 executed ");
		}
	}

}
