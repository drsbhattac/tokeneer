package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class readAdminToken{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public readAdminToken(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.TOKENID.has(readAdminToken_token) && new BSet<Integer>(machine.quiescent,machine.waitingRemoveTokenFail).has(machine.get_entry_status2()) && machine.get_enclaveStatus2().equals(machine.enclaveQuiescent) && machine.get_adminToken().inverse().domain().has(readAdminToken_token) && machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(readAdminToken_token)).equals(machine.NoRole) && machine.get_adminTokenPresence().equals(machine.present)); */
	public /*@ pure */ boolean guard_readAdminToken( Integer readAdminToken_token) {
		return (
				machine.TOKENID.has(readAdminToken_token) && new 
				BSet<Integer>(machine.quiescent,machine.waitingRemoveTokenFail).has(machine.get_entry_status2()) && 
				machine.get_enclaveStatus2().equals(machine.enclaveQuiescent) && 
				machine.get_adminToken().inverse().domain().has(readAdminToken_token) && 
				machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(readAdminToken_token)).equals(machine.NoRole) && 
				machine.get_adminTokenPresence().equals(machine.present));
	}

	/*@ public normal_behavior
		requires guard_readAdminToken(readAdminToken_token);
		assignable machine.enclaveStatus2, machine.currentAdminToken;
		ensures guard_readAdminToken(readAdminToken_token) &&  machine.get_enclaveStatus2() == \old(machine.gotAdminToken) &&  machine.get_currentAdminToken() == \old(readAdminToken_token); 
	 also
		requires !guard_readAdminToken(readAdminToken_token);
		assignable \nothing;
		ensures true; */
	public void run_readAdminToken( Integer readAdminToken_token){
		if(guard_readAdminToken(readAdminToken_token)) {
			Integer enclaveStatus2_tmp = machine.get_enclaveStatus2();
			Integer currentAdminToken_tmp = machine.get_currentAdminToken();

			machine.set_enclaveStatus2(machine.gotAdminToken);
			machine.set_currentAdminToken(readAdminToken_token);

			System.out.println("readAdminToken executed readAdminToken_token: " + readAdminToken_token + " ");
		}
	}

}
