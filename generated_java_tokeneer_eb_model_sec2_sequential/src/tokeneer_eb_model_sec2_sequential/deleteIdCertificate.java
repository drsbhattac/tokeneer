package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class deleteIdCertificate{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public deleteIdCertificate(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_certificates().has(deleteCertificate_ce) && machine.get_isValidatedBy().domain().has(deleteCertificate_ce) && machine.get_validityPeriods().domain().has(deleteCertificate_ce) && machine.get_idCert().has(deleteCertificate_ce) && machine.get_subject().domain().has(deleteCertificate_ce) && machine.get_subjectPubKey().domain().has(machine.get_subject().apply(deleteCertificate_ce)) && machine.get_publicKeys().has(machine.get_subjectPubKey().apply(machine.get_subject().apply(deleteCertificate_ce)))); */
	public /*@ pure */ boolean guard_deleteIdCertificate( Integer deleteCertificate_ce) {
		return (machine.get_certificates().has(deleteCertificate_ce) && machine.get_isValidatedBy().domain().has(deleteCertificate_ce) && machine.get_validityPeriods().domain().has(deleteCertificate_ce) && machine.get_idCert().has(deleteCertificate_ce) && machine.get_subject().domain().has(deleteCertificate_ce) && machine.get_subjectPubKey().domain().has(machine.get_subject().apply(deleteCertificate_ce)) && machine.get_publicKeys().has(machine.get_subjectPubKey().apply(machine.get_subject().apply(deleteCertificate_ce))));
	}

	/*@ public normal_behavior
		requires guard_deleteIdCertificate(deleteCertificate_ce);
		assignable machine.certificates, machine.isValidatedBy, machine.validityPeriods, machine.certificateID, machine.idCert, machine.subject, machine.subjectPubKey, machine.baseCertID, machine.user;
		ensures guard_deleteIdCertificate(deleteCertificate_ce) &&  machine.get_certificates().equals(\old(machine.get_certificates().difference(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_isValidatedBy().equals(\old(machine.get_isValidatedBy().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_validityPeriods().equals(\old(machine.get_validityPeriods().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_certificateID().equals(\old(machine.get_certificateID().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_idCert().equals(\old(machine.get_idCert().difference(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_subject().equals(\old(machine.get_subject().difference(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(deleteCertificate_ce,machine.get_subject().apply(deleteCertificate_ce)))))) &&  machine.get_subjectPubKey().equals(\old(machine.get_subjectPubKey().difference(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_subject().apply(deleteCertificate_ce),machine.get_subjectPubKey().apply(machine.get_subject().apply(deleteCertificate_ce))))))) &&  machine.get_baseCertID().equals(\old(machine.get_baseCertID().rangeSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_user().equals(\old(machine.get_user().difference(new BSet<Integer>(machine.get_subject().apply(deleteCertificate_ce))))); 
	 also
		requires !guard_deleteIdCertificate(deleteCertificate_ce);
		assignable \nothing;
		ensures true; */
	public void run_deleteIdCertificate( Integer deleteCertificate_ce){
		if(guard_deleteIdCertificate(deleteCertificate_ce)) {
			BSet<Integer> certificates_tmp = machine.get_certificates();
			BRelation<Integer,Integer> isValidatedBy_tmp = machine.get_isValidatedBy();
			BRelation<Integer,Integer> validityPeriods_tmp = machine.get_validityPeriods();
			BRelation<Integer,Integer> certificateID_tmp = machine.get_certificateID();
			BSet<Integer> idCert_tmp = machine.get_idCert();
			BRelation<Integer,Integer> subject_tmp = machine.get_subject();
			BRelation<Integer,Integer> subjectPubKey_tmp = machine.get_subjectPubKey();
			BRelation<Integer,Integer> baseCertID_tmp = machine.get_baseCertID();
			BSet<Integer> user_tmp = machine.get_user();

			machine.set_certificates(certificates_tmp.difference(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_isValidatedBy(isValidatedBy_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_validityPeriods(validityPeriods_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_certificateID(certificateID_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_idCert(idCert_tmp.difference(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_subject(subject_tmp.difference(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(deleteCertificate_ce,subject_tmp.apply(deleteCertificate_ce)))));
			machine.set_subjectPubKey(subjectPubKey_tmp.difference(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(subject_tmp.apply(deleteCertificate_ce),subjectPubKey_tmp.apply(subject_tmp.apply(deleteCertificate_ce))))));
			machine.set_baseCertID(baseCertID_tmp.rangeSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_user(user_tmp.difference(new BSet<Integer>(subject_tmp.apply(deleteCertificate_ce))));

			System.out.println("deleteIdCertificate executed deleteCertificate_ce: " + deleteCertificate_ce + " ");
		}
	}

}
