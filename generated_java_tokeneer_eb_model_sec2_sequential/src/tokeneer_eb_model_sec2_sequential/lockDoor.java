package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class lockDoor{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public lockDoor(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (NAT.instance.has(currentTime) && (currentTime).compareTo(machine.MAXTIME) < 0 && machine.get_locked().equals(false) && (currentTime).compareTo(new Integer(machine.get_latchTimeout1() - machine.get_latchUnlockDuration())) <= 0 || (currentTime).compareTo(machine.get_latchTimeout1()) >= 0); */
	public /*@ pure */ boolean guard_lockDoor( Integer currentTime) {
		return (NAT.instance.has(currentTime) && (currentTime).compareTo(machine.MAXTIME) < 0 && machine.get_locked().equals(false) && (currentTime).compareTo(new Integer(machine.get_latchTimeout1() - machine.get_latchUnlockDuration())) <= 0 || (currentTime).compareTo(machine.get_latchTimeout1()) >= 0);
	}

	/*@ public normal_behavior
		requires guard_lockDoor(currentTime);
		assignable machine.locked;
		ensures guard_lockDoor(currentTime) &&  machine.get_locked() == \old(true); 
	 also
		requires !guard_lockDoor(currentTime);
		assignable \nothing;
		ensures true; */
	public void run_lockDoor( Integer currentTime){
		if(guard_lockDoor(currentTime)) {
			Boolean locked_tmp = machine.get_locked();

			machine.set_locked(true);

			System.out.println("lockDoor executed currentTime: " + currentTime + " ");
		}
	}

}
