package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class addIandaCertificate_no_TID{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public addIandaCertificate_no_TID(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.CERTIFICATES.difference(machine.get_certificates()).has(addCertificate_ce) && NAT.instance.has(addCertificate_period) && machine.get_publicKeys().has(addCertificate_pubkey) && !machine.get_isValidatedBy().range().has(addCertificate_pubkey) && machine.get_idCert().has(addAttCertificate_idc) && !machine.get_baseCertID().range().has(addAttCertificate_idc) && machine.get_fingerprint().difference(machine.get_fpTemplate().range()).has(addIandaCertificate_fingerPrint) && !machine.get_fpTemplate().domain().has(addCertificate_ce) && !machine.get_fpTemplate().has(new Pair<Integer,Integer>(addCertificate_ce,addIandaCertificate_fingerPrint)) && machine.get_tokenID().difference(machine.get_attCertTokID().range()).has(addPrivCertificate_tid) && !machine.get_attCertTokID().domain().has(addCertificate_ce)); */
	public /*@ pure */ boolean guard_addIandaCertificate_no_TID( Integer addCertificate_ce, Integer addCertificate_period, Integer addCertificate_pubkey, Integer addAttCertificate_idc, Integer addIandaCertificate_fingerPrint, Integer addPrivCertificate_tid) {
		return (machine.CERTIFICATES.difference(machine.get_certificates()).has(addCertificate_ce) && NAT.instance.has(addCertificate_period) && machine.get_publicKeys().has(addCertificate_pubkey) && !machine.get_isValidatedBy().range().has(addCertificate_pubkey) && machine.get_idCert().has(addAttCertificate_idc) && !machine.get_baseCertID().range().has(addAttCertificate_idc) && machine.get_fingerprint().difference(machine.get_fpTemplate().range()).has(addIandaCertificate_fingerPrint) && !machine.get_fpTemplate().domain().has(addCertificate_ce) && !machine.get_fpTemplate().has(new Pair<Integer,Integer>(addCertificate_ce,addIandaCertificate_fingerPrint)) && machine.get_tokenID().difference(machine.get_attCertTokID().range()).has(addPrivCertificate_tid) && !machine.get_attCertTokID().domain().has(addCertificate_ce));
	}

	/*@ public normal_behavior
		requires guard_addIandaCertificate_no_TID(addCertificate_ce,addCertificate_period,addCertificate_pubkey,addAttCertificate_idc,addIandaCertificate_fingerPrint,addPrivCertificate_tid);
		assignable machine.certificates, machine.validityPeriods, machine.isValidatedBy, machine.attCert, machine.baseCertID, machine.iandaCert, machine.fpTemplate, machine.attCertTokID;
		ensures guard_addIandaCertificate_no_TID(addCertificate_ce,addCertificate_period,addCertificate_pubkey,addAttCertificate_idc,addIandaCertificate_fingerPrint,addPrivCertificate_tid) &&  machine.get_certificates().equals(\old((machine.get_certificates().union(new BSet<Integer>(addCertificate_ce))))) &&  machine.get_validityPeriods().equals(\old((machine.get_validityPeriods().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addCertificate_period)))))) &&  machine.get_isValidatedBy().equals(\old((machine.get_isValidatedBy().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addCertificate_pubkey)))))) &&  machine.get_attCert().equals(\old((machine.get_attCert().union(new BSet<Integer>(addCertificate_ce))))) &&  machine.get_baseCertID().equals(\old((machine.get_baseCertID().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addAttCertificate_idc)))))) &&  machine.get_iandaCert().equals(\old((machine.get_iandaCert().union(new BSet<Integer>(addCertificate_ce))))) &&  machine.get_fpTemplate().equals(\old((machine.get_fpTemplate().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addIandaCertificate_fingerPrint)))))) &&  machine.get_attCertTokID().equals(\old((machine.get_attCertTokID().union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addPrivCertificate_tid)))))); 
	 also
		requires !guard_addIandaCertificate_no_TID(addCertificate_ce,addCertificate_period,addCertificate_pubkey,addAttCertificate_idc,addIandaCertificate_fingerPrint,addPrivCertificate_tid);
		assignable \nothing;
		ensures true; */
	public void run_addIandaCertificate_no_TID( Integer addCertificate_ce, Integer addCertificate_period, Integer addCertificate_pubkey, Integer addAttCertificate_idc, Integer addIandaCertificate_fingerPrint, Integer addPrivCertificate_tid){
		if(guard_addIandaCertificate_no_TID(addCertificate_ce,addCertificate_period,addCertificate_pubkey,addAttCertificate_idc,addIandaCertificate_fingerPrint,addPrivCertificate_tid)) {
			BSet<Integer> certificates_tmp = machine.get_certificates();
			BRelation<Integer,Integer> validityPeriods_tmp = machine.get_validityPeriods();
			BRelation<Integer,Integer> isValidatedBy_tmp = machine.get_isValidatedBy();
			BSet<Integer> attCert_tmp = machine.get_attCert();
			BRelation<Integer,Integer> baseCertID_tmp = machine.get_baseCertID();
			BSet<Integer> iandaCert_tmp = machine.get_iandaCert();
			BRelation<Integer,Integer> fpTemplate_tmp = machine.get_fpTemplate();
			BRelation<Integer,Integer> attCertTokID_tmp = machine.get_attCertTokID();

			machine.set_certificates((certificates_tmp.union(new BSet<Integer>(addCertificate_ce))));
			machine.set_validityPeriods((validityPeriods_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addCertificate_period)))));
			machine.set_isValidatedBy((isValidatedBy_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addCertificate_pubkey)))));
			machine.set_attCert((attCert_tmp.union(new BSet<Integer>(addCertificate_ce))));
			machine.set_baseCertID((baseCertID_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addAttCertificate_idc)))));
			machine.set_iandaCert((iandaCert_tmp.union(new BSet<Integer>(addCertificate_ce))));
			machine.set_fpTemplate((fpTemplate_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addIandaCertificate_fingerPrint)))));
			machine.set_attCertTokID((attCertTokID_tmp.union(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(addCertificate_ce,addPrivCertificate_tid)))));

			System.out.println("addIandaCertificate_no_TID executed addCertificate_ce: " + addCertificate_ce + " addCertificate_period: " + addCertificate_period + " addCertificate_pubkey: " + addCertificate_pubkey + " addAttCertificate_idc: " + addAttCertificate_idc + " addIandaCertificate_fingerPrint: " + addIandaCertificate_fingerPrint + " addPrivCertificate_tid: " + addPrivCertificate_tid + " ");
		}
	}

}
