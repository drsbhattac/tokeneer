package tokeneer_eb_model_sec2_sequential;


import java.util.Random;

import Util.Utilities;
import eventb_prelude.*;

public class Test_ref6_admin{

	public static Integer random_shutdownOp;
	public static Integer random_unclassified;
	public static Integer random_badFloppy;
	public static Integer random_invalidRequest;
	public static Integer random_invalidData;
	public static Integer random_busy;
	public static Integer random_updateConfigData;
	public static Integer random_absent;
	public static Integer random_wait;
	public static Integer random_doingOp;
	public static Integer random_validatingEnrolmentData;
	public static Integer random_badFP;
	public static Integer random_insertEnrolmentData;
	public static Integer random_enclaveQuiescent;
	public static Integer random_open;
	public static BSet<Integer> random_time;
	public static Integer random_NoOp;
	public static Integer random_topsecret;
	public static Integer random_welcome;
	public static Integer random_quiescent;
	public static Integer random_tokenUpdateFailed;
	public static Integer random_gotFinger;
	public static Integer random_unmarked;
	public static Integer random_removeToken;
	public static Integer random_archiveLog;
	public static Integer random_NoRole;
	public static Integer random_noFloppy;
	public static Integer random_waitingUpdateToken;
	public static Integer random_noKB;
	public static Integer random_waitingStartAdminOp;
	public static Integer random_goodT;
	public static Integer random_goodF;
	public static Integer random_waitingRemoveTokenSuccess;
	public static Integer random_present;
	public static Integer random_requestAdminOp;
	public static Integer random_auditManager;
	public static Integer random_noFP;
	public static Integer random_notEnrolled;
	public static Integer random_enrolmentFailed;
	public static Integer random_userOnly;
	public static Integer random_good3;
	public static Integer random_good2;
	public static Integer random_insertFinger;
	public static Integer random_good1;
	public static Integer random_overrideLock;
	public static Integer random_welcomeAdmin;
	public static Integer random_gotUserToken;
	public static Integer random_initFPTry;
	public static Integer random_removeAdminToken;
	public static Integer random_clear;
	public static Integer random_waitingEndEnrol;
	public static Integer random_guard;
	public static Integer random_noT;
	public static Integer random_waitingRemoveAdminTokenFail;
	public static Integer random_waitingRemoveTokenFail;
	public static Integer random_waitingFinger;
	public static Integer random_securityOfficer;
	public static Integer random_closed;
	public static BSet<Integer> random_AdminPrivilege;
	public static Integer random_gotAdminToken;
	public static Integer random_confidential;
	public static Integer random_closeDoor;
	public static Integer random_shutdown;
	public static Integer random_waitingFinishAdminOp;
	public static Integer random_badT;
	public static Integer random_blank;
	public static Integer random_insertConfigData;
	public static Integer random_restricted;
	public static Integer random_initTokenTry;
	public static Integer random_openDoor;
	public static Integer random_archiveFailed;
	public static Integer random_waitingEntry;
	public static Integer random_emptyFloppy;
	public static Integer random_doorUnlocked;
	public static Integer random_badKB;
	public static Integer random_insertBlankFloppy;
	public static Integer random_secret;
	public static Integer random_MAXTIME;
	public static Integer random_waitingEnrol;
	public static BRelation<Integer,Integer> random_availableOps;
	
	public static Integer random_d_alarming;
	public static Integer random_d_silent;
	public static BSet<Integer> random_ss;
	

	static Random rnd = new Random();
	static Integer max_size_BSet = 10;
	Integer min_integer = Utilities.min_integer;
	Integer max_integer = Utilities.max_integer;

	public Integer GenerateRandomInteger(){
		BSet<Integer> S =  new BSet<Integer>(
				new Enumerated(min_integer, max_integer)
				);
		/** User defined code that reflects axioms and theorems: Begin **/

		/** User defined code that reflects axioms and theorems: End **/

		return (Integer) S.toArray()[rnd.nextInt(S.size())];
	}

	public boolean GenerateRandomBoolean(){
		boolean res = (Boolean) BOOL.instance.toArray()[rnd.nextInt(2)];

		/** User defined code that reflects axioms and theorems: Begin **/

		/** User defined code that reflects axioms and theorems: End **/

		return res;
	}

	public BSet<Integer> GenerateRandomIntegerBSet(){
		int size = rnd.nextInt(max_size_BSet);
		BSet<Integer> S = new BSet<Integer>();
		while (S.size() != size){
			S.add(GenerateRandomInteger());
		}

		/** User defined code that reflects axioms and theorems: Begin **/

		/** User defined code that reflects axioms and theorems: End **/

		return S;
	}

	public BSet<Boolean> GenerateRandomBooleanBSet(){
		int size = rnd.nextInt(2);
		BSet<Boolean> res = new BSet<Boolean>();
		if (size == 0){
			res = new BSet<Boolean>(GenerateRandomBoolean());
		}else{
			res = new BSet<Boolean>(true,false);
		}

		/** User defined code that reflects axioms and theorems: Begin **/

		/** User defined code that reflects axioms and theorems: End **/

		return res;
	}

	public BRelation<Integer,Integer> GenerateRandomBRelation(){
		BRelation<Integer,Integer> res = new BRelation<Integer,Integer>();
		int size = rnd.nextInt(max_size_BSet);
		while (res.size() != size){
			res.add(
					new Pair<Integer, Integer>(GenerateRandomInteger(), GenerateRandomInteger()));
		}
		/** User defined code that reflects axioms and theorems: Begin **/

		/** User defined code that reflects axioms and theorems: End **/

		return res;
	}

	public static String print_display_message(Integer msg){
		if (msg.equals(ref6_admin.blank)){
			return "SYSTEM NOT OPERATIONAL";
		}
		if (msg.equals(ref6_admin.welcome)){
			return "WELCOME TO TIS\nENTER TOKEN";
		}
		if (msg.equals(ref6_admin.insertFinger)){
			return "AUTHENTICATING USER\n\tINSERT FINGER";
		}
		if (msg.equals(ref6_admin.wait)){
			return "AUTHENTICATING USER\n\tPLEASE WAIT";
		}
		if (msg.equals(ref6_admin.openDoor)){
			return "REMOVE TOKEN AND ENTER";
		}
		if (msg.equals(ref6_admin.removeToken)){
			return "ENTRY DENIED\n\tREMOVE TOKEN";
		}
		if (msg.equals(ref6_admin.tokenUpdateFailed)){
			return "TOKEN UPDATE FAILED";
		}
		if (msg.equals(ref6_admin.doorUnlocked)){
			return "ENTER ENCLAVE";
		}
		return "No valid message"; 
	}

	public static String print_screen_message(Integer msg){
		if (msg.equals(ref6_admin.clear)){
			return "clear";
		}
		if (msg.equals(ref6_admin.welcomeAdmin)){
			return "WELCOME TO TIS";
		}
		if (msg.equals(ref6_admin.busy)){
			return "SYSTEM BUSY PLEASE WAIT";
		}
		if (msg.equals(ref6_admin.removeAdminToken)){
			return "REMOVE TOKEN";
		}
		if (msg.equals(ref6_admin.closeDoor)){
			return "CLOSE ENCLAVE DOOR";
		}
		if (msg.equals(ref6_admin.requestAdminOp)){
			return "ENTER REQUIRED OPERATION";
		}
		if (msg.equals(ref6_admin.doingOp)){
			return "PERFORMING OPERATION PLEASE WAIT";
		}
		if (msg.equals(ref6_admin.invalidRequest)){
			return "INVALID REQUEST: PLEASE ENTER NEW OPERATION";
		}
		if (msg.equals(ref6_admin.invalidData)){
			return "INVALID DATA: PLEASE ENTER NEW OPERATION";
		}
		if (msg.equals(ref6_admin.archiveFailed)){
			return "ARCHIVE FAILD: PLEASE ENTER NEW OPERATION";
		}
		if (msg.equals(ref6_admin.insertEnrolmentData)){
			return "PLEASE INSERT ENROLMENT DATA FLOPPY";
		}
		if (msg.equals(ref6_admin.validatingEnrolmentData)){
			return "VALIDATING ENROLMENT DATA PLEASE WAIT";
		}
		if (msg.equals(ref6_admin.enrolmentFailed)){
			return "INVALID ENROLMENT DATA";
		}
		if (msg.equals(ref6_admin.insertBlankFloppy)){
			return "INSERT BLANK FLOPPY";
		}
		if (msg.equals(ref6_admin.insertConfigData)){
			return "INSERT CONFIGURATION DATA FLOPPY";
		} 

		return "No valid message";
	}


	

	@SuppressWarnings({ "static-access", "unchecked" })
	//public static void main(String[] args){
	public static void initialize(){
		//Test_ref6_admin test = new Test_ref6_admin();

		/** User defined code that reflects axioms and theorems: Begin **/
		random_userOnly = 1;
		random_guard = 2; 
		random_securityOfficer = 3;
		random_auditManager = 4;
		random_NoRole = 5;

		random_unmarked = 1; 
		random_unclassified = 2;
		random_restricted = 3;
		random_confidential = 4;
		random_secret = 5;
		random_topsecret = 6;

		random_quiescent = 1;
		random_gotUserToken = 2;
		random_waitingFinger = 3;
		random_gotFinger = 4;
		random_waitingUpdateToken = 5;
		random_waitingEntry = 6;
		random_waitingRemoveTokenSuccess = 7;
		random_waitingRemoveTokenFail = 8;

		random_badFP = 1;
		random_noFP = 2;
		random_initFPTry = 3;
		random_goodF = 4;

		random_blank = 1;
		random_welcome = 2;
		random_insertFinger = 3;
		random_openDoor = 4;
		random_wait = 5;
		random_removeToken = 6;
		random_tokenUpdateFailed = 7;
		random_doorUnlocked = 8;

		random_goodT = 1;
		random_badT = 2;
		random_noT = 3;
		random_initTokenTry = 4;

		random_absent = 1;
		random_present = 2;

		random_notEnrolled = 1;
		random_waitingEnrol = 2;
		random_waitingEndEnrol = 3;
		random_enclaveQuiescent = 4;
		random_gotAdminToken = 5;
		random_waitingRemoveAdminTokenFail = 6;
		random_waitingStartAdminOp = 7;
		random_waitingFinishAdminOp = 8;
		random_shutdown = 9;

		random_noFloppy = 1;
		random_emptyFloppy = 2;
		random_badFloppy = 3;

		random_clear = 1;
		random_welcomeAdmin = 2;
		random_busy = 3;
		random_removeAdminToken = 4;
		random_closeDoor = 5;
		random_requestAdminOp = 6;
		random_doingOp = 7;
		random_invalidRequest = 8;
		random_invalidData = 9;
		random_insertEnrolmentData = 10;
		random_validatingEnrolmentData = 11;
		random_enrolmentFailed = 12;
		random_archiveFailed = 13;
		random_insertBlankFloppy = 14;
		random_insertConfigData = 15;

		random_archiveLog = 1;
		random_updateConfigData = 2;
		random_overrideLock = 3;
		random_shutdownOp = 4;
		random_NoOp = 5;

		random_AdminPrivilege = new BSet<Integer>(
				random_guard, 
				random_securityOfficer, 
				random_auditManager, 
				random_NoRole);

		random_noKB = 1;
		random_badKB = 2;
		random_good1 = 3;
		random_good2 = 4;
		random_good3 = 5;

		random_open = 1;
		random_closed = 2;

		random_MAXTIME = 100;
		random_time = new Enumerated(1,100);
		random_availableOps = 
				new BRelation<Integer,Integer>(
						new Pair<Integer,Integer>(random_NoOp,random_NoRole),
						new Pair<Integer,Integer>(random_overrideLock,random_guard),
						new Pair<Integer,Integer>(random_archiveLog,random_auditManager),
						new Pair<Integer,Integer>(random_updateConfigData,random_securityOfficer),
						new Pair<Integer,Integer>(random_shutdownOp,random_securityOfficer)
						);
		
		
		random_d_alarming = 1;
		random_d_silent = 2;
		random_ss = new BSet<Integer>(random_quiescent, 
				random_gotUserToken, 
				random_waitingEntry, 
				random_waitingRemoveTokenSuccess);
		
		/** User defined code that reflects axioms and theorems: End **/

		//ref6_admin machine = new ref6_admin();
		/*new test_Enrolment1(machine);
		new test_Enrolment2(machine);
		new test_Enrolment3(machine);
		new test_UserEntry1(machine);
		new test_UpdateConfig1(machine);
		new test_UserEntry2(machine);
		new test_UserEntry3(machine);
		new test_Override1(machine);
		new test_UserEntry4(machine);
		new test_UserEntry5(machine);
		new test_UserEntry6(machine);
		new test_UserEntry7(machine);
		new test_UserEntry8(machine);
		new test_UserEntry9(machine);
		new test_AdminLogin1(machine);
		new test_AdminLogin2(machine);
		new test_UserEntry10(machine);
		new test_UserEntry11(machine);
		new test_UserEntry12(machine);
		new test_UpdateConfig2(machine);
		new test_UpdateConfig3(machine);
		new test_AdminLogout1(machine);
		new test_AdminLogin3 (machine);
		new test_UpdateConfig4(machine);
		new test_UpdateConfig5(machine);
		new test_Shutdown1(machine);
		new test_ArchiveLog1(machine);
		new test_ArchiveLog2(machine);
		new test_ArchiveLog3(machine);
		new test_UserEntry13(machine);
		new test_UserEntry14(machine);
		new test_Shutdown2(machine);*/
	}

}


/************
Type definitions in Spark ADA (can be found in <praxis-docs>/code/core/*type*)

type StatusT is (Alarming, Silent);
type PresenceT is (Present, Absent);
type CertificateT is (IDCert, AuthCert, PrivCert, IandACert);
type PrivilegeT is (
      UserOnly,
      Guard,
      AuditManager,
      SecurityOfficer
      );
subtype AdminPrivilegeT is PrivilegeT range Guard .. SecurityOfficer;
type ClassT is (
      Unmarked,
      Unclassified,
      Restricted,
      Confidential,
      Secret,
      Topsecret
      );
/************/