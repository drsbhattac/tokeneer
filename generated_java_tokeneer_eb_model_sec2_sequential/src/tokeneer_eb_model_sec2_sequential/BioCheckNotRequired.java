package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class BioCheckNotRequired{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public BioCheckNotRequired(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (NAT.instance.has(currentTime) && machine.get_tokenAuthCert().domain().has(machine.get_currentToken()) && machine.get_validityPeriods().image(new BSet<Integer>(machine.get_tokenAuthCert().apply(machine.get_currentToken()))).has(currentTime) && machine.get_attCertTokID().range().has(machine.get_currentToken()) && machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) && machine.get_entry_status1().equals(machine.gotUserToken) && machine.get_userTokenPresence().equals(machine.present) && machine.get_goodTok().domain().has(machine.get_currentToken()) && machine.get_status_sec().has(machine.gotUserToken)) && (machine.variant() >= 0); */
	public /*@ pure */ boolean guard_BioCheckNotRequired( Integer currentTime) {
		return (
				NAT.instance.has(currentTime) && machine.get_tokenAuthCert().domain().has(machine.get_currentToken()) && machine.get_validityPeriods().image(new BSet<Integer>(machine.get_tokenAuthCert().apply(machine.get_currentToken()))).has(currentTime) && machine.get_attCertTokID().range().has(machine.get_currentToken()) && machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) && machine.get_entry_status1().equals(machine.gotUserToken) && machine.get_userTokenPresence().equals(machine.present) && machine.get_goodTok().domain().has(machine.get_currentToken()) && machine.get_status_sec().has(machine.gotUserToken)) && (machine.variant() >= 0);
	}

	/*@ public normal_behavior
		requires guard_BioCheckNotRequired(currentTime);
		assignable machine.entry_status1, machine.displayMessage1, machine.status_sec;
		ensures guard_BioCheckNotRequired(currentTime) &&  machine.get_entry_status1() == \old(machine.waitingEntry) &&  machine.get_displayMessage1() == \old(machine.wait) &&  machine.get_status_sec().equals(\old(machine.get_status_sec().difference(new BSet<Integer>(machine.gotUserToken))))
			 && machine.variant() < \old(machine.variant()); 
	 also
		requires !guard_BioCheckNotRequired(currentTime);
		assignable \nothing;
		ensures true; */
	public void run_BioCheckNotRequired( Integer currentTime){
		if(guard_BioCheckNotRequired(currentTime)) {
			Integer entry_status1_tmp = machine.get_entry_status1();
			Integer displayMessage1_tmp = machine.get_displayMessage1();
			BSet<Integer> status_sec_tmp = machine.get_status_sec();

			machine.set_entry_status1(machine.waitingEntry);
			machine.set_displayMessage1(machine.wait);
			machine.set_status_sec(status_sec_tmp.difference(new BSet<Integer>(machine.gotUserToken)));

			System.out.println("BioCheckNotRequired executed currentTime: " + currentTime + " ");
		}
	}

}
