package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class ValidateAdminTokenFail_2{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public ValidateAdminTokenFail_2(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_enclaveStatus2().equals(machine.gotAdminToken) && machine.get_adminTokenPresence().equals(machine.present) && !machine.get_tokenAuthCert().domain().has(machine.get_currentAdminToken())); */
	public /*@ pure */ boolean guard_ValidateAdminTokenFail_2() {
		return (
			machine.get_enclaveStatus2().equals(machine.gotAdminToken) && 
			machine.get_adminTokenPresence().equals(machine.present) && 
			!machine.get_tokenAuthCert().domain().has(machine.get_currentAdminToken()));
	}

	/*@ public normal_behavior
		requires guard_ValidateAdminTokenFail_2();
		assignable machine.screenMsg2, machine.enclaveStatus2;
		ensures guard_ValidateAdminTokenFail_2() &&  machine.get_screenMsg2() == \old(machine.removeAdminToken) &&  machine.get_enclaveStatus2() == \old(machine.waitingRemoveAdminTokenFail); 
	 also
		requires !guard_ValidateAdminTokenFail_2();
		assignable \nothing;
		ensures true; */
	public void run_ValidateAdminTokenFail_2(){
		if(guard_ValidateAdminTokenFail_2()) {
			Integer screenMsg2_tmp = machine.get_screenMsg2();
			Integer enclaveStatus2_tmp = machine.get_enclaveStatus2();

			machine.set_screenMsg2(machine.removeAdminToken);
			machine.set_enclaveStatus2(machine.waitingRemoveAdminTokenFail);

			System.out.println("ValidateAdminTokenFail_2 executed ");
		}
	}

}
