package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class ValidateFingerOK{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public ValidateFingerOK(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_entry_status1().equals(machine.gotFinger) && machine.get_fingerprint().has(ValidateFingerOK_currentFinger) && machine.get_goodFP().apply(ValidateFingerOK_currentFinger).equals(machine.goodF) && machine.get_userTokenPresence().equals(machine.present)); */
	public /*@ pure */ boolean guard_ValidateFingerOK( Integer ValidateFingerOK_currentFinger) {
		return (machine.get_entry_status1().equals(machine.gotFinger) && machine.get_fingerprint().has(ValidateFingerOK_currentFinger) && machine.get_goodFP().apply(ValidateFingerOK_currentFinger).equals(machine.goodF) && machine.get_userTokenPresence().equals(machine.present));
	}

	/*@ public normal_behavior
		requires guard_ValidateFingerOK(ValidateFingerOK_currentFinger);
		assignable machine.entry_status1, machine.displayMessage1;
		ensures guard_ValidateFingerOK(ValidateFingerOK_currentFinger) &&  machine.get_entry_status1() == \old(machine.waitingUpdateToken) &&  machine.get_displayMessage1() == \old(machine.wait); 
	 also
		requires !guard_ValidateFingerOK(ValidateFingerOK_currentFinger);
		assignable \nothing;
		ensures true; */
	public void run_ValidateFingerOK( Integer ValidateFingerOK_currentFinger){
		if(guard_ValidateFingerOK(ValidateFingerOK_currentFinger)) {
			Integer entry_status1_tmp = machine.get_entry_status1();
			Integer displayMessage1_tmp = machine.get_displayMessage1();

			machine.set_entry_status1(machine.waitingUpdateToken);
			machine.set_displayMessage1(machine.wait);

			System.out.println("ValidateFingerOK executed ValidateFingerOK_currentFinger: " + ValidateFingerOK_currentFinger + " ");
		}
	}

}
