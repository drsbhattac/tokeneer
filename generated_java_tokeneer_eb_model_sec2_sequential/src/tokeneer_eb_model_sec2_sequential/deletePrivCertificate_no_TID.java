package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class deletePrivCertificate_no_TID{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public deletePrivCertificate_no_TID(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_certificates().has(deleteCertificate_ce) && machine.get_isValidatedBy().domain().has(deleteCertificate_ce) && machine.get_validityPeriods().domain().has(deleteCertificate_ce) && machine.get_attCert().has(deleteCertificate_ce) && machine.get_privCert().has(deleteCertificate_ce) && !machine.get_authCert().has(deleteCertificate_ce) && !machine.get_iandaCert().has(deleteCertificate_ce) && machine.get_tokenPrivCert().range().has(deleteCertificate_ce) && !machine.get_attCertTokID().domain().has(deleteCertificate_ce)); */
	public /*@ pure */ boolean guard_deletePrivCertificate_no_TID( Integer deleteCertificate_ce) {
		return (machine.get_certificates().has(deleteCertificate_ce) && machine.get_isValidatedBy().domain().has(deleteCertificate_ce) && machine.get_validityPeriods().domain().has(deleteCertificate_ce) && machine.get_attCert().has(deleteCertificate_ce) && machine.get_privCert().has(deleteCertificate_ce) && !machine.get_authCert().has(deleteCertificate_ce) && !machine.get_iandaCert().has(deleteCertificate_ce) && machine.get_tokenPrivCert().range().has(deleteCertificate_ce) && !machine.get_attCertTokID().domain().has(deleteCertificate_ce));
	}

	/*@ public normal_behavior
		requires guard_deletePrivCertificate_no_TID(deleteCertificate_ce);
		assignable machine.certificates, machine.isValidatedBy, machine.validityPeriods, machine.certificateID, machine.attCert, machine.baseCertID, machine.privCert, machine.privCertRole, machine.privCertClearence, machine.tokenPrivCert, machine.tokenIandaCert, machine.tokenAuthCert, machine.tokenID, machine.goodTok;
		ensures guard_deletePrivCertificate_no_TID(deleteCertificate_ce) &&  machine.get_certificates().equals(\old(machine.get_certificates().difference(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_isValidatedBy().equals(\old(machine.get_isValidatedBy().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_validityPeriods().equals(\old(machine.get_validityPeriods().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_certificateID().equals(\old(machine.get_certificateID().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_attCert().equals(\old(machine.get_attCert().difference(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_baseCertID().equals(\old(machine.get_baseCertID().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_privCert().equals(\old(machine.get_privCert().difference(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_privCertRole().equals(\old(machine.get_privCertRole().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_privCertClearence().equals(\old(machine.get_privCertClearence().domainSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_tokenPrivCert().equals(\old(machine.get_tokenPrivCert().rangeSubtraction(new BSet<Integer>(deleteCertificate_ce)))) &&  machine.get_tokenIandaCert().equals(\old(machine.get_tokenIandaCert().domainSubtraction(new BSet<Integer>(machine.get_tokenPrivCert().inverse().apply(deleteCertificate_ce))))) &&  machine.get_tokenAuthCert().equals(\old(machine.get_tokenAuthCert().domainSubtraction(new BSet<Integer>(machine.get_tokenPrivCert().inverse().apply(deleteCertificate_ce))))) &&  machine.get_tokenID().equals(\old(machine.get_tokenID().difference(new BSet<Integer>(machine.get_tokenPrivCert().inverse().apply(deleteCertificate_ce))))) &&  machine.get_goodTok().equals(\old(machine.get_goodTok().domainSubtraction(new BSet<Integer>(machine.get_tokenPrivCert().inverse().apply(deleteCertificate_ce))))); 
	 also
		requires !guard_deletePrivCertificate_no_TID(deleteCertificate_ce);
		assignable \nothing;
		ensures true; */
	public void run_deletePrivCertificate_no_TID( Integer deleteCertificate_ce){
		if(guard_deletePrivCertificate_no_TID(deleteCertificate_ce)) {
			BSet<Integer> certificates_tmp = machine.get_certificates();
			BRelation<Integer,Integer> isValidatedBy_tmp = machine.get_isValidatedBy();
			BRelation<Integer,Integer> validityPeriods_tmp = machine.get_validityPeriods();
			BRelation<Integer,Integer> certificateID_tmp = machine.get_certificateID();
			BSet<Integer> attCert_tmp = machine.get_attCert();
			BRelation<Integer,Integer> baseCertID_tmp = machine.get_baseCertID();
			BSet<Integer> privCert_tmp = machine.get_privCert();
			BRelation<Integer,Integer> privCertRole_tmp = machine.get_privCertRole();
			BRelation<Integer,Integer> privCertClearence_tmp = machine.get_privCertClearence();
			BRelation<Integer,Integer> tokenPrivCert_tmp = machine.get_tokenPrivCert();
			BRelation<Integer,Integer> tokenIandaCert_tmp = machine.get_tokenIandaCert();
			BRelation<Integer,Integer> tokenAuthCert_tmp = machine.get_tokenAuthCert();
			BSet<Integer> tokenID_tmp = machine.get_tokenID();
			BRelation<Integer,Integer> goodTok_tmp = machine.get_goodTok();

			machine.set_certificates(certificates_tmp.difference(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_isValidatedBy(isValidatedBy_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_validityPeriods(validityPeriods_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_certificateID(certificateID_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_attCert(attCert_tmp.difference(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_baseCertID(baseCertID_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_privCert(privCert_tmp.difference(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_privCertRole(privCertRole_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_privCertClearence(privCertClearence_tmp.domainSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_tokenPrivCert(tokenPrivCert_tmp.rangeSubtraction(new BSet<Integer>(deleteCertificate_ce)));
			machine.set_tokenIandaCert(tokenIandaCert_tmp.domainSubtraction(new BSet<Integer>(tokenPrivCert_tmp.inverse().apply(deleteCertificate_ce))));
			machine.set_tokenAuthCert(tokenAuthCert_tmp.domainSubtraction(new BSet<Integer>(tokenPrivCert_tmp.inverse().apply(deleteCertificate_ce))));
			machine.set_tokenID(tokenID_tmp.difference(new BSet<Integer>(tokenPrivCert_tmp.inverse().apply(deleteCertificate_ce))));
			machine.set_goodTok(goodTok_tmp.domainSubtraction(new BSet<Integer>(tokenPrivCert_tmp.inverse().apply(deleteCertificate_ce))));

			System.out.println("deletePrivCertificate_no_TID executed deleteCertificate_ce: " + deleteCertificate_ce + " ");
		}
	}

}
