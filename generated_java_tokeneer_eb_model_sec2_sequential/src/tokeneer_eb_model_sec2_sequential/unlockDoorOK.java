package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class unlockDoorOK{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public unlockDoorOK(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_entry_status1().equals(machine.waitingRemoveTokenSuccess) && NAT.instance.has(currentTime) && (currentTime).compareTo(machine.MAXTIME) <= 0 && (currentTime).compareTo(new Integer(machine.get_latchTimeout1() - machine.get_latchUnlockDuration())) <= 0 && (currentTime).compareTo(machine.get_latchTimeout1()) >= 0 && machine.get_userTokenPresence().equals(machine.absent)); */
	public /*@ pure */ boolean guard_unlockDoorOK( Integer currentTime) {
		return (
			machine.get_entry_status1().equals(machine.waitingRemoveTokenSuccess) && 
			NAT.instance.has(currentTime) && 
			(currentTime).compareTo(machine.MAXTIME) <= 0 && 
			(currentTime).compareTo(new Integer(machine.get_latchTimeout1() - machine.get_latchUnlockDuration())) > 0 && 
			(currentTime).compareTo(machine.get_latchTimeout1()) >= 0 && 
			machine.get_userTokenPresence().equals(machine.absent));
	}

	/*@ public normal_behavior
		requires guard_unlockDoorOK(currentTime);
		assignable machine.entry_status1, machine.displayMessage1, machine.latchTimeout1, machine.alarmTimeout1, machine.status_sec;
		ensures guard_unlockDoorOK(currentTime) &&  machine.get_entry_status1() == \old(machine.quiescent) &&  machine.get_displayMessage1() == \old(machine.doorUnlocked) &&  machine.get_latchTimeout1() == \old(new Integer(currentTime + machine.get_latchUnlockDuration())) &&  machine.get_alarmTimeout1() == \old(new Integer(currentTime + machine.get_latchUnlockDuration() + machine.get_alarmSilentDuration())) &&  machine.get_status_sec().equals(\old(new BSet<Integer>(machine.quiescent,machine.gotUserToken,machine.waitingEntry,machine.waitingRemoveTokenSuccess))); 
	 also
		requires !guard_unlockDoorOK(currentTime);
		assignable \nothing;
		ensures true; */
	public void run_unlockDoorOK( Integer currentTime){
		if(guard_unlockDoorOK(currentTime)) {
			Integer entry_status1_tmp = machine.get_entry_status1();
			Integer displayMessage1_tmp = machine.get_displayMessage1();
			Integer latchTimeout1_tmp = machine.get_latchTimeout1();
			Integer alarmTimeout1_tmp = machine.get_alarmTimeout1();
			BSet<Integer> status_sec_tmp = machine.get_status_sec();

			machine.set_entry_status1(machine.quiescent);
			machine.set_displayMessage1(machine.doorUnlocked);
			machine.set_latchTimeout1(new Integer(currentTime + machine.get_latchUnlockDuration()));
			machine.set_alarmTimeout1(new Integer(currentTime + machine.get_latchUnlockDuration() + machine.get_alarmSilentDuration()));
			machine.set_status_sec(new BSet<Integer>(machine.quiescent,machine.gotUserToken,machine.waitingEntry,machine.waitingRemoveTokenSuccess));

			System.out.println("unlockDoorOK executed currentTime: " + currentTime + " ");
		}
	}

}
