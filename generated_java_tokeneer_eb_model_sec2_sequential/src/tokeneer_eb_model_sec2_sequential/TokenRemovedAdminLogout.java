package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class TokenRemovedAdminLogout{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public TokenRemovedAdminLogout(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_enclaveStatus2().equals(machine.enclaveQuiescent) && machine.get_adminToken().range().has(machine.get_currentAdminToken()) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && machine.get_adminTokenPresence().equals(machine.absent)); */
	public /*@ pure */ boolean guard_TokenRemovedAdminLogout() {
		return (machine.get_enclaveStatus2().equals(machine.enclaveQuiescent) && machine.get_adminToken().range().has(machine.get_currentAdminToken()) && !machine.get_rolePresent().apply(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken())).equals(machine.NoRole) && machine.get_adminTokenPresence().equals(machine.absent));
	}

	/*@ public normal_behavior
		requires guard_TokenRemovedAdminLogout();
		assignable machine.rolePresent, machine.currentAdminOp, machine.availableOps;
		ensures guard_TokenRemovedAdminLogout() &&  machine.get_rolePresent().equals(\old((machine.get_rolePresent().override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoRole)))))) &&  machine.get_currentAdminOp().equals(\old((machine.get_currentAdminOp().override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))))) &&  machine.get_availableOps().equals(\old((machine.get_availableOps().override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))))); 
	 also
		requires !guard_TokenRemovedAdminLogout();
		assignable \nothing;
		ensures true; */
	public void run_TokenRemovedAdminLogout(){
		if(guard_TokenRemovedAdminLogout()) {
			BRelation<Integer,Integer> rolePresent_tmp = machine.get_rolePresent();
			BRelation<Integer,Integer> currentAdminOp_tmp = machine.get_currentAdminOp();
			BRelation<Integer,Integer> availableOps_tmp = machine.get_availableOps();
			
			machine.set_rolePresent((rolePresent_tmp.override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoRole)))));
			
			machine.set_currentAdminOp((currentAdminOp_tmp.override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))));
			machine.set_availableOps((
					availableOps_tmp.override(new BRelation<Integer,Integer>(new Pair<Integer,Integer>(
							machine.get_adminToken().inverse().apply(machine.get_currentAdminToken()),machine.NoOp)))));

			System.out.println("TokenRemovedAdminLogout executed ");
		}
	}

}
