package tokeneer_eb_model_sec2_sequential; 

import eventb_prelude.*;
import Util.Utilities;

public class EntryNotAllowed_1{
	/*@ spec_public */ private ref6_admin machine; // reference to the machine 

	/*@ public normal_behavior
		requires true;
		assignable \everything;
		ensures this.machine == m; */
	public EntryNotAllowed_1(ref6_admin m) {
		this.machine = m;
	}

	/*@ public normal_behavior
		requires true;
 		assignable \nothing;
		ensures \result <==> (machine.get_entry_status1().equals(machine.waitingEntry) && machine.get_userTokenPresence().equals(machine.present) && NAT.instance.has(currentTime) && !(machine.get_tokenID().has(machine.get_currentToken()) && machine.get_attCertTokID().inverse().domain().has(machine.get_currentToken()) && machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) && machine.get_isValidatedBy().domain().has(machine.get_tokenPrivCert().apply(machine.get_currentToken())) && machine.get_isValidatedBy().domain().has(machine.get_tokenIandaCert().apply(machine.get_currentToken())) && machine.get_entryPeriod().apply(machine.get_privCertRole().apply(machine.get_tokenPrivCert().apply(machine.get_currentToken()))).apply(machine.get_privCertClearence().apply(machine.get_tokenPrivCert().apply(machine.get_currentToken()))).has(currentTime))); */
	public /*@ pure */ boolean guard_EntryNotAllowed_1( Integer currentTime) {
		return (machine.get_entry_status1().equals(machine.waitingEntry) && machine.get_userTokenPresence().equals(machine.present) && NAT.instance.has(currentTime) && !(machine.get_tokenID().has(machine.get_currentToken()) && machine.get_attCertTokID().inverse().domain().has(machine.get_currentToken()) && machine.get_isValidatedBy().domain().has(machine.get_attCertTokID().inverse().apply(machine.get_currentToken())) && machine.get_isValidatedBy().domain().has(machine.get_tokenPrivCert().apply(machine.get_currentToken())) && machine.get_isValidatedBy().domain().has(machine.get_tokenIandaCert().apply(machine.get_currentToken())) && machine.get_entryPeriod().apply(machine.get_privCertRole().apply(machine.get_tokenPrivCert().apply(machine.get_currentToken()))).apply(machine.get_privCertClearence().apply(machine.get_tokenPrivCert().apply(machine.get_currentToken()))).has(currentTime)));
	}

	/*@ public normal_behavior
		requires guard_EntryNotAllowed_1(currentTime);
		assignable machine.entry_status1, machine.displayMessage1;
		ensures guard_EntryNotAllowed_1(currentTime) &&  machine.get_entry_status1() == \old(machine.waitingRemoveTokenFail) &&  machine.get_displayMessage1() == \old(machine.removeToken); 
	 also
		requires !guard_EntryNotAllowed_1(currentTime);
		assignable \nothing;
		ensures true; */
	public void run_EntryNotAllowed_1( Integer currentTime){
		if(guard_EntryNotAllowed_1(currentTime)) {
			Integer entry_status1_tmp = machine.get_entry_status1();
			Integer displayMessage1_tmp = machine.get_displayMessage1();

			machine.set_entry_status1(machine.waitingRemoveTokenFail);
			machine.set_displayMessage1(machine.removeToken);

			System.out.println("EntryNotAllowed_1 executed currentTime: " + currentTime + " ");
		}
	}

}
